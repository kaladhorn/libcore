module test_interpreter
    use core
    use mod_expression
    use ext_character
    use mod_exception
    use mod_test
    use mod_timer, only: FTimer

    implicit none
    private

    public :: testAll
contains
    subroutine testAll
        call testSimple
        call testComp
        call testTrig
        call testHyperbolic
        call testFunctions
        call testPerformance
        call testErrors
        call testString
        call testOutput

        call fparserTest
        
        call testComparison
    end subroutine

    subroutine testComparison
        type(FExpression) :: f
        
        call startTestGroup("comparison operators")
        
        f = FExpression("2 == 1 + 1")
        call check(f%evalf()==1, "== comparison 1", "wrong result: " // f%evalf() // " instead of 1.")
        f = FExpression("2 == 1")
        call check(f%evalf()==0, "== comparison 2", "wrong result: " // f%evalf() // " instead of 0.")
        
        f = FExpression("2 > 1")
        call check(f%evalf()==1, "> comparison 1", "wrong result: " // f%evalf() // " instead of 1.")
        f = FExpression("2 > 2")
        call check(f%evalf()==0, "> comparison 2", "wrong result: " // f%evalf() // " instead of 0.")
        f = FExpression("1 > 2")
        call check(f%evalf()==0, "> comparison 3", "wrong result: " // f%evalf() // " instead of 0.")
        
        f = FExpression("2 >= 1")
        call check(f%evalf()==1, ">= comparison 1", "wrong result: " // f%evalf() // " instead of 1.")
        f = FExpression("2 >= 2")
        call check(f%evalf()==1, ">= comparison 2", "wrong result: " // f%evalf() // " instead of 1.")
        f = FExpression("1 >= 2")
        call check(f%evalf()==0, ">= comparison 3", "wrong result: " // f%evalf() // " instead of 0.")
        
        f = FExpression("2 < 1")
        call check(f%evalf()==0, "< comparison 1", "wrong result: " // f%evalf() // " instead of 0.")
        f = FExpression("2 < 2")
        call check(f%evalf()==0, "< comparison 2", "wrong result: " // f%evalf() // " instead of 0.")
        f = FExpression("1 < 2")
        call check(f%evalf()==1, "< comparison 3", "wrong result: " // f%evalf() // " instead of 1.")

        f = FExpression("2 <= 1")
        call check(f%evalf()==0, "< comparison 1", "wrong result: " // f%evalf() // " instead of 0.")
        f = FExpression("2 <= 2")
        call check(f%evalf()==1, "< comparison 2", "wrong result: " // f%evalf() // " instead of 1.")
        f = FExpression("1 <= 2")
        call check(f%evalf()==1, "< comparison 3", "wrong result: " // f%evalf() // " instead of 1.")
        
        f = FExpression("2 > 1 && 1 > 0")
        call check(f%evalf()==1, "&& test 1", "wrong result: " // f%evalf() // " instead of 1.")
        f = FExpression("2 > 1 && 3 > 42")
        call check(f%evalf()==0, "&& test 2", "wrong result: " // f%evalf() // " instead of 0.")

        f = FExpression(".not.2 < -1")
        call check(f%evalf()==0, ".not. test 1", "wrong result: " // f%evalf() // " instead of 0.")
        f = FExpression(".not.(2 < -1)")
        call check(f%evalf()==1, ".not. test 2", "wrong result: " // f%evalf() // " instead of 1.")
    end subroutine
    
    ! Test standard trigonometric functions

    subroutine testTrig
        type(FExpression), target :: f, g, h

        call startTestGroup("standard trigonometric functions")

        f = FExpression("cos(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.54030230586813977_e, "evaluate f(x) = cos(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("-sin(x)"), "derive f(x) = cos(x)", "wrong derivative: "//g%string())
        call g%setVariable("x", 1.0_e)
        call check(g%evalf() == -0.84147098480789650_e, "evaluate g(x) = d/dx[cos(x)]", "wrong result: "//g%evalf())

        f = FExpression("sin(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.84147098480789650_e, "evaluate f(x) = sin(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("cos(x)"), "derive f(x) = sin(x)", "wrong derivative: "//g%string())

        f = FExpression("tan(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf().approx.1.5574077246549021_e, "evaluate f(x) = tan(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("1/cos(x)^2"), "derive f(x) = tan(x)", "wrong derivative: "//g%string())

        f = FExpression("acos(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.0_e, "evaluate f(x) = acos(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("-(1/sqrt(1-x^2))"), "derive f(x) = acos(x)", "wrong derivative: "//g%string())

        f = FExpression("asin(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 1.5707963267948966_e, "evaluate f(x) = asin(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("1/sqrt(1-x^2)"), "derive f(x) = asin(x)", "wrong derivative: "//g%string())

        f = FExpression("atan(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.78539816339744828_e, "evaluate f(x) = atan(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("1/(1+x**2)"), "derive f(x) = atan(x)", "wrong derivative: "//g%string())
    end subroutine

    ! Test hyperbolic trigonometric functions

    subroutine testHyperbolic
        type(FExpression) :: f, g

        call startTestGroup("hyperbolic trigonometric functions")

        f = FExpression("cosh(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 1.5430806348152437_e, "evaluate f(x) = cosh(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("sinh(x)"), "derive f(x) = cosh(x)", "wrong derivative: "//g%string())

        f = FExpression("sinh(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 1.1752011936438014_e, "evaluate f(x) = sinh(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("cosh(x)"), "derive f(x) = sinh(x)", "wrong derivative: "//g%string())

        f = FExpression("tanh(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.76159415595576485_e, "evaluate f(x) = tanh(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("1/cosh(x)^2"), "derive f(x) = tanh(x)", "wrong derivative: "//g%string())

        f = FExpression("acosh(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == acosh(1.0_e), "evaluate f(x) = acosh(x)", "wrong result: "//f%evalf())
        call check(f%evalf() .approx. 0.0_e, "evaluate f(x) = acosh(x)", "wrong precision: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("1/sqrt(x^2-1)"), "derive f(x) = acosh(x)", "wrong derivative: "//g%string())

        f = FExpression("asinh(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == asinh(1.0_e), "evaluate f(x) = asinh(x)", "wrong result: "//f%evalf())
        call check(f%evalf() .approx. 0.88137358701954305_e, "evaluate f(x) = asinh(x)", "wrong precision: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("1/sqrt(1+x^2)"), "derive f(x) = asinh(x)", "wrong derivative: "//g%string())

        f = FExpression("atanh(x)")
        call f%setVariable("x", 0.0_e)
        call check(f%evalf() == atanh(0.0_e), "evaluate f(x) = atanh(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("1/(1-x^2)"), "derive f(x) = atanh(x)", "wrong derivative: "//g%string())
    end subroutine

    ! Test string output

    subroutine testString
        type(FExpression) :: f

        call startTestGroup("string output")

        f = FExpression("-1 + 2*x + 10*pi")
        call check(f%string() == "-1+2*x+10*π", "string representation for -1 + 2*x + 10*pi", &
            "wrong string: "//f%string())
    end subroutine

    ! Test simple operators

    subroutine testSimple
        type(FExpression) :: f, g, h
        real(e) :: x

        call startTestGroup("standard operators")

        f = FExpression("x")
        call f%setVariable("x", 1.0_e)
        call f%eval(x)
        call check(x == 1.0_e, "evaluate f(x) = x", "wrong result: "//x)
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "1", "derive f(x) = x", "wrong derivative: "//g%string())

        f = FExpression("+x")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 1.0_e, "evaluate f(x) = +x", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "1", "derive f(x) = +x", "wrong derivative: "//g%string())

        f = FExpression("1*(+x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 1.0_e, "evaluate f(x) = 1*(+x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "1", "derive f(x) = 1*(+x)", "wrong derivative: "//g%string())

        f = FExpression("-x")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == -1.0_e, "evaluate f(x) = -x", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "-1", "derive f(x) = -x", "wrong derivative: "//g%string())

        f = FExpression("abs(x)")
        call f%setVariable("x", -1.0_e)
        call check(f%evalf() == 1.0_e, "evaluate f(x) = abs(x)", "wrong result: "//f%evalf())

        f = FExpression("x + 2")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 3.0_e, "evaluate f(x) = 2+x", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "1", "derive f(x) = 2+x", "wrong derivative: "//g%string())

        f = FExpression("x - 2")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == -1.0_e, "evaluate f(x) = x-2", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "1", "derive f(x) = x-2", "wrong derivative: "//g%string())

        f = FExpression("x * 2")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 2.0_e, "evaluate f(x) = 2*x", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "2", "derive f(x) = 2*x", "wrong derivative: "//g%string())

        f = FExpression("x / 2")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.5_e, "evaluate f(x) = x/2", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "2/4", "derive f(x) = x/2", "wrong derivative: "//g%string())

        f = FExpression("x ** 2")
        call f%setVariable("x", 2.0_e)
        call check(f%evalf() == 4.0_e, "evaluate f(x) = x**2", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "2*x", "derive f(x) = x**2", "wrong derivative: "//g%string())

        f = FExpression("x ** y")
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "y*x**(y-1)", "derive f(x) = x**y", "wrong derivative: "//g%string())

        f = FExpression("(0*log(x)+(2*1)/x)*x**2*3+x**2*0-0")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 6, "evaluate f(x) = 6*1", "wrong result: "//f%evalf())
    end subroutine

    subroutine testFunctions
        type(FExpression) :: f, g, h

        call startTestGroup("standard functions")

        f = FExpression("sqrt(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 1.0_e, "evaluate f(x) = sqrt(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "1/(2*sqrt(x))", "derive f(x) = sqrt(x)", "wrong derivative: "//g%string())

        f = FExpression("exp(x)")
        call f%setVariable("x", 0.0_e)
        call check(f%evalf() == 1.0_e, "evaluate f(x) = exp(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "exp(x)", "derive f(x) = exp(x)", "wrong derivative: "//g%string())

        f = FExpression("ln(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.0_e, "evaluate f(x) = ln(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "1/x", "derive f(x) = ln(x)", "wrong derivative: "//g%string())

        f = FExpression("log10(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.0_e, "evaluate f(x) = log10(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g%string() == "1/(x*ln(10))", "derive f(x) = log10(x)", "wrong derivative: "//g%string())

        f = FExpression("erf(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.84270079294971478_e, "evaluate f(x) = erf(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        h = FExpression("2/sqrt(pi)*exp(-x**2)")
        call check(g == FExpression("2/sqrt(pi)*exp(-x**2)"), "derive f(x) = erf(x)", "wrong derivative: "//g%string())

        f = FExpression("erfc(x)")
        call f%setVariable("x", 1.0_e)
        call check(f%evalf() == 0.15729920705028516_e, "evaluate f(x) = erfc(x)", "wrong result: "//f%evalf())
        call f%getDerivative("x", g)
        call g%simplify
        call check(g == FExpression("-2/sqrt(pi)*exp(-x**2)"), "derive f(x) = erfc(x)", "wrong derivative: "//g%string())

        f = FExpression("exp(-x^2)")
        call f%setVariable("x", 0.0_e)
        call check(f%evalf() == 1.0_e, "evaluate f(x) = exp(-x^2)", "wrong result: "//f%evalf())
    end subroutine

    subroutine testComp
        type(FExpression) :: f, g
        real(e) :: x, n
        integer :: i
        character(:), allocatable :: string, out
!------
        x = 0.175_e
!------
        f = FExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2")
        call f%getDerivative("r", g)
        call g%simplify
        call assert(g%string() == &
            "2*(1/4*2*20*2/sqrt(π)*exp(-(20*(r-1.5))**2))/r**8+-((2*8*r**7)/r**16*(1+erf(20*(r-1.5)))/2)", &
            "wrong derivative for f(r) = "//f%string()//" : "//g%string())

        f = FExpression("1/r^8")
        call f%getDerivative("r", g)
        call g%simplify
        call assert(g%string() == "-((8*r**7)/r**16)", "wrong derivative 2: "//g%string())

    end subroutine

    subroutine testErrors
        type(FExpression) :: f
        real(e) :: y
        type(FException) :: err
        character(:), allocatable :: c

        call startTestGroup("error conditions")

        ! Make sure that evaluating an uninitialised expression returns an error
        call f%eval(y, err)
        call check(err /= 0, "undefined expression", "failed to report the error")
        call err%discard

        ! Make sure that forgetting a right parenthesis returns an error
        f = FExpression("sin(x", err)
        call check(err /= 0, "parsing sin(x", "failed to report the error")
        call err%discard

        ! Check empty expressions
        f = FExpression("")
        call check(f%string() == "", "empty expression", "wrong string: "//f%string())

        ! Add test for  3*(3+10)))
    end subroutine

    subroutine testOutput
        type(FExpression) :: f

        call startTestGroup("standard output")

        f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
            "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")
        call f%print
    end subroutine

    subroutine testPerformance

        call startTestGroup("performances")

        call testPerformanceDirect
        call testPerformanceDirectFunction
        call testPerformanceInterpreter
        call testPerformancesBytecode
        call testPerformancesVector

!        call testBlockSize

!        call testProblemSize
    end subroutine

    subroutine testPerformanceDirectFunction
        real(e) :: x, y, z
        integer :: i, Niter
        type(FTimer) :: timer
        real(e), dimension(:), allocatable :: res

        Niter = 1000000
        allocate(res(Niter))
        res = 0

        x = 0.175_e
        y = 0.110_e
        z = 0.900_e

        call timer%start
        do i = 1, Niter
            res(i) = f(x, y, z)
        end do
        call timer%stop
        write(*,*) "Simple function time:", timer%walltime()
    contains
        function f(x, y, z)
            real(e), intent(in) :: x, y, z
            real(e) :: f

            f = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/&
                (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z))*3+&
                sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))
        end function
    end subroutine

    subroutine testPerformanceDirect
        real(e) :: x, y, z
        integer :: i, Niter
        type(FTimer) :: timer
        real(e), dimension(:), allocatable :: res

        Niter = 1000000
        allocate(res(Niter))
        res = 0

        x = 0.175_e
        y = 0.110_e
        z = 0.900_e

        call timer%start
        do i = 1, Niter
            res(i) = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+ &
                x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))
        end do
        call timer%stop
        write(*,*) "Direct time:", timer%walltime()
    end subroutine

    ! This test comes from the FunctionParser test
    ! fparser is about 2x as fast as FExpression
    subroutine testPerformanceInterpreter
        real(e) :: x, y, z, answer
        integer :: i, Niter
        type(FExpression) :: f
        type(FTimer) :: timer
        real(e), dimension(:), allocatable :: res

        Niter = 1000000
        allocate(res(Niter))
        res = 0

        x = 0.175_e
        y = 0.110_e
        z = 0.900_e

        answer = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+ &
            x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))

        f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
            "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")

        call f%setVariable("x", x)
        call f%setVariable("y", y)
        call f%setVariable("z", z)
        call timer%start
        do i = 1, Niter
!            call f%interpret(res(i))
            res(i) = f%interpret_f()
        end do
        call timer%stop
        write(*,*) "allow error stop:", timer%walltime()
        call check(all(res .approx. answer), "accuracy of the interpreted evaluation", &
            "Wrong result in "//count(res /= answer)//" elements: "//res(1)//" instead of "//answer)
    end subroutine

    subroutine testPerformancesBytecode
        real(e) :: x, y, z, answer
        integer :: i, Niter
        type(FExpression) :: f
        type(FTimer) :: timer
        real(e), dimension(:), allocatable :: res

        Niter = 1000000
        allocate(res(Niter))
        res = 0

        x = 0.175_e
        y = 0.110_e
        z = 0.900_e

        answer = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+ &
            x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))

        f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
            "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")

        call f%setVariable("x", x)
        call f%setVariable("y", y)
        call f%setVariable("z", z)
        call timer%start
        do i = 1, Niter
            call f%eval(res(i))
!            res(i) = f%evalf()
        end do
        call timer%stop
        write(*,*) "Bytecode time:", timer%walltime()
        call check(all(res .approx. answer), "accuracy of the bytecode evaluation", &
            "Wrong result in "//count(res /= answer)//" elements: "//res(1)//" instead of "//answer)
    end subroutine

    subroutine testPerformancesVector
        real(e) :: x, y, z, answer
        integer :: i, Niter
        type(FExpression) :: f
        type(FTimer) :: timer
        real(e), dimension(:), allocatable :: res

        Niter = 1000000
        allocate(res(Niter))
        res = 0

        x = 0.175_e
        y = 0.110_e
        z = 0.900_e

        answer = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+ &
            x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))

        f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
            "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")

        call f%setVariable("x", x)
        call f%setVariable("y", y)
        call f%setVariable("z", z)
!        call f%printBytecode
        call timer%start
        call f%eval("z", 0.900_e, Niter, 0.0_e, res)
        call timer%stop
        write(*,*) "Array time:", timer%walltime()
!        call check(all(res .approx. answer), "accuracy of the vectorised bytecode evaluation", &
!            "Wrong result in "//count(res /= answer)//" elements: "//res(1)//" instead of "//answer)
    end subroutine

    subroutine testProblemSize
        real(e) :: x, y, z
        integer :: i, Niter, b
        type(FExpression) :: f
        type(FTimer) :: timer
        real(e), dimension(:), allocatable :: res


        x = 0.175_e
        y = 0.110_e
        z = 0.900_e

        f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
            "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")

        call f%setVariable("x", x)
        call f%setVariable("y", y)
        call f%setVariable("z", z)
        write(*,*)
        write(*,*) "BENCHMARK"
        write(*,*) "============================"
        do Niter = 1000, 5000000, 1000

            if (allocated(res)) deallocate(res)
            allocate(res(Niter))
            res = 0

            call timer%start
            call f%eval("z", 0.900_e, Niter, 0.0_e, res)
            call timer%stop
            write(*,*) Niter, timer%walltime(), timer%cpuTime(), timer%cpuTime()/Niter
            call timer%reset
        end do
!        write(*,*) "Array time:", timer%walltime()
!        write(*,*) "Answer:", res(1)
!        stop
!        do i=1, size(res)
!            write(*,*) res(i)
!        end do
    end subroutine

    subroutine testBlockSize
        real(e) :: x, y, z
        integer :: i, Niter, b
        type(FExpression) :: f
        type(FTimer) :: timer
        real(e), dimension(:), allocatable :: res

        Niter = 5000000
        allocate(res(Niter))
        res = 0

        x = 0.175_e
        y = 0.110_e
        z = 0.900_e

        f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
            "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")

        call f%setVariable("x", x)
        call f%setVariable("y", y)
        call f%setVariable("z", z)
        write(*,*)
        write(*,*) "BENCHMARK"
        write(*,*) "============================"
        do b=1, 100
            call timer%start
            call f%eval("z", 0.900_e, Niter, 0.0_e, res, b)
            call timer%stop
            write(*,*) b, timer%walltime(), timer%cpuTime()
            call timer%reset
        end do
    end subroutine

    ! This test comes from the fparser test suite (http://fparser.sourceforge.net)
    ! fparser is about 3x as fast as FExpression
    subroutine fparserTest
        type(FExpression) :: e1, e2, e3
        type(FTimer) :: expressionTimer, codeTimer
        integer :: i, nIter
        real(e) :: res1, res2, res3, v, beta, alpha

        nIter = 1000000
        e1 = FExpression("v*cos(beta)")
        e2 = FExpression("v*sin(beta)*cos(alpha)")
        e3 = FExpression("v*sin(beta)*sin(alpha)")

        v = 10.0_e
        alpha = 1.5_e
        beta = 2.0_e

        call expressionTimer%start()
        do i=1, nIter
            call e1%setVariable("v", v)
            call e1%setVariable("beta", beta)
            call e1%eval(res1)

            call e2%setVariable("v", v)
            call e2%setVariable("alpha", alpha)
            call e2%setVariable("beta", beta)
            call e2%eval(res2)

            call e3%setVariable("v", v)
            call e3%setVariable("alpha", alpha)
            call e3%setVariable("beta", beta)
            call e3%eval(res3)
        end do
        call expressionTimer%stop()

        write(*,*)
        write(*,*) "FExpression"
        write(*,*) "Results:", res1, res2, res3
        write(*,*) expressionTimer%wallTime(), expressionTimer%cpuTime()

        call codeTimer%start()
        do i=1, nIter
           res1 = v*cos(beta)
           res2 = v*sin(beta)*cos(alpha)
           res3 = v*sin(beta)*sin(alpha)
        end do
        call codeTimer%stop()

        write(*,*)
        write(*,*) "Code"
        write(*,*) "Results:", res1, res2, res3
        write(*,*) codeTimer%wallTime(), codeTimer%cpuTime()
    end subroutine
end module

program test
    use test_interpreter
    use mod_test

    call testAll
    call testsFinished
end program