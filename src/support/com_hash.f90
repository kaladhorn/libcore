!******************************************************************************!
!                             com_hash module
!______________________________________________________________________________!
!> Procedures related to hash calculations.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!  Contains interfaces to SuperFastHash and MurmurHash3
!
!  SuperFastHash has been written by Paul Hsieh
!  Copyright (c) 2010 Paul Hsieh
!
!  MurmurHash3 has been created by Austin Appleby.
!  The C port was done by Peter Scott and is in the public domain.
!  It is available at https://github.com/PeterScott/murmur3
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
!>
!> This module contains two subroutines: `[[getHash(function)]]` to calculate
!> hashes from different types of variables, and `[[hash_seed(function)]]` to
!> set the seed parameter used by some of the hash functions.
!>
!> It also contains the interfaces `[[superFastHash(interface)]]`,
!> `[[MurmurHash3_x86_32(interface)]]`, `[[MurmurHash3_x86_128(interface)]]`
!> and `[[MurmurHash3_x64_128(interface)]]` to the C functions with the same
!> name from SuperFastHash and MurmurHash3.
!******************************************************************************!
module com_hash
    use iso_c_binding

    use core, only: wp

    implicit none
    private
    save

    integer(c_int), private :: SEED = 42 !< Seed used by the Murmur3 hash functions

!******************************************************************************!
! Interface to hash functions implemented in C
! See files superFastHash.h and murmur3.h
!******************************************************************************!
    interface
        !> Binding to the SuperFastHash function.
        function superFastHash(string, length) bind(C, name="SuperFastHash")
            use iso_c_binding
            type(c_ptr),    value :: string     !< data from which the hash is calculated
            integer(c_int), value :: length     !< length of the data in bytes
            integer(c_int32_t) :: superFastHash !< 32-bit hash of the data
        end function
        !> Binding to the MurmurHash3 function to calculate a 32-bit hash.
        subroutine MurmurHash3_x86_32(key, length, seed, out) bind(C, name="MurmurHash3_x86_32")
            use iso_c_binding
            type(c_ptr),        intent(in), value :: key    !< data from which the hash is calculated
            integer(c_int),     intent(in), value :: length !< length of the data in bytes
            integer(c_int32_t), intent(in), value :: seed   !< seed used in the hash calculation
            integer(c_int32_t), intent(out)       :: out    !< 32-bit hash of the data
        end subroutine
        !> Binding to the MurmurHash3 function to calculate a 128-bit hash (x86 version).
        subroutine MurmurHash3_x86_128(key, length, seed, out) bind(C, name="MurmurHash3_x86_128")
            use iso_c_binding
            type(c_ptr),        intent(in), value :: key    !< data from which the hash is calculated
            integer(c_int),     intent(in), value :: length !< length of the data in bytes
            integer(c_int32_t), intent(in), value :: seed   !< seed used in the hash calculation
            integer(c_int32_t), dimension(4), intent(out) :: out !< 128-bit hash of the data
        end subroutine
        !> Binding to the MurmurHash3 function to calculate a 128-bit hash (x86_64 version).
        subroutine MurmurHash3_x64_128(key, length, seed, out) bind(C, name="MurmurHash3_x64_128")
            use iso_c_binding
            type(c_ptr),        intent(in), value :: key    !< data from which the hash is calculated
            integer(c_int),     intent(in), value :: length !< length of the data in bytes
            integer(c_int32_t), intent(in), value :: seed   !< seed used in the hash calculation
            integer(c_int32_t), dimension(4), intent(out) :: out !< 128-bit hash of the data
        end subroutine
    end interface
    public :: superFastHash
    public :: MurmurHash3_x86_32
    public :: MurmurHash3_x86_128
    public :: MurmurHash3_x64_128
    public :: hash_seed

!******************************************************************************!
!> Generic interface to the hash functions. The second argument contains the
!> hash, and can be an integer or a character variable with a length greater
!> than 32. In the first case, the hash function used is
!> `[[superFastHash(function)]]`, otherwise `[[MurmurHash3_x64_128]]` is used.
!******************************************************************************!
    interface getHash
        module procedure hashCharacter_SFH, hashCharacter_MH3, &
            hash1DRealArray_MH3, hash2DRealArray_MH3
    end interface
    public :: getHash

contains
!******************************************************************************!
!> Set and get the seed used by the Murmur3 hash function.
!> If not explicitely set, the seed is a fixed to ensure reproducible test runs.
!******************************************************************************!
    subroutine hash_seed(get, put)
        integer, intent(out), optional :: get
        integer, intent(in), optional :: put
!------
        if (present(put)) then
            SEED = put
        end if
        if (present(get)) then
            get = SEED
        end if
    end subroutine
!******************************************************************************!
!> Calculate the hash of a character scalar.
!> The function used is `SuperFastHash`, the hash is a 32-bit integer.
!******************************************************************************!
    subroutine hashCharacter_SFH(in, out)
        character(*), intent(in), target :: in  !< input data
        integer(c_int32_t), intent(out)  :: out !< hash
!------
        integer(c_int) :: length
        character(1) :: x
!------
        length = len(in)*storage_size(x) / 8
        out = superFastHash(c_loc(in(1:1)), length)
    end subroutine
!******************************************************************************!
!> Calculate the hash of a character scalar.
!> The function used is `MurmurHash3`, the hash is 128-bit and returned as a
!> character variable containing its hexadecimal value.
!******************************************************************************!
    subroutine hashCharacter_MH3(in, out)
        character(*), intent(in), target :: in !< input data
        character(32), intent(out) :: out      !< hash
!------
        integer(c_int) :: length
        character(1) :: x
        integer(c_int32_t), dimension(4) :: hash
!------
        length = len(in)*storage_size(x) / 8
        call MurmurHash3_x64_128(c_loc(in(1:1)), length, SEED, hash)
        write(out,'(4z8.8)') hash
    end subroutine
!******************************************************************************!
!> Calculate the hash of a real 1-dimensional array.
!> The function used is `MurmurHash3`, the hash is 128-bit and returned as a
!> character variable containing its hexadecimal value.
!******************************************************************************!
    subroutine hash1DRealArray_MH3(in, out)
        real(wp), dimension(:), intent(in), target :: in !< input data
        character(32), intent(out) :: out                !< hash
!------
        integer(c_int) :: length
        real(wp) :: x
        integer(c_int32_t), dimension(4) :: hash
!------
        length = size(in)*storage_size(x) / 8
        hash = 0
        call MurmurHash3_x64_128(c_loc(in(1)), length, SEED, hash)
        write(out,'(4z8.8)') hash
    end subroutine
!******************************************************************************!
!> Calculate the hash of a real 2-dimensional array.
!> The function used is `MurmurHash3`, the hash is 128-bit and returned as a
!> character variable containing its hexadecimal value.
!******************************************************************************!
    subroutine hash2DRealArray_MH3(in, out)
        real(wp), dimension(:,:), intent(in), target :: in !< input data
        character(32), intent(out) :: out                  !< hash
!------
        integer(c_int) :: length
        real(wp) :: x
        integer(c_int32_t), dimension(4) :: hash
!------
        length = size(in)*storage_size(x) / 8
        hash = 0
        call MurmurHash3_x64_128(c_loc(in(1,1)), length, SEED, hash)
        write(out,'(4z8.8)') hash
    end subroutine
!******************************************************************************!
!> Calculate the hash of a generic object.
!> The function used is `MurmurHash3`, the hash is 128-bit and returned as a
!> character variable containing its hexadecimal value.
!>
!> This is commented out for now; NAG does not like it.
!******************************************************************************!
!    subroutine hash_MH3(in, out)
!        type(*), dimension(..), intent(in), target :: in !< input data
!        character(32), intent(out) :: out                !< hash
!!------
!        integer(c_int) :: length
!        real(wp) :: x
!        integer(c_int32_t), dimension(4) :: hash
!!------
!        length = size(in)*storage_size(x) / 8
!        hash = 0
!        call MurmurHash3_x64_128(c_loc(in), length, SEED, hash)
!        write(out,'(4z8.8)') hash
!    end subroutine
end module
