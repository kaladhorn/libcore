    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine c1f5kb(ido, l1, na, cc, in1, ch, in2, wa)
        implicit none
        include 'kinds.inc'
        integer :: ido, l1, na, in1, in2
        real (rwp) cc(in1, l1, ido, 5), ch(in2, l1, 5, ido), wa(ido, 4, 2)
        
        real(rwp) :: chold1, chold2, ci2, ci3, ci4, ci5, cr2, cr3, cr4, cr5
        real(rwp) :: di2, di3, di4, di5, dr2, dr3, dr4, dr5, ti2, ti3, ti4, ti5
        real(rwp) :: tr2, tr3, tr4, tr5
        real(rwp) :: tr11, ti11, tr12, ti12
        integer :: i, k
        data tr11, ti11, tr12, ti12/.3090169943749474, .9510565162951536, &
          -.8090169943749474, .5877852522924731/

        ! FFTPACK 5.1 auxiliary routine

        if (ido>1 .or. na==1) go to 100
        do k = 1, l1
            ti5 = cc(2, k, 1, 2) - cc(2, k, 1, 5)
            ti2 = cc(2, k, 1, 2) + cc(2, k, 1, 5)
            ti4 = cc(2, k, 1, 3) - cc(2, k, 1, 4)
            ti3 = cc(2, k, 1, 3) + cc(2, k, 1, 4)
            tr5 = cc(1, k, 1, 2) - cc(1, k, 1, 5)
            tr2 = cc(1, k, 1, 2) + cc(1, k, 1, 5)
            tr4 = cc(1, k, 1, 3) - cc(1, k, 1, 4)
            tr3 = cc(1, k, 1, 3) + cc(1, k, 1, 4)
            chold1 = cc(1, k, 1, 1) + tr2 + tr3
            chold2 = cc(2, k, 1, 1) + ti2 + ti3
            cr2 = cc(1, k, 1, 1) + tr11*tr2 + tr12*tr3
            ci2 = cc(2, k, 1, 1) + tr11*ti2 + tr12*ti3
            cr3 = cc(1, k, 1, 1) + tr12*tr2 + tr11*tr3
            ci3 = cc(2, k, 1, 1) + tr12*ti2 + tr11*ti3
            cc(1, k, 1, 1) = chold1
            cc(2, k, 1, 1) = chold2
            cr5 = ti11*tr5 + ti12*tr4
            ci5 = ti11*ti5 + ti12*ti4
            cr4 = ti12*tr5 - ti11*tr4
            ci4 = ti12*ti5 - ti11*ti4
            cc(1, k, 1, 2) = cr2 - ci5
            cc(1, k, 1, 5) = cr2 + ci5
            cc(2, k, 1, 2) = ci2 + cr5
            cc(2, k, 1, 3) = ci3 + cr4
            cc(1, k, 1, 3) = cr3 - ci4
            cc(1, k, 1, 4) = cr3 + ci4
            cc(2, k, 1, 4) = ci3 - cr4
            cc(2, k, 1, 5) = ci2 - cr5
        end do
        return
100     do k = 1, l1
            ti5 = cc(2, k, 1, 2) - cc(2, k, 1, 5)
            ti2 = cc(2, k, 1, 2) + cc(2, k, 1, 5)
            ti4 = cc(2, k, 1, 3) - cc(2, k, 1, 4)
            ti3 = cc(2, k, 1, 3) + cc(2, k, 1, 4)
            tr5 = cc(1, k, 1, 2) - cc(1, k, 1, 5)
            tr2 = cc(1, k, 1, 2) + cc(1, k, 1, 5)
            tr4 = cc(1, k, 1, 3) - cc(1, k, 1, 4)
            tr3 = cc(1, k, 1, 3) + cc(1, k, 1, 4)
            ch(1, k, 1, 1) = cc(1, k, 1, 1) + tr2 + tr3
            ch(2, k, 1, 1) = cc(2, k, 1, 1) + ti2 + ti3
            cr2 = cc(1, k, 1, 1) + tr11*tr2 + tr12*tr3
            ci2 = cc(2, k, 1, 1) + tr11*ti2 + tr12*ti3
            cr3 = cc(1, k, 1, 1) + tr12*tr2 + tr11*tr3
            ci3 = cc(2, k, 1, 1) + tr12*ti2 + tr11*ti3
            cr5 = ti11*tr5 + ti12*tr4
            ci5 = ti11*ti5 + ti12*ti4
            cr4 = ti12*tr5 - ti11*tr4
            ci4 = ti12*ti5 - ti11*ti4
            ch(1, k, 2, 1) = cr2 - ci5
            ch(1, k, 5, 1) = cr2 + ci5
            ch(2, k, 2, 1) = ci2 + cr5
            ch(2, k, 3, 1) = ci3 + cr4
            ch(1, k, 3, 1) = cr3 - ci4
            ch(1, k, 4, 1) = cr3 + ci4
            ch(2, k, 4, 1) = ci3 - cr4
            ch(2, k, 5, 1) = ci2 - cr5
        end do
        if (ido==1) return
        do i = 2, ido
            do k = 1, l1
                ti5 = cc(2, k, i, 2) - cc(2, k, i, 5)
                ti2 = cc(2, k, i, 2) + cc(2, k, i, 5)
                ti4 = cc(2, k, i, 3) - cc(2, k, i, 4)
                ti3 = cc(2, k, i, 3) + cc(2, k, i, 4)
                tr5 = cc(1, k, i, 2) - cc(1, k, i, 5)
                tr2 = cc(1, k, i, 2) + cc(1, k, i, 5)
                tr4 = cc(1, k, i, 3) - cc(1, k, i, 4)
                tr3 = cc(1, k, i, 3) + cc(1, k, i, 4)
                ch(1, k, 1, i) = cc(1, k, i, 1) + tr2 + tr3
                ch(2, k, 1, i) = cc(2, k, i, 1) + ti2 + ti3
                cr2 = cc(1, k, i, 1) + tr11*tr2 + tr12*tr3
                ci2 = cc(2, k, i, 1) + tr11*ti2 + tr12*ti3
                cr3 = cc(1, k, i, 1) + tr12*tr2 + tr11*tr3
                ci3 = cc(2, k, i, 1) + tr12*ti2 + tr11*ti3
                cr5 = ti11*tr5 + ti12*tr4
                ci5 = ti11*ti5 + ti12*ti4
                cr4 = ti12*tr5 - ti11*tr4
                ci4 = ti12*ti5 - ti11*ti4
                dr3 = cr3 - ci4
                dr4 = cr3 + ci4
                di3 = ci3 + cr4
                di4 = ci3 - cr4
                dr5 = cr2 + ci5
                dr2 = cr2 - ci5
                di5 = ci2 - cr5
                di2 = ci2 + cr5
                ch(1, k, 2, i) = wa(i, 1, 1)*dr2 - wa(i, 1, 2)*di2
                ch(2, k, 2, i) = wa(i, 1, 1)*di2 + wa(i, 1, 2)*dr2
                ch(1, k, 3, i) = wa(i, 2, 1)*dr3 - wa(i, 2, 2)*di3
                ch(2, k, 3, i) = wa(i, 2, 1)*di3 + wa(i, 2, 2)*dr3
                ch(1, k, 4, i) = wa(i, 3, 1)*dr4 - wa(i, 3, 2)*di4
                ch(2, k, 4, i) = wa(i, 3, 1)*di4 + wa(i, 3, 2)*dr4
                ch(1, k, 5, i) = wa(i, 4, 1)*dr5 - wa(i, 4, 2)*di5
                ch(2, k, 5, i) = wa(i, 4, 1)*di5 + wa(i, 4, 2)*dr5
            end do
        end do
    end subroutine
