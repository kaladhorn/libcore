    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mradfg(m, ido, ip, l1, idl1, cc, c1, c2, im1, in1, ch, ch2, &
      im2, in2, wa)
        include 'kinds.inc'
        real (rwp) ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
          c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), wa(ido)

        m1d = (m-1)*im1 + 1
        m2s = 1 - im2
        tpi = 2.*4.*atan(1.0_rwp)
        arg = tpi/float(ip)
        dcp = cos(arg)
        dsp = sin(arg)
        ipph = (ip+1)/2
        ipp2 = ip + 2
        idp2 = ido + 2
        nbd = (ido-1)/2
        if (ido==1) go to 130
        do ik = 1, idl1
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch2(m2, ik, 1) = c2(m1, ik, 1)
            end do
        end do
        do j = 2, ip
            do k = 1, l1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch(m2, 1, k, j) = c1(m1, 1, k, j)
                end do
            end do
        end do
        if (nbd>l1) go to 100
        is = -ido
        do j = 2, ip
            is = is + ido
            idij = is
            do i = 3, ido, 2
                idij = idij + 2
                do k = 1, l1
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        ch(m2, i-1, k, j) = wa(idij-1)*c1(m1, i-1, k, j) + &
                          wa(idij)*c1(m1, i, k, j)
                        ch(m2, i, k, j) = wa(idij-1)*c1(m1, i, k, j) - &
                          wa(idij)*c1(m1, i-1, k, j)
                    end do
                end do
            end do
        end do
        go to 110
100     is = -ido
        do j = 2, ip
            is = is + ido
            do k = 1, l1
                idij = is
                do i = 3, ido, 2
                    idij = idij + 2
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        ch(m2, i-1, k, j) = wa(idij-1)*c1(m1, i-1, k, j) + &
                          wa(idij)*c1(m1, i, k, j)
                        ch(m2, i, k, j) = wa(idij-1)*c1(m1, i, k, j) - &
                          wa(idij)*c1(m1, i-1, k, j)
                    end do
                end do
            end do
        end do
110     if (nbd<l1) go to 120
        do j = 2, ipph
            jc = ipp2 - j
            do k = 1, l1
                do i = 3, ido, 2
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        c1(m1, i-1, k, j) = ch(m2, i-1, k, j) + &
                          ch(m2, i-1, k, jc)
                        c1(m1, i-1, k, jc) = ch(m2, i, k, j) - &
                          ch(m2, i, k, jc)
                        c1(m1, i, k, j) = ch(m2, i, k, j) + ch(m2, i, k, jc)
                        c1(m1, i, k, jc) = ch(m2, i-1, k, jc) - &
                          ch(m2, i-1, k, j)
                    end do
                end do
            end do
        end do
        go to 140
120     do j = 2, ipph
            jc = ipp2 - j
            do i = 3, ido, 2
                do k = 1, l1
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        c1(m1, i-1, k, j) = ch(m2, i-1, k, j) + &
                          ch(m2, i-1, k, jc)
                        c1(m1, i-1, k, jc) = ch(m2, i, k, j) - &
                          ch(m2, i, k, jc)
                        c1(m1, i, k, j) = ch(m2, i, k, j) + ch(m2, i, k, jc)
                        c1(m1, i, k, jc) = ch(m2, i-1, k, jc) - &
                          ch(m2, i-1, k, j)
                    end do
                end do
            end do
        end do
        go to 140
130     do ik = 1, idl1
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                c2(m1, ik, 1) = ch2(m2, ik, 1)
            end do
        end do
140     do j = 2, ipph
            jc = ipp2 - j
            do k = 1, l1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    c1(m1, 1, k, j) = ch(m2, 1, k, j) + ch(m2, 1, k, jc)
                    c1(m1, 1, k, jc) = ch(m2, 1, k, jc) - ch(m2, 1, k, j)
                end do
            end do
        end do

        ar1 = 1.
        ai1 = 0.
        do l = 2, ipph
            lc = ipp2 - l
            ar1h = dcp*ar1 - dsp*ai1
            ai1 = dcp*ai1 + dsp*ar1
            ar1 = ar1h
            do ik = 1, idl1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch2(m2, ik, l) = c2(m1, ik, 1) + ar1*c2(m1, ik, 2)
                    ch2(m2, ik, lc) = ai1*c2(m1, ik, ip)
                end do
            end do
            dc2 = ar1
            ds2 = ai1
            ar2 = ar1
            ai2 = ai1
            do j = 3, ipph
                jc = ipp2 - j
                ar2h = dc2*ar2 - ds2*ai2
                ai2 = dc2*ai2 + ds2*ar2
                ar2 = ar2h
                do ik = 1, idl1
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        ch2(m2, ik, l) = ch2(m2, ik, l) + ar2*c2(m1, ik, j)
                        ch2(m2, ik, lc) = ch2(m2, ik, lc) + ai2*c2(m1, ik, jc)
                    end do
                end do
            end do
        end do
        do j = 2, ipph
            do ik = 1, idl1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch2(m2, ik, 1) = ch2(m2, ik, 1) + c2(m1, ik, j)
                end do
            end do
        end do

        if (ido<l1) go to 150
        do k = 1, l1
            do i = 1, ido
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    cc(m1, i, 1, k) = ch(m2, i, k, 1)
                end do
            end do
        end do
        go to 160
150     do i = 1, ido
            do k = 1, l1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    cc(m1, i, 1, k) = ch(m2, i, k, 1)
                end do
            end do
        end do
160     do j = 2, ipph
            jc = ipp2 - j
            j2 = j + j
            do k = 1, l1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    cc(m1, ido, j2-2, k) = ch(m2, 1, k, j)
                    cc(m1, 1, j2-1, k) = ch(m2, 1, k, jc)
                end do
            end do
        end do
        if (ido==1) return
        if (nbd<l1) go to 170
        do j = 2, ipph
            jc = ipp2 - j
            j2 = j + j
            do k = 1, l1
                do i = 3, ido, 2
                    ic = idp2 - i
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        cc(m1, i-1, j2-1, k) = ch(m2, i-1, k, j) + &
                          ch(m2, i-1, k, jc)
                        cc(m1, ic-1, j2-2, k) = ch(m2, i-1, k, j) - &
                          ch(m2, i-1, k, jc)
                        cc(m1, i, j2-1, k) = ch(m2, i, k, j) + &
                          ch(m2, i, k, jc)
                        cc(m1, ic, j2-2, k) = ch(m2, i, k, jc) - &
                          ch(m2, i, k, j)
                    end do
                end do
            end do
        end do
        return
170     do j = 2, ipph
            jc = ipp2 - j
            j2 = j + j
            do i = 3, ido, 2
                ic = idp2 - i
                do k = 1, l1
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        cc(m1, i-1, j2-1, k) = ch(m2, i-1, k, j) + &
                          ch(m2, i-1, k, jc)
                        cc(m1, ic-1, j2-2, k) = ch(m2, i-1, k, j) - &
                          ch(m2, i-1, k, jc)
                        cc(m1, i, j2-1, k) = ch(m2, i, k, j) + &
                          ch(m2, i, k, jc)
                        cc(m1, ic, j2-2, k) = ch(m2, i, k, jc) - &
                          ch(m2, i, k, j)
                    end do
                end do
            end do
        end do
    end subroutine
