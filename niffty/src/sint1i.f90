    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine sint1i(n, wsave, lensav, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        integer n, lensav, ier
        real (rwp) wsave(lensav)

        ier = 0

        if (lensav<n/2+n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('SINT1I', 3)
            go to 100
        end if

        pi = 4.*atan(1.0_rwp)
        if (n<=1) return
        ns2 = n/2
        np1 = n + 1
        dt = pi/float(np1)
        do k = 1, ns2
            wsave(k) = 2.*sin(k*dt)
        end do
        lnsv = np1 + int(log(real(np1, rwp))/log(2.0_rwp)) + 4
        call rfft1i(np1, wsave(ns2+1), lnsv, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('SINT1I', -5)
        end if

100     continue
    end subroutine
