    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine rfft2f(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
        implicit none
        integer ldim, l, m, lensav, lenwrk, ier, idx, modl, modm, idh, idw
        include 'kinds.inc'
        real (rwp) r(ldim, m), wsave(lensav), work(lenwrk)
        integer ldx, i, j, ier1, ldh, ldw, lwsav, mmsav, mwsav


        ! INITIALIZE IER

        ier = 0

        ! VERIFY LENSAV

        lwsav = l + int(log(real(l, rwp))/log(2.0_rwp)) + 4
        mwsav = 2*m + int(log(real(m, rwp))/log(2.0_rwp)) + 4
        mmsav = m + int(log(real(m, rwp))/log(2.0_rwp)) + 4

        if (lensav<lwsav+mwsav+mmsav) then
            ier = 2
            call xerfft('RFFT2F', 6)
            go to 100
        end if

        ! VERIFY LENWRK

        if (lenwrk<(l+1)*m) then
            ier = 3
            call xerfft('RFFT2F', 8)
            go to 100
        end if

        ! VERIFY LDIM IS AS BIG AS L

        if (ldim<l) then
            ier = 5
            call xerfft('RFFT2F', -6)
            go to 100
        end if

        ! TRANSFORM FIRST DIMENSION OF ARRAY

        call rfftmf(m, ldim, l, 1, r, m*ldim, wsave(1), l+int(log( &
          real(l, rwp))/log(2.0_rwp))+4, work, lenwrk, ier1)

        if (ier1/=0) then
            ier = 20
            call xerfft('RFFT2F', -5)
            go to 100
        end if

        ldx = 2*int((l+1)/2) - 1
        do i = 2, ldx
            do j = 1, m
                r(i, j) = .5_rwp*r(i, j)
            end do
        end do
        do j = 1, m
            do i = 3, ldx, 2
                r(i, j) = -r(i, j)
            end do
        end do

        ! PRINT*, 'FORWARD TRANSFORM IN THE I DIRECTION'
        ! DO I=1,L
        ! PRINT*, (R(I,J),J=1,M)
        ! END DO

        ! RESHUFFLE TO ADD IN NYQUIST IMAGINARY COMPONENTS

        modl = mod(l, 2)
        modm = mod(m, 2)

        ! TRANSFORM SECOND DIMENSION OF ARRAY

        call rfftmf(1, 1, m, ldim, r, m*ldim, wsave(lwsav+mwsav+1), mmsav, &
          work, lenwrk, ier1)
        do j = 2, 2*((m+1)/2) - 1
            r(1, j) = .5_rwp*r(1, j)
        end do
        do j = 3, m, 2
            r(1, j) = -r(1, j)
        end do
        ldh = int((l+1)/2)
        if (ldh>1) then
            ldw = ldh + ldh

            ! R AND WORK ARE SWITCHED BECAUSE THE THE FIRST DIMENSION
            ! OF THE INPUT TO COMPLEX(CWP) CFFTMF MUST BE EVEN.

            call r2w(ldim, ldw, l, m, r, work)
            call cfftmf(ldh-1, 1, m, ldh, work(2), ldh*m, wsave(lwsav+1), &
              mwsav, r, l*m, ier1)
            if (ier1/=0) then
                ier = 20
                call xerfft('RFFT2F', -5)
                go to 100
            end if
            call w2r(ldim, ldw, l, m, r, work)
        end if

        if (modl==0) then
            call rfftmf(1, 1, m, ldim, r(l,1), m*ldim, wsave(lwsav+mwsav+1), &
              mmsav, work, lenwrk, ier1)
            do j = 2, 2*((m+1)/2) - 1
                r(l, j) = .5_rwp*r(l, j)
            end do
            do j = 3, m, 2
                r(l, j) = -r(l, j)
            end do
        end if

        ! PRINT*, 'FORWARD TRANSFORM IN THE J DIRECTION'
        ! DO I=1,L
        ! PRINT*, (R(I,J),J=1,M)
        ! END DO

        if (ier1/=0) then
            ier = 20
            call xerfft('RFFT2F', -5)
            go to 100
        end if


100     continue
    end subroutine
