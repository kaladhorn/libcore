    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine c1f3kf(ido, l1, na, cc, in1, ch, in2, wa)
        implicit none
        include 'kinds.inc'
        integer :: ido, l1, na, in1, in2
        real(rwp) :: cc(in1, l1, ido, 3), ch(in2, l1, 3, ido), wa(ido, 2, 2)

        real(rwp) :: ci2, ci3, cr2, cr3, di2, di3, dr2, dr3, ti2, tr2, sn
        integer :: i, k
        real(rwp), parameter :: taur = -0.5_rwp
        real(rwp), parameter :: taui = -0.866025403784439_rwp

        if (ido>1) go to 110
        sn = 1.0_rwp/real(3*l1, rwp)
        if (na==1) go to 100
        do k = 1, l1
            tr2 = cc(1, k, 1, 2) + cc(1, k, 1, 3)
            cr2 = cc(1, k, 1, 1) + taur*tr2
            cc(1, k, 1, 1) = sn*(cc(1,k,1,1)+tr2)
            ti2 = cc(2, k, 1, 2) + cc(2, k, 1, 3)
            ci2 = cc(2, k, 1, 1) + taur*ti2
            cc(2, k, 1, 1) = sn*(cc(2,k,1,1)+ti2)
            cr3 = taui*(cc(1,k,1,2)-cc(1,k,1,3))
            ci3 = taui*(cc(2,k,1,2)-cc(2,k,1,3))
            cc(1, k, 1, 2) = sn*(cr2-ci3)
            cc(1, k, 1, 3) = sn*(cr2+ci3)
            cc(2, k, 1, 2) = sn*(ci2+cr3)
            cc(2, k, 1, 3) = sn*(ci2-cr3)
        end do
        return
100     do k = 1, l1
            tr2 = cc(1, k, 1, 2) + cc(1, k, 1, 3)
            cr2 = cc(1, k, 1, 1) + taur*tr2
            ch(1, k, 1, 1) = sn*(cc(1,k,1,1)+tr2)
            ti2 = cc(2, k, 1, 2) + cc(2, k, 1, 3)
            ci2 = cc(2, k, 1, 1) + taur*ti2
            ch(2, k, 1, 1) = sn*(cc(2,k,1,1)+ti2)
            cr3 = taui*(cc(1,k,1,2)-cc(1,k,1,3))
            ci3 = taui*(cc(2,k,1,2)-cc(2,k,1,3))
            ch(1, k, 2, 1) = sn*(cr2-ci3)
            ch(1, k, 3, 1) = sn*(cr2+ci3)
            ch(2, k, 2, 1) = sn*(ci2+cr3)
            ch(2, k, 3, 1) = sn*(ci2-cr3)
        end do
        return
110     do k = 1, l1
            tr2 = cc(1, k, 1, 2) + cc(1, k, 1, 3)
            cr2 = cc(1, k, 1, 1) + taur*tr2
            ch(1, k, 1, 1) = cc(1, k, 1, 1) + tr2
            ti2 = cc(2, k, 1, 2) + cc(2, k, 1, 3)
            ci2 = cc(2, k, 1, 1) + taur*ti2
            ch(2, k, 1, 1) = cc(2, k, 1, 1) + ti2
            cr3 = taui*(cc(1,k,1,2)-cc(1,k,1,3))
            ci3 = taui*(cc(2,k,1,2)-cc(2,k,1,3))
            ch(1, k, 2, 1) = cr2 - ci3
            ch(1, k, 3, 1) = cr2 + ci3
            ch(2, k, 2, 1) = ci2 + cr3
            ch(2, k, 3, 1) = ci2 - cr3
        end do
        do i = 2, ido
            do k = 1, l1
                tr2 = cc(1, k, i, 2) + cc(1, k, i, 3)
                cr2 = cc(1, k, i, 1) + taur*tr2
                ch(1, k, 1, i) = cc(1, k, i, 1) + tr2
                ti2 = cc(2, k, i, 2) + cc(2, k, i, 3)
                ci2 = cc(2, k, i, 1) + taur*ti2
                ch(2, k, 1, i) = cc(2, k, i, 1) + ti2
                cr3 = taui*(cc(1,k,i,2)-cc(1,k,i,3))
                ci3 = taui*(cc(2,k,i,2)-cc(2,k,i,3))
                dr2 = cr2 - ci3
                dr3 = cr2 + ci3
                di2 = ci2 + cr3
                di3 = ci2 - cr3
                ch(2, k, 2, i) = wa(i, 1, 1)*di2 - wa(i, 1, 2)*dr2
                ch(1, k, 2, i) = wa(i, 1, 1)*dr2 + wa(i, 1, 2)*di2
                ch(2, k, 3, i) = wa(i, 2, 1)*di3 - wa(i, 2, 2)*dr3
                ch(1, k, 3, i) = wa(i, 2, 1)*dr3 + wa(i, 2, 2)*di3
            end do
        end do
    end subroutine
