!******************************************************************************!
!                       mod_stringDictionary module
!______________________________________________________________________________!
!> `[[FStringDictionary(type)]]` derived type and TBPs, a string-valued
!> dictionary.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_stringDictionary
    use core

    use mod_string,     only: FString
    use mod_exception,  only: FException, MESSAGE_LEVEL_WARNING
    use mod_dictionary, only: FDictionary

    implicit none
    private
    save

!******************************************************************************!
!> String dictionary description.
!******************************************************************************!
    type, public, extends(FDictionary) :: FStringDictionary
        type(FString), dimension(:), allocatable :: val
    contains
        procedure, private :: stringDictionary_setCharacterScalar
        procedure, private :: stringDictionary_setStringScalar
        generic, public :: set => stringDictionary_setCharacterScalar, &
            stringDictionary_setStringScalar

        procedure, private :: getValueScalarS
        procedure, private :: getValueScalarC
        procedure, private :: getValueScalarCS
        procedure, public :: getValueScalarSC
        generic, public :: getValue => getValueScalarS, &
            getValueScalarC, &
            getValueScalarCS, &
            getValueScalarSC

        procedure, private :: valueScalarS
        procedure, private :: valueScalarC
        generic, public :: value => valueScalarS, &
            valueScalarC

        ! Comparison operators
        procedure, private :: stringDictionary_compareEqual
        generic, public :: operator(==) => stringDictionary_compareEqual

        procedure, public :: print
    end type

!******************************************************************************!
!> String dictionary constructor.
!******************************************************************************!
    interface FStringDictionary
        module procedure stringDictionary_init
    end interface

contains
!******************************************************************************!
!> Test whether two `[[FStringDictionary(type)]]` objects are identical.
!******************************************************************************!
    elemental function stringDictionary_compareEqual(this, to) result(test)
        class(FStringDictionary), intent(in) :: this
        type(FStringDictionary), intent(in) :: to
        logical :: test
!------
        integer :: i, j
        character(:), allocatable :: name
!------
        test = .true.

        ! If the sizes of the dictionaries are different, then they cannot be identical
        if (this%size() /= to%size()) then
            test = .false.
            return
        end if

        ! Consider each key in the input dictionary
        keyLoop: do i=1, size(this%val)

            name = this%key(i)

            do j=1, size(to%val)
                if (to%key(j) == name) then

                    ! Check whether the values are the same too
                    test = (this%val(i) == to%val(j))

                    ! Return now if there is a difference
                    if (.not.test) then
                        return
                    end if

                    ! Otherwise, process the other keys
                    cycle keyLoop
                end if
            end do
        end do keyLoop
    end function
!******************************************************************************!
!> Initialise a `[[FStringDictionary(type)]]` object with a list of keys and
!> values.
!******************************************************************************!
    function stringDictionary_init(keys, values, stat) result(this)
        type(FStringDictionary) :: this
        type(FString), dimension(:),   intent(in) :: keys
        type(FString), dimension(:),   intent(in) :: values
        type(FException), intent(out), optional   :: stat
!------
        type(FException) :: istat
!------
        if (size(values) == size(keys)) then
            this%key_ = keys
            this%val = values
            this%size_ = size(keys)
        else
            call istat%raise("Keys and values arrays do not match.")
        end if

        if (present(stat)) call stat%transfer(istat)
    end function
!******************************************************************************!
!> Add a key to a dictionary object (Character version).
!******************************************************************************!
    subroutine stringDictionary_setCharacterScalar(this, key, value)
        class(FStringDictionary), intent(inout) :: this
        character(*), intent(in) :: key
        character(*), intent(in) :: value
!------
        integer :: i
!------
        ! Get the value to add to the dictionary

        ! Make sure the key is not already defined
        do i=1, this%size()
            if (this%key(i) == key) then
                ! If it is, set the value
                this%val(i) = value
                return
            end if
        end do

        ! Increase the size of the internal arrays if needed
        this%size_ = this%size_ + 1
        if (.not.allocated(this%key_) .or. .not. allocated(this%val)) then
            call extendArrays(this)
        else
            if (this%size() > size(this%key_) .or. this%size() > size(this%val)) &
                call extendArrays(this)
        end if

        ! Set the value
        this%key_(this%size()) = key
        this%val(this%size()) = value
    end subroutine
!******************************************************************************!
!> Add a key to a dictionary object (`[[FString(type)]]` version). Thin wrapper
!> around the character version.
!******************************************************************************!
    subroutine stringDictionary_setStringScalar(this, key, value)
        class(FStringDictionary), intent(inout) :: this
        type(FString), intent(in) :: key
        type(FString), intent(in) :: value
!------
        call this%set(key%string(), value%string())
    end subroutine
!******************************************************************************!
!> Get the value for a given key (character version).
!******************************************************************************!
    subroutine getValueScalarC(this, key, value, err)
        class(FStringDictionary), intent(in) :: this
        character(*), intent(in)  :: key
        character(:), allocatable, intent(inout) :: value
        type(FException), intent(out), optional :: err !< Error status
!------
        integer :: i
        type(FException) :: err_
!------
        do i=1, this%size()
            if (this%key_(i) == key) then
                value = this%val(i)%string()
                return
            end if
        end do

        ! Print a warning if no value has been found for the given key
        call err_%raise("the key '" // key // "' is not defined.", MESSAGE_LEVEL_WARNING)
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the value for a given key (character/`[[FString(type)]]` version).
!******************************************************************************!
    subroutine getValueScalarCS(this, key, value, err)
        class(FStringDictionary), intent(in) :: this
        character(*), intent(in)  :: key
        type(FString), intent(out) :: value
        type(FException), intent(out), optional :: err !< Error status
!------
        integer :: i
        type(FException) :: err_
!------
        do i=1, this%size()
            if (this%key_(i) == key) then
                value = this%val(i)
                return
            end if
        end do

        ! Print a warning if no value has been found for the given key
        call err_%raise("the key '" // key // "' is not defined.", MESSAGE_LEVEL_WARNING)
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the value for a given key (`[[FString(type)]]`/character version).
!******************************************************************************!
    subroutine getValueScalarSC(this, key, value, err)
        class(FStringDictionary), intent(in) :: this
        type(FString), intent(in)  :: key
        character(*), intent(out) :: value
        type(FException), intent(out), optional :: err !< Error status
!------
        integer :: i
        type(FException) :: err_
!------
        do i=1, this%size()
            if (this%key_(i) == key) then
                value = this%val(i)%string()
                return
            end if
        end do

        ! Print a warning if no value has been found for the given key
        call err_%raise("the key '" // key // "' is not defined.", MESSAGE_LEVEL_WARNING)
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the value for a given key (`[[FString(type)]]` version).
!******************************************************************************!
    subroutine getValueScalarS(this, key, value, err)
        class(FStringDictionary), intent(in) :: this
        type(FString), intent(in)  :: key
        type(FString), intent(out) :: value
        type(FException), intent(out), optional :: err !< Error status
!------
        integer :: i
        type(FException) :: err_
!------
        do i=1, this%size()
            if (this%key_(i) == key) then
                value = this%val(i)
                return
            end if
        end do

        ! Print a warning if no value has been found for the given key
        call err_%raise("the key '" // key // "' is not defined.", MESSAGE_LEVEL_WARNING)
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the value for a given key (character version).
!******************************************************************************!
    pure function valueScalarC(this, key) result(value)
        class(FStringDictionary), intent(in) :: this
        character(*), intent(in)  :: key
        type(FString) :: value
!------
        integer :: i
!------
        do i=1, this%size()
            if (this%key_(i) == key) then
                value = this%val(i)%string()
                return
            end if
        end do

        ! return UNDEF if the key does not exist in the dictionary
        value = UNDEF
    end function
!******************************************************************************!
!> Get the value for a given key (`[[FString(type)]]` version).
!******************************************************************************!
    function valueScalarS(this, key) result(value)
        class(FStringDictionary), intent(in) :: this
        type(FString), intent(in)  :: key
        type(FString) :: value
!------
        integer :: i
!------
        do i=1, this%size()
            if (this%key_(i) == key) then
                value = this%val(i)
                return
            end if
        end do

        ! return UNDEF if the key does not exist in the dictionary
        value = UNDEF
    end function
!******************************************************************************!
!> Print a human-readable representation fo a `[[FStringDictionary(type)]]`
!> object to standard output.
!******************************************************************************!
!LCOV_EXCL_START
    subroutine print(this)
        class(FStringDictionary), intent(in) :: this
!------
        integer :: i
!------
        write(*,'(a)') "{"

        ! Print the first key-value pair
        write(*,'(a)',advance="no") "    "//this%key(1)//" : "//this%val(1)

        ! Print the rest of the dictionary
        do i=2, this%size()
            write(*,'(a)') ","
            write(*,'(a)',advance="no") "    "//this%key(i)//" : "//this%val(i)
        end do
        write(*,*)

        write(*,'(a)') "}"
    end subroutine
!LCOV_EXCL_STOP
!******************************************************************************!
!> Reallocate the internal arrays of a dictionary object to mare space
!> for more elements. For internal use only.
!******************************************************************************!
    pure subroutine extendArrays(this)
        type(FStringDictionary), intent(inout) :: this
!------
        type(FString), dimension(:), allocatable :: tmps ! Temporary array for the keys
        type(FString), dimension(:), allocatable :: tmpr ! Temporary array for the values
!------
        if (.not.allocated(this%key_)) then
            allocate(this%key_(this%initialSize))
        else
            call move_alloc(from=this%key_, to=tmps)
            allocate(this%key_(size(tmps)+this%sizeIncrement))
            this%key_(:size(tmps)) = tmps
        end if

        if (.not.allocated(this%val)) then
            allocate(this%val(this%initialSize))
        else
            allocate(tmpr(size(this%val) + this%sizeIncrement))
            tmpr(:size(this%val)) = this%val(:)
            deallocate(this%val)
            call move_alloc(from=tmpr, to=this%val)
        end if
    end subroutine
end module !LCOV_EXCL_LINE
