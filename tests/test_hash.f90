module test_character
    use core
    use com_hash
    use ext_character
    use mod_test
    implicit none
    private
    
    public :: testAll
contains
    subroutine testAll()
        call testHash
    end subroutine
    
    subroutine testHash
        character(:), allocatable :: string
        integer :: i, data_size, j
        integer, dimension(1) :: b
        real(e), dimension(:), allocatable :: data
        real(e), dimension(:,:), allocatable :: data2D
        character(32) :: hash
        
        string = "hello"
        call getHash(string, i)
        call check(i == -1331836805, "hash value for character scalar", "wrong hash for string ""hello"": "//i)
        
        string = "hellp"
        call getHash(string, i)
        call check(i == -377253211, "hash value for modified character scalar", "wrong hash for string ""hellp"": "//i)
        
        call hash_seed(get=i)
        call check(i==42, "seed for hash functions", "wrong seed: "//i)
        
        string = "qsmobubfq0823mjbqdm18Ypubimqdà'm12OUmé'& qzd1#"
        call getHash(string, hash)
        call check(hash == "AE2DBF4043D53595A3B9099853EFC647", "hash string for character scalar", &
            "wrong hash for string: "//hash//" (should be AE2DBF4043D53595A3B9099853EFC647)")
        allocate(data(100))
        data(:) = [ &
            0.99755959009261719_e,     0.56682470761127335_e,     0.96591537549612494_e,     0.74792768547143218_e, &
            0.36739089737475572_e,     0.48063689875473148_e,     7.3754263633984518E-002_e, 5.3552292777272470E-003_e, &
            0.34708128851801545_e,     0.34224381607283505_e,     0.21795172633847260_e,     0.13316041001365930_e, &
            0.90052451442189241_e,     0.38676601045740355_e,     0.44548228938784806_e,     0.66193218089584283_e, &
            1.6108300430559330E-002_e, 0.65085483610391681_e,     0.64640882548382539_e,     0.32298729094055578_e, &
            0.85569240288533133_e,     0.40128691936381389_e,     0.20687432921875692_e,     0.96853946421659987_e, &
            0.59839953431813464_e,     0.67298073277626325_e,     0.45688231067296203_e,     0.33001512357337470_e, &
            0.10038292650217728_e,     0.75545330475972683_e,     0.60569326698023185_e,     0.71904791340845298_e, &
            0.89733460388652986_e,     0.65822912048110771_e,     0.15071683713010464_e,     0.61231490499670005_e, &
            0.97866023813214653_e,     0.99914226942680318_e,     0.25679798618316030_e,     0.55086540315522059_e, &
            0.65904751789363791_e,     0.55400513539681562_e,     0.97776009860945046_e,     0.90192330446045499_e, &
            0.65792468442021090_e,     0.72885850839956368_e,     0.40245526506327511_e,     0.92862766315403733_e, &
            0.14783519004389145_e,     0.67452929906378811_e,     0.76961430927589625_e,     0.33932255214578511_e, &
            0.11581885494899313_e,     0.61436918436151444_e,     0.82061713946583192_e,     0.94709465592406572_e, &
            0.73112865185355502_e,     0.49760390646303887_e,     0.37480174078720052_e,     0.42150585600453350_e, &
            0.55290303825578491_e,     0.99791927917810419_e,     0.99039474804151328_e,     0.74630965530730953_e, &
            0.95375906174797886_e,     9.3274690333437715E-002_e, 0.73402368711584287_e,     0.75176161592562596_e, &
            0.94684850567599865_e,     0.70617636325115329_e,     0.81380966648249919_e,     0.55859451959655482_e, &
            6.1705576875077828E-002_e, 0.48038078018536035_e,     0.59768977219632924_e,     0.13753191883633586_e, &
            0.58739520040668203_e,     0.51996826291639975_e,     0.88587834481295791_e,     0.30381017199048876_e, &
            0.66965730397225831_e,     0.66494009152976974_e,     0.50367689939794036_e,     0.26157512041192688_e, &
            7.6559501510817363E-002_e, 0.10124966279442871_e,     0.54926573972185189_e,     0.37558494749081928_e, &
            1.5149502579022811E-002_e, 0.79291544607443887_e,     0.62087754490155123_e,     0.77360357742855124_e, &
            0.95358076098624234_e,     0.11424437213389738_e,     0.31846264287556358_e,     0.59681984622356055_e, &
            4.8152902605581582E-002_e, 0.11420577802735099_e,     0.21596491917612204_e,     0.10057339249283215_e]
        
        call getHash(data, hash)
        call check(hash=="0067B3C1F9F5936EDD2BB914E3328AC2", "hash string for 1D real array", &
            "wrong hash for 1D real array: "//hash//" (should be 0067B3C1F9F5936EDD2BB914E3328AC2)")
        
        data(100) = 0.10057339249283214_e

        call getHash(data, hash)
        call check(hash=="02310DDF64CF8779B63DD9B4621F38C3", "hash string for modified 1D real array", &
            "wrong hash for 1D real array: "//hash//" (should be 02310DDF64CF8779B63DD9B4621F38C3)")
        
        call hash_seed(put=24)
        call getHash(data, hash)
        call check(hash=="6133DCF08175652264A094AB51A88986", "hash string with new seed", &
            "wrong hash for 1D real array: "//hash//" (should be 6133DCF08175652264A094AB51A88986)")
        
        allocate(data2D(50,2))
        data2D(:,1) = data(1:50)
        data2D(:,2) = data(51:100)
        call getHash(data2D, hash)
        call check(hash=="6133DCF08175652264A094AB51A88986", "hash string for 2D real array", &
            "wrong hash for 2D real array: "//hash//" (should be 6133DCF08175652264A094AB51A88986)")
    end subroutine
end module

program test
    use mod_test
    use test_character
    
    call testAll
    call testsFinished
end program
