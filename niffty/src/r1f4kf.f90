    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine r1f4kf(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3)
        implicit none
        include 'kinds.inc'
        integer :: ido, l1, in1, in2
        real(rwp) :: cc(in1, ido, l1, 4)
        real(rwp) :: ch(in2, ido, 4, l1)
        real(rwp) :: wa1(ido)
        real(rwp) :: wa2(ido), wa3(ido)
        
        integer :: i, k, ic, idp2
        real(rwp) :: hsqt2

        hsqt2 = sqrt(2.0_rwp)/2.
        do k = 1, l1
            ch(1, 1, 1, k) = (cc(1,1,k,2)+cc(1,1,k,4)) + &
              (cc(1,1,k,1)+cc(1,1,k,3))
            ch(1, ido, 4, k) = (cc(1,1,k,1)+cc(1,1,k,3)) - &
              (cc(1,1,k,2)+cc(1,1,k,4))
            ch(1, ido, 2, k) = cc(1, 1, k, 1) - cc(1, 1, k, 3)
            ch(1, 1, 3, k) = cc(1, 1, k, 4) - cc(1, 1, k, 2)
        end do
        if (ido-2) 120, 110, 100
100     idp2 = ido + 2
        do k = 1, l1
            do i = 3, ido, 2
                ic = idp2 - i
                ch(1, i-1, 1, k) = ((wa1(i-2)*cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k, &
                  2))+(wa3(i-2)*cc(1,i-1,k,4)+wa3(i-1)*cc(1,i,k, &
                  4))) + (cc(1,i-1,k,1)+(wa2(i-2)*cc(1,i-1,k,3)+wa2(i-1)*cc(1, &
                  i,k,3)))
                ch(1, ic-1, 4, k) = (cc(1,i-1,k,1)+(wa2(i-2)*cc(1,i-1,k, &
                  3)+wa2(i-1)*cc(1,i,k,3))) - ((wa1(i-2)*cc(1,i-1,k, &
                  2)+wa1(i-1)*cc(1,i,k,2))+(wa3(i-2)*cc(1,i-1,k, &
                  4)+wa3(i-1)*cc(1,i,k,4)))
                ch(1, i, 1, k) = ((wa1(i-2)*cc(1,i,k,2)-wa1(i-1)*cc(1,i-1,k, &
                  2))+(wa3(i-2)*cc(1,i,k,4)-wa3(i-1)*cc(1,i-1,k, &
                  4))) + (cc(1,i,k,1)+(wa2(i-2)*cc(1,i,k,3)-wa2(i-1)*cc(1,i-1, &
                  k,3)))
                ch(1, ic, 4, k) = ((wa1(i-2)*cc(1,i,k,2)-wa1(i-1)*cc(1,i-1,k, &
                  2))+(wa3(i-2)*cc(1,i,k,4)-wa3(i-1)*cc(1,i-1,k, &
                  4))) - (cc(1,i,k,1)+(wa2(i-2)*cc(1,i,k,3)-wa2(i-1)*cc(1,i-1, &
                  k,3)))
                ch(1, i-1, 3, k) = ((wa1(i-2)*cc(1,i,k,2)-wa1(i-1)*cc(1,i-1,k, &
                  2))-(wa3(i-2)*cc(1,i,k,4)-wa3(i-1)*cc(1,i-1,k, &
                  4))) + (cc(1,i-1,k,1)-(wa2(i-2)*cc(1,i-1,k,3)+wa2(i-1)*cc(1, &
                  i,k,3)))
                ch(1, ic-1, 2, k) = (cc(1,i-1,k,1)-(wa2(i-2)*cc(1,i-1,k, &
                  3)+wa2(i-1)*cc(1,i,k,3))) - ((wa1(i-2)*cc(1,i,k, &
                  2)-wa1(i-1)*cc(1,i-1,k,2))-(wa3(i-2)*cc(1,i,k, &
                  4)-wa3(i-1)*cc(1,i-1,k,4)))
                ch(1, i, 3, k) = ((wa3(i-2)*cc(1,i-1,k,4)+wa3(i-1)*cc(1,i,k, &
                  4))-(wa1(i-2)*cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k, &
                  2))) + (cc(1,i,k,1)-(wa2(i-2)*cc(1,i,k,3)-wa2(i-1)*cc(1,i-1, &
                  k,3)))
                ch(1, ic, 2, k) = ((wa3(i-2)*cc(1,i-1,k,4)+wa3(i-1)*cc(1,i,k, &
                  4))-(wa1(i-2)*cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k, &
                  2))) - (cc(1,i,k,1)-(wa2(i-2)*cc(1,i,k,3)-wa2(i-1)*cc(1,i-1, &
                  k,3)))
            end do
        end do
        if (mod(ido,2)==1) return
110     continue
        do k = 1, l1
            ch(1, ido, 1, k) = (hsqt2*(cc(1,ido,k,2)-cc(1,ido,k, &
              4))) + cc(1, ido, k, 1)
            ch(1, ido, 3, k) = cc(1, ido, k, 1) - (hsqt2*(cc(1,ido,k,2)-cc(1, &
              ido,k,4)))
            ch(1, 1, 2, k) = (-hsqt2*(cc(1,ido,k,2)+cc(1,ido,k, &
              4))) - cc(1, ido, k, 3)
            ch(1, 1, 4, k) = (-hsqt2*(cc(1,ido,k,2)+cc(1,ido,k, &
              4))) + cc(1, ido, k, 3)
        end do
120     return
    end subroutine
