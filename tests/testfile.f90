module test_file
    use core
    use mod_string, only: FString
    use mod_file, only: FFile
    use mod_exception, only: FException
    implicit none
    private
    
    public :: testAll
contains
    subroutine testAll()
        call testCreation
    end subroutine
    
    subroutine testCreation
        type(FFile) :: file
        type(FException) :: stat
        character(:), allocatable :: line
        
        file = FFile("../test_file.f90", "read")
        call file%open
        call file%getNextLine(line, stat)
        call assert(line == "module test_file", "wrong first line: "//line//".")
        call file%close
    end subroutine
end module

program test
    use test_file
    
    call testAll
end program