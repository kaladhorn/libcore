#******************************************************************************#
#                              libCore util                                    #
#______________________________________________________________________________#
#
#  Version 2.0
#
#  Written by Paul Fossati, <paul.fossati@gmail.com>
#  Copyright (c) 2009-2017 Paul Fossati
#------------------------------------------------------------------------------#
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
#     * Redistributions of source code must retain the above copyright notice, #
#       this list of conditions and the following disclaimer.                  #
#                                                                              #
#     * Redistributions in binary form must reproduce the above copyright      #
#       notice, this list of conditions and the following disclaimer in the    #
#       documentation and/or other materials provided with the distribution.   #
#                                                                              #
#     * The name of the author may not be used to endorse or promote products  #
#      derived from this software without specific prior written permission    #
#      from the author.                                                        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#******************************************************************************#
UTIL_SOURCES= util/mod_timer.f90 util/mod_console.f90 util/mod_dictionary.f90  \
    util/mod_numericalDictionary.f90 util/mod_stringDictionary.f90             \
    util/mod_test.f90

#******************************************************************************!
# Dependencies
#******************************************************************************!

util/mod_timer.o: util/mod_timer.f90 core.mod
mod_timer.mod: util/mod_timer.o

util/mod_console.o: util/mod_console.f90 mod_regex.mod mod_string.mod         \
    mod_formattedstring.mod
mod_console.mod: util/mod_console.o

util/mod_dictionary.o: util/mod_dictionary.f90 ext_character.mod               \
	mod_exception.mod mod_string.mod
mod_dictionary.mod:	util/mod_dictionary.o

util/mod_numericalDictionary.o: util/mod_numericalDictionary.f90               \
	mod_dictionary.mod mod_exception.mod mod_string.mod
mod_numericaldictionary.mod: util/mod_numericalDictionary.o

util/mod_stringDictionary.o: util/mod_stringDictionary.f90 mod_dictionary.mod  \
	mod_string.mod mod_exception.mod
mod_stringdictionary.mod: util/mod_stringDictionary.o


util/mod_test.o: util/mod_test.f90 ext_character.mod
mod_test.mod: util/mod_test.o
