    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mradf2(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) ch(in2, ido, 2, l1), cc(in1, ido, l1, 2), wa1(ido)

        m1d = (m-1)*im1 + 1
        m2s = 1 - im2
        do k = 1, l1
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch(m2, 1, 1, k) = cc(m1, 1, k, 1) + cc(m1, 1, k, 2)
                ch(m2, ido, 2, k) = cc(m1, 1, k, 1) - cc(m1, 1, k, 2)
            end do
        end do
        if (ido-2) 120, 110, 100
100     idp2 = ido + 2
        do k = 1, l1
            do i = 3, ido, 2
                ic = idp2 - i
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch(m2, i, 1, k) = cc(m1, i, k, 1) + &
                      (wa1(i-2)*cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k,2))
                    ch(m2, ic, 2, k) = (wa1(i-2)*cc(m1,i,k,2)-wa1(i-1)*cc(m1,i &
                      -1,k,2)) - cc(m1, i, k, 1)
                    ch(m2, i-1, 1, k) = cc(m1, i-1, k, 1) + &
                      (wa1(i-2)*cc(m1,i-1,k,2)+wa1(i-1)*cc(m1,i,k,2))
                    ch(m2, ic-1, 2, k) = cc(m1, i-1, k, 1) - &
                      (wa1(i-2)*cc(m1,i-1,k,2)+wa1(i-1)*cc(m1,i,k,2))
                end do
            end do
        end do
        if (mod(ido,2)==1) return
110     do k = 1, l1
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch(m2, 1, 2, k) = -cc(m1, ido, k, 2)
                ch(m2, ido, 1, k) = cc(m1, ido, k, 1)
            end do
        end do
120     return
    end subroutine
