    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine c1fm1b(n, inc, c, ch, wa, fnf, fac)
        implicit none
        include 'kinds.inc'
        complex(cwp) :: c(*)
        real(rwp) :: ch(*), wa(*), fac(*)
        integer :: n, inc
        real(rwp) :: fnf

        integer :: ido, inc2, ip, iw, k1, l1, l2, lid, na, nbr, nf
        
        ! FFTPACK 5.1 auxiliary routine

        inc2 = inc + inc
        nf = fnf
        na = 0
        l1 = 1
        iw = 1
        do k1 = 1, nf
            ip = fac(k1)
            l2 = ip*l1
            ido = n/l2
            lid = l1*ido
            nbr = 1 + na + 2*min(ip-2, 4)
            go to (100, 110, 120, 130, 140, 150, 160, 170, 180, 190), nbr
100         call c1f2kb(ido, l1, na, c, inc2, ch, 2, wa(iw))
            go to 200
110         call c1f2kb(ido, l1, na, ch, 2, c, inc2, wa(iw))
            go to 200
120         call c1f3kb(ido, l1, na, c, inc2, ch, 2, wa(iw))
            go to 200
130         call c1f3kb(ido, l1, na, ch, 2, c, inc2, wa(iw))
            go to 200
140         call c1f4kb(ido, l1, na, c, inc2, ch, 2, wa(iw))
            go to 200
150         call c1f4kb(ido, l1, na, ch, 2, c, inc2, wa(iw))
            go to 200
160         call c1f5kb(ido, l1, na, c, inc2, ch, 2, wa(iw))
            go to 200
170         call c1f5kb(ido, l1, na, ch, 2, c, inc2, wa(iw))
            go to 200
180         call c1fgkb(ido, ip, l1, lid, na, c, c, inc2, ch, ch, 2, wa(iw))
            go to 200
190         call c1fgkb(ido, ip, l1, lid, na, ch, ch, 2, c, c, inc2, wa(iw))
200         l1 = l2
            iw = iw + (ip-1)*(ido+ido)
            if (ip<=5) na = 1 - na
        end do
    end subroutine
