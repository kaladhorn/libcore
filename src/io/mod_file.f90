!******************************************************************************!
!                            mod_file module
!------------------------------------------------------------------------------!
!> FFile derived type and type-bound procedures, for file I/O.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_File
    use iso_c_binding, only: c_int, c_char, c_int64_t, c_ptr, c_size_t, C_NULL_CHAR

    use core
    use com_io
    use ext_character

    use mod_regex,            only: FRegex
    use mod_string,           only: FString
    use mod_directory,        only: FDirectory
    use mod_exception,        only: FException, FExceptionDescription, MESSAGE_LEVEL_ERROR
    use mod_fileReader,       only: FFileReader
    use mod_textFileReader,   only: FTextFileReader
    use mod_gzipFileReader,   only: FGzipFileReader
    use mod_bzip2FileReader,  only: FBZip2FileReader
    use mod_stringDictionary, only: FStringDictionary

    implicit none
    private
    save

    integer, parameter, public :: FILE_FORMAT_UNDEF = -1
    integer, parameter, public :: FILE_FORMAT_NONE  = 0
    integer, parameter, public :: FILE_FORMAT_GZIP  = 1
    integer, parameter, public :: FILE_FORMAT_BZIP2 = 2
    integer, parameter, public :: FILE_FORMAT_ZIP   = 3
    integer, parameter, public :: FILE_FORMAT_7ZIP  = 4

    character(*), parameter, public :: FILE_MODE_UNDEF  = UNDEF
    character(*), parameter, public :: FILE_MODE_READ   = "r"
    character(*), parameter, public :: FILE_MODE_WRITE  = "w"
    character(*), parameter, public :: FILE_MODE_APPEND = "a"

!******************************************************************************!
!> File description
!******************************************************************************!
    type, public :: FFile
        private
        class(FFileReader), allocatable :: reader   !< Reader object
        character(:), allocatable :: path           !< Path to the file
        character(:), allocatable :: name           !< Name of the file
        character(:), allocatable :: extension      !< File extension
        integer :: currentLine = 0                  !< Last line read
        character(:), allocatable :: mode           !< I/O mode
        integer :: format = FILE_FORMAT_UNDEF       !< Compression format
        character(:), allocatable :: lastLine       !< Last line read from the file
        integer :: lastLineLength = 0               !< Length of the last string read from the file
    contains
        procedure, public :: fullName
        procedure, public :: string => fullName
        procedure, public :: baseName

        procedure, public :: open
        procedure, public :: close
        procedure, public :: getParentDirectory    !< Get the file parent directory
        procedure, public :: createParentDirectory !< Create the file parent directory
        procedure, public :: lineNumber

        ! Metadata
        procedure, public :: exists   => file_exists
        procedure, public :: canRead  => file_canRead
        procedure, public :: canWrite => file_canWrite
        procedure, public :: isEmpty  => file_isEmpty
        procedure, private :: getModificationTime_int
        procedure, private :: getModificationTime_character
        procedure, public :: getModificationTime_timestamp
        generic, public :: getModificationTime => getModificationTime_int, &
            getModificationTime_character, getModificationTime_timestamp

        ! Read from file
        procedure, private :: getNextLine_character
        procedure, private :: getNextLine_string
        generic, public :: getNextLine => getNextLine_character, getNextLine_string
        procedure, public :: getLines

        ! Write to file
        procedure, private :: writeChar
        procedure, private :: writeString
        generic,   public  :: writeLine => writeChar, writeString
        procedure, public  :: writeLines

        procedure, public :: compareContent

        procedure, public :: initialised

        final :: file_finalise
    end type

!******************************************************************************!
!> File object constructor
!******************************************************************************!
    interface FFile
        module procedure file_init, files_initWithRegex
    end interface

!******************************************************************************!
!> I/O error description.
!******************************************************************************!
    type, public, extends(FExceptionDescription) :: FFileError
        private
        character(:), allocatable :: filename
        integer :: line
        character(:), allocatable :: content
    contains
        procedure, public :: string => fileError_string
    end type

!******************************************************************************!
!> I/O error constructor
!******************************************************************************!
    interface FFileError
        module procedure :: fileError_init
    end interface

    ! Modes for open files
    character(*), parameter :: MODE_READ   = "read"
    character(*), parameter :: MODE_WRITE  = "write"
    character(*), parameter :: MODE_APPEND = "append"

    ! Magic numbers for various compression formats
    character(*), parameter, private :: zip7  = "37 7A BC AF 27 1C"
    character(*), parameter, private :: bzip2 = "42 5A 68"
    character(*), parameter, private :: gzip  = "1F 8B 08"
    character(*), parameter, private :: zip   = "50 4B 03 04"

!******************************************************************************!
! Interfaces to C functions
!******************************************************************************!
    interface
        function getModificationTime_wrapper(filename) bind(C, name="getModificationTime_wrapper")
            use iso_c_binding
            character(1,c_char), dimension(*), intent(in) :: filename
            integer(C_INT64_T) :: getModificationTime_wrapper
        end function

        function canRead(filename) bind(C, name="canRead")
            use iso_c_binding
            character(1,c_char), dimension(*), intent(in) :: filename
            integer(c_int) :: canRead
        end function

        function isEmpty(filename) bind(C, name="isEmpty")
            use iso_c_binding
            character(1,c_char), dimension(*), intent(in) :: filename
            integer(c_int) :: isEmpty
        end function

        function canWrite(filename) bind(C, name="canWrite")
            use iso_c_binding
            character(1,c_char), dimension(*), intent(in) :: filename
            integer(c_int) :: canWrite
        end function

        function localtime(timer) bind(C, name="localtime")
            use iso_c_binding
            type(c_ptr) :: localtime
            integer(C_INT64_T), intent(in) :: timer
        end function

        function strftime(s, size, template, brokentime) bind(C, name="strftime")
            use iso_c_binding
            integer(c_size_t) :: strftime
            character(1,c_char), dimension(*), intent(inout) :: s
            integer(c_size_t), value, intent(in) :: size
            character(1,c_char), dimension(*), intent(in) :: template
            type(c_ptr), value, intent(in) :: brokentime
        end function
    end interface
contains
    include 'io/file_error.inc'

    pure function initialised(this)
        class(FFile), intent(in) :: this
        logical :: initialised
!------
        initialised = .true.
        if (.not.allocated(this%name)) initialised = .false.
    end function
!******************************************************************************!
!> File object destructor
!******************************************************************************!
    subroutine file_finalise(this)
        type(FFile), intent(inout) :: this
!------
        if (allocated(this%reader)) deallocate(this%reader)
    end subroutine
!******************************************************************************!
!> Get a list of files whose name matches a regex.
!******************************************************************************!
    function files_initWithRegex(regex) result(this)
        type(FRegex), intent(in) :: regex
        type(FFile), dimension(:), allocatable :: this
!------
        type(FRegex) :: pathRegex, name
        type(FDirectory), dimension(:), allocatable :: dir
        type(FString), dimension(:), allocatable :: files
        integer :: nFiles, i, j, n, k
        character(:), allocatable :: path_, namestring
        type(FStringDictionary) :: dict
        logical, dimension(:), allocatable :: processed
!------
        if (regex%string() == "" .or. regex%string() == ".") then
            dir = [FDirectory(".")]
            name = FRegex("^.*$")
        else
            ! Split the file name into path, base name, and extension
            pathRegex = &
                FRegex("^(?<path>.*/)?(?:$|(?<basename>.+?)(?:(?<extension>(\\)?\.[^.]*$)|$))", &
                options="i")
            path_ = regex%string()

            call pathRegex%capture(path_, dict)

            ! Get the directories
            if (dict%value("path") /= "") then
                dir = FDirectory(FRegex(dict%value("path")))
            else
                dir = [FDirectory(".")]
            end if

            ! Get the files
            if (dict%value("basename") /= "") then
                if (dict%value("extension") /= "") then
                    namestring = dict%value("basename")//dict%value("extension")
                else
                    namestring = dict%value("basename")
                end if

                ! Make sure file name matching starts at the begining of the names
                if (namestring(1:1) /= "^") &
                    namestring = "^"//namestring

                ! Make sure file name matching ends at the end of the names
                if (namestring(len(namestring):len(namestring)) /= "$") &
                    namestring = namestring // "$"

                name = FRegex(namestring)
            else
                name = FRegex("^.*$")
            end if
        end if

        ! Count the number of files
        nFiles = 0
        do i=1, size(dir)
            call dir(i)%getFileList(files, name)
            nFiles = nFiles + size(files)
        end do

        ! Get the file objects
        allocate(this(nFiles))
        nFiles = 0
        do i=1, size(dir)
            call dir(i)%getFileList(files, name)

            if (allocated(processed)) deallocate(processed)
            allocate(processed(size(files)))
            processed = .false.

            do k=1, size(files)
                n = 0
                do j=1, size(files)
                    if (processed(j)) cycle
                    if (n == 0) then
                        n = j
                    else if (files(j) < files(n)) then
                        n = j
                    end if
                end do

                this(k) = FFile(dir(i)%string()//"/"//files(n), MODE_READ)
                processed(n) = .true.
            end do
        end do
    end function
!******************************************************************************!
!> File object constructor
!******************************************************************************!
    function file_init(path, mode, stat) result(this)
        character(*), intent(in) :: path
        character(*), intent(in) :: mode
        type(FException), intent(out), optional :: stat
        type(FFile) :: this
!------
        type(FRegex) :: pathRegex, extRegex
        logical :: exists, compressed
        type(FString), dimension(:), allocatable :: capture, extCapture
        integer :: unit, iostat
        integer(int8), dimension(6) :: magicBytes
        character(17) :: magicString
        type(FException) :: istat
        character(200) :: iomsg
        character(:), allocatable :: compression
!------
        body: block
            ! Split the file name into path, base name, and extension
            pathRegex = FRegex("^(?<path>.*/)?(?:$|(?<basename>.+?)" // &
                "(?:(?<extension>\.[^.]*$)|$))", options="i")
            call pathRegex%capture(path, capture)
            this%path = capture(1)%string()
            this%name = capture(2)%string()
            this%extension = capture(3)%string()
            if (this%extension == ".bz2" .or. &
                this%extension == ".gz"  .or. &
                this%extension == ".zip" .or. &
                this%extension == ".7z"  .or. &
                this%extension == ".xz") then

                compression = this%extension
                extRegex = FRegex("^(.*)(\.[^\.]*)$")
                if (extRegex%doesMatch(this%name)) then
                    call extRegex%capture(this%name, extCapture)
                    this%name = extCapture(1)%string()
                    this%extension = extCapture(2)%string() // this%extension
                end if
            else
                compression = ""
            end if

            ! Set the mode
            this%mode = mode

            compressed = .false.

            if (mode == MODE_WRITE .or. mode == "w") then

                ! Try to guess the compression format from the file extension
                select case(compression)
                    case (".bz2")
                        allocate(FBZip2FileReader :: this%reader)
                        compressed = .true.
                    case (".gz")
                        allocate(FGzipFileReader :: this%reader)
                        compressed = .true.
                    case (".zip")
                        call istat%raise("The zip compression format is not supported yet")
                        compressed = .true.
                    case (".7z")
                        call istat%raise("The 7-zip compression format is not supported yet")
                        compressed = .true.
                    case (".xz")
                        call istat%raise("The xz compression format is not supported yet")
                        compressed = .true.
                    case default
                        allocate(FTextFileReader :: this%reader)
                end select
            else if (mode == MODE_READ .or. mode == "r") then

                ! Check whether the file exists
                inquire(file=this%fullName(), exist=exists)

                ! Stop here if the file does not exist
                if (.not.exists) then
                    call istat%raise(FExceptionDescription("File does not exist.", &
                        " when trying to open file " // this%fullName() // ":"))
                    exit body
                end if

                ! The file exists, try to guess its format from its magic numbers
                ! Get the magic bytes
                open(newunit=unit, &
                        file=this%fullName(), &
                      action="read", &
                      status="old", &
                      access="stream", &
                        form="unformatted", &
                      iostat=iostat, &
                       iomsg=iomsg)

                ! Check whether the file can be opened
                if (iostat /= 0) then
                    call istat%raise(FExceptionDescription( &
                        trim(iomsg), &
                        "while opening file '" // this%fullName() // "':"))
                    close(unit, iostat=iostat)
                    exit body
                end if

                ! Try to read the magic bytes from the file
                read(unit, iostat=iostat) magicBytes
                close(unit)

                if (iostat == 0) then
                    ! Compare the magic string to some known compression formats
                    write(magicString,'(z2.2,5(1x,z2.2))') magicBytes
                    if (magicString(:len(bzip2)) == bzip2) then
                        ! The file looks like a bzip2 archive
                        allocate(FBzip2FileReader :: this%reader)
                    else if (magicString(:len(gzip)) == gzip) then
                        ! The file looks like a gzip archive
                        allocate(FGzipFileReader :: this%reader)
                    else if (magicString(:len(zip)) == zip) then
                        ! The file looks like a zip archive
                        call istat%raise("The zip compression format is not supported yet")
                    else if (magicString(:len(zip7)) == zip7) then
                        ! The file looks like a 7-zip archive
                        call istat%raise("The 7-zip compression format is not supported yet")
                    else
                        ! Assume that the file is not compressed
                        allocate(FTextFileReader :: this%reader)
                    end if
                else if (iostat == IOSTAT_END) then
                    ! Assume that the file is not compressed
                    allocate(FTextFileReader :: this%reader)
                else
                    write(*,*) "Serious issue with file " // path
                end if
            else
                call istat%raise("wrong file mode: " // mode // ".")
                exit body
            end if

            ! Initialise the current line counter
            this%currentLine = 0

        end block body

        ! Report any issue
        if (present(stat)) call stat%transfer(istat)
    end function
!******************************************************************************!
!> Test whether a file is empty.
!> Returns `.true.` if the file exists and contains something, `.false.`
!> otherwise.
!******************************************************************************!
    function file_isEmpty(this) result(test)
        class(FFile), intent(in) :: this
        logical :: test
!------
        integer(c_int) :: out
!------
        out = isEmpty(this%fullName()//C_NULL_CHAR)
        test = (out==0)
    end function
!******************************************************************************!
!> Test whether a file exists.
!> Returns `.true.` if the file exists, `.false.` otherwise.
!******************************************************************************!
    function file_exists(this) result(test)
        class(FFile), intent(in) :: this
        logical :: test
!------
        integer :: unit, iostat
!------
        open(newunit=unit, file=this%fullName(), iostat=iostat, status="old", access="stream")
        close(unit)
        test = (iostat == 0)
    end function
!******************************************************************************!
!> Test whether a file can be read.
!******************************************************************************!
    function file_canRead(this) result(test)
        class(FFile), intent(in) :: this
        logical :: test
!------
        character(:,c_char), allocatable :: file
        integer(c_int) :: res
!------
        test = .false.
        file = this%fullName() // C_NULL_CHAR
        if (file /= C_NULL_CHAR) then
            res = canRead(file)
            test = (res /= 0)
        end if
    end function
!******************************************************************************!
!> Test whether a file can be written.
!******************************************************************************!
    function file_canWrite(this) result(test)
        class(FFile), intent(in) :: this
        logical :: test
!------
        character(:,c_char), allocatable :: file
        integer(c_int) :: res
!------
        test = .false.
        file = this%fullName() // C_NULL_CHAR
        if (file /= C_NULL_CHAR) then
            res = canWrite(file)
            test = (res /= 0)
        end if
    end function
!******************************************************************************!
!> Get the parent directory of a file object.
!******************************************************************************!
    subroutine getParentDirectory(this, parent, stat)
        class(FFile), intent(in) :: this
        type(FDirectory), intent(out) :: parent
        class(FException), intent(out), optional :: stat
!------
        parent = FDirectory(this%path)
    end subroutine
!******************************************************************************!
!> Make sure the parent directory of a file object exists.
!******************************************************************************!
    subroutine createParentDirectory(this, stat)
        class(FFile), intent(in) :: this
        class(FException), intent(out), optional :: stat
!------
        call execute_command_line("test -d "//this%path//" || mkdir "//this%path)
    end subroutine
!******************************************************************************!
!> Get the current line number
!******************************************************************************!
    pure function lineNumber(this)
        class(FFile), intent(in) :: this
        integer :: lineNumber
!------
        lineNumber = this%currentLine
    end function
!******************************************************************************!
!> Return the full name of the file.
!******************************************************************************!
    pure function fullName(this)
        class(FFile), intent(in) :: this
        character(:), allocatable :: fullName
!------
        if (allocated(this%path) .and. allocated(this%name) .and. allocated(this%extension)) then
            fullName = this%path // this%name // this%extension
        else if (allocated(this%path) .and. allocated(this%name) .and. .not.allocated(this%extension)) then
            fullName = this%path // this%name
        else if (.not.allocated(this%path) .and. allocated(this%name) .and. allocated(this%extension)) then
            fullName = this%name // this%extension
        else if (.not.allocated(this%path) .and. allocated(this%name) .and. .not.allocated(this%extension)) then
            fullName = this%name
        end if
    end function
!******************************************************************************!
!> Return the base name of the file.
!******************************************************************************!
    pure function baseName(this)
        class(FFile), intent(in) :: this
        character(:), allocatable :: baseName
!------
        if (allocated(this%name)) then
            baseName = this%name
        else
            baseName = UNDEF
        end if
    end function
!******************************************************************************!
!> Close the file.
!******************************************************************************!
    elemental impure subroutine close(this)
        class(FFile), intent(inout) :: this
!------
        ! Close the file
        if (allocated(this%reader)) then

            ! Delegate to the file reader if there is one
            call this%reader%closeFile
        end if
    end subroutine
!******************************************************************************!
!> Open the file. Does nothing in most cases (the file will be automatically
!> opened when trying to read or write). It allows to change the mode of the
!> file.
!******************************************************************************!
    subroutine open(this, mode, stat)
        class(FFile), intent(inout) :: this
        character(*), intent(in), optional :: mode
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
!------
        if (present(mode)) then
            ! Make sure the file is closed
            call this%close

            ! Set the new mode
            select case(mode)
                case ("r", MODE_READ)
                    this%mode = FILE_MODE_READ
                case ("w", MODE_WRITE)
                    this%mode = FILE_MODE_WRITE
                case ("a", MODE_APPEND)
                    this%mode = FILE_MODE_APPEND
                case default
                    call istat%raise(FFileError(this, "unknown file mode: " // mode // "."))
            end select
        end if

        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Read the next line from the file. The program will stop if the file is not
!> open for reading and no actual argument is provided for stat.
!******************************************************************************!
    subroutine getNextLine_character(this, line, stat)
        class(FFile), intent(inout) :: this
        character(*), intent(out) :: line
        class(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_, istat
!------
        if (this%initialised()) then
            ! Read a line
            if (allocated(this%reader)) then
                if (.not.this%reader%canRead()) call this%reader%openFile(this%fullName(), this%mode, stat_)
                if (stat_ == 0) then
                    ! Delegate to the file reader if there is one
                    call this%reader%getNextLine(line, stat_)
                    this%currentLine = this%currentLine + 1
                    if (stat_ == 0) then
                        if (allocated(this%lastLine)) then
                            if (len(this%lastLine) < len_trim(line)) deallocate(this%lastLine)
                        end if
                        if (.not.allocated(this%lastLine)) then
                            this%lastLine = trim(line)
                        else
                            this%lastLine(:) = line
                        end if
                        this%lastLineLength = len_trim(this%lastLine)
                    else
                        if (allocated(this%lastLine)) deallocate(this%lastLine)
                        call istat%raise(FFileError(this, stat_%string()))
                        call stat_%discard
                    end if
                else
                    call istat%raise(FExceptionDescription(stat_%string(), &
                        "when trying to open file " // this%fullName() // ":"))
                    call stat_%discard
                end if
            else

                ! Otherwise, raise an issue
                call istat%raise("No reader defined for file "//this%fullName()//".")
            end if
        else
            ! Raise an issue if the file is not properly set up
            call istat%raise("Trying to read from uninitialised file")
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get the next line in a file. If a stat variable is provided and an error
!> occurs, it will contain information about the error.
!******************************************************************************!
    subroutine getNextLine_string(this, line, stat)
        class(FFile), intent(inout) :: this
        type(FString), intent(out) :: line
        class(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        if (this%initialised()) then
            ! Read a line
            if (allocated(this%reader)) then
                if (.not.this%reader%canRead()) call this%reader%openFile(this%fullName(), this%mode, stat_)
                if (stat_ == 0) then
                    ! Delegate to the file reader if there is one
                    call this%reader%getNextLine(line, stat_)
                    if (stat_ == 0) then
                        this%currentLine = this%currentLine + 1
                        this%lastLine = line%string()
                        this%lastLineLength = len(this%lastLine)
                    end if
                end if
            else

                ! Otherwise, raise an issue
                call stat_%raise("No reader defined for file "//this%fullName()//".")
            end if
        else
            ! Raise an issue if the file is not properly set up
            call stat_%raise("Trying to read from uninitialised file")
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Read all the lines from the file
!>
!> Read all the lines from the file. The program will stop if the file is not
!> open and no actual argument is provided for stat.
!******************************************************************************!
    subroutine getLines(this, lines, stat)
        class(FFile), intent(inout):: this
        type(FString), dimension(:), allocatable, intent(out) :: lines
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        if (this%initialised()) then
            ! Get the lines array
            if (allocated(this%reader)) then
                if (.not.this%reader%canRead()) call this%reader%openFile(this%fullName(), this%mode, stat_)
                if (stat_ == 0) then
                    ! Delegate to the file reader if there is one
                    call this%reader%getLines(lines, stat_)
                    if (stat_ == 0) this%currentLine = this%currentLine + size(lines)
                end if
            else

                ! Otherwise, raise an issue
                call stat_%raise("No reader defined for file "//this%fullName()//".")
            end if
        else
            ! Raise an issue if the file is not properly set up
            call stat_%raise("Trying to read from uninitialised file")
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Write a line to the file
!>
!> Read the next line from the file. The program will stop if the file is not
!> open for writing and no actual argument is provided for stat.
!******************************************************************************!
    subroutine writeChar(this, line, stat)
        class(FFile), intent(inout):: this
        character(*), intent(in) :: line
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        if (this%initialised()) then
            ! Write the line
            if (allocated(this%reader)) then
                if (.not.this%reader%canWrite()) call this%reader%openFile(this%fullName(), this%mode, stat_)
                if (stat_ == 0) then
                    ! Delegate to the file reader if there is one
                    call this%reader%writeLine(line, stat_)
                    if (stat_ == 0) this%currentLine = this%currentLine + 1
                end if
            else

                ! Otherwise, raise an issue
                call stat_%raise("No reader defined for file "//this%fullName()//".")
            end if
        else
            ! Raise an issue if the file is not properly set up
            call stat_%raise("Trying to write to uninitialised file")
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine writeString(this, line, stat)
        class(FFile), intent(inout):: this
        type(FString), intent(in) :: line
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------

        call this%writeLine(line%string(), stat_)

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine writeLines(this, lines, stat)
        class(FFile), intent(inout):: this
        type(FString), dimension(:), intent(in) :: lines
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        if (this%initialised()) then
            ! Write the line
            if (allocated(this%reader)) then
                if (.not.this%reader%canWrite()) call this%reader%openFile(this%fullName(), this%mode, stat_)
                if (stat_ == 0) then
                    ! Delegate to the file reader if there is one
                    call this%reader%writeLines(lines, stat_)
                    if (stat_ == 0) this%currentLine = this%currentLine + size(lines)
                end if
            else

                ! Otherwise, raise an issue
                if (this%fullName() == "") then
                    call stat_%raise("No reader defined for unknown file.")
                else
                    call stat_%raise("No reader defined for file "//this%fullName()//".")
                end if
            end if
        else
            ! Raise an issue if the file is not properly set up
            call stat_%raise("Trying to write to uninitialised file")
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Compare the content of two files
!******************************************************************************!
    subroutine compareContent(this, to, identical, stat)
        class(FFile), intent(inout) :: this
        type(FFile), intent(inout) :: to
        logical, intent(out) :: identical
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: err1, err2
        type(FString) :: line1, line2
!------
        identical = .true.

        ! Test every line
        do while(err1==0 .and. err2==0)
            ! Read one line from each file
            call this%getNextLine(line1, err1)
            call to%getNextLine(line2, err2)

            ! The files are different if one line is different
            if (line1 /= line2) then
                identical = .false.
                exit
            end if
        end do
        call err1%discard
        call err2%discard
    end subroutine
!******************************************************************************!
!> Get a file's modification time as an integer array. This is largely inspired
!> from code posted by Clive Page on comp.lang.fortran, and reproduced below:
!>
!>        *+unix2c Converts Unix system time to date/time integer array.
!>              subroutine unix2c(utime, idate)
!>              implicit none
!>              integer utime, idate(6)
!>        *utime  input  Unix system time, seconds since 1970.0
!>        *idate  output Array: 1=year, 2=month, 3=date, 4=hour, 5=minute, 6=secs
!>        *-Author  Clive Page, Leicester University, UK.   1995-MAY-2
!>              integer mjday, nsecs
!>              real day
!>        *Note the MJD algorithm only works from years 1901 to 2099.
!>              mjday    = int(utime/86400 + 40587)
!>              idate(1) = 1858 + int( (mjday + 321.51) / 365.25)
!>              day      = aint( mod(mjday + 262.25, 365.25) ) + 0.5
!>              idate(2) = 1 + int(mod(day / 30.6 + 2.0, 12.0) )
!>              idate(3) = 1 + int(mod(day,30.6))
!>              nsecs    = mod(utime, 86400)
!>              idate(6) = mod(nsecs, 60)
!>              nsecs    = nsecs / 60
!>              idate(5) = mod(nsecs, 60)
!>              idate(4) = nsecs / 60
!>              end
!>
!******************************************************************************!
    subroutine getModificationTime_int(this, idate)
        class(FFile), intent(in) :: this
        integer, dimension(6), intent(out) :: idate
!------
        integer(long) :: utime
        integer :: mjday, nsecs
        real :: day
!------
        utime = getModificationTime_wrapper(this%fullName())

        mjday    = int(utime/86400 + 40587)
        idate(1) = 1858 + int( (mjday + 321.51) / 365.25)
        day      = aint( mod(mjday + 262.25, 365.25) ) + 0.5
        idate(2) = 1 + int(mod(day / 30.6 + 2.0, 12.0) )
        idate(3) = 1 + int(mod(day,30.6))
        nsecs    = int(mod(utime, 86400_long))
        idate(6) = mod(nsecs, 60)
        nsecs    = nsecs / 60
        idate(5) = mod(nsecs, 60)
        idate(4) = nsecs / 60
    end subroutine
!******************************************************************************!
!> Get a file's modification time as a timestamp.
!******************************************************************************!
    subroutine getModificationTime_timestamp(this, time)
        class(FFile), intent(in) :: this
        integer(long) :: time
!------
        time = getModificationTime_wrapper(this%fullName())
    end subroutine
!******************************************************************************!
!> Get a file's modification time as a character string.
!******************************************************************************!
    subroutine getModificationTime_character(this, time)
        class(FFile), intent(in) :: this
        character(:), allocatable, intent(out) :: time
!------
        integer(long) :: utime
        character(25,c_char) :: buffer
        integer(c_int64_t) :: s
        type(c_ptr) :: ptr
!------
        utime = getModificationTime_wrapper(this%fullName())
        ptr = localtime(utime)
        buffer = ""
        s = strftime(buffer, 25_c_size_t, c_char_"%Y-%m-%d %H:%M:%S"//C_NULL_CHAR, ptr)
        if (buffer(len_trim(buffer):len_trim(buffer)) == C_NULL_CHAR) &
            buffer(len_trim(buffer):len_trim(buffer)) = " "
        time = trim(buffer)
    end subroutine
end module
