module test_test
    use core
    use ext_character
    use mod_test
    
    implicit none
    private
    
    public :: testAll
contains
    subroutine testAll
        call test_group
        call test_pass
        call test_xfail
        call test_xpass
        call test_fail
    end subroutine
    
    subroutine test_group
        call startTestGroup("test infrastructure", "Unit tests for the procedures in mod_test.")
    end subroutine
    
    subroutine test_pass
        call check(.true., "successful test")
    end subroutine
    
    subroutine test_xfail
        call xfail(.false., "expected failure")
    end subroutine
    
    subroutine test_xpass
        call xfail(.true., "unexpected success")
    end subroutine
    
    subroutine test_fail
        call check(.false., "failed test")
    end subroutine
end module

program test
    use test_test
    use mod_test
    
    call testAll
    call testsFinished
end program
