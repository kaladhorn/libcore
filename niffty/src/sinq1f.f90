    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine sinq1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        integer n, inc, lenx, lensav, lenwrk, ier
        real (rwp) x(inc, *), wsave(lensav), work(lenwrk)

        ier = 0

        if (lenx<inc*(n-1)+1) then
            ier = 1
            call xerfft('SINQ1F', 6)
            go to 100
        else if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('SINQ1F', 8)
            go to 100
        else if (lenwrk<n) then
            ier = 3
            call xerfft('SINQ1F', 10)
            go to 100
        end if

        if (n==1) return
        ns2 = n/2
        do k = 1, ns2
            kc = n - k
            xhold = x(1, k)
            x(1, k) = x(1, kc+1)
            x(1, kc+1) = xhold
        end do
        call cosq1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('SINQ1F', -5)
            go to 100
        end if
        do k = 2, n, 2
            x(1, k) = -x(1, k)
        end do
100     return
    end subroutine
