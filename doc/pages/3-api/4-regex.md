title: Using regexes

# API details

## Regex objects

`[[FRegex(type):setCompilationOption(bound)]]`


## Pattern matching

`[[FRegex(type):doesMatch(bound)]]`
`[[FRegex(type):match(bound)]]`
`[[FRegex(type):match_all(bound)]]`


## Substitutions

`[[FRegex(type):replace(bound)]]`
`[[FRegex(type):replace_all(bound)]]`


# Example
