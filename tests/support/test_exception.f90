module test_error
    use core

    use mod_test

    use mod_exception, only: FException, MESSAGE_LEVEL_WARNING, FExceptionDescription, FWarning, FError

    implicit none
    private

    public :: testAll
contains
    subroutine testAll
        call testSimple
        call testReport
        call testComparison
        call testsFinished
    end subroutine

    subroutine testSimple

        call startTestGroup("initialisation")

        block
            type(FException) :: ex

            call check(ex%string()=="", "string representation of uninitialised description", "")
            call ex%raise(FExceptionDescription())
            call check(ex%string()=="", "string representation of uninitialised message", "")
            call ex%discard
        end block
        block
            type(FException) :: ex
            call ex%raise("this is a message")
            call check(ex%string()=="this is a message", &
                "string representation with defined message", &
                "wrong string: """ // ex%string() // """ instead of ""this is a message"".")
            call ex%discard
        end block
        block
            type(FException) :: ex
            call ex%raise()
            call check(ex%string()=="Unknown error.", "string representation of uninitialised message", &
                "wrong string: """ // ex%string() // """ instead of ""Unknown error.""")
            call ex%discard
        end block
        block
            type(FException) :: ex
            call ex%raise(FWarning("this is a test warning message"))
            call check(ex%string()=="this is a test warning message", &
                "string representation with warning constructor", "")
            call ex%discard
        end block
        block
            type(FException) :: ex
            call ex%raise(FError("this is a test warning message"))
            call check(ex%string()=="this is a test warning message", &
                "string representation with error constructor", "")
            call ex%discard
        end block
    end subroutine

    subroutine testReport

        call startTestGroup("reports")
        write(*,*) "Ignore the warning and error messages in this test group. " // &
            "As long as the program does not abort, everything should be fine."

        ! Test uninitialised error
        block
            type(FException) :: err
            call err%report
        end block

        ! Test warning without explicit message
        block
            type(FException) :: err
            call err%raise(FExceptionDescription(level = MESSAGE_LEVEL_WARNING))
            call err%report
            call err%discard
        end block

        ! Test error with explicit message
        block
            type(FException) :: err
            call err%raise("this is a dummy warning message.", level = MESSAGE_LEVEL_WARNING)
            call err%report
            call err%discard
        end block

        ! Test warning wit context message
        block
            type(FException) :: err
            call err%raise(FExceptionDescription("this is a dummy warning message.", &
                "this is a context description", &
                level = MESSAGE_LEVEL_WARNING))
            call err%report
            call err%discard
        end block
    end subroutine

    subroutine testComparison
        type(FException) :: err1, err2, err

        call startTestGroup("exception comparison")
        ! Compare two uninitialised error objects
        call check(err1 == err2, "uninitialised exception == comparison", &
            "wrong result: uninitialised exceptions were found to be different")
        call check(.not.(err1 /= err2), "uninitialised exception /= comparison", &
            "wrong result: uninitialised exceptions were found to be different")

        ! Compare one uninitialised error object with a character variable
        call assert(err1 /= "")
        call assert(.not.(err1 == ""))

        call err1%raise("Error message 1")
        call assert(err1 /= err2)
        call assert(.not.(err1 == err2))

        call err2%raise("Error message 2")

        ! Error comparison
        call assert(err1 /= err2)
        call assert(err1 /= err)
        call assert(.not.(err1 == err2))

        ! Character comparison
        call assert(err1 == "Error message 1")
        call assert(.not.(err1 /= "Error message 1"))
        call assert(err1 /= "Error message 2")
        call assert(.not.(err1 == "Error message 2"))

        call err1%discard
        call err2%discard
    end subroutine
end module

program test
    use test_error
    use core
    implicit none

    testing = .true.
    call testAll
end program