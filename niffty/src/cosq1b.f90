    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cosq1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        integer n, inc, lenx, lensav, lenwrk, ier
        real (rwp) x(inc, *), wsave(lensav), work(lenwrk)

        ier = 0

        if (lenx<inc*(n-1)+1) then
            ier = 1
            call xerfft('COSQ1B', 6)
            go to 120
        else if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('COSQ1B', 8)
            go to 120
        else if (lenwrk<n) then
            ier = 3
            call xerfft('COSQ1B', 10)
            go to 120
        end if

        if (n-2) 120, 100, 110
100     ssqrt2 = 1.0_rwp/sqrt(2.0_rwp)
        x1 = x(1, 1) + x(1, 2)
        x(1, 2) = ssqrt2*(x(1,1)-x(1,2))
        x(1, 1) = x1
        return
110     call cosqb1(n, inc, x, wsave, work, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('COSQ1B', -5)
        end if

120     return
    end subroutine
