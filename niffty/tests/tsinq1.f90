    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    program tsinq1
        implicit none
        include 'kinds.inc'

        integer i, n, lensav, ier, lenwrk
        parameter (n=1000)
        ! PARAMETER(LENSAV= 2*N + INT(LOG(REAL(n))/LOG(2)) + 4)
        parameter (lensav=2013)
        parameter (lenwrk=n)
        real (rwp) r(n), rcopy(n)
        real (rwp) wsave(lensav), work(lenwrk), diff

        ! --- INITIALIZE FFT

        write (6, *) 'PROGRAM TSINQ1 AND RELATED MESSAGES:'
        call sinq1i(n, wsave, lensav, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE SINQ1I'
            stop
        end if

        ! --- GENERATE TEST VECTOR FOR FORWARD-BACKWARD TEST

        call random_seed()
        call random_number(r)
        rcopy = r

        ! --- PERFORM FORWARD TRANSFORM

        call sinq1f(n, 1, r, n, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE SINQ1F !'
            stop
        end if

        ! --- PERFORM BACKWARD TRANSFORM

        call sinq1b(n, 1, r, n, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE SINQ1B !'
            stop
        end if

        ! --- PRINT TEST RESULTS

        ier = 0
        diff = 0.
        do i = 1, n
            diff = max(diff, abs(r(i)-rcopy(i)))
        end do
        write (6, *) 'SINQ1 FORWARD-BACKWARD MAX ERROR =', diff

        ! --- GENERATE TEST VECTOR FOR BACKWARD-FORWARD TEST

        call random_seed()
        call random_number(r)
        rcopy = r

        ! --- PERFORM BACKWARD TRANSFORM

        call sinq1b(n, 1, r, n, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE SINQ1B !'
            stop
        end if

        ! --- PERFORM FORWARD TRANSFORM

        call sinq1f(n, 1, r, n, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE SINQ1F !'
            stop
        end if


        ! --- PRINT TEST RESULTS
        ier = 0
        diff = 0.
        do i = 1, n
            diff = max(diff, abs(r(i)-rcopy(i)))
        end do
        write (6, *) 'SINQ1 BACKWARD-FORWARD MAX ERROR =', diff

        write (6, '(A,/)') ' END PROGRAM TSINQ1 AND RELATED MESSAGES'
        stop
    end program
