    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cmf2kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) cc(2, in1, l1, ido, 2), ch(2, in2, l1, 2, ido), &
          wa(ido, 1, 2)

        m1d = (lot-1)*im1 + 1
        m2s = 1 - im2
        if (ido>1) go to 110
        sn = 1.0_rwp/real(2*l1)
        if (na==1) go to 100
        do k = 1, l1
            do m1 = 1, m1d, im1
                chold1 = sn*(cc(1,m1,k,1,1)+cc(1,m1,k,1,2))
                cc(1, m1, k, 1, 2) = sn*(cc(1,m1,k,1,1)-cc(1,m1,k,1,2))
                cc(1, m1, k, 1, 1) = chold1
                chold2 = sn*(cc(2,m1,k,1,1)+cc(2,m1,k,1,2))
                cc(2, m1, k, 1, 2) = sn*(cc(2,m1,k,1,1)-cc(2,m1,k,1,2))
                cc(2, m1, k, 1, 1) = chold2
            end do
        end do
        return
100     do k = 1, l1
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch(1, m2, k, 1, 1) = sn*(cc(1,m1,k,1,1)+cc(1,m1,k,1,2))
                ch(1, m2, k, 2, 1) = sn*(cc(1,m1,k,1,1)-cc(1,m1,k,1,2))
                ch(2, m2, k, 1, 1) = sn*(cc(2,m1,k,1,1)+cc(2,m1,k,1,2))
                ch(2, m2, k, 2, 1) = sn*(cc(2,m1,k,1,1)-cc(2,m1,k,1,2))
            end do
        end do
        return
110     do k = 1, l1
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch(1, m2, k, 1, 1) = cc(1, m1, k, 1, 1) + cc(1, m1, k, 1, 2)
                ch(1, m2, k, 2, 1) = cc(1, m1, k, 1, 1) - cc(1, m1, k, 1, 2)
                ch(2, m2, k, 1, 1) = cc(2, m1, k, 1, 1) + cc(2, m1, k, 1, 2)
                ch(2, m2, k, 2, 1) = cc(2, m1, k, 1, 1) - cc(2, m1, k, 1, 2)
            end do
        end do
        do i = 2, ido
            do k = 1, l1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch(1, m2, k, 1, i) = cc(1, m1, k, i, 1) + &
                      cc(1, m1, k, i, 2)
                    tr2 = cc(1, m1, k, i, 1) - cc(1, m1, k, i, 2)
                    ch(2, m2, k, 1, i) = cc(2, m1, k, i, 1) + &
                      cc(2, m1, k, i, 2)
                    ti2 = cc(2, m1, k, i, 1) - cc(2, m1, k, i, 2)
                    ch(2, m2, k, 2, i) = wa(i, 1, 1)*ti2 - wa(i, 1, 2)*tr2
                    ch(1, m2, k, 2, i) = wa(i, 1, 1)*tr2 + wa(i, 1, 2)*ti2
                end do
            end do
        end do
    end subroutine
