module test_character
    use ieee_exceptions
    use iso_c_binding

    use core
    use mod_test
    use ext_character
    use mod_timer, only: FTimer

    implicit none
    private

    public :: testAll
contains
    subroutine testAll()
        call testIntegerOperations
        call testSingleOperations
        call testDoubleOperations
        call testQuadOperations
        call testLogicalOperations
        call testPerformance
        call testAccuracy
    end subroutine

    subroutine testAccuracy
        real(e) :: x, y
        character(:), allocatable :: core, grisu, intrinsic
        integer :: i, n, imax, d
        character(25,c_char), target :: dst
        character(1000) :: buf
        type(FTimer) :: timer

        imax = 100000

        call timer%start
        d = 0
        do i=1, imax
            call random_number(x)
            x = x*huge(0.0_dp)
            core = x
            read(core,*) y
            if (y /= x) d = d + 1
!            write(*,*) x
        end do
        call timer%stop
        write(*,*) "Core: ", timer%wallTime(), d

        call timer%reset
        call timer%start
        d = 0
        do i=1, imax
            call random_number(x)
            x = x*huge(0.0_dp)
            n = grisu3(x, c_loc(dst))
            grisu = dst(:n)
            read(grisu,*) y
            if (y /= x) d = d + 1
        end do
        call timer%stop
        write(*,*) "Grisu: ", timer%wallTime(), d

        call timer%reset
        call timer%start
        d = 0
        do i=1, imax
            call random_number(x)
            x = x*huge(0.0_dp)
            write(buf,*) x
            intrinsic = trim(buf)
            read(intrinsic,*) y
            if (y /= x) d = d + 1
        end do
        call timer%stop
        write(*,*) "Intrinsic: ", timer%wallTime(), d
    end subroutine

    subroutine testPerformance
        real(e) :: x
        character(:), allocatable :: core, grisu, intrinsic
        integer :: i, n, imax
        character(25,c_char), target :: dst
        character(1000) :: buf
        type(FTimer) :: timer

        imax = 100000

        call timer%start
        do i=1, imax
            call random_number(x)
            x = x*huge(0.0_dp)
            core = x
        end do
        call timer%stop
        write(*,*) "Core: ", timer%wallTime()

        call timer%reset
        call timer%start
        do i=1, imax
            call random_number(x)
            x = x*huge(0.0_dp)
            n = grisu3(x, c_loc(dst))
            grisu = dst(:n)
        end do
        call timer%stop
        write(*,*) "Grisu: ", timer%wallTime()

        call timer%reset
        call timer%start
        do i=1, imax
            call random_number(x)
            x = x*huge(0.0_dp)
            write(buf,*) x
            intrinsic = trim(buf)
        end do
        call timer%stop
        write(*,*) "Intrinsic: ", timer%wallTime()

    end subroutine

    subroutine testIntegerOperations
        integer :: i
        character(:), allocatable :: var, var2

        call startTestGroup("Integer operations")
        i = 42

        ! Test the conversion of a positive integer
        var = i
        call check(var == "42", "integer to character assignment", "wrong result: " // var // "instead of 42")

        !
        var2 = "The answer is " // i
        call check(var2 == "The answer is 42", "character // integer concatenation", &
            "wrong result: " // var2)

        !
        var2 = i // ", the answer is"
        call check(var2 == "42, the answer is", "integer // character concatenation", &
            "wrong result: " // var2)

        ! Test the conversion of a negative integer
        var = -i
        call check(var == "-42", "negative integer to character assignment", &
            "wrong result: " // var)
        var = 0
        call check(var == "0", "0 to character assignment", "wrong integer to character assignment: " // var)

        ! Test integer arrays
        var = [1, 2, 3]
        call check(var == "[1, 2, 3]", "small integer array to character assignment", &
            "wrong result: " // var)
        var = [1, 2, 3, 4, 5]
        call check(var == "(1, 2, 3, 4, 5)", "long integer array to character assignment", &
            "wrong result: " // var)
        var = "The answer is " // [1, 2, 3]
        call check(var == "The answer is [1, 2, 3]", "character // integer array concatenation", &
            "wrong result: " // var)
        var = [1, 2, 3] // ", the answer is"
        call check(var == "[1, 2, 3], the answer is", "integer array // character concatenation", &
            "wrong result: " // var)
    end subroutine

    subroutine testSingleOperations
        use ieee_arithmetic

        real(sp) :: x, inf, minf
        character(:), allocatable :: var, var2
        logical :: flag, mode
        integer :: n

        call startTestGroup("Low-precision real operations")

        inf = ieee_value(inf, IEEE_POSITIVE_INF)
        minf = ieee_value(minf, IEEE_NEGATIVE_INF)

!------ Continue with the tests
        ! Test a standard real value
        x = PI
        var = x
        call check(var=="3.141593", "single-precision to character assignment", &
            "wrong result: """ // var // """ instead of ""3.141593"".")

        ! Test concatenation
        var2 = "The answer is " // x
        call check(var2 == "The answer is 3.141593", "character // single-precision concatenation", &
            "wrong character // integer concatenation: " // var2 // ".")
        var2 = x // ", the answer is"
        call check(var2 == "3.141593, the answer is", "single-precision // character concatenation", &
            "wrong character // integer concatenation: " // var2 // ".")

        ! Test a huge real value
        x = 3.402823E+38_sp
        var = x
!        call assert(var == "3.402823E+38", "wrong real to character assignment 2: " // var)

        ! Test Inf and -Inf
        var = inf
        call check(var=="Infinity", "infinity", &
            "wrong result: """ // var // """ instead of ""Infinity"".")
        var = minf
        call check(var=="-Infinity", "negative infinity", &
            "wrong result: """ // var // """ instead of ""-Infinity"".")

        ! Test a tiny real value
        x = 2.220446E-16_sp
        var = x
        call check(var=="2.220446E-16", "small single-precision to character assignment", &
            "wrong result: """ // var // """ instead of ""2.220446E-16"".")

        ! Test single-precision arrays
        var = [1.1_sp, 1.0_sp, 2.3_sp]
        call check(var == "[1.1, 1, 2.3]", "small single-precision array assignment", &
            "wrong character representation for the small integer array: " // var)
        var = "The answer is " // [1.1_sp, 1.0_sp, 2.3_sp]
        call check(var == "The answer is [1.1, 1, 2.3]", "single-precision array // character concatenation", &
            "wrong character // integer concatenation: " // var)
        var = [1.1_sp, 1.0_sp, 2.3_sp] // ", the answer is"
        call check(var == "[1.1, 1, 2.3], the answer is", "character // single-precision array concatenation", &
            "wrong character // integer concatenation: " // var)
        var = [1.0_sp, 2.0_sp, 3.0_sp, 4.0_sp, 5.0_sp]
        call check(var == "(1, 2, 3, 4, 5)", "long single-precision array assignment", &
            "wrong character representation for the large single-precision array: " // var)
    end subroutine

    subroutine testDoubleOperations
        use ieee_arithmetic
        use iso_c_binding

        real(dp) :: x
        integer :: i
        character(:), allocatable :: var, var2, c_var
        character(25,c_char), target :: dst
        type(c_ptr) :: ptr
        real(dp) :: inf, minf, nan
        type(ieee_status_type) :: status

        call startTestGroup("Standard-precision real operations")

        ! Get special values
        if (ieee_support_datatype(inf)) then

            ! Easy solution: use IEEE function when available
            inf  = ieee_value(inf,  IEEE_POSITIVE_INF)
            minf = ieee_value(minf, IEEE_NEGATIVE_INF)
            nan  = ieee_value(nan,  IEEE_QUIET_NAN)
        else

            ! Otherwise, hack around

            ! Save the environment
            call ieee_get_status(status)

            ! Set all the flags to false
            call ieee_set_flag(IEEE_USUAL, .false.)

            ! Set the halting modes to false to avoid interruptions
            call ieee_set_halting_mode(IEEE_ALL, .false.)

            ! Get the special values
            inf = huge(0.0_dp)
            inf = inf * 10
            minf = -huge(0.0_dp)
            minf = minf * 10
            nan = -1.0_dp
            nan = log(nan)

            ! Reset the IEEE modes
            call ieee_set_status(status)
        end if

        ! Test a standard real value
        x = PI
        var = x
        call check(var=="3.141592653589793", "double-precision to character assignment", &
            "wrong result: """ // var // """ instead of ""3.141592653589793"".")

        ! Test concatenation
        var2 = "The answer is " // x
        call check(var2 == "The answer is 3.141592653589793", "character // double-precision concatenation", &
            "wrong character // integer concatenation: " // var2 // ".")
        var2 = x // ", the answer is"
        call check(var2 == "3.141592653589793, the answer is", "double-precision // character concatenation", &
            "wrong character // integer concatenation: " // var2 // ".")

        ! Test a huge real value
        x = 1.797693E+308_dp
        var = x
        call check(var=="1797693e302", "large double-precision to character assignment", &
            "wrong result: """ // var // """ instead of ""1797693e302"".")
        ptr = c_loc(dst)

        ! Test a tiny real value
        x = 2.220446E-16_dp
        var = x
        call check(var=="2.220446e-16", "small double-precision to character assignment", &
            "wrong result: """ // var // """ instead of ""2.220446e-16"".")

        ! Test an integer value
        x =  42.0_dp
        var = x
        call check(var=="42", "integer double-precision to character assignment", &
            "wrong result: """ // var // """ instead of ""42"".")

        ! Test infinity
        x = huge(0.0_dp)
        var = inf
        call check(var=="Infinity", "infinity", &
            "wrong result: """ // var // """ instead of ""Infinity"".")

        ! Test minus infinity
        x = huge(0.0_dp)
        var = minf
        call check(var=="-Infinity", "negative infinity", &
            "wrong result: """ // var // """ instead of ""-Infinity"".")

        ! Test array assignment
        var = [1.0_e, 2.0_e, 3.0_e, 42.0_e] // " test"
        call check(var == "[1, 2, 3, 42] test", "double-precision // character array concatenation", &
            "wrong character representation for the small integer array: " // var)
        var = "test: " // [1.1_e, 2.5_e, 3.0_e, 42.0_e]
        call check(var == "test: [1.1, 2.5, 3, 42]", "character // double-precision array concatenation", &
            "wrong character representation for the small integer array: " // var)
        var = [1.0_dp, 2.0_dp, 3.0_dp, 4.0_dp, 5.0_dp]
        call check(var == "(1, 2, 3, 4, 5)", "long double-precision array assignment", &
            "wrong character representation for the large double-precision array: " // var)

        ! Test problematic values
        x = 7.2484129085898137e-10_dp
        var = x
        call check(var=="7.248412908589814e-10", "special double-precision to character assignment", &
            "wrong result: """ // var // """ instead of ""7.248412908589814e-10"".")
    end subroutine

    subroutine testQuadOperations
        use ieee_arithmetic

        real(qp) :: x, inf, minf, nan
        character(:), allocatable :: var, var2
        logical :: flag, mode
        character(:), allocatable :: string, model
        integer :: na, nb, i, n
        type(ieee_status_type) :: status

        call startTestGroup("High-precision real operations")

        ! Get special values
        if (ieee_support_datatype(inf)) then

            ! Easy solution: use IEEE function when available
            inf  = ieee_value(inf,  IEEE_POSITIVE_INF)
            minf = ieee_value(minf, IEEE_NEGATIVE_INF)
            nan  = ieee_value(nan,  IEEE_QUIET_NAN)
        else

            ! Otherwise, hack around

            ! Save the environment
            call ieee_get_status(status)

            ! Set all the flags to false
            call ieee_set_flag(IEEE_USUAL, .false.)

            ! Set the halting modes to false to avoid interruptions
            call ieee_set_halting_mode(IEEE_ALL, .false.)

            ! Get the special values
            inf = huge(0.0_qp)
            inf = inf * 10
            minf = -huge(0.0_qp)
            minf = minf * 10
            nan = -1.0_qp
            nan = log(nan)

            ! Reset the IEEE modes
            call ieee_set_status(status)
        end if

        ! Test a standard real value
        x = 3.1415926535897932384626433832795029_qp
        var = x

        ! Get a correct string representation of pi
        string = "3.141592653589793238462643383279503"

!------ Continue with the tests
        call check(var == string, "high-precision real to character assignment", &
            "wrong result: " // var // " instead of "//string)

        ! Test concatenation
        var2 = "The answer is " // x
        call check(var2 == "The answer is "//string, "character // high-precision real concatenation", &
            "wrong result: " // var2)
        var2 = x // ", the answer is"
        call check(var2 == string//", the answer is", "high-precision real // character concatenation", &
            "wrong result: " // var2)

        ! Test an integer value
        x =  42.0_qp
        var = x
        call check(var == "42", "high-precision real to character assignment (integer value)", &
            "wrong real: " // var // " instead of 42")

        ! Test an NaN value
        var = nan
        call check(var == "NaN", "high-precision real to character assignment (NaN value)", &
            "wrong result: " // var // " instead of NaN")

        ! Test a huge real value
        x = 3.402823E+38_qp
        var = x
        string = "3.402823000000000000000000000000000E+38"
        call check(var == string, "real to character assignment (large)", "wrong string: " // var)

        ! Test Inf and -Inf
        var = inf
        call check(var == "Infinity", "real to character assignment (Infinity)", "wrong string: " // var)
        var = minf
        call check(var == "-Infinity", "real to character assignment (-Infinity)", "wrong string: " // var)

        ! Test a tiny real value
        x = 2.220446E-16_qp
        var = x
        string = "2.220446000000000000000000000000000E-16"
        call check(var == string, "real to character assignment (small)", "wrong string: " // var)


        ! Test quadruple-precision arrays
        var = [1.1_qp, 1.0_qp, 2.3_qp]
        call check(var == "[1.1, 1, 2.3]", "small quad-precision array representation", &
            "wrong result: " // var)
        var = "The answer is " // [1.1_qp, 1.0_qp, 2.3_qp]
        call check(var == "The answer is [1.1, 1, 2.3]", "quad-precision array // character concatenation", &
            "wrong result: " // var)
        var = [1.1_qp, 1.0_qp, 2.3_qp] // ", the answer is"
        call check(var == "[1.1, 1, 2.3], the answer is", "character // quad-precision array concatenation", &
            "wrong result: " // var)
        var = [1.0_qp, 2.0_qp, 3.0_qp, 4.0_qp, 5.0_qp]
        call check(var == "(1, 2, 3, 4, 5)", "large quad-precision array representation", &
            "wrong result: " // var)
    end subroutine


    subroutine testLogicalOperations
        logical :: x
        character(:), allocatable :: var

        call startTestGroup("Logical operations")
        x = .false.
        var = x
        call assert(var == "no", "wrong logical to character assignment: " // var)

        x = .true.
        var = x
        call assert(var == "yes", "wrong logical to character assignment: " // var)

        var = x // " is true"
        call assert(var == "yes is true", "wrong logical to character assignment: " // var)

        var = "true is " // x
        call assert(var == "true is yes", "wrong logical to character assignment: " // var)

    end subroutine
end module

program test
    use mod_test
    use test_character

    call testAll
    call testsFinished
end program
