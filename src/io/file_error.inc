!******************************************************************************!
!> Set up a file error object based on a file and a user-provided message.
!******************************************************************************!
    function fileError_init(file, message, level) result(this)
        type(FFile), intent(in) :: file
        character(*), intent(in) :: message
        integer, intent(in), optional :: level
        type(FFileError) :: this
!------
        character(:), allocatable :: context
        integer :: ilevel
!------
        this%filename = file%fullName()
        this%line = file%lineNumber()

        ! Get the context (file name and possibly line number)
        if (this%line == 0) then
            context = bold(this%filename // ":")
        else
            context = bold(this%filename // ":" // this%line // ":")
        end if

        ! Set the content if available
        if (allocated(file%lastLine)) then
            context = context // new_line("") // new_line("") // " " // &
                file%lastLine(:file%lastLineLength) // new_line("")
        end if

        ! Set the message level
        if (present(level)) then
            ilevel = level
        else
            ilevel = MESSAGE_LEVEL_ERROR
        end if

        ! Call the super-class' constructor
        this%FExceptionDescription = &
            FExceptionDescription(message=message, &
                                  context=context, &
                                    level=ilevel)
    end function
!******************************************************************************!
!> Get a short description of a file error object.
!******************************************************************************!
    pure function fileError_string(this) result(string)
        class(FFileError), intent(in) :: this
        character(:), allocatable :: string
!------
        if (this%line == 0) then
            string = this%filename // ": " // this%FExceptionDescription%string()
        else
            string = this%filename // ":" // this%line // ": " // this%FExceptionDescription%string()
        end if
    end function
