title: Using files


This section describes how to read and write files using the `[[FFile(type)]]` and `[[FDirectory(type)]]` derived types.

# Directories
A variable of the `[[FDirectory(type)]]` type is the representation of a directory. 
Basic operations can be performed on the file system using its type-bound procedures, as shown in the example below.
For a complete API description, see the `[[FDirectory(type)]]` page.

```fortran

    program simple_directories
        use mod_regex,     only: FRegex
        use mod_string,    only: FString
        use mod_directory, only: FDirectory
    
        implicit none
    
        type(FDirectory) :: dir
        type(FString), dimension(:), allocatable :: files, subdirs
        type(FRegex) :: re
    
        dir = FDirectory("./testdir") ! Initialise the directory object with a relative path
    
        if (dir%exists()) then ! Check whether the directory exists in the file system
        
            call dir%getFileList(files) ! If yes, get the names of the files it contains
            call dir%getDirectoryList(subdirs) ! Get the names of its sub-directories
        
            re = FRegex("aaa[0-9]+\.dir")
            call dir%getDirectoryList(subdirs, re) ! Get the names of its sub-directories whose names
                                                   ! match the regular expression /aaa[0-9]+\.dir/
        else
            call dir%create() ! Create the directory if it does not exist
        end if
    
    
        if (dir%isEmpty()) call dir%remove ! Remove the directory if it is empty
    end program
```

# Reading files

A file is open for reading if the `[[FFile(type)]]` constructor is called with the mode `read`.
An error is returned if the file does not exist or is otherwise inaccessible.

## Compressed files

Compressed files are detected automatically during the initialisation of the `[[FFile(type)]]` object.
Supported file types are currently:

  - standard text files;
  - gzip-compressed files;
  - bzip2-compressed files.

This format is deduced from the file's magic bytes, so this is independent from the possible file extension.
This is done internally by assigning different delegates in the `[[FFile(type)]]` object, so nothing particular 
needs to be done by the developer to support compressed files. For the details, see the API documentation for 
the text file reader `[[FTextFileReader(type)]]`, the gzip file reader `[[FGzipFileReader(type)]]` and the 
bzip2 file reader `[[FBZip2FileReader(type)]]`.

##Example

```fortran

	! Useful definitions
	type(FFile) :: file
	type(FError) :: iostat
	character(:), allocatable :: filename, line
    
	! Get the path to the file to open
	filename = "path/to/file.txt" ! The file can be compressed with gzip or bzip2, 
                                  ! the rest of the code is the same in any case
    
	! Open the file for reading
	file = FFile(filename, "read")
    
	! Read the first line
	call file%readLine(line, iostat)
    
	! Proceed until no line can be read from the file anymore 
	do while(iostat == 0)
		! Print the line on standard output
		write(*,*) line
		! Try to read the next line
		call file%readLine(line, iostat)
	end do
    
	! Close the file
	call file%close
```

# Writing files

A file is open for writing if the `[[FFile(type)]]` constructor is called with the mode `write`.
The file is created if it does not exist already, along with any missing directory in its path.
If the file exists, its content is deleted.
