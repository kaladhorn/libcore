# Signature of the current package.
m4_define([AT_PACKAGE_NAME],      [niffty])
m4_define([AT_PACKAGE_TARNAME],   [niffty])
m4_define([AT_PACKAGE_VERSION],   [1.0])
m4_define([AT_PACKAGE_STRING],    [niffty 1.0])
m4_define([AT_PACKAGE_BUGREPORT], [paul.fossati@gmail.com])
m4_define([AT_PACKAGE_URL],       [])
