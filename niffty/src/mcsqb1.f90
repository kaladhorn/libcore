    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mcsqb1(lot, jump, n, inc, x, wsave, work, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) x, wsave, work
        dimension x(inc, *), wsave(*), work(lot, *)

        ier = 0
        lj = (lot-1)*jump + 1
        ns2 = (n+1)/2
        np2 = n + 2
        do i = 3, n, 2
            do m = 1, lj, jump
                xim1 = x(m, i-1) + x(m, i)
                x(m, i) = .5_rwp*(x(m,i-1)-x(m,i))
                x(m, i-1) = .5_rwp*xim1
            end do
        end do
        do m = 1, lj, jump
            x(m, 1) = .5_rwp*x(m, 1)
        end do
        modn = mod(n, 2)
        if (modn/=0) go to 100
        do m = 1, lj, jump
            x(m, n) = .5_rwp*x(m, n)
        end do
100     continue
        lenx = (lot-1)*jump + inc*(n-1) + 1
        lnsv = n + int(log(real(n, rwp))/log(2.0_rwp)) + 4
        lnwk = lot*n

        call rfftmb(lot, jump, n, inc, x, lenx, wsave(n+1), lnsv, work, lnwk, &
          ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('MCSQB1', -5)
            go to 120
        end if

        do k = 2, ns2
            kc = np2 - k
            m1 = 0
            do m = 1, lj, jump
                m1 = m1 + 1
                work(m1, k) = wsave(k-1)*x(m, kc) + wsave(kc-1)*x(m, k)
                work(m1, kc) = wsave(k-1)*x(m, k) - wsave(kc-1)*x(m, kc)
            end do
        end do
        if (modn/=0) go to 110
        do m = 1, lj, jump
            x(m, ns2+1) = wsave(ns2)*(x(m,ns2+1)+x(m,ns2+1))
        end do
110     do k = 2, ns2
            kc = np2 - k
            m1 = 0
            do m = 1, lj, jump
                m1 = m1 + 1
                x(m, k) = work(m1, k) + work(m1, kc)
                x(m, kc) = work(m1, k) - work(m1, kc)
            end do
        end do
        do m = 1, lj, jump
            x(m, 1) = x(m, 1) + x(m, 1)
        end do
120     continue
    end subroutine
