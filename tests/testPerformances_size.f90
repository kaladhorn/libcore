program testProblemSize
    use core
    use ext_character
    
    use mod_expression, only: FExpression
    use mod_timer, only: FTimer
    
    implicit none
    
    real(e) :: x, y, z
    integer :: i, Niter, b
    type(FExpression) :: f
    type(FTimer) :: timer
    real(e), dimension(:), allocatable :: res
    
    
    x = 0.175_e
    y = 0.110_e
    z = 0.900_e
    
    f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
        "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")
    
    call f%setVariable("x", x)
    call f%setVariable("y", y)
    call f%setVariable("z", z)
    do Niter = 1000, 5000000, 1000
        
        if (allocated(res)) deallocate(res)
        allocate(res(Niter))
        res = 0
        
        call timer%start
        call f%eval("z", 0.900_e, Niter, 0.0_e, res)
        call timer%stop
        write(*,*) Niter, timer%walltime(), timer%cpuTime(), timer%cpuTime()/Niter
        call timer%reset
    end do
end program
