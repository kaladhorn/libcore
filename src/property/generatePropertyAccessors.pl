#!/usr/bin/env perl
use strict;

my @type = ("C", "I", "L", "R", "S");
my %fortranType = ("C", "character(*)", "I", "integer", "L", "logical", "R", "real(e)", "S", "type(FString)");
my %friendlyType = ("C", "Character", "I", "Integer", "L", "Logical", "R", "Real", "S", "String");
my @kind = ("Scalar", "Matrix", "List", "Vector");
my %fortranKind = ("Scalar", "", "Matrix", ", dimension(:,:)", "List", ", dimension(:), allocatable", "Vector", ", dimension(:)");

my $setString = "";
my $getString = "";
my $firstRun = 0;

open BINDING, ">property/generated_accessors_bindings.inc";
open SET, ">property/generated_setter_generic_binding.inc";
open GET, ">property/generated_getter_generic_binding.inc";
open FUNCTION, ">property/generated_functions.inc";

print SET "\ngeneric :: set => &\n";
print GET "\ngeneric :: get => &\n";
my $getName;
my $setName;
my $queryName;

for my $kind (@kind) {

    my $firstKind = 0;

    for my $type (@type) {
        if ($kind eq "Scalar") {
            $getName = "getValue_scalar_$friendlyType{$type}";
            $setName = "setValue_scalar_$friendlyType{$type}";
            $queryName = "is$friendlyType{$type}";
        } else {
            $getName = "getValue_${type}_$friendlyType{$type}$kind";
            $setName = "setValue_${type}_$friendlyType{$type}$kind";
            $queryName = "is$friendlyType{$type}$kind";
        }

        # Print the query function
        print BINDING "procedure, public :: $queryName\n";
        if ($kind eq "Scalar") {
            print FUNCTION <<END;
    function $queryName(this, name) result(test)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: name
        logical :: test
!------
        type(FProperty), pointer :: property
!------
        test = .false.
        call getPointerToProperty(this, name, property)
        if (.not.associated(property)) return
        if (.not.allocated(property\%value_)) return
        test = is${type}Definition(property\%value_)
    end function
END

        } else {
            print FUNCTION <<END;
    function $queryName(this, name) result(test)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: name
        logical :: test
!------
        type(FProperty), pointer :: property
        integer :: i
!------
        test = .false.
        if (.not. this%is$kind(name)) return

        call getPointerToProperty(this, name, property)
        do i=1, property\%size
            if (.not.is${type}Definition(property\%children(i)\%value_)) return
        end do

        test = .true.
    end function
END
        }

        # Print the set function
        print BINDING "procedure :: $setName\n";
        print BINDING "procedure :: $getName\n";
        if (!($kind eq "List")) {
            if ($firstRun == 0) {
                print SET " $setName";
                print GET " $getName";
                $firstRun = 1;
            } else {
                print SET ", &\n $setName";
                print GET ", &\n $getName";
            }
        }
        if ($firstKind == 0) {
            $firstKind = 1;
        } else {
        }


#         if ($type eq "S" && $kind eq "Scalar") {
#             print FUNCTION <<END;
#     subroutine $setName(this, name, value)
#         class(FProperty), target, intent(inout) :: this
#         character(*), intent(in) :: name
#         $fortranType{$type} $fortranKind{$kind}, intent(in) :: value
# !------
#         call setCharacter(this, name, value%string())
#     end subroutine
# END
#         } elsif ($kind eq "Scalar") {
#             print FUNCTION <<END;
#     subroutine $setName(this, name, value)
#         class(FProperty), target, intent(inout) :: this
#         character(*), intent(in) :: name
#         $fortranType{$type} $fortranKind{$kind}, intent(in) :: value
# !------
#         integer :: iostat
#         character(1000) :: buffer
# !------
#         write(buffer,*,iostat=iostat) value
#         if (iostat == 0) then
#             call setCharacter(this, name, trim(adjustl(buffer)))
#         end if
#     end subroutine
# END
#             } elsif ($kind eq "List" && $type eq "C") {
            if ($kind eq "List" && $type eq "C") {
                print FUNCTION <<END;
    subroutine $setName(this, name, value)
        class(FProperty), target, intent(inout) :: this
        character(*), intent(in) :: name
        $fortranType{$type}, dimension(:), intent(in) :: value
!------
        integer :: i
!------

        do i=1, size(value)
            call this\%setValue_scalar_$friendlyType{$type}(name//"-"//i, value(i))
        end do
    end subroutine
END
            } elsif ($kind eq "List") {
                print FUNCTION <<END;
    subroutine $setName(this, name, value)
        class(FProperty), target, intent(inout) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(in) :: value
!------
        integer :: i
!------

        do i=1, size(value)
            call this\%setValue_scalar_$friendlyType{$type}(name//"-"//i, value(i))
        end do
    end subroutine
END
            } elsif ($kind eq "Matrix") {
                print FUNCTION <<END;
    subroutine $setName(this, name, value)
        class(FProperty), target, intent(inout) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(in) :: value
!------
        integer :: i, j
        type(FProperty), pointer :: property
!------

        if (size(value, 1)>4 .or. size(value, 2)>4) return

        do i=1, size(value, 1)
            do j=1, size(value, 2)
                call this\%setValue_scalar_$friendlyType{$type}(name//"-"//vectortag(i)//"-"//vectortag(j), value(i,j))
            end do
            call getPointerToProperty(this, name//"-"//vectortag(i), property)
            property\%rank_ = RANK_VECTOR
        end do
        call getPointerToProperty(this, name, property)
        property\%rank_ = RANK_VECTOR
    end subroutine
END
            } elsif ($kind eq "Vector") {
                print FUNCTION <<END;
    subroutine $setName(this, name, value)
        class(FProperty), target, intent(inout) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(in) :: value
!------
        integer :: i
        type(FProperty), pointer :: property
!------

        if (size(value)>4) return

        do i=1, size(value)
            call this\%setValue_scalar_$friendlyType{$type}(name//"-"//vectortag(i), value(i))
        end do
        call getPointerToProperty(this, name, property)
        property\%rank_ = RANK_VECTOR
    end subroutine
END
        }

        # Print the get function
#         if ($type eq "S" && $kind eq "Scalar") {
#             print FUNCTION <<END;
#     subroutine $getName(this, name, value, stat)
#         class(FProperty), target, intent(in) :: this
#         character(*), intent(in) :: name
#         $fortranType{$type} $fortranKind{$kind}, intent(out) :: value
#         type(FException), intent(out), optional :: stat
# !------
#
#         ! Get the string value of the property
#         value = this%value(name)
#     end subroutine
# END
#         } elsif ($kind eq "Scalar" && $type ne "C") {
#             print FUNCTION <<END;
#     subroutine $getName(this, name, value, stat)
#         class(FProperty), target, intent(in) :: this
#         character(*), intent(in) :: name
#         $fortranType{$type} $fortranKind{$kind}, intent(out) :: value
#         type(FException), intent(out), optional :: stat
# !------
#         type(FProperty), pointer :: property
#         type(FException) :: istat
#         integer :: iostat
#         character(:), allocatable :: v
# !------
#
#         body: block
#             ! Get the string value of the property
#             v = this%value(name)
#
#             ! Check that the property is set
#             if (v == UNDEF) then
#                 call istat%raise("The property "//name//" is not set", MESSAGE_LEVEL_WARNING)
#                 exit body
#             end if
#
#             ! Convert the value
#             read(v,*,iostat=iostat) value
#
#             ! Check that the conversion worked
#             if (iostat /= 0) then
#                 call istat%raise("The property "//name//" is not a valid double-precision real", MESSAGE_LEVEL_WARNING)
#                 exit body
#             end if
#         end block body
#
#         ! Report any issue if needed
#         if (present(stat)) call stat%transfer(istat)
#     end subroutine
# END
#         } elsif ($kind eq "List" && $type eq "C") {
        if ($kind eq "List" && $type eq "C") {
                print FUNCTION <<END;
    subroutine $getName(this, name, value)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(out) :: value
!------
        type(FProperty), pointer :: property
        integer :: i
        character(:), allocatable :: v
!------
        call getPointerToProperty(this, name, property)
        if (.not.associated(property)) return

        allocate(value(property\%size))

        do i=1, property\%size
            call this\%getValue_scalar_$friendlyType{$type}(name//"-"//i, v)
            value(i) = v
        end do
    end subroutine
END
            } elsif ($kind eq "List" && $type ne "C") {
                print FUNCTION <<END;
    subroutine $getName(this, name, value)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(out) :: value
!------
        type(FProperty), pointer :: property
        integer :: i
!------
        call getPointerToProperty(this, name, property)
        if (.not.associated(property)) return

        allocate(value(property\%size))

        do i=1, property\%size
            call this\%getValue_scalar_$friendlyType{$type}(name//"-"//i, value(i))
        end do
    end subroutine
END
            } elsif ($kind eq "Matrix" && $type eq "C") {
                print FUNCTION <<END;
    subroutine $getName(this, name, value)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(out) :: value
!------
        integer :: i, j
        character(:), allocatable :: v
!------

        if (size(value, 1)>4 .or. size(value, 2)>4) return

        do i=1, size(value, 1)
            do j=1, size(value, 2)
                call this\%getValue_scalar_$friendlyType{$type}(name//"-"//vectortag(i)//"-"//vectortag(j), v)
                value(i,j) = v
            end do
        end do
    end subroutine
END
            } elsif ($kind eq "Matrix" && $type ne "C") {
                print FUNCTION <<END;
    subroutine $getName(this, name, value)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(out) :: value
!------
        integer :: i, j
!------

        if (size(value, 1)>4 .or. size(value, 2)>4) return

        do i=1, size(value, 1)
            do j=1, size(value, 2)
                call this\%getValue_scalar_$friendlyType{$type}(name//"-"//vectortag(i)//"-"//vectortag(j), value(i,j))
            end do
        end do
    end subroutine
END
            } elsif ($kind eq "Vector" && $type eq "C") {
                print FUNCTION <<END;
    subroutine $getName(this, name, value)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(out) :: value
!------
        integer :: i
        character(:), allocatable :: v
!------

        if (size(value)>4) return

        do i=1, size(value)
            call this\%getValue_scalar_$friendlyType{$type}(name//"-"//vectortag(i), v)
            value(i) = v
        end do
    end subroutine
END
            } elsif ($kind eq "Vector" && $type ne "C") {
                print FUNCTION <<END;
    subroutine $getName(this, name, value)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: name
        $fortranType{$type} $fortranKind{$kind}, intent(out) :: value
!------
        integer :: i
!------

        if (size(value)>4) return

        do i=1, size(value)
            call this\%getValue_scalar_$friendlyType{$type}(name//"-"//vectortag(i), value(i))
        end do
    end subroutine
END
        }
    }
}

close BINDING;
close FUNCTION;
