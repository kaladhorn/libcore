    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mrfti1(n, wa, fac)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) wa(n), fac(15)
        integer ntryh(4)
        double precision tpi, argh, argld, arg
        data ntryh(1), ntryh(2), ntryh(3), ntryh(4)/4, 2, 3, 5/

        nl = n
        nf = 0
        j = 0
100     j = j + 1
        if (j-4) 110, 110, 120
110     ntry = ntryh(j)
        go to 130
120     ntry = ntry + 2
130     nq = nl/ntry
        nr = nl - ntry*nq
        if (nr) 100, 140, 100
140     nf = nf + 1
        fac(nf+2) = ntry
        nl = nq
        if (ntry/=2) go to 150
        if (nf==1) go to 150
        do i = 2, nf
            ib = nf - i + 2
            fac(ib+2) = fac(ib+1)
        end do
        fac(3) = 2
150     if (nl/=1) go to 130
        fac(1) = n
        fac(2) = nf
        tpi = 8.d0*datan(1.d0)
        argh = tpi/float(n)
        is = 0
        nfm1 = nf - 1
        l1 = 1
        if (nfm1==0) return
        do k1 = 1, nfm1
            ip = fac(k1+2)
            ld = 0
            l2 = l1*ip
            ido = n/l2
            ipm = ip - 1
            do j = 1, ipm
                ld = ld + l1
                i = is
                argld = float(ld)*argh
                fi = 0.
                do ii = 3, ido, 2
                    i = i + 2
                    fi = fi + 1.
                    arg = fi*argld
                    wa(i-1) = dcos(arg)
                    wa(i) = dsin(arg)
                end do
                is = is + ido
            end do
            l1 = l2
        end do
    end subroutine
