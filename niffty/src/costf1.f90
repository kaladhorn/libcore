    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine costf1(n, inc, x, wsave, work, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) x(inc, *), wsave(*)
        double precision dsum

        ier = 0
        nm1 = n - 1
        np1 = n + 1
        ns2 = n/2
        if (n-2) 160, 100, 110
100     x1h = x(1, 1) + x(1, 2)
        x(1, 2) = .5_rwp*(x(1,1)-x(1,2))
        x(1, 1) = .5_rwp*x1h
        go to 160
110     if (n>3) go to 120
        x1p3 = x(1, 1) + x(1, 3)
        tx2 = x(1, 2) + x(1, 2)
        x(1, 2) = .5_rwp*(x(1,1)-x(1,3))
        x(1, 1) = .25*(x1p3+tx2)
        x(1, 3) = .25*(x1p3-tx2)
        go to 160
120     dsum = x(1, 1) - x(1, n)
        x(1, 1) = x(1, 1) + x(1, n)
        do k = 2, ns2
            kc = np1 - k
            t1 = x(1, k) + x(1, kc)
            t2 = x(1, k) - x(1, kc)
            dsum = dsum + wsave(kc)*t2
            t2 = wsave(k)*t2
            x(1, k) = t1 - t2
            x(1, kc) = t1 + t2
        end do
        modn = mod(n, 2)
        if (modn==0) go to 130
        x(1, ns2+1) = x(1, ns2+1) + x(1, ns2+1)
130     lenx = inc*(nm1-1) + 1
        lnsv = nm1 + int(log(real(nm1, rwp))/log(2.0_rwp)) + 4
        lnwk = nm1

        call rfft1f(nm1, inc, x, lenx, wsave(n+1), lnsv, work, lnwk, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('COSTF1', -5)
            go to 160
        end if

        snm1 = 1.0_rwp/float(nm1)
        dsum = snm1*dsum
        if (mod(nm1,2)/=0) go to 140
        x(1, nm1) = x(1, nm1) + x(1, nm1)
140     do i = 3, n, 2
            xi = .5_rwp*x(1, i)
            x(1, i) = .5_rwp*x(1, i-1)
            x(1, i-1) = dsum
            dsum = dsum + xi
        end do
        if (modn/=0) go to 150
        x(1, n) = dsum
150     x(1, 1) = .5_rwp*x(1, 1)
        x(1, n) = .5_rwp*x(1, n)
160     return
    end subroutine
