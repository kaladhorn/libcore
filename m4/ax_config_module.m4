#
# SYNOPSIS
#
#   AX_FORTRAN_CONFIG()
#
# DESCRIPTION
#
#   Creates a module containing constants for Fortran programs. This is a 
#   Fortran equivalent to the config.h headers.
#
#
# LICENSE
#
#   Copyright (c) 2015 Paul Fossati
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#
#     * The name of the author may not be used to endorse or promote products
#      derived from this software without specific prior written permission
#      from the author.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

AC_DEFUN([AX_FORTRAN_CONFIG], [
dnl Make sure the header is defined
if test x"$_ax_fortran_header" = "x"
then
  AX_FORTRAN_CONFIG_COMMENT
fi

dnl Get the module name (default value: config)
if test x"$1" = "x"
then
  _ax_fortran_module_name="config"
else
  _ax_fortran_module_name=$1
fi

dnl Get the file name (default value: ${srcdir}/src/${_ax_fortran_module_name}.f90)
if test x"$2" = "x"
then
  _ax_fortran_module_file="$srcdir/src/$_ax_fortran_module_name.f90"
else
  _ax_fortran_module_file=$2
fi

dnl Print the header
cat << CMEOF > $_ax_fortran_module_file
$_ax_fortran_header
module $_ax_fortran_module_name
    implicit none
    public
    save
CMEOF


dnl Print the parameters
_echotest=`echo -e`
if test x$_echotest = x
then
  echo -e "$_ax_fortran_parameter_list" >> $_ax_fortran_module_file
else
  echo "$_ax_fortran_parameter_list" >> $_ax_fortran_module_file
fi

dnl Print the footer
echo "end module $_ax_fortran_module_name" >> $_ax_fortran_module_file
])


# SYNOPSIS
#
#   AX_CONFIG_MODULE()
#
# DESCRIPTION
#
#   Creates a module containing constants for Fortran programs. This is a 
#   Fortran equivalent to the config.h headers.

AC_DEFUN([AX_CONFIG_MODULE],[

dnl Set the module header
AH_TOP([
module config
    implicit none
    public
    save

])

dnl Set the module footer
AH_BOTTOM([
end module config
])

dnl Print the header
AC_CONFIG_HEADERS($1.h)

dnl Fortranise the header
sed -E -e 's|^/\*(.*)\*/$|    !\1|' \
    -e 's|^\s*\#define ([a-zA-Z][a-zA-Z0-9_]*) ([0-9]+)\s*$|    integer, parameter :: \1 = \2|' \
    -e 's|^\s*\#define ([a-zA-Z][a-zA-Z0-9_]*) (".*")\s*$|    character(*), parameter :: \1 = \2|' \
    $1.h > $1

])


#
# SYNOPSIS
#
#   AX_FORTRAN_PARAMETER()
#
# DESCRIPTION
#
#   Define a parameter to be added to the config module.

AC_DEFUN([AX_FORTRAN_PARAMETER], [

if test x"$_ax_fortran_parameter_list" = "x"
then
  _ax_fortran_parameter_list=
fi

dnl Make sure a parameter name is provided
if test x"$1" = "x"
then
  AC_MSG_ERROR([Missing parameter name])
fi

dnl Make sure the name is valid
_ax_fortran_name="$1"
if echo "$1" | grep -e '^[[[:alpha:]]][[[:alnum:]_]]*$' > /dev/null
then
  :
else
  AC_MSG_ERROR([Wrong parameter name: $_ax_fortran_name])
fi

dnl Make sure a parameter value is provided
if test x"$2" = "x"
then
  AC_MSG_ERROR([Missing value for parameter $_ax_fortran_name])
fi

dnl Check the type of the parameter
if echo "$2" | grep -e '^[[0-9]][[0-9]]*$' > /dev/null
then
  _ax_fortran_parameter_list="$_ax_fortran_parameter_list\n    integer, parameter:: $_ax_fortran_name = $2"
else
  _ax_fortran_parameter_list="$_ax_fortran_parameter_list\n    character(*), parameter:: $_ax_fortran_name = \"$2\""
fi
])

#
# SYNOPSIS
#
#   AX_FORTRAN_CONFIG_COMMENT()
#
# DESCRIPTION
#
#   Set comments to be printed before the config module.

AC_DEFUN([AX_FORTRAN_CONFIG_COMMENT], [
if test x"$1" = "x"
then
  _ax_fortran_header="! Config.f90 module\n! Generated automatically, please do not edit"
else
  _ax_fortran_header="$1"
fi
])

