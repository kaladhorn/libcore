AC_DEFUN([AX_USE_LIB],[dnl
dnl $1: user-friendly library name
dnl $2: space-separated list of short library names
dnl $3: symbol to check in the library
dnl $4: header to find

found_system_$1=no

# Look for the library in default paths
AC_SEARCH_LIBS(
[$3], dnl symbol
[$2], dnl library name
[found_system_$1=yes]
)

# Look for the header in default paths
if test x$4 != x
then
AC_CHECK_HEADER($4, [found_system_$1=yes], [found_system_$1=no])
fi

# Stop if the library was not found
if test "x$found_system_$1" = xno
then
  AC_MSG_RESULT([])
  AC_MSG_ERROR([The library $1 could not be found. If it is installed in a non-standard path, set the environment variables LDFLAGS and CFLAGS appropriately.])
fi
])
