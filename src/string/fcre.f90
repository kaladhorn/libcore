!******************************************************************************!
!>                   Fortran-Compatible Regular Expressions
!------------------------------------------------------------------------------!
!> Fortran bindings for the PCRE library.
!>
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!>
!> Based on the pcre.h header from the PCRE library written by Philip Hazel,
!> Copyright (c) 1997-2014 University of Cambridge
!> available at http://www.pcre.org
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module fcre
  use iso_c_binding

  implicit none
  public
  save

!******************************************************************************!
! FCRE-specific error codes
!******************************************************************************!
  integer(c_int), parameter :: FCRE_ERROR = -100
  integer(c_int), parameter :: FCRE_ERROR_BADOPTION = -101
  integer(c_int), parameter :: FCRE_ERROR_UNSET_PATTERN = -102
  integer(c_int), parameter :: FCRE_ERROR_EMPTY_PATTERN = -103
  integer(c_int), parameter :: FCRE_ERROR_UNSUPPORTED_OPTION= -104
  integer(c_int), parameter :: FCRE_REF_BEFORE = -1
  integer(c_int), parameter :: FCRE_REF_AFTER = -2
  integer(c_int), parameter :: FCRE_REF_LAST = -3
  integer(c_int), parameter :: FCRE_REF_MATCH = -4
!******************************************************************************!
! Parameters from pcre.h
!******************************************************************************!
integer(c_int), parameter :: PCRE_CASELESS          = int(z'00000001', c_int)
integer(c_int), parameter :: PCRE_MULTILINE         = int(z'00000002', c_int)
integer(c_int), parameter :: PCRE_DOTALL            = int(z'00000004', c_int)
integer(c_int), parameter :: PCRE_EXTENDED          = int(z'00000008', c_int)
integer(c_int), parameter :: PCRE_ANCHORED          = int(z'00000010', c_int)
integer(c_int), parameter :: PCRE_DOLLAR_ENDONLY    = int(z'00000020', c_int)
integer(c_int), parameter :: PCRE_EXTRA_            = int(z'00000040', c_int)
integer(c_int), parameter :: PCRE_NOTBOL            = int(z'00000080', c_int)
integer(c_int), parameter :: PCRE_NOTEOL            = int(z'00000100', c_int)
integer(c_int), parameter :: PCRE_UNGREEDY          = int(z'00000200', c_int)
integer(c_int), parameter :: PCRE_NOTEMPTY          = int(z'00000400', c_int)
integer(c_int), parameter :: PCRE_UTF8              = int(z'00000800', c_int)
integer(c_int), parameter :: PCRE_UTF16             = int(z'00000800', c_int)
integer(c_int), parameter :: PCRE_UTF32             = int(z'00000800', c_int)
integer(c_int), parameter :: PCRE_NO_AUTO_CAPTURE   = int(z'00001000', c_int)
integer(c_int), parameter :: PCRE_NO_UTF8_CHECK     = int(z'00002000', c_int)
integer(c_int), parameter :: PCRE_NO_UTF16_CHECK    = int(z'00002000', c_int)
integer(c_int), parameter :: PCRE_NO_UTF32_CHECK    = int(z'00002000', c_int)
integer(c_int), parameter :: PCRE_AUTO_CALLOUT      = int(z'00004000', c_int)
integer(c_int), parameter :: PCRE_PARTIAL_SOFT      = int(z'00008000', c_int)
integer(c_int), parameter :: PCRE_PARTIAL           = int(z'00008000', c_int)
integer(c_int), parameter :: PCRE_NEVER_UTF         = int(z'00010000', c_int)
integer(c_int), parameter :: PCRE_DFA_SHORTEST      = int(z'00010000', c_int)
integer(c_int), parameter :: PCRE_NO_AUTO_POSSESS   = int(z'00020000', c_int)
integer(c_int), parameter :: PCRE_DFA_RESTART       = int(z'00020000', c_int)
integer(c_int), parameter :: PCRE_FIRSTLINE         = int(z'00040000', c_int)
integer(c_int), parameter :: PCRE_DUPNAMES          = int(z'00080000', c_int)
integer(c_int), parameter :: PCRE_NEWLINE_CR        = int(z'00100000', c_int)
integer(c_int), parameter :: PCRE_NEWLINE_LF        = int(z'00200000', c_int)
integer(c_int), parameter :: PCRE_NEWLINE_CRLF      = int(z'00300000', c_int)
integer(c_int), parameter :: PCRE_NEWLINE_ANY       = int(z'00400000', c_int)
integer(c_int), parameter :: PCRE_NEWLINE_ANYCRLF   = int(z'00500000', c_int)
integer(c_int), parameter :: PCRE_BSR_ANYCRLF       = int(z'00800000', c_int)
integer(c_int), parameter :: PCRE_BSR_UNICODE       = int(z'01000000', c_int)
integer(c_int), parameter :: PCRE_JAVASCRIPT_COMPAT = int(z'02000000', c_int)
integer(c_int), parameter :: PCRE_NO_START_OPTIMIZE = int(z'04000000', c_int)
integer(c_int), parameter :: PCRE_NO_START_OPTIMISE = int(z'04000000', c_int)
integer(c_int), parameter :: PCRE_PARTIAL_HARD      = int(z'08000000', c_int)
integer(c_int), parameter :: PCRE_NOTEMPTY_ATSTART  = int(z'10000000', c_int)
integer(c_int), parameter :: PCRE_UCP               = int(z'20000000', c_int)
integer(c_int), parameter :: PCRE_ERROR_NOMATCH = -1
integer(c_int), parameter :: PCRE_ERROR_NULL = -2
integer(c_int), parameter :: PCRE_ERROR_BADOPTION = -3
integer(c_int), parameter :: PCRE_ERROR_BADMAGIC = -4
integer(c_int), parameter :: PCRE_ERROR_UNKNOWN_OPCODE = -5
integer(c_int), parameter :: PCRE_ERROR_UNKNOWN_NODE = -5
integer(c_int), parameter :: PCRE_ERROR_NOMEMORY = -6
integer(c_int), parameter :: PCRE_ERROR_NOSUBSTRING = -7
integer(c_int), parameter :: PCRE_ERROR_MATCHLIMIT = -8
integer(c_int), parameter :: PCRE_ERROR_CALLOUT = -9
integer(c_int), parameter :: PCRE_ERROR_BADUTF8 = -10
integer(c_int), parameter :: PCRE_ERROR_BADUTF16 = -10
integer(c_int), parameter :: PCRE_ERROR_BADUTF32 = -10
integer(c_int), parameter :: PCRE_ERROR_BADUTF8_OFFSET = -11
integer(c_int), parameter :: PCRE_ERROR_BADUTF16_OFFSET = -11
integer(c_int), parameter :: PCRE_ERROR_PARTIAL = -12
integer(c_int), parameter :: PCRE_ERROR_BADPARTIAL = -13
integer(c_int), parameter :: PCRE_ERROR_INTERNAL = -14
integer(c_int), parameter :: PCRE_ERROR_BADCOUNT = -15
integer(c_int), parameter :: PCRE_ERROR_DFA_UITEM = -16
integer(c_int), parameter :: PCRE_ERROR_DFA_UCOND = -17
integer(c_int), parameter :: PCRE_ERROR_DFA_UMLIMIT = -18
integer(c_int), parameter :: PCRE_ERROR_DFA_WSSIZE = -19
integer(c_int), parameter :: PCRE_ERROR_DFA_RECURSE = -20
integer(c_int), parameter :: PCRE_ERROR_RECURSIONLIMIT = -21
integer(c_int), parameter :: PCRE_ERROR_NULLWSLIMIT = -22
integer(c_int), parameter :: PCRE_ERROR_BADNEWLINE = -23
integer(c_int), parameter :: PCRE_ERROR_BADOFFSET = -24
integer(c_int), parameter :: PCRE_ERROR_SHORTUTF8 = -25
integer(c_int), parameter :: PCRE_ERROR_SHORTUTF16 = -25
integer(c_int), parameter :: PCRE_ERROR_RECURSELOOP = -26
integer(c_int), parameter :: PCRE_ERROR_JIT_STACKLIMIT = -27
integer(c_int), parameter :: PCRE_ERROR_BADMODE = -28
integer(c_int), parameter :: PCRE_ERROR_BADENDIANNESS = -29
integer(c_int), parameter :: PCRE_ERROR_DFA_BADRESTART = -30
integer(c_int), parameter :: PCRE_ERROR_JIT_BADOPTION = -31
integer(c_int), parameter :: PCRE_ERROR_BADLENGTH = -32
integer(c_int), parameter :: PCRE_ERROR_UNSET = -33
integer(c_int), parameter :: PCRE_UTF8_ERR0 = 0
integer(c_int), parameter :: PCRE_UTF8_ERR1 = 1
integer(c_int), parameter :: PCRE_UTF8_ERR2 = 2
integer(c_int), parameter :: PCRE_UTF8_ERR3 = 3
integer(c_int), parameter :: PCRE_UTF8_ERR4 = 4
integer(c_int), parameter :: PCRE_UTF8_ERR5 = 5
integer(c_int), parameter :: PCRE_UTF8_ERR6 = 6
integer(c_int), parameter :: PCRE_UTF8_ERR7 = 7
integer(c_int), parameter :: PCRE_UTF8_ERR8 = 8
integer(c_int), parameter :: PCRE_UTF8_ERR9 = 9
integer(c_int), parameter :: PCRE_UTF8_ERR10 = 10
integer(c_int), parameter :: PCRE_UTF8_ERR11 = 11
integer(c_int), parameter :: PCRE_UTF8_ERR12 = 12
integer(c_int), parameter :: PCRE_UTF8_ERR13 = 13
integer(c_int), parameter :: PCRE_UTF8_ERR14 = 14
integer(c_int), parameter :: PCRE_UTF8_ERR15 = 15
integer(c_int), parameter :: PCRE_UTF8_ERR16 = 16
integer(c_int), parameter :: PCRE_UTF8_ERR17 = 17
integer(c_int), parameter :: PCRE_UTF8_ERR18 = 18
integer(c_int), parameter :: PCRE_UTF8_ERR19 = 19
integer(c_int), parameter :: PCRE_UTF8_ERR20 = 20
integer(c_int), parameter :: PCRE_UTF8_ERR21 = 21
integer(c_int), parameter :: PCRE_UTF8_ERR22 = 22
integer(c_int), parameter :: PCRE_UTF16_ERR0 = 0
integer(c_int), parameter :: PCRE_UTF16_ERR1 = 1
integer(c_int), parameter :: PCRE_UTF16_ERR2 = 2
integer(c_int), parameter :: PCRE_UTF16_ERR3 = 3
integer(c_int), parameter :: PCRE_UTF16_ERR4 = 4
integer(c_int), parameter :: PCRE_UTF32_ERR0 = 0
integer(c_int), parameter :: PCRE_UTF32_ERR1 = 1
integer(c_int), parameter :: PCRE_UTF32_ERR2 = 2
integer(c_int), parameter :: PCRE_UTF32_ERR3 = 3
integer(c_int), parameter :: PCRE_INFO_OPTIONS = 0
integer(c_int), parameter :: PCRE_INFO_SIZE = 1
integer(c_int), parameter :: PCRE_INFO_CAPTURECOUNT = 2
integer(c_int), parameter :: PCRE_INFO_BACKREFMAX = 3
integer(c_int), parameter :: PCRE_INFO_FIRSTBYTE = 4
integer(c_int), parameter :: PCRE_INFO_FIRSTCHAR = 4
integer(c_int), parameter :: PCRE_INFO_FIRSTTABLE = 5
integer(c_int), parameter :: PCRE_INFO_LASTLITERAL = 6
integer(c_int), parameter :: PCRE_INFO_NAMEENTRYSIZE = 7
integer(c_int), parameter :: PCRE_INFO_NAMECOUNT = 8
integer(c_int), parameter :: PCRE_INFO_NAMETABLE = 9
integer(c_int), parameter :: PCRE_INFO_STUDYSIZE = 10
integer(c_int), parameter :: PCRE_INFO_DEFAULT_TABLES = 11
integer(c_int), parameter :: PCRE_INFO_OKPARTIAL = 12
integer(c_int), parameter :: PCRE_INFO_JCHANGED = 13
integer(c_int), parameter :: PCRE_INFO_HASCRORLF = 14
integer(c_int), parameter :: PCRE_INFO_MINLENGTH = 15
integer(c_int), parameter :: PCRE_INFO_JIT = 16
integer(c_int), parameter :: PCRE_INFO_JITSIZE = 17
integer(c_int), parameter :: PCRE_INFO_MAXLOOKBEHIND = 18
integer(c_int), parameter :: PCRE_INFO_FIRSTCHARACTER = 19
integer(c_int), parameter :: PCRE_INFO_FIRSTCHARACTERFLAGS = 20
integer(c_int), parameter :: PCRE_INFO_REQUIREDCHAR = 21
integer(c_int), parameter :: PCRE_INFO_REQUIREDCHARFLAGS = 22
integer(c_int), parameter :: PCRE_INFO_MATCHLIMIT = 23
integer(c_int), parameter :: PCRE_INFO_RECURSIONLIMIT = 24
integer(c_int), parameter :: PCRE_INFO_MATCH_EMPTY = 25
integer(c_int), parameter :: PCRE_CONFIG_UTF8 = 0
integer(c_int), parameter :: PCRE_CONFIG_NEWLINE = 1
integer(c_int), parameter :: PCRE_CONFIG_LINK_SIZE = 2
integer(c_int), parameter :: PCRE_CONFIG_POSIX_MALLOC_THRESHOLD = 3
integer(c_int), parameter :: PCRE_CONFIG_MATCH_LIMIT = 4
integer(c_int), parameter :: PCRE_CONFIG_STACKRECURSE = 5
integer(c_int), parameter :: PCRE_CONFIG_UNICODE_PROPERTIES = 6
integer(c_int), parameter :: PCRE_CONFIG_MATCH_LIMIT_RECURSION = 7
integer(c_int), parameter :: PCRE_CONFIG_BSR = 8
integer(c_int), parameter :: PCRE_CONFIG_JIT = 9
integer(c_int), parameter :: PCRE_CONFIG_UTF16 = 10
integer(c_int), parameter :: PCRE_CONFIG_JITTARGET = 11
integer(c_int), parameter :: PCRE_CONFIG_UTF32 = 12
integer(c_int), parameter :: PCRE_STUDY_JIT_COMPILE = int(z'0001', c_int)
integer(c_int), parameter :: PCRE_STUDY_JIT_PARTIAL_SOFT_COMPILE = int(z'0002', c_int)
integer(c_int), parameter :: PCRE_STUDY_JIT_PARTIAL_HARD_COMPILE = int(z'0004', c_int)
integer(c_int), parameter :: PCRE_STUDY_EXTRA_NEEDED = int(z'0008', c_int)
integer(c_int), parameter :: PCRE_EXTRA_STUDY_DATA = int(z'0001', c_int)
integer(c_int), parameter :: PCRE_EXTRA_MATCH_LIMIT = int(z'0002', c_int)
integer(c_int), parameter :: PCRE_EXTRA_CALLOUT_DATA = int(z'0004', c_int)
integer(c_int), parameter :: PCRE_EXTRA_TABLES = int(z'0008', c_int)
integer(c_int), parameter :: PCRE_EXTRA_MATCH_LIMIT_RECURSION = int(z'0010', c_int)
integer(c_int), parameter :: PCRE_EXTRA_MARK = int(z'0020', c_int)
integer(c_int), parameter :: PCRE_EXTRA_EXECUTABLE_JIT = int(z'0040', c_int)
!******************************************************************************!
! Derived types
!******************************************************************************!
! pcre_extra {
!    unsigned long int flags;
!    void *study_data;
!    unsigned long int match_limit;
!    void *callout_data;
!    const unsigned char *tables;
!    unsigned long int match_limit_recursion;
!    unsigned char **mark;
!    void *executable_jit;
!}
  type, bind(C) :: pcre_extra
    integer(c_long) :: flags
    type(c_ptr) :: study_data
    integer(c_long) :: match_limit
    type(c_ptr) :: callout_data
    type(c_ptr) :: tables
    integer(c_long) :: match_limit_recursion
    type(c_ptr) :: mark
    type(c_ptr) :: executable_jit
  end type pcre_extra
! pcre_callout_block {
!      int version;
!      int callout_number;
!      int *offset_vector;
!      const char * subject;
!      int subject_length;
!      int start_match;
!      int current_position;
!      int capture_top;
!      int capture_last;
!      void *callout_data;
!      int pattern_position;
!      int next_item_length;
!      const unsigned char *mark;
!}
  type, bind(C) :: pcre_callout_block
    integer(c_int) :: version
    integer(c_int) :: callout_number
    type(c_ptr) :: offset_vector
    type(c_ptr) :: subject
    integer(c_int) :: subject_length
    integer(c_int) :: start_match
    integer(c_int) :: current_position
    integer(c_int) :: capture_top
    integer(c_int) :: capture_last
    type(c_ptr) :: callout_data
    integer(c_int) :: pattern_position
    integer(c_int) :: next_item_length
    type(c_ptr) :: mark
  end type pcre_callout_block
  interface
!******************************************************************************!
! Wrapper interfaces
!******************************************************************************!
  function pcre_malloc(size) bind(C, name="pcre_malloc_wrapper")
    import
    type(c_ptr) :: pcre_malloc
    integer(c_size_t), value :: size
  end function

  function pcre_stack_malloc(size) bind(C, name="pcre_stack_malloc_wrapper")
    import
    integer(c_size_t), value :: size
    type(c_ptr) :: pcre_stack_malloc
  end function

  function pcre_callout(block) bind(C, name="cre_callout_wrapper")
    import
    type(c_ptr), value :: block
    integer(c_int) :: pcre_callout
  end function

  subroutine pcre_free(regex) bind(C, name="pcre_free_wrapper")
    import
    type(c_ptr), value :: regex
  end subroutine

  subroutine pcre_stack_free(regex) bind(C, name="pcre_stack_free_wrapper")
    import
    type(c_ptr), value :: regex
  end subroutine

!******************************************************************************!
! Subroutine interfaces
!******************************************************************************!
  ! pcre_free_substring (const char *)
      subroutine pcre_free_substring(stringptr) &
          bind(C)
        import
        character(kind=c_char), dimension(*) :: stringptr
      end subroutine pcre_free_substring
  ! pcre_free_substring_list (const char **)
      subroutine pcre_free_substring_list(stringptr) &
          bind(C)
        import
        type(c_ptr), value :: stringptr
      end subroutine pcre_free_substring_list
  ! pcre_free_study (pcre_extra *)
      subroutine pcre_free_study(extra) &
          bind(C)
        import
        type(c_ptr), value :: extra
      end subroutine pcre_free_study
  ! pcre_jit_stack_free (pcre_jit_stack *)
      subroutine pcre_jit_stack_free(stack) &
          bind(C)
        import
        type(c_ptr), value :: stack
      end subroutine pcre_jit_stack_free

!******************************************************************************!
! Function interfaces
!******************************************************************************!
! pcre * pcre_compile (const char *, int, const char **, int *, const unsigned char *)
    function pcre_compile(pattern, options, errptr, erroffset, tableptr) &
        bind(C)
      import
      type(c_ptr) :: pcre_compile
      character(kind=c_char), dimension(*) :: pattern
      integer(c_int), value :: options
      type(c_ptr) :: errptr
      integer(c_int), intent(inout) :: erroffset
      type(c_ptr), value :: tableptr
    end function pcre_compile
! pcre * pcre_compile2 (const char *, int, int *, const char **, int *, const unsigned char *)
    function pcre_compile2(pattern, options, error, errptr, erroffset, tableptr) &
        bind(C)
      import
      type(c_ptr) :: pcre_compile2
      character(kind=c_char), dimension(*) :: pattern
      integer(c_int), value :: options
      integer(c_int) :: error
      type(c_ptr) :: errptr
      integer(c_int), intent(inout) :: erroffset
      type(c_ptr), value :: tableptr
    end function pcre_compile2
! int  pcre_config (int, void *)
    function pcre_config(what, where) &
        bind(C)
      import
      integer(c_int) :: pcre_config
      integer(c_int), value :: what
      type(c_ptr), value :: where
    end function pcre_config
! int  pcre_copy_named_substring (const pcre *, const char *, int *, int, const char *, char *, int)
    function pcre_copy_named_substring(code, subject, ovector, stringcount, stringnumber, buffer, buffersize) &
        bind(C)
      import
      integer(c_int) :: pcre_copy_named_substring
      type(c_ptr), value :: code
      character(kind=c_char), dimension(*), intent(in) :: subject
      integer(c_int), dimension(*), intent(in) :: ovector
      integer(c_int), value :: stringcount
      integer(c_int), value :: stringnumber
      character(kind=c_char), dimension(*), intent(out) :: buffer
      integer(c_int), value :: buffersize
    end function pcre_copy_named_substring
! int  pcre_copy_substring (const char *, int *, int, int, char *, int)
    function pcre_copy_substring(subject, ovector, stringcount, stringnumber, buffer, buffersize) &
        bind(C)
      import
      integer(c_int) :: pcre_copy_substring
      character(kind=c_char), dimension(*), intent(in) :: subject
      integer(c_int), dimension(*), intent(in) :: ovector
      integer(c_int), value :: stringcount
      integer(c_int), value :: stringnumber
      character(kind=c_char), dimension(*), intent(out) :: buffer
      integer(c_int), value :: buffersize
    end function pcre_copy_substring
! int  pcre_dfa_exec (const pcre *, const pcre_extra *, const char *, int, int, int, int *, int , int *, int)
    function pcre_dfa_exec(code, extra, subject, length, startoffset, options, ovector, ovecsize, workspace, wscount) &
        bind(C)
      import
      integer(c_int) :: pcre_dfa_exec
      type(c_ptr), value :: code
      type(c_ptr), value :: extra
      character(kind=c_char), dimension(*), intent(in) :: subject
      integer(c_int), value :: length
      integer(c_int), value :: startoffset
      integer(c_int), value :: options
      integer(c_int), dimension(*), intent(out) :: ovector
      integer(c_int), value :: ovecsize
      integer(c_int), dimension(*), intent(in) :: workspace
      integer(c_int), value :: wscount
    end function pcre_dfa_exec
    ! int  pcre_exec (const pcre *, const pcre_extra *, const char *, int, int, int, int *, int)
    function pcre_exec(code, extra, subject, length, startoffset, options, ovector, ovecsize) &
        bind(C)
      import
      integer(c_int) :: pcre_exec
      type(c_ptr), value :: code
      type(c_ptr), value :: extra
      character(kind=c_char), dimension(*) :: subject
      integer(c_int), value :: length
      integer(c_int), value :: startoffset
      integer(c_int), value :: options
      integer(c_int), dimension(*) :: ovector
      integer(c_int), value :: ovecsize
    end function pcre_exec
! int  pcre_jit_exec (const pcre *, const pcre_extra *, const char *, int, int, int, int *, int, pcre_jit_stack *)
    function pcre_jit_exec(code, extra, subject, length, startoffset, options, ovector, ovecsize, jstack) &
        bind(C)
      import
      integer(c_int) :: pcre_jit_exec
      type(c_ptr), value :: code
      type(c_ptr), value :: extra
      character(kind=c_char), dimension(*) :: subject
      integer(c_int), value :: length
      integer(c_int), value :: startoffset
      integer(c_int), value :: options
      integer(c_int), dimension(*) :: ovector
      integer(c_int), value :: ovecsize
      type(c_ptr), value :: jstack
    end function pcre_jit_exec
! int  pcre_fullinfo (const pcre *, const pcre_extra *, int, void *)
    function pcre_fullinfo(code, extra, what, where) &
        bind(C)
      import
      integer(c_int) :: pcre_fullinfo
      type(c_ptr), value :: code
      type(c_ptr), value :: extra
      integer(c_int), value :: what
      type(c_ptr), value :: where
    end function pcre_fullinfo
    function fcre_nametable(code, extra, what, where) &
        bind(C,name="pcre_nametable")
      import
      integer(c_int) :: fcre_nametable
      type(c_ptr), value :: code
      type(c_ptr), value :: extra
      integer(c_int), value :: what
      type(c_ptr), intent(inout) :: where
    end function fcre_nametable
! int  pcre_get_named_substring (const pcre *, const char *, int *, int, const char *, const char **)
    function pcre_get_named_substring(code, subject, ovector, stringcount, stringname, stringptr) &
        bind(C)
      import
      integer(c_int) :: pcre_get_named_substring
      type(c_ptr), value :: code
      character(kind=c_char), dimension(*), intent(in) :: subject
      type(c_ptr), value :: ovector
      integer(c_int), value :: stringcount
      character(kind=c_char), dimension(*), intent(in) :: stringname
      type(c_ptr) :: stringptr
    end function pcre_get_named_substring
! int  pcre_get_stringnumber (const pcre *, const char *)
    function pcre_get_stringnumber(code, name) &
        bind(C)
      import
      integer(c_int) :: pcre_get_stringnumber
      type(c_ptr), value :: code
      character(kind=c_char), dimension(*) :: name
    end function pcre_get_stringnumber
! int  pcre_get_stringtable_entries (const pcre *, const char *, char **, char **)
    function pcre_get_stringtable_entries(code, name, first, last) &
        bind(C)
      import
      integer(c_int) :: pcre_get_stringtable_entries
      type(c_ptr), value :: code
      character(kind=c_char), dimension(*), intent(in) :: name
      type(c_ptr) :: first
      type(c_ptr) :: last
    end function pcre_get_stringtable_entries
! int  pcre_get_substring (const char *, int *, int, int, const char **)
    function pcre_get_substring(subject, ovector, stringcount, stringnumber, stringptr) &
        bind(C)
      import
      integer(c_int) :: pcre_get_substring
      character(kind=c_char), dimension(*) :: subject
      integer(c_int), dimension(*) :: ovector
      integer(c_int), value :: stringcount
      integer(c_int), value :: stringnumber
      type(c_ptr) :: stringptr
    end function pcre_get_substring
! int  pcre_refcount (pcre *, int)
    function pcre_refcount(code, adjust) &
        bind(C)
      import
      integer(c_int) :: pcre_refcount
      type(c_ptr), value :: code
      integer(c_int), value :: adjust
    end function pcre_refcount
! pcre_extra * pcre_study (const pcre *, int, const char **)
    function pcre_study(code, options, errptr) &
        bind(C)
      import
      type(c_ptr) :: pcre_study
      type(c_ptr), value :: code
      integer(c_int), value :: options
      type(c_ptr) :: errptr
    end function pcre_study
! const char * pcre_version (void)
    function pcre_version() &
        bind(C)
      import
      type(c_ptr) :: pcre_version
    end function pcre_version
! int  pcre_pattern_to_host_byte_order (pcre *, pcre_extra *, const unsigned char *)
    function pcre_pattern_to_host_byte_order(code, extra, tables) &
        bind(C)
      import
      integer(c_int) :: pcre_pattern_to_host_byte_order
      type(c_ptr), value :: code
      type(c_ptr), value :: extra
      character(kind=c_char), dimension(*) :: tables
    end function pcre_pattern_to_host_byte_order
! pcre_jit_stack * pcre_jit_stack_alloc (int, int)
    function pcre_jit_stack_alloc(startsize, maxsize) &
        bind(C)
      import
      type(c_ptr) :: pcre_jit_stack_alloc
      integer(c_int), value :: startsize
      integer(c_int), value :: maxsize
    end function pcre_jit_stack_alloc
  end interface
contains
!******************************************************************************!
!> Get the error message corresponding to an error code
!******************************************************************************!
    function fcre_error_message(code) result(message)
        integer,intent(in) :: code           !< Error code
        character(:), allocatable :: message
!------
        select case(code)
            case (PCRE_ERROR_NOMATCH)
                message = "No match found."
            case (PCRE_ERROR_NULL)
                message = "NULL code or subject or ovector."
            case (PCRE_ERROR_BADOPTION)
                message = "Unrecognized option bit."
            case (FCRE_ERROR_BADOPTION)
                message = "Unrecognized option code."
            case (PCRE_ERROR_BADMAGIC)
                message = "Bad magic number in code."
            case (PCRE_ERROR_UNKNOWN_NODE)
                message = "Bad node in pattern."
            case (FCRE_ERROR_UNSET_PATTERN)
                message = "Pattern not set."
            case (FCRE_ERROR_EMPTY_PATTERN)
                message = "Empty pattern."
            case (FCRE_ERROR_UNSUPPORTED_OPTION)
                message = "Unsupported option in getPatternInfo."
            case default
                message = "Unknown error."
        end select
    end function
!******************************************************************************!
!> Get the position of the bit corresponding to a given option
!******************************************************************************!
    function fcre_option_position(code, status) result(position)
        integer, intent(in) :: code !< Option code
        integer :: position         !< Position in the compilation options
        integer, intent(out), optional :: status         !< Optional error code
!------
        select case(code)
            case (PCRE_CASELESS) !
                position = 0
            case(PCRE_MULTILINE) !
                position = 1
            case(PCRE_DOTALL) !
                position = 2
            case(PCRE_EXTENDED) !
                position = 3
            case(PCRE_ANCHORED) !
                position = 4
            case(PCRE_DOLLAR_ENDONLY) !
                position = 5
            case(PCRE_EXTRA_) !
                position = 6
            case(PCRE_NOTBOL)
                position = 7
            case(PCRE_NOTEOL)
                position = 8
            case(PCRE_UNGREEDY) !
                position = 9
            case(PCRE_NOTEMPTY)
                position = 10
            case(PCRE_UTF8) !
                position = 11
            case(PCRE_NO_AUTO_CAPTURE) !
                position = 12
            case(PCRE_NO_UTF8_CHECK) !
                position = 13
            case(PCRE_AUTO_CALLOUT) !
                position = 14
            case(PCRE_PARTIAL_SOFT) ! also PCRE_PARTIAL
                position = 15
            case(PCRE_NEVER_UTF) ! alse PCRE_DFA_SHORTEST
                position = 16
            case(PCRE_NO_AUTO_POSSESS) ! also PCRE_DFA_RESTART
                position = 17
            case(PCRE_FIRSTLINE) !
                position = 18
            case(PCRE_DUPNAMES) !
                position = 19
            case(PCRE_NEWLINE_CR) !
                position = 20
            case(PCRE_NEWLINE_LF) !
                position = 21
            case(PCRE_NEWLINE_CRLF) !
                position = -1 !>\todo Combined options are not supported yet
            case(PCRE_NEWLINE_ANY) !
                position = 22
            case(PCRE_NEWLINE_ANYCRLF) !
                position = -1 !>\todo Combined options are not supported yet
            case(PCRE_BSR_ANYCRLF) !
                position = 23
            case(PCRE_BSR_UNICODE) !
                position = 24
            case(PCRE_JAVASCRIPT_COMPAT) !
                position = 25
            case(PCRE_NO_START_OPTIMISE) ! also PCRE_NO_START_OPTIMIZE
                position = 26
            case(PCRE_PARTIAL_HARD)
                position = 27
            case(PCRE_NOTEMPTY_ATSTART)
                position = 28
            case(PCRE_UCP) !
                position = 29
            case default
                position = -1
        end select

        ! Error check
        if (position < 0) then
            if (present(status)) then
                status = FCRE_ERROR_BADOPTION
                return
            else
                write(*,*) fcre_error_message(FCRE_ERROR_BADOPTION)
                stop
            end if
        end if
    end function
end module fcre
