#******************************************************************************#
#                              libCore property                                #
#______________________________________________________________________________#
#
#  Version 2.0
#
#  Written by Paul Fossati, <paul.fossati@gmail.com>
#  Copyright (c) 2009-2017 Paul Fossati
#------------------------------------------------------------------------------#
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
#     * Redistributions of source code must retain the above copyright notice, #
#       this list of conditions and the following disclaimer.                  #
#                                                                              #
#     * Redistributions in binary form must reproduce the above copyright      #
#       notice, this list of conditions and the following disclaimer in the    #
#       documentation and/or other materials provided with the distribution.   #
#                                                                              #
#     * The name of the author may not be used to endorse or promote products  #
#      derived from this software without specific prior written permission    #
#      from the author.                                                        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#******************************************************************************#
PROPERTY_SOURCES= property/mod_property.f90

AM_FCFLAGS+= -Iproperty

clean-local:
	rm -f property/generated_*_.inc

#******************************************************************************!
# Dependencies
#******************************************************************************!
property/mod_property.o:  property/mod_property.f90 core.mod mod_file.mod      \
    mod_expression.mod \
    property/generated_accessors_bindings.inc                                  \
    property/generated_getter_generic_binding.inc                              \
    property/generated_setter_generic_binding.inc
mod_property.mod: property/mod_property.o

property/generated_accessors_bindings.inc: property/generatePropertyAccessors.pl
	$(perl) $(srcdir)/property/generatePropertyAccessors.pl

property/generated_getter_generic_binding.inc:                                 \
    property/generatePropertyAccessors.pl
	$(perl) $(srcdir)/property/generatePropertyAccessors.pl

property/generated_setter_generic_binding.inc: property/generatePropertyAccessors.pl
	$(perl) $(srcdir)/property/generatePropertyAccessors.pl
