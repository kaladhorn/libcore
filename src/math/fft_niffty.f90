!******************************************************************************!
!                           com_fft module
!------------------------------------------------------------------------------!
!> Wrappers around the Niffty version of the FFTPACK Fourier transform
!> procedures
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module com_fft
    use ieee_arithmetic

    use core
    use mod_niffty

    implicit none
    private
    save

    public :: fft_1d_rc
    public :: fft_1d_cc
    public :: ifft_1d_cc

contains
!******************************************************************************!
!> 1-dimensional real to complex FFT
!******************************************************************************!
    subroutine fft_1d_rc(f, g)
        real(e), dimension(:), intent(in) :: f
        complex(e), dimension(:), allocatable, intent(out) :: g
!------
        integer :: n, l, i, err, s
        real(e) :: norm
        real(e), dimension(:), allocatable :: save, tmp, work
        type(ieee_status_type) :: status
!------

        ! Get the array sizes
        n = size(f)
        if (nint(n*1.0_e/2) == n*1.0_e/2) then
            ! n is even
            l = n / 2 - 1
            s = n / 2
        else
            ! n is odd
            l = (n-1)/2
            s = (n-1)/2
        end if

        norm = sqrt(n*1.0_e)

        ! Allocate the work array
        allocate(save(N + ceiling(log(N*1.0_e)/log(2._e))+4))

        ! Allocate the output array
        allocate(tmp(n))
        tmp = 0
        tmp(:size(f)) = f

        ! Allocate the work array
        allocate(work(n))
        work = 0

        ! Save the environment
        call ieee_get_status(status)

        ! Set all the flags and halting modes to false
        call ieee_set_flag(IEEE_USUAL, .false.)
        call ieee_set_halting_mode(IEEE_ALL, .false.)

        ! Calculate the coefficients
        err = 0
        call rfft1i(n, save, size(save), err)

        ! Calculate the transform
        call rfft1f(n, 1, tmp, size(tmp), save, size(save), work, size(work), err)

        ! Restore the environment
        call ieee_set_status(status)

!        block
!            integer :: unit
!            open(newunit=unit, file="raw_fft.dat")
!            do i=1, size(tmp)
!                write(unit,*) i, tmp(i)
!            end do
!            close(unit)
!        end block

        allocate(g(n))
        g = 0
        do i=1, l
            g(l+i+1) = cmplx(tmp(i*2),  tmp(i*2+1), e) / 2.0_e
            g(l-i+1) = cmplx(tmp(i*2), -tmp(i*2+1), e) / 2.0_e
        end do
        g(l+1) = cmplx(tmp(1), 0, e)
        g(n) = g(1)

        g = g * n / sqrt(TWOPI)
    end subroutine
!******************************************************************************!
!> 1-dimensional complex to complex FFT
!******************************************************************************!
    subroutine fft_1d_cc(f, g)
        complex(e), dimension(:), allocatable, intent(in) :: f
        complex(e), dimension(:), allocatable, intent(out) :: g
!------
        integer :: n, l, i, err
        real(e) :: norm
        complex(e), dimension(:), allocatable :: tmp
        real(e), dimension(:), allocatable :: save, work
        type(ieee_status_type) :: status
!------

        n = size(f)
        norm = sqrt(n*1.0_e)

        ! Allocate the work arrays
        allocate(save(2*N + int(log(real(N,e))/log(2._e)) + 4))
        save = 0
        allocate(work(2*n))
        work = 0

        ! Allocate the output array
        allocate(tmp(n))
        tmp(:) = f

        ! Save the environment
        call ieee_get_status(status)

        ! Set all the flags and halting modes to false
        call ieee_set_flag(IEEE_USUAL, .false.)
        call ieee_set_halting_mode(IEEE_ALL, .false.)

        ! Calculate the coefficients
        err = 0
        call cfft1i(n, save, size(save), err)

        ! Calculate the transform
        err = 0
        call cfft1f(n, 1, tmp, size(tmp), save, size(save), work, size(work), err)

        ! Restore the environment
        call ieee_set_status(status)

!        block
!            integer :: unit
!            open(newunit=unit, file="raw_cfft.dat")
!            do i=1, size(tmp)
!                write(unit,*) i, abs(tmp(i)), real(tmp(i), e), aimag(tmp(i))
!            end do
!            close(unit)
!        end block

        if (nint(n*1.0_e/2) == n*1.0_e/2) then
            ! n is even
            l = n / 2 - 1
        else
            ! n is odd
            l = (n-1)/2
        end if

        allocate(g(n))
        g = 0
        do i=1, l
            g(l+i+1) = conjg(tmp(i+1))
            g(l-i+1) = conjg(tmp(n-i+1))
        end do
        g(l+1) = tmp(1)

        g = g * n / sqrt(TWOPI)
    end subroutine
!******************************************************************************!
!> 1-dimensional complex to complex inverse FFT
!******************************************************************************!
    subroutine ifft_1d_cc(g, f)
        complex(e), dimension(:), intent(in) :: g
        complex(e), dimension(:), allocatable, intent(out) :: f
!------
        integer :: n, l, err, s, i
        real(e) :: norm
        real(e), dimension(:), allocatable :: save, work
        complex(e), dimension(:), allocatable :: tmp
        type(ieee_status_type) :: status
!------

        n = size(g)
        norm = sqrt(n*1.0_e)

        ! Allocate the work array
        allocate(save(2*N + int(log(real(N,e))/log(2._e)) + 4))

        ! Calculate the coefficients
        err = 0
        call cfft1i(n, save, size(save), err)

        ! Allocate the output array
        n = size(g)
        if (nint(n*1.0_e/2) == n*1.0_e/2) then
            ! n is even
            l = n / 2 - 1
            s = n / 2
        else
            ! n is odd
            l = (n-1)/2
            s = (n-1)/2
        end if

        allocate(tmp(n))
        tmp = 2
        do i=1, (n-s)
            tmp(i) = conjg(g(s+i))
        end do
        do i=1, s
            tmp(n-s+i) = conjg(g(i))
        end do
        tmp = tmp * sqrt(TWOPI) / n

        block
            integer :: unit
            open(newunit=unit, file="raw_cifft.dat")
            do i=1, size(tmp)
                write(unit,*) i, abs(tmp(i)), real(tmp(i), e), aimag(tmp(i))
            end do
            close(unit)
        end block

        allocate(work(2*n))
        work = 0

        ! Save the environment
        call ieee_get_status(status)

        ! Set all the flags and halting modes to false
        call ieee_set_flag(IEEE_USUAL, .false.)
        call ieee_set_halting_mode(IEEE_ALL, .false.)

        ! Calculate the transform
        err = 0
        call cfft1b(n, 1, tmp, size(tmp), save, size(save), work, size(work), err)

        ! Restore the environment
        call ieee_set_status(status)

        f = tmp
    end subroutine
!******************************************************************************!
!> Does nothing, but allows to check whether the library includes the Niffty
!> version of FFT procedures by doing
!> `nm src/libcore.a | grep com_fft_is_niffty`.
!******************************************************************************!
!LCOV_EXCL_START
    subroutine com_fft_is_niffty() bind(C)
    end subroutine
!LCOV_EXCL_STOP
end module