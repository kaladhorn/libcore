project: libcore
summary: Core library
project_dir: ../src
output_dir: ./html
media_dir: images
author: Paul Fossati
email: paul.fossati@gmail.com
year: 2009-2016
version: 1.0
predocmark: >
docmark: <
docmark_alt:
display: public
         protected
source: true
graph: true
search: true
extra_mods: iso_fortran_env:https://gcc.gnu.org/onlinedocs/gfortran/ISO_005fFORTRAN_005fENV.html
extra_mods: iso_c_binding_:https://gcc.gnu.org/onlinedocs/gfortran/ISO_005fC_005fBINDING.html
page_dir: pages

[![License](https://img.shields.io/badge/license-BSD3-red.svg)](page/5-license.html)


The Core library is a Fortran library designed to help developers with common use-cases in scientific applications.
For example, it includes:

 - derived types to interact in a simple way with file systems including compressed [files](page/3-api/3-files.html);
 - object-oriented interfaces to the PCRE library for [regular expressions](page/3-api/4-regex.html);
 - arbitrary [mathematical expressions](page/3-api/5-expressions.html) parsing and evaluation;
 - [properties](page/3-api/7-properties.html).

It is an object-oriented library implemented in a modern Fortran dialect.

For an high-level developer documentation, see [this page](page/3-api/index.html).
