/*******************************************************************************
*                         mod_directory_wrapper.c                              *
*------------------------------------------------------------------------------*
* Wrapper around system-dependent functions to manipulate directories.         *
*                                                                              *
*  Written by Paul Fossati, <paul.fossati@gmail.com>                           *
*  Copyright (c) 2009-2016 Paul Fossati                                        *
*                                                                              *
*------------------------------------------------------------------------------*
* Redistribution and use in source and binary forms, with or without           *
* modification, are permitted provided that the following conditions are met:  *
*                                                                              *
*     * Redistributions of source code must retain the above copyright notice, *
*       this list of conditions and the following disclaimer.                  *
*                                                                              *
*     * Redistributions in binary form must reproduce the above copyright      *
*       notice, this list of conditions and the following disclaimer in the    *
*       documentation and/or other materials provided with the distribution.   *
*                                                                              *
*     * The name of the author may not be used to endorse or promote products  *
*      derived from this software without specific prior written permission    *
*      from the author.                                                        *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*******************************************************************************/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <errno.h>

/*******************************************************************************
* Get the error code                                                           *
*******************************************************************************/
int errno_wrapper() {
	return errno;
}

/*******************************************************************************
* Wrapper around the POSIX mkdir function                                      *
*******************************************************************************/
int mkdir_wrapper(char *path) {

	// Reset error status
	errno = 0;

	// Create the directory
    return mkdir(path, 0777);
}

/*******************************************************************************
* Wrapper around the POSIX rmdir function                                      *
*******************************************************************************/
int rmdir_wrapper(char *path) {

	// Reset error status
	errno = 0;

	// Remove the directory
    return rmdir(path);
}

/*******************************************************************************
* Wrapper around the POSIX opendir function                                    *
*******************************************************************************/
DIR * opendir_wrapper(char *dirname) {

	// Reset error status
	errno = 0;

	// Open the directory
    return opendir(dirname);
}

/*******************************************************************************
* Wrapper around the POSIX closedir function                                   *
*******************************************************************************/
int closedir_wrapper(DIR *dirp) {

	// Reset error status
	errno = 0;

	// Close the directory
    return closedir(dirp);
}

/*******************************************************************************
* Wrapper around the POSIX readdir function                                    *
*******************************************************************************/
int readdir_wrapper(DIR *dirp, char *filename, int nameLength, int * type) {

	// Reset error status
	errno = 0;

	// Read the directory
	struct dirent *dp;
	dp = readdir(dirp);

	if (dp) {
		// Get the file name
		int n = snprintf(filename, nameLength, "%s", dp->d_name);

		// Get the file type
		if (dp->d_type & DT_DIR) {
			// The entry is a directory
			*type = 1;
		} else {
			// The entry is something else
			*type = 0;
		}

		// Return the length of the filename string if dp->d_name was too long
		if (n >= nameLength)
		return nameLength;

		// Return the lenght of the file name
		return n;
	}

	// Default type is 0 (no file)
	return 0;
}

/*******************************************************************************
* Get a file modification time.                                                *
*******************************************************************************/
long getModificationTime_wrapper(char *filename) {
	struct stat t;

	lstat(filename, &t);
	return t.st_mtime;
}

/*******************************************************************************
* Test whether a file can be read.                                             *
*******************************************************************************/
int canRead(char *filename) {
    struct stat fileStat;
    if(stat(filename, &fileStat) < 0)
        return 0;

	return (fileStat.st_mode & S_IRUSR);
}

/*******************************************************************************
* Test whether a file can be written.                                          *
*******************************************************************************/
int canWrite(char *filename) {
    struct stat fileStat;
    if(stat(filename, &fileStat) < 0)
        return 0;

	return (fileStat.st_mode & S_IWUSR);
}

/*******************************************************************************
* Test whether a file is empty.                                                *
*******************************************************************************/
int isEmpty(char *filename) {
    struct stat fileStat;
    if(stat(filename, &fileStat) < 0)
        return 0;

	return fileStat.st_size;
}

/*******************************************************************************
* Count the number of lines in a file.                                         *
*******************************************************************************/
int countLines(FILE *stream) {
	int count = 0;
	char buf[1000];
	while(fgets(buf, sizeof(buf), stream) != NULL) {
	  count++;
	}
	return count;
}
