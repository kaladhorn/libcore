    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine rfftb1(n, in, c, ch, wa, fac)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) ch(*), c(in, *), wa(n), fac(15)

        nf = fac(2)
        na = 0
        do k1 = 1, nf
            ip = fac(k1+2)
            na = 1 - na
            if (ip<=5) go to 100
            if (k1==nf) go to 100
            na = 1 - na
100     end do
        half = .5_rwp
        halfm = -.5
        modn = mod(n, 2)
        nl = n - 2
        if (modn/=0) nl = n - 1
        if (na==0) go to 110
        ch(1) = c(1, 1)
        ch(n) = c(1, n)
        do j = 2, nl, 2
            ch(j) = half*c(1, j)
            ch(j+1) = halfm*c(1, j+1)
        end do
        go to 120
110     do j = 2, nl, 2
            c(1, j) = half*c(1, j)
            c(1, j+1) = halfm*c(1, j+1)
        end do
120     l1 = 1
        iw = 1
        do k1 = 1, nf
            ip = fac(k1+2)
            l2 = ip*l1
            ido = n/l2
            idl1 = ido*l1
            if (ip/=4) go to 150
            ix2 = iw + ido
            ix3 = ix2 + ido
            if (na/=0) go to 130
            call r1f4kb(ido, l1, c, in, ch, 1, wa(iw), wa(ix2), wa(ix3))
            go to 140
130         call r1f4kb(ido, l1, ch, 1, c, in, wa(iw), wa(ix2), wa(ix3))
140         na = 1 - na
            go to 270
150         if (ip/=2) go to 180
            if (na/=0) go to 160
            call r1f2kb(ido, l1, c, in, ch, 1, wa(iw))
            go to 170
160         call r1f2kb(ido, l1, ch, 1, c, in, wa(iw))
170         na = 1 - na
            go to 270
180         if (ip/=3) go to 210
            ix2 = iw + ido
            if (na/=0) go to 190
            call r1f3kb(ido, l1, c, in, ch, 1, wa(iw), wa(ix2))
            go to 200
190         call r1f3kb(ido, l1, ch, 1, c, in, wa(iw), wa(ix2))
200         na = 1 - na
            go to 270
210         if (ip/=5) go to 240
            ix2 = iw + ido
            ix3 = ix2 + ido
            ix4 = ix3 + ido
            if (na/=0) go to 220
            call r1f5kb(ido, l1, c, in, ch, 1, wa(iw), wa(ix2), wa(ix3), &
              wa(ix4))
            go to 230
220         call r1f5kb(ido, l1, ch, 1, c, in, wa(iw), wa(ix2), wa(ix3), &
              wa(ix4))
230         na = 1 - na
            go to 270
240         if (na/=0) go to 250
            call r1fgkb(ido, ip, l1, idl1, c, c, c, in, ch, ch, 1, wa(iw))
            go to 260
250         call r1fgkb(ido, ip, l1, idl1, ch, ch, ch, 1, c, c, in, wa(iw))
260         if (ido==1) na = 1 - na
270         l1 = l2
            iw = iw + (ip-1)*ido
        end do
    end subroutine
