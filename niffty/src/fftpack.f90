    Module fftpack
      include 'kinds.inc'
      ! Interface module generated on 2015-11-21 at 18:51:51 +0100.
      Interface
        Subroutine c1f2kb(ido, l1, na, cc, in1, ch, in2, wa)
            import
          ! Interface, source="c1f2kb.f90".
          Real (rwp) :: cc(in1, l1, ido, 2), ch(in2, l1, 2, ido), &
            wa(ido, 1, 2)
        End Subroutine
        Subroutine c1f2kf(ido, l1, na, cc, in1, ch, in2, wa)
            import
          ! Interface, source="c1f2kf.f90".
          Real (rwp) :: cc(in1, l1, ido, 2), ch(in2, l1, 2, ido), &
            wa(ido, 1, 2)
        End Subroutine
        Subroutine c1f3kb(ido, l1, na, cc, in1, ch, in2, wa)
            import
          ! Interface, source="c1f3kb.f90".
          Real (rwp) :: cc(in1, l1, ido, 3), ch(in2, l1, 3, ido), &
            wa(ido, 2, 2)
        End Subroutine
        Subroutine c1f3kf(ido, l1, na, cc, in1, ch, in2, wa)
            import
          ! Interface, source="c1f3kf.f90".
          Real (rwp) :: cc(in1, l1, ido, 3), ch(in2, l1, 3, ido), &
            wa(ido, 2, 2)
        End Subroutine
        Subroutine c1f4kb(ido, l1, na, cc, in1, ch, in2, wa)
            import
          ! Interface, source="c1f4kb.f90".
          Real (rwp) :: cc(in1, l1, ido, 4), ch(in2, l1, 4, ido), &
            wa(ido, 3, 2)
        End Subroutine
        Subroutine c1f4kf(ido, l1, na, cc, in1, ch, in2, wa)
            import
          ! Interface, source="c1f4kf.f90".
          Real (rwp) :: cc(in1, l1, ido, 4), ch(in2, l1, 4, ido), &
            wa(ido, 3, 2)
        End Subroutine
        Subroutine c1f5kb(ido, l1, na, cc, in1, ch, in2, wa)
            import
          ! Interface, source="c1f5kb.f90".
          Real (rwp) :: cc(in1, l1, ido, 5), ch(in2, l1, 5, ido), &
            wa(ido, 4, 2)
        End Subroutine
        Subroutine c1f5kf(ido, l1, na, cc, in1, ch, in2, wa)
            import
          ! Interface, source="c1f5kf.f90".
          Real (rwp) :: cc(in1, l1, ido, 5), ch(in2, l1, 5, ido), &
            wa(ido, 4, 2)
        End Subroutine
        Subroutine c1fgkb(ido, ip, l1, lid, na, cc, cc1, in1, ch, ch1, in2, &
          wa)
          import
          ! Interface, source="c1fgkb.f90".
          Real (rwp) :: ch(in2, l1, ido, ip), cc(in1, l1, ip, ido), &
            cc1(in1, lid, ip), ch1(in2, lid, ip), wa(ido, ip-1, 2)
        End Subroutine
        Subroutine c1fgkf(ido, ip, l1, lid, na, cc, cc1, in1, ch, ch1, in2, &
          wa)
          import
          ! Interface, source="c1fgkf.f90".
          Real (rwp) :: ch(in2, l1, ido, ip), cc(in1, l1, ip, ido), &
            cc1(in1, lid, ip), ch1(in2, lid, ip), wa(ido, ip-1, 2)
        End Subroutine
        Subroutine c1fm1b(n, inc, c, ch, wa, fnf, fac)
          import
          ! Interface, source="c1fm1b.f90".
          Complex (cwp) :: c(*)
          Real (rwp) :: ch(*), wa(*), fac(*)
        End Subroutine
        Subroutine c1fm1f(n, inc, c, ch, wa, fnf, fac)
          import
          ! Interface, source="c1fm1f.f90".
          Complex (cwp) :: c(*)
          Real (rwp) :: ch(*), wa(*), fac(*)
        End Subroutine
        Subroutine cfft1b(n, inc, c, lenc, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="cfft1b.f90".
          Integer :: n, inc, lenc, lensav, lenwrk, ier
          Complex (cwp) :: c(lenc)
          Real (rwp) :: wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cfft1f(n, inc, c, lenc, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="cfft1f.f90".
          Integer :: n, inc, lenc, lensav, lenwrk, ier
          Complex (cwp) :: c(lenc)
          Real (rwp) :: wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cfft1i(n, wsave, lensav, ier)
          import
          ! Interface, source="cfft1i.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine cfft2b(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="cfft2b.f90".
          Integer :: l, m, ldim, lensav, lenwrk, ier
          Complex (cwp) :: c(ldim, m)
          Real (rwp) :: wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cfft2f(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="cfft2f.f90".
          Integer :: l, m, ldim, lensav, lenwrk, ier
          Complex (cwp) :: c(ldim, m)
          Real (rwp) :: wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cfft2i(l, m, wsave, lensav, ier)
          import
          ! Interface, source="cfft2i.f90".
          Integer :: l, m, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine cfftmb(lot, jump, n, inc, c, lenc, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="cfftmb.f90".
          Integer :: lot, jump, n, inc, lenc, lensav, lenwrk, ier
          Complex (cwp) :: c(lenc)
          Real (rwp) :: wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cfftmf(lot, jump, n, inc, c, lenc, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="cfftmf.f90".
          Integer :: lot, jump, n, inc, lenc, lensav, lenwrk, ier
          Complex (cwp) :: c(lenc)
          Real (rwp) :: wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cfftmi(n, wsave, lensav, ier)
          import
          ! Interface, source="cfftmi.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine cmf2kb(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
          import
          ! Interface, source="cmf2kb.f90".
          Real (rwp) :: cc(2, in1, l1, ido, 2), ch(2, in2, l1, 2, ido), &
            wa(ido, 1, 2)
        End Subroutine
        Subroutine cmf2kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
          import
          ! Interface, source="cmf2kf.f90".
          Real (rwp) :: cc(2, in1, l1, ido, 2), ch(2, in2, l1, 2, ido), &
            wa(ido, 1, 2)
        End Subroutine
        Subroutine cmf3kb(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
          import
          ! Interface, source="cmf3kb.f90".
          Real (rwp) :: cc(2, in1, l1, ido, 3), ch(2, in2, l1, 3, ido), &
            wa(ido, 2, 2)
        End Subroutine
        Subroutine cmf3kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
          import
          ! Interface, source="cmf3kf.f90".
          Real (rwp) :: cc(2, in1, l1, ido, 3), ch(2, in2, l1, 3, ido), &
            wa(ido, 2, 2)
        End Subroutine
        Subroutine cmf4kb(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
          import
          ! Interface, source="cmf4kb.f90".
          Real (rwp) :: cc(2, in1, l1, ido, 4), ch(2, in2, l1, 4, ido), &
            wa(ido, 3, 2)
        End Subroutine
        Subroutine cmf4kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
          import
          ! Interface, source="cmf4kf.f90".
          Real (rwp) :: cc(2, in1, l1, ido, 4), ch(2, in2, l1, 4, ido), &
            wa(ido, 3, 2)
        End Subroutine
        Subroutine cmf5kb(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
          import
          ! Interface, source="cmf5kb.f90".
          Real (rwp) :: cc(2, in1, l1, ido, 5), ch(2, in2, l1, 5, ido), &
            wa(ido, 4, 2)
        End Subroutine
        Subroutine cmf5kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
          import
          ! Interface, source="cmf5kf.f90".
          Real (rwp) :: cc(2, in1, l1, ido, 5), ch(2, in2, l1, 5, ido), &
            wa(ido, 4, 2)
        End Subroutine
        Subroutine cmfgkb(lot, ido, ip, l1, lid, na, cc, cc1, im1, in1, ch, &
          ch1, im2, in2, wa)
          import
          ! Interface, source="cmfgkb.f90".
          Real (rwp) :: ch(2, in2, l1, ido, ip), cc(2, in1, l1, ip, ido), &
            cc1(2, in1, lid, ip), ch1(2, in2, lid, ip), wa(ido, ip-1, 2)
        End Subroutine
        Subroutine cmfgkf(lot, ido, ip, l1, lid, na, cc, cc1, im1, in1, ch, &
          ch1, im2, in2, wa)
          import
          ! Interface, source="cmfgkf.f90".
          Real (rwp) :: ch(2, in2, l1, ido, ip), cc(2, in1, l1, ip, ido), &
            cc1(2, in1, lid, ip), ch1(2, in2, lid, ip), wa(ido, ip-1, 2)
        End Subroutine
        Subroutine cmfm1b(lot, jump, n, inc, c, ch, wa, fnf, fac)
          import
          ! Interface, source="cmfm1b.f90".
          Complex (cwp) :: c(*)
          Real (rwp) :: ch(*), wa(*), fac(*)
        End Subroutine
        Subroutine cmfm1f(lot, jump, n, inc, c, ch, wa, fnf, fac)
          import
          ! Interface, source="cmfm1f.f90".
          Complex (cwp) :: c(*)
          Real (rwp) :: ch(*), wa(*), fac(*)
        End Subroutine
        Subroutine cosq1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="cosq1b.f90".
          Integer :: n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cosq1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="cosq1f.f90".
          Integer :: n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cosq1i(n, wsave, lensav, ier)
          import
          ! Interface, source="cosq1i.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine cosqb1(n, inc, x, wsave, work, ier)
          import
          ! Interface, source="cosqb1.f90".
          Real (rwp) :: x, wsave, work
          Dimension :: x(inc, *), wsave(*), work(*)
        End Subroutine
        Subroutine cosqf1(n, inc, x, wsave, work, ier)
          import
          ! Interface, source="cosqf1.f90".
          Real (rwp) :: x, wsave, work
          Dimension :: x(inc, *), wsave(*), work(*)
        End Subroutine
        Subroutine cosqmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="cosqmb.f90".
          Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cosqmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="cosqmf.f90".
          Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cosqmi(n, wsave, lensav, ier)
          import
          ! Interface, source="cosqmi.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine cost1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="cost1b.f90".
          Integer :: n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cost1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="cost1f.f90".
          Integer :: n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine cost1i(n, wsave, lensav, ier)
          import
          ! Interface, source="cost1i.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine costb1(n, inc, x, wsave, work, ier)
          import
          ! Interface, source="costb1.f90".
          Real (rwp) :: x(inc, *), wsave(*)
        End Subroutine
        Subroutine costf1(n, inc, x, wsave, work, ier)
          import
          ! Interface, source="costf1.f90".
          Real (rwp) :: x(inc, *), wsave(*)
        End Subroutine
        Subroutine costmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="costmb.f90".
          Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine costmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="costmf.f90".
          Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine costmi(n, wsave, lensav, ier)
          import
          ! Interface, source="costmi.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine factor(n, nf, fac)
          import
          ! Interface, source="factor.f90".
          Real (rwp) :: fac(*)
        End Subroutine
        Subroutine mcfti1(n, wa, fnf, fac)
          import
          ! Interface, source="mcfti1.f90".
          Real (rwp) :: wa(*), fac(*)
        End Subroutine
        Subroutine mcsqb1(lot, jump, n, inc, x, wsave, work, ier)
          import
          ! Interface, source="mcsqb1.f90".
          Real (rwp) :: x, wsave, work
          Dimension :: x(inc, *), wsave(*), work(lot, *)
        End Subroutine
        Subroutine mcsqf1(lot, jump, n, inc, x, wsave, work, ier)
          import
          ! Interface, source="mcsqf1.f90".
          Real (rwp) :: x, wsave, work
          Dimension :: x(inc, *), wsave(*), work(lot, *)
        End Subroutine
        Subroutine mcstb1(lot, jump, n, inc, x, wsave, dsum, work, ier)
          import
          ! Interface, source="mcstb1.f90".
          Real (rwp) :: x(inc, *), wsave(*)
          Double Precision :: dsum(*)
        End Subroutine
        Subroutine mcstf1(lot, jump, n, inc, x, wsave, dsum, work, ier)
          import
          ! Interface, source="mcstf1.f90".
          Real (rwp) :: x(inc, *), wsave(*)
          Double Precision :: dsum(*)
        End Subroutine
        Subroutine mradb2(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1)
          import
          ! Interface, source="mradb2.f90".
          Real (rwp) :: cc(in1, ido, 2, l1), ch(in2, ido, l1, 2), wa1(ido)
        End Subroutine
        Subroutine mradb3(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2)
          import
          ! Interface, source="mradb3.f90".
          Real (rwp) :: cc(in1, ido, 3, l1), ch(in2, ido, l1, 3), wa1(ido), &
            wa2(ido)
        End Subroutine
        Subroutine mradb4(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, &
          wa3)
          import
          ! Interface, source="mradb4.f90".
          Real (rwp) :: cc(in1, ido, 4, l1), ch(in2, ido, l1, 4), wa1(ido), &
            wa2(ido), wa3(ido)
        End Subroutine
        Subroutine mradb5(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, &
          wa3, wa4)
          import
          ! Interface, source="mradb5.f90".
          Real (rwp) :: cc(in1, ido, 5, l1), ch(in2, ido, l1, 5), wa1(ido), &
            wa2(ido), wa3(ido), wa4(ido)
        End Subroutine
        Subroutine mradbg(m, ido, ip, l1, idl1, cc, c1, c2, im1, in1, ch, ch2, &
          im2, in2, wa)
          import
          ! Interface, source="mradbg.f90".
          Real (rwp) :: ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
            c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), &
            wa(ido)
        End Subroutine
        Subroutine mradf2(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1)
          import
          ! Interface, source="mradf2.f90".
          Real (rwp) :: ch(in2, ido, 2, l1), cc(in1, ido, l1, 2), wa1(ido)
        End Subroutine
        Subroutine mradf3(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2)
          import
          ! Interface, source="mradf3.f90".
          Real (rwp) :: ch(in2, ido, 3, l1), cc(in1, ido, l1, 3), wa1(ido), &
            wa2(ido)
        End Subroutine
        Subroutine mradf4(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, &
          wa3)
          import
          ! Interface, source="mradf4.f90".
          Real (rwp) :: cc(in1, ido, l1, 4), ch(in2, ido, 4, l1), wa1(ido), &
            wa2(ido), wa3(ido)
        End Subroutine
        Subroutine mradf5(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, &
          wa3, wa4)
          import
          ! Interface, source="mradf5.f90".
          Real (rwp) :: cc(in1, ido, l1, 5), ch(in2, ido, 5, l1), wa1(ido), &
            wa2(ido), wa3(ido), wa4(ido)
        End Subroutine
        Subroutine mradfg(m, ido, ip, l1, idl1, cc, c1, c2, im1, in1, ch, ch2, &
          im2, in2, wa)
          import
          ! Interface, source="mradfg.f90".
          Real (rwp) :: ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
            c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), &
            wa(ido)
        End Subroutine
        Subroutine mrftb1(m, im, n, in, c, ch, wa, fac)
          import
          ! Interface, source="mrftb1.f90".
          Real (rwp) :: ch(m, *), c(in, *), wa(n), fac(15)
        End Subroutine
        Subroutine mrftf1(m, im, n, in, c, ch, wa, fac)
          import
          ! Interface, source="mrftf1.f90".
          Real (rwp) :: ch(m, *), c(in, *), wa(n), fac(15)
        End Subroutine
        Subroutine mrfti1(n, wa, fac)
          import
          ! Interface, source="mrfti1.f90".
          Real (rwp) :: wa(n), fac(15)
        End Subroutine
        Subroutine msntb1(lot, jump, n, inc, x, wsave, dsum, xh, work, ier)
          import
          ! Interface, source="msntb1.f90".
          Real (rwp) :: x(inc, *), wsave(*), xh(lot, *)
          Double Precision :: dsum(*)
        End Subroutine
        Subroutine msntf1(lot, jump, n, inc, x, wsave, dsum, xh, work, ier)
          import
          ! Interface, source="msntf1.f90".
          Real (rwp) :: x(inc, *), wsave(*), xh(lot, *)
          Double Precision :: dsum(*)
        End Subroutine
        Subroutine r1f2kb(ido, l1, cc, in1, ch, in2, wa1)
          import
          ! Interface, source="r1f2kb.f90".
          Real (rwp) :: cc(in1, ido, 2, l1), ch(in2, ido, l1, 2), wa1(ido)
        End Subroutine
        Subroutine r1f2kf(ido, l1, cc, in1, ch, in2, wa1)
          import
          ! Interface, source="r1f2kf.f90".
          Real (rwp) :: ch(in2, ido, 2, l1), cc(in1, ido, l1, 2), wa1(ido)
        End Subroutine
        Subroutine r1f3kb(ido, l1, cc, in1, ch, in2, wa1, wa2)
          import
          ! Interface, source="r1f3kb.f90".
          Real (rwp) :: cc(in1, ido, 3, l1), ch(in2, ido, l1, 3), wa1(ido), &
            wa2(ido)
        End Subroutine
        Subroutine r1f3kf(ido, l1, cc, in1, ch, in2, wa1, wa2)
          import
          ! Interface, source="r1f3kf.f90".
          Real (rwp) :: ch(in2, ido, 3, l1), cc(in1, ido, l1, 3), wa1(ido), &
            wa2(ido)
        End Subroutine
        Subroutine r1f4kb(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3)
          import
          ! Interface, source="r1f4kb.f90".
          Real (rwp) :: cc(in1, ido, 4, l1), ch(in2, ido, l1, 4), wa1(ido), &
            wa2(ido), wa3(ido)
        End Subroutine
        Subroutine r1f4kf(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3)
          import
          ! Interface, source="r1f4kf.f90".
          Real (rwp) :: cc(in1, ido, l1, 4), ch(in2, ido, 4, l1), wa1(ido), &
            wa2(ido), wa3(ido)
        End Subroutine
        Subroutine r1f5kb(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3, wa4)
          import
          ! Interface, source="r1f5kb.f90".
          Real (rwp) :: cc(in1, ido, 5, l1), ch(in2, ido, l1, 5), wa1(ido), &
            wa2(ido), wa3(ido), wa4(ido)
        End Subroutine
        Subroutine r1f5kf(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3, wa4)
          import
          ! Interface, source="r1f5kf.f90".
          Real (rwp) :: cc(in1, ido, l1, 5), ch(in2, ido, 5, l1), wa1(ido), &
            wa2(ido), wa3(ido), wa4(ido)
        End Subroutine
        Subroutine r1fgkb(ido, ip, l1, idl1, cc, c1, c2, in1, ch, ch2, in2, &
          wa)
          import
          ! Interface, source="r1fgkb.f90".
          Real (rwp) :: ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
            c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), &
            wa(ido)
        End Subroutine
        Subroutine r1fgkf(ido, ip, l1, idl1, cc, c1, c2, in1, ch, ch2, in2, &
          wa)
          import
          ! Interface, source="r1fgkf.f90".
          Real (rwp) :: ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
            c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), &
            wa(ido)
        End Subroutine
        Subroutine r2w(ldr, ldw, l, m, r, w)
          import
          ! Interface, source="r2w.f90".
          Dimension :: r(ldr, *), w(ldw, *)
        End Subroutine
        Subroutine rfft1b(n, inc, r, lenr, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="rfft1b.f90".
          Implicit None
          Integer :: n, inc, lenr, lensav, lenwrk, ier
          Real (rwp) :: r(lenr), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine rfft1f(n, inc, r, lenr, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="rfft1f.f90".
          Implicit None
          Integer :: n, inc, lenr, lensav, lenwrk, ier
          Real (rwp) :: r(lenr), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine rfft1i(n, wsave, lensav, ier)
          import
          ! Interface, source="rfft1i.f90".
          Implicit None
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine rfft2b(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="rfft2b.f90".
          Implicit None
          Integer :: ldim, l, m, lensav, lenwrk, ier
          Real (rwp) :: r(ldim, m), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine rfft2f(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="rfft2f.f90".
          Implicit None
          Integer :: ldim, l, m, lensav, lenwrk, ier, idx, modl, modm, idh, &
            idw
          Real (rwp) :: r(ldim, m), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine rfft2i(l, m, wsave, lensav, ier)
          import
          ! Interface, source="rfft2i.f90".
          Implicit None
          Integer :: l, m, lensav, ier
          Integer :: lwsav, mwsav, mmsav, ier1
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine rfftb1(n, in, c, ch, wa, fac)
          import
          ! Interface, source="rfftb1.f90".
          Real (rwp) :: ch(*), c(in, *), wa(n), fac(15)
        End Subroutine
        Subroutine rfftf1(n, in, c, ch, wa, fac)
          import
          ! Interface, source="rfftf1.f90".
          Real (rwp) :: ch(*), c(in, *), wa(n), fac(15)
        End Subroutine
        Subroutine rffti1(n, wa, fac)
          import
          ! Interface, source="rffti1.f90".
          Real (rwp) :: wa(n), fac(15)
        End Subroutine
        Subroutine rfftmb(lot, jump, n, inc, r, lenr, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="rfftmb.f90".
          Integer :: lot, jump, n, inc, lenr, lensav, lenwrk, ier
          Real (rwp) :: r(lenr), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine rfftmf(lot, jump, n, inc, r, lenr, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="rfftmf.f90".
          Integer :: lot, jump, n, inc, lenr, lensav, lenwrk, ier
          Real (rwp) :: r(lenr), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine rfftmi(n, wsave, lensav, ier)
          import
          ! Interface, source="rfftmi.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine sinq1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="sinq1b.f90".
          Integer :: n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine sinq1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="sinq1f.f90".
          Integer :: n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine sinq1i(n, wsave, lensav, ier)
          import
          ! Interface, source="sinq1i.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine sinqmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="sinqmb.f90".
          Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine sinqmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="sinqmf.f90".
          Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine sinqmi(n, wsave, lensav, ier)
          import
          ! Interface, source="sinqmi.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine sint1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="sint1b.f90".
          Integer :: n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine sint1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
          import
          ! Interface, source="sint1f.f90".
          Integer :: n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine sint1i(n, wsave, lensav, ier)
          import
          ! Interface, source="sint1i.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine sintb1(n, inc, x, wsave, xh, work, ier)
          import
          ! Interface, source="sintb1.f90".
          Real (rwp) :: x(inc, *), wsave(*), xh(*)
        End Subroutine
        Subroutine sintf1(n, inc, x, wsave, xh, work, ier)
          import
          ! Interface, source="sintf1.f90".
          Real (rwp) :: x(inc, *), wsave(*), xh(*)
        End Subroutine
        Subroutine sintmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="sintmb.f90".
          Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine sintmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
          lenwrk, ier)
          import
          ! Interface, source="sintmf.f90".
          Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
          Real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
        End Subroutine
        Subroutine sintmi(n, wsave, lensav, ier)
          import
          ! Interface, source="sintmi.f90".
          Integer :: n, lensav, ier
          Real (rwp) :: wsave(lensav)
        End Subroutine
        Subroutine tables(ido, ip, wa)
          import
          ! Interface, source="tables.f90".
          Real (rwp) :: wa(ido, ip-1, 2)
        End Subroutine
        Subroutine w2r(ldr, ldw, l, m, r, w)
          import
          ! Interface, source="w2r.f90".
          Dimension :: r(ldr, *), w(ldw, *)
        End Subroutine
        Logical Function xercon(inc, jump, n, lot)
          import
          ! Interface, source="xercon.f90".
          Integer :: inc, jump, n, lot
        End Function
        Subroutine xerfft(srname, info)
          import
          ! Interface, source="xerfft.f90".
          Character (6) :: srname
          Integer :: info
        End Subroutine
      End Interface
    End Module
