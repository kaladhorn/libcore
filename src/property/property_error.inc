!******************************************************************************!
!> Set up a property error object based on a file and a user-provided message.
!******************************************************************************!
    function propertyError_initWithFile(filename, lineNumber, position, context, message, level) result(this)
        character(*), intent(in) :: filename
        integer, intent(in) :: lineNumber
        integer, intent(in) :: position
        character(*), intent(in) :: context
        character(*), intent(in) :: message
        integer, intent(in), optional :: level
        type(FPropertyError) :: this
!------
        character(:), allocatable :: icontext, space
        integer :: ilevel
!------
        this%filename = filename
        this%line = lineNumber

        ! Get the context (file name and possibly line number)
        if (position > 0) then
            allocate(character(4+position) :: space)
            space(:) = " "
            space(len(space):len(space)) = "^"
        else
            space = ""
        end if
        if (this%line == 0) then
            icontext = bold(this%filename // ":") // new_line(icontext) // &
                new_line(icontext) // "    " // context // new_line(icontext) // space
        else
            icontext = bold(this%filename // ":" // this%line // ":") // &
                new_line(icontext) // new_line(icontext) // "    " // &
                context // new_line(icontext) // space
        end if

!        ! Set the content if available
!        if (allocated(file%lastLine)) then
!            context = context // new_line("") // new_line("") // " " // &
!                file%lastLine(:file%lastLineLength) // new_line("")
!        end if

        ! Set the message level
        if (present(level)) then
            ilevel = level
        else
            ilevel = MESSAGE_LEVEL_ERROR
        end if

        ! Call the super-class' constructor
        this%FExceptionDescription = &
            FExceptionDescription(message=message, &
                                  context=icontext, &
                                    level=ilevel)
    end function
!******************************************************************************!
!> Get a short description of a property error object.
!******************************************************************************!
    pure function propertyError_string(this) result(string)
        class(FPropertyError), intent(in) :: this
        character(:), allocatable :: string
!------
        if (this%line == 0) then
            string = this%filename // ": " // this%FExceptionDescription%string()
        else
            string = this%filename // ":" // this%line // ": " // this%FExceptionDescription%string()
        end if
    end function
