!******************************************************************************!
!                              regex_match module
!______________________________________________________________________________!
!> Procedures used when using a regex object for pattern matching.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2016 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!


!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_single_position_string(this, subject, position, stat)
        class(FRegex), intent(in) :: this
        type(FString), intent(in) :: subject
        integer, intent(out) :: position
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
!------
        call match_single_internal(this, subject%string(), position, stat=istat)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_single_substring_string(this, subject, substring, stat)
        class(FRegex), intent(in) :: this
        type(FString), intent(in) :: subject
        type(FString), allocatable, intent(out) :: substring
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        integer :: ind
        character(:), allocatable :: isubstring
!------
        call match_single_internal(this, subject%string(), ind, isubstring, stat_)
        substring = FString(isubstring)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_single_both_string(this, subject, position, substring, stat)
        class(FRegex), intent(in) :: this
        type(FString), intent(in) :: subject
        integer, intent(out) :: position
        type(FString), allocatable, intent(out) :: substring
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        character(:), allocatable :: isubstring
!------
        call match_single_internal(this, subject%string(), position, isubstring, stat_)
        substring = FString(isubstring)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_single_position_character(this, subject, position, stat)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        integer, intent(out) :: position
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
!------
        call match_single_internal(this, subject, position, stat=istat)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_single_substring_character(this, subject, substring, stat)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        character(:), allocatable, intent(out) :: substring
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        integer :: ind
!------
        call match_single_internal(this, subject, ind, substring, stat_)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_single_both_character(this, subject, position, substring, stat)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        integer, intent(out) :: position
        character(:), allocatable, intent(out) :: substring
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        call match_single_internal(this, subject, position, substring, stat_)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject string object that matches a
!> regex.
!******************************************************************************!
    subroutine match_single_internal(this, subject, position, substring, stat)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        integer, intent(out), optional :: position
        character(:), allocatable, intent(out), optional :: substring
        type(FException), intent(out), optional :: stat
!------
        integer :: rc, ind, length
        integer(c_int), dimension(:), allocatable :: ovector
        type(FException) :: stat_
!------

        ind = 0

        body: block

            ! Make sure the regex is useable
            if (.not.c_associated(this%re) .or. &
                .not.c_associated(this%study)) then

                call stat_%raise("trying to use an uninitialised regex object.")
                exit body
            end if

            ! Try to execute the regex
            allocate(ovector(this%ovecsize))
            ovector = 0
            rc = pcre_exec(        &
                this%re,           &
                this%study,        &
                subject,           &
                len(subject),      &
                this%offset,       &
                this%execOptions,  &
                ovector,           &
                size(ovector))

            ! Error check
            if (rc < 0) then
                call stat_%raise(fcre_error_message(rc))
                exit body
            end if

            ! Position of the matched substring
            ind = ovector(1)+1
            length = ovector(2)-ovector(1)-1

            ! Get the matched substring if needed
            if (present(substring)) substring = subject(ind:ind+length)
        end block body

        if (present(position)) position = ind

        ! Report any issue if required
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_all_position_string(this, subject, position, stat)
        class(FRegex), intent(in) :: this
        type(FString), intent(in) :: subject
        integer, dimension(:), allocatable, intent(out) :: position
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        call match_all_internal(this, subject%string(), position, stat=stat_)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_all_substring_string(this, subject, substring, stat)
        class(FRegex), intent(in) :: this
        type(FString), intent(in) :: subject
        type(FString), dimension(:), allocatable, intent(out) :: substring
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
!------
        call match_all_internal(this, subject%string(), substring=substring, stat=istat)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_all_both_string(this, subject, position, substring, stat)
        class(FRegex), intent(in) :: this
        type(FString), intent(in) :: subject
        integer, dimension(:), allocatable, intent(out) :: position
        type(FString), dimension(:), allocatable, intent(out) :: substring
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        call match_all_internal(this, subject%string(), position, substring, stat_)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_all_position_character(this, subject, position, stat)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        integer, dimension(:), allocatable, intent(out) :: position
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
!------
        call match_all_internal(this, subject, position, stat=istat)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_all_substring_character(this, subject, substring, stat)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        type(FString), dimension(:), allocatable, intent(out) :: substring
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
!------
        call match_all_internal(this, subject, substring=substring, stat=istat)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get the first substring of a given subject character scalar that matches a
!> regex.
!******************************************************************************!
    subroutine match_all_both_character(this, subject, position, substring, stat)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        integer, dimension(:), allocatable, intent(out) :: position
        type(FString), dimension(:), allocatable, intent(out) :: substring
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
!------
        call match_all_internal(this, subject, position, substring, istat)

        ! Report any issue if required
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Match a regular expression to a character variable. If no match is found,
!> the index and lengths arrays are not allocated.
!******************************************************************************!
    subroutine match_all_internal(this, subject, position, substring, stat)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        integer, dimension(:), allocatable, intent(out), optional :: position
        type(FString), dimension(:), allocatable, intent(out), optional :: substring
        type(FException), intent(out), optional :: stat
!------
        integer, dimension(:), allocatable :: length, ind, ovector
        integer :: i, n, rc
        integer(c_int) :: offset
        type(FException) :: istat
!------

        body: block

            ! Make sure the regex is useable
            if (.not.c_associated(this%re) .or. &
                .not.c_associated(this%study)) then

                call istat%raise("trying to use an uninitialised regex object.")
                allocate(position(0))
                exit body
            end if

            ! Try to execute the regex
            allocate(ovector(this%ovecsize))
            ovector = 0
            offset = 0
            rc = pcre_exec(        &
                this%re,           &
                this%study,        &
                subject,           &
                len(subject),      &
                offset,            &
                this%execOptions,  &
                ovector,           &
                size(ovector))

            ! Error check
            if (rc < 0) then
                call istat%raise(fcre_error_message(rc))
                allocate(position(0))
                exit body
            end if

            ! Get all the matches
            allocate(ind(len(subject)))
            ind = 0
            allocate(length(len(subject)))
            length = 0
            n = 0
            do while(rc > 0 .and. offset < len(subject)-1)
                n = n + 1

                ! Position of the matched substring
                ind(n) = ovector(1)+1
                length(n) = ovector(2)-ovector(1)-1

                ! Change the offset
                offset = ovector(2)

                rc = pcre_exec(        &
                    this%re,           &
                    this%study,        &
                    subject,           &
                    len(subject),      &
                    offset,            &
                    this%execOptions,  &
                    ovector,           &
                    size(ovector))
            end do

            ! Get the positions of the matched substrings if needed
            if (present(position)) position = ind(:n)

            ! Get the matched substrings if needed
            if (present(substring)) then
                allocate(substring(n))
                do i=1, n
                    substring(i) = subject(ind(i):ind(i)+length(i))
                end do
            end if
        end block body

        ! Report any issue if required
        if (present(stat)) call stat%transfer(istat)
    end subroutine
