!******************************************************************************!
!                         mod_numericalFunction module
!------------------------------------------------------------------------------!
!> `[[FNumericalFunction(type)]]` derived type and type-bound procedures, for
!> numerical functions evaluation.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_numericalFunction
    use iso_c_binding

    use core, only: e, wp, PI, TWOPI, operator(.approx.)
    use ext_character, only: operator(//)
    use com_fft, only: fft_1d_rc, fft_1d_cc, ifft_1d_cc

    use mod_string,     only: FString
    use mod_file,       only: FFile, FFileError
    use mod_exception,  only: FException, MESSAGE_LEVEL_WARNING
    use mod_expression, only: FExpression
    use mod_numericalDictionary, only: FNumericalDictionary

    implicit none
    private
    save

    integer, parameter, public :: INTERPOLATION_LINEAR    = 1
    integer, parameter, public :: INTERPOLATION_QUADRATIC = 2
    integer, parameter, public :: INTERPOLATION_SPLINE    = 3

    type, public :: FNumericalFunction
        private
        real(wp), dimension(:), allocatable, public :: y
        complex(wp), dimension(:), allocatable :: y_complex
        real(wp), dimension(:), allocatable :: yp
        type(FException) :: status
        real(e), dimension(2) :: range = 0
        real(e) :: dx = 0
        character(:), allocatable :: expression
        character(:), allocatable :: variable
        integer :: interpolationMode = INTERPOLATION_LINEAR
    contains
        ! Metadata
        procedure, public :: lowerBound
        procedure, public :: upperBound
        procedure, public :: precision => numericalFunction_precision

        ! Function comparison
        procedure, private :: compare_equal
        generic, public :: operator(==) => compare_equal
        procedure, private :: compare_different
        generic, public :: operator(/=) => compare_different

        ! Function operators
        procedure, private :: addFunction
        generic, public :: operator(+) => addFunction
        procedure, private :: substractFunction
        generic, public :: operator(-) => substractFunction
        procedure, private :: multiply_scalar
        procedure, private, pass(this) :: scalar_multiply
        generic, public :: operator(*) => multiply_scalar, scalar_multiply

        procedure, public :: isSimilar
        procedure, public :: getMaximumDifference
        procedure, public :: getSquaredDifference

        ! Calculations related to the scalar product
        procedure, public :: getScalarProduct
        procedure, public :: norm
        procedure, public :: normalise
        procedure, public :: scale
        procedure, public :: integral

        ! Function evaluation
        procedure, public :: approxf

        ! I/O
        procedure, public :: string
        procedure, public :: saveToFile

        ! Function transformations
        procedure, public :: getDerivative
        procedure, public :: getFFT
        procedure, public :: getRFFT
        procedure, public :: getIFFT

        ! Helper functions, for internal use
        procedure, private :: setExpression
    end type

    interface FNumericalFunction
        module procedure numericalFunction_initWithExpression, numericalFunction_initWithFile
    end interface

    public :: numericalFunction_initWithExpression
contains
!******************************************************************************!
!> Get the lower bound of the domain of a numerical function.
!******************************************************************************!
    pure function lowerBound(this)
        class(FNumericalFunction), intent(in) :: this
        real(e) :: lowerBound
!------
        lowerBound = this%range(1)
    end function
!******************************************************************************!
!> Get the upper bound of the domain of a numerical function.
!******************************************************************************!
    pure function upperBound(this)
        class(FNumericalFunction), intent(in) :: this
        real(e) :: upperBound
!------
        upperBound = this%range(2)
    end function
!******************************************************************************!
!> Get the precision of the internal representation of a numerical function.
!******************************************************************************!
    pure function numericalFunction_precision(this) result(precision)
        class(FNumericalFunction), intent(in) :: this
        real(e) :: precision
!------
        precision = this%dx
    end function
!******************************************************************************!
!> Rescale a numerical function. The transformation is
!> $$ f \rightarrow \lambda\cdot f\ .$$
!******************************************************************************!
    subroutine scale(this, lambda)
        class(FNumericalFunction), intent(inout) :: this
        real(e), intent(in) :: lambda
!------
        this%y = this%y * lambda
        if (allocated(this%y_complex)) then
            this%y_complex = this%y_complex * lambda
        end if
    end subroutine
!******************************************************************************!
!> Multiply a function by a scalar number. The transformation is
!> $$ f \rightarrow \lambda\cdot f\ .$$
!******************************************************************************!
    pure function multiply_scalar(this, lambda) result(f)
        class(FNumericalFunction), intent(in) :: this
        real(e), intent(in) :: lambda
        type(FNumericalFunction) :: f
!------
        f = this
        f%y = f%y * lambda
        if (allocated(f%y_complex)) then
            f%y_complex = f%y_complex * lambda
        end if
    end function
!******************************************************************************!
!> Multiply a function by a scalar number. The transformation is
!> $$ f \rightarrow \lambda\cdot f\ .$$
!******************************************************************************!
    pure function scalar_multiply(lambda, this) result(f)
        real(e), intent(in) :: lambda
        class(FNumericalFunction), intent(in) :: this
        type(FNumericalFunction) :: f
!------
        f = this
        f%y = f%y * lambda
        if (allocated(f%y_complex)) then
            f%y_complex = f%y_complex * lambda
        end if
    end function
!******************************************************************************!
!> Normalise a numerical function.
!>
!> The transformation is $$ f \rightarrow \lambda\cdot\frac{f}{|f|} $$. The
!> parameter $$\lambda$$ is optional. If it is not present, the function is
!> normalised to 1.
!******************************************************************************!
    subroutine normalise(this, lambda)
        class(FNumericalFunction), intent(inout) :: this
        real(e), intent(in), optional :: lambda
!------
        real(e) :: n, t
!------
        ! Set the scaling factor
        if (present(lambda)) then
            t = lambda
        else
            t = 1
        end if

        ! Calculate the norm
        n = this%norm()

        ! Rescale the function
        if (n /= 0) then
            this%y(:) = t * this%y(:) / n
            if (allocated(this%y_complex)) then
                this%y_complex = t * this%y_complex / n
            end if
        end if
    end subroutine
!******************************************************************************!
!> Calculate the norm of a numerical function. The norm is defined by
!> \( |f| = \sqrt{\<f,f\>} = \sqrt{\int_a^b (f(x)^2)\ dx} \).
!******************************************************************************!
    pure function norm(this)
        class(FNumericalFunction), intent(in) :: this
        real(e) :: norm
!------
        integer :: n
!------
        n = size(this%y)
        norm = (dot_product(this%y, this%y) - (this%y(1)**2 + this%y(n)**2) / 2) * this%dx
        norm = sqrt(norm)
    end function
!******************************************************************************!
!> Calculate the integral of a numerical function.
!> The integral is defined by \( I = \int_a^b f(x)\ dx \).
!******************************************************************************!
    pure function integral(this) result(I)
        class(FNumericalFunction), intent(in) :: this
        real(e) :: I
!------
        integer :: n
!------
        n = size(this%y)
!------
!        I = (sum(this%y(2:n-1)) + (this%y(1) + this%y(n))/2) * this%dx
!------
        I = (2*sum(this%y(2:n-2:2)) + 4*sum(this%y(1:n-1:2)) + (this%y(1) + this%y(n))/2) * &
            this%dx / 3.0_e
!------
    end function

    function isSimilar(this, to) result(test)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(in) :: to
        logical :: test
!------
        test = .false.
!------
        if (.not.allocated(this%y) .or..not.allocated(to%y)) return

        if (size(this%y) /= size(to%y)) return

        if (.not. (this%dx .approx. to%dx)) return

        if (.not. (this%range(1) .approx. to%range(1))) return

        if (.not. (this%range(2) .approx. to%range(2))) return

        test = .true.
    end function

    subroutine getScalarProduct(this, f, p)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(in) :: f
        real(e), intent(out) :: p
!------
        integer :: n
!------
        p = 0
        if (.not.this%isSimilar(f)) return
        n = size(this%y)
        p = dot_product(this%y, f%y)
        p = p - ((this%y(1)*f%y(1)) + (this%y(n)*f%y(n))) / 2
        p = p * this%dx
    end subroutine

    subroutine getSquaredDifference(this, f, d)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(in) :: f
        real(e), intent(out) :: d
!------
        integer :: i
!------
        d = 0
        if (.not.this%isSimilar(f)) return

        do i=1, size(this%y)
            d = d + (this%y(i)-f%y(i))**2
        end do
        d = d  * this%dx
    end subroutine

    subroutine getMaximumDifference(this, f, d)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(in) :: f
        real(e), intent(out) :: d
!------
        integer :: i
!------
        d = 0
        if (.not.this%isSimilar(f)) return

        do i=1, size(this%y)-1
            if (abs(this%y(i)-f%y(i)) > d) d = abs(this%y(i)-f%y(i))
        end do
    end subroutine

    pure function string(this)
        class(FNumericalFunction), intent(in) :: this
        character(:), allocatable :: string
!------
        if (allocated(this%expression)) then
            string = this%expression
        else
            string = ""
        end if
    end function
!******************************************************************************!
!> Set the values of a numerical function using an expression object
!******************************************************************************!
    subroutine setExpression(this, expression, deriv)
        class(FNumericalFunction), intent(inout) :: this
        type(FExpression), intent(inout) :: expression
        type(FExpression), intent(inout), optional :: deriv
!------
        integer :: i
        real(e) :: x
!------
        call expression%simplify

        do i=1, size(this%y)
            x = this%range(1) + (i-1)*this%dx

            ! Calculate the value of the function at x
            call expression%setVariable(this%variable, x)
            call expression%eval(this%y(i))

            ! Calculate the derivative
            if (present(deriv)) then
                call deriv%setVariable(this%variable, x)
                call deriv%eval(this%yp(i))
            end if
        end do
    end subroutine
!******************************************************************************!
!> Initialise a numerical function object with an analytical expression
!******************************************************************************!
    function numericalFunction_initWithExpression(expression, variable, range, &
        points, mode, parameters) result(this)

        character(*), intent(in) :: expression
        character(*), intent(in) :: variable
        real(e), dimension(2), intent(in) :: range
        integer, intent(in) :: points
        character(*), intent(in), optional :: mode
        type(FNumericalDictionary), intent(in), optional :: parameters
        type(FNumericalFunction) :: this
!------
        character(:), allocatable :: mode_
        type(FExpression) :: interpreter, derivInterpreter
!------
        ! The representation of the function cannot have a negative number of points
        if (points < 0) call this%status%raise("Negative point count.")

        ! Check the range consistency
        if (range(1) >  range(2)) call this%status%raise("Invalid range")
        if (range(1) /= range(1)) call this%status%raise("Invalid lower bound")
        if (range(2) /= range(2)) call this%status%raise("Invalid upper bound")

        ! Set the range
        this%range = range

        ! Allocate the internal representation
        allocate(this%y(points))
        this%y = 0
        allocate(this%yp(points))
        this%yp = 0

        ! Set the internal representation
        this%dx = (range(2)-range(1)) / (points-1)

        ! Set the interpreters
        this%variable = variable
        this%expression = expression
        interpreter = FExpression(expression)
        if (present(parameters)) then
            call interpreter%setVariables(parameters)
        end if

        if (present(mode)) then
            mode_ = mode
        else
            mode_ = ""
        end if

        ! Set the internal representation depending on the interpolation mode
        select case(mode_)
            case ("spline", "cubic")
                call interpreter%getDerivative(variable, derivInterpreter)
                if (present(parameters)) then
                    call derivInterpreter%setVariables(parameters)
                end if
                call this%setExpression(interpreter, derivInterpreter)
                this%interpolationMode = INTERPOLATION_SPLINE
            case ("quadratic")
                call interpreter%getDerivative(variable, derivInterpreter)
                if (present(parameters)) then
                    call derivInterpreter%setVariables(parameters)
                end if
                call this%setExpression(interpreter, derivInterpreter)
                this%interpolationMode = INTERPOLATION_QUADRATIC
            case ("linear")
                call this%setExpression(interpreter)
                this%interpolationMode = INTERPOLATION_LINEAR
            case default
                call this%setExpression(interpreter)
                this%interpolationMode = INTERPOLATION_LINEAR
        end select
    end function
!******************************************************************************!
!> Get the derivative of a numerical function
!******************************************************************************!
    subroutine getDerivative(this, deriv, parameter, err)
        class(FNumericalFunction), intent(in) :: this
        type(FNumericalFunction), intent(out) :: deriv
        type(FNumericalDictionary), intent(in), optional :: parameter
        type(FException), intent(out), optional :: err
!------
        type(FException) :: err_
!------
        deriv = this

        ! Check whether we have an analytical expression for the function
        if (allocated(this%expression)) then

            ! If we do not, derive the function analytically
            if (present(parameter)) then
                call getExactDerivative(deriv, parameter, err_)
            else
                call getExactDerivative(deriv, err=err_)
            end if
        else

            ! If we do not, derive the function numerically
            call getNumericalDerivative(deriv, err_)
        end if

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the derivative of a numerical function using its analytical form
!******************************************************************************!
    subroutine getExactDerivative(this, parameter, err)
        type(FNumericalFunction), intent(inout) :: this
        type(FNumericalDictionary), intent(in), optional :: parameter
        type(FException), intent(out), optional :: err
!------
        type(FExpression) :: expr, expr_deriv
!------
        ! Derive the expression
        expr = FExpression(this%expression)
        call expr%getDerivative(this%variable, expr_deriv)
        if (present(parameter)) then
            call expr_deriv%setVariables(parameter)
        end if
        this%expression = expr_deriv%string()

        ! Set the function expression to the derivative
        call this%setExpression(expr_deriv)
    end subroutine
!******************************************************************************!
!> Get the derivative of a numerical function using its numerical form
!> NOT YET IMPLEMENTED
!******************************************************************************!
    subroutine getNumericalDerivative(this, err)
        class(FNumericalFunction), intent(inout) :: this
        type(FException), intent(out) :: err
!------
!------

    end subroutine
!******************************************************************************!
!> Initialise a numerical function object with the content of a data file
!******************************************************************************!
    function numericalFunction_initWithFile(filename, stat) result(this)
        character(*), intent(in) :: filename
        type(FException), intent(out), optional :: stat
        type(FNumericalFunction) :: this
!------
        type(FFile) :: file
        type(FException) :: stat_
        integer :: i, iostat, p, c
        real(e) :: x, re, im
        character(1000) :: line
        real(e), dimension(3) :: val
!------
        body: block

!------ Prepare the data structure
            ! Open the file
            file = FFile(filename, "r", stat_)
            if (stat_ /= 0) exit body

            ! Get the first non-comment line
            line = ""
            do while(line == "")
                call file%getNextLine(line, stat_)
                c = index(line, "#")
                if (c > 0) line(c:) = ""
                if (stat_ /= 0) exit body
            end do

            ! Get the number of numbers per line
            read(line,*,iostat=iostat) val(:3)
            if (iostat /= 0) then
                read(line,*,iostat=iostat) val(:2)
                if (iostat /= 0) then
                    read(line,*,iostat=iostat) val(:1)
                    if (iostat /= 0) then
                        call stat_%raise(&
                            FFileError(file, "Wrong data format."))
                        exit body
                    else
                        p = 1
                    end if
                else
                    p = 2
                end if
            else
                p = 3
            end if

            ! Count the number of lines
            call file%getNextLine(line, stat_)
            i = 1
            do while(stat_ == 0)
                i = i + 1
                call file%getNextLine(line, stat_)
            end do
            call stat_%discard
            call file%close

            ! Allocate the internal representation
            if (p > 2) then
                allocate(this%y_complex(i))
                this%y_complex = 0
            end if
            allocate(this%y(i))
            this%y = 0
            this%range(1) = huge(0.0_e)
            this%range(2) = -huge(0.0_e)

!------ Read data from file

            ! Get the first non-comment line
            line = ""
            do while(line == "")
                call file%getNextLine(line, stat_)
                c = index(line, "#")
                if (c > 0) line(c:) = ""
            end do

            ! Get the data from the string array
            i = 0
            do while(stat_ == 0)
                i = i + 1
                if (p == 2) then
                    read(line,*,iostat=iostat) x, this%y(i)
                else if (p == 3) then
                    read(line,*,iostat=iostat) x, re, im
                    this%y_complex(i) = re + (0.0_e,1.0_e) * im
                    this%y(i) = abs(this%y_complex(i))
                else if (p == 4) then
                    read(line,*,iostat=iostat) x, this%y(i), re, im
                    this%y_complex(i) = re + (0.0_e,1.0_e) * im
                end if

                if (iostat /= 0) then
                    call stat_%raise("Error while trying to read file "//file%fullName()//":"//i//": "//trim(line))
                    exit body
                end if

                ! Update the range
                if (x < this%range(1)) this%range(1) = x
                if (x > this%range(2)) this%range(2) = x

                ! Get next line
                call file%getNextLine(line, stat_)
            end do
            call stat_%discard

            this%dx = (this%range(2) - this%range(1)) / (size(this%y)-1)
        end block body

        if (present(stat)) call stat%transfer(stat_)
    end function
!******************************************************************************!
!> Check whether two numerical function objects are similar
!******************************************************************************!
    function compare_equal(this, to) result(identical)
        use ieee_arithmetic
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(in) :: to
        logical :: identical
!------
        integer :: i
!------

        body: block
            identical = .true.

            if (allocated(this%y) .neqv. allocated(to%y)) then
                identical = .false.
                exit body
            end if

            if (.not.all(this%range.approx.to%range)) then
                identical = .false.
                exit body
            end if

            if (.not.(this%dx.approx.to%dx)) then
                identical = .false.
                exit body
            end if

            ! Stop here if there is already a difference
            if (.not.identical) exit body

            ! Compare the function values
            if (allocated(this%y)) then

                do i=1, size(this%y)
                    if (.not.(this%y(i).approx.to%y(i)) .and. .not. &
                        (ieee_is_nan(this%y(i)) .and. ieee_is_nan(to%y(i)))) then

                        identical = .false.
                        exit body
                    end if
                end do
            end if
        end block body
    end function
!******************************************************************************!
!> Check whether two numerical function objects are different
!******************************************************************************!
    function compare_different(this, to) result(different)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(in) :: to
        logical :: different
!------
        different = .not.(this == to)
    end function
!******************************************************************************!
!> Get an numerical approximation of a function at a given point
!******************************************************************************!
    function approxf(this, x)
        class(FNumericalFunction), intent(in) :: this
        real(wp), intent(in) :: x
        real(wp) :: approxf
!------
        integer :: n0
        real(wp) :: x0, y0, x1, y1, t, a, b, c, dy0, dy1
!------
        approxf = 0

        ! Basic range check
        if (x<this%range(1) .or. x> this%range(2)) return

        n0 = floor((x-this%range(1))/this%dx)

        x0 = this%range(1)+n0*this%dx
        y0 = this%y(n0+1)

        x1 = this%range(1)+(n0+1)*this%dx
        y1 = this%y(n0+2)

        t = (x-x0) / (x1-x0)

        select case(this%interpolationMode)
            case (INTERPOLATION_SPLINE)

                ! Cubic spline interpolation
                a =  this%yp(n0+1) * (x1-x0) - (y1-y0)
                b = -this%yp(n0+2) * (x1-x0) + (y1-y0)

                approxf = (1-t)*y0 + t*y1 + t*(1-t)*(a*(1-t)+b*t)

            case (INTERPOLATION_QUADRATIC)

                ! Quadratic interpolation
                dy0 = this%yp(n0+1)
                dy1 = this%yp(n0+2)

                a = (dy1-dy0) / (2*this%dx)
                b = dy0 - 2 * a * x0
                c = y0 - a*x0*x0 - b*x0

                approxf = a*x*x + b*x + c

            case default

                ! Linear interpolation
                approxf = (1-t)*y0 + t*y1
        end select
    end function
!******************************************************************************!
!> Save a numerical function objects to a text file
!******************************************************************************!
    subroutine saveToFile(this, filename, stat)
        class(FNumericalFunction), intent(in) :: this
        character(*), intent(in) :: filename
        type(FException), intent(out), optional :: stat
!------
        type(FFile) :: file
        type(FException) :: stat_
        integer :: i
        real(wp) :: x
!------

        if (.not.allocated(this%y)) call stat_%raise("Trying to save an empty function to file "//filename)

        ! Create the file
        if (stat_ == 0) then
            file = FFile(filename, "w")
        end if

        ! Write the tabulated function to the file
        if (stat_ == 0) then
            do i=1, size(this%y)
                x = this%range(1) + (i-1)*this%dx
                if (allocated(this%y_complex)) then
                    call file%writeLine(x//"    "//this%y(i)//" "//real(this%y_complex(i), kind=e)//" "//aimag(this%y_complex(i)))
                else
                    call file%writeLine(x//"    "//this%y(i))
                end if
            end do
        end if

        ! Close the file
        call file%close
    end subroutine
!******************************************************************************!
!> Calculate the Discrete Fourier Transform of a numerical function
!******************************************************************************!
    subroutine getRFFT(this, fft)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(out) :: fft
!------
        integer :: i, n
!------
        n = size(this%y)

        ! Calculate the Fourier transform
        if (allocated(this%y_complex)) then
            call fft_1d_cc(this%y_complex, fft%y_complex)
        else
            call fft_1d_rc(this%y, fft%y_complex)
        end if
        fft%y_complex = fft%y_complex * this%dx

        ! Calculate the real part
        allocate(fft%y(n))
        do i=1, n
            fft%y(i) = abs(fft%y_complex(i))
        end do

        ! Calculate the boundaries and resolution of the Fourier transform
        fft%dx = TWOPI / (this%dx * size(this%y))
        if (nint(n*1.0_e/2) == n*1.0_e/2) then
            ! n is even
            fft%range(1) = -(size(fft%y)-2)*fft%dx/2
            fft%range(2) = fft%range(1) + (size(fft%y)-1) * fft%dx
        else
            ! n is odd
            fft%range(1) = -(size(fft%y)-1)*fft%dx/2
            fft%range(2) = fft%range(1) + (size(fft%y)-1) * fft%dx
        end if
    end subroutine
!******************************************************************************!
!> Calculate the Discrete Fourier Transform of a numerical function
!******************************************************************************!
    subroutine getFFT(this, fft)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(out) :: fft
!------
        integer :: i, n
        complex(e), dimension(:), allocatable :: y
!------
        n = size(this%y)
        allocate(y(size(this%y)))

        if (allocated(this%y_complex)) then
            y(:) = this%y_complex
        else
            y(:) = this%y
        end if

        ! Calculate the Fourier transform
        call fft_1d_cc(y, fft%y_complex)
        fft%y_complex = fft%y_complex * this%dx

        ! Calculate the real part
        allocate(fft%y(n))
        do i=1, n
            fft%y(i) = abs(fft%y_complex(i))
        end do

        ! Calculate the boundaries and resolution of the Fourier transform
        fft%dx = TWOPI / (this%dx * size(this%y))
        if (nint(n*1.0_e/2) == n*1.0_e/2) then
            ! n is even
            fft%range(1) = -(size(fft%y)-2)*fft%dx/2
            fft%range(2) = fft%range(1) + (size(fft%y)-1) * fft%dx
        else
            ! n is odd
            fft%range(1) = -(size(fft%y)-1)*fft%dx/2
            fft%range(2) = fft%range(1) + (size(fft%y)-1) * fft%dx
        end if
    end subroutine
!******************************************************************************!
!> Calculate the Inverse Discrete Fourier Transform of a numerical function.
!******************************************************************************!
    subroutine getIFFT(this, ifft)
        class(FNumericalFunction), intent(inout) :: this
        class(FNumericalFunction), intent(out) :: ifft
!------
        complex(c_double_complex), dimension(:), allocatable :: c
        integer :: i, df, n
!------
        df = size(this%y)
        ifft%dx = 2.0_e*PI / (this%dx * df)

        c = this%y_complex / ifft%dx

        call ifft_1d_cc(c, ifft%y_complex)

        n = size(ifft%y_complex)
        allocate(ifft%y(n))
        do i=1, n
            ifft%y(i) = abs(ifft%y_complex(i))
        end do

        ifft%range(1) = 0.0
        ifft%range(2) = (size(ifft%y)-1)*ifft%dx
    end subroutine
!******************************************************************************!
!> Add two numerical functions.
!******************************************************************************!
    function addFunction(this, f) result(g)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(in) :: f
        type(FNumericalFunction) :: g

        if (.not.this%isSimilar(f)) return
        g%range = this%range
        g%dx = this%dx
        g%y = this%y + f%y
        if (allocated(this%y_complex) .and. allocated(f%y_complex)) g%y_complex = this%y_complex + f%y_complex
    end function
!******************************************************************************!
!> Substract two numerical functions.
!******************************************************************************!
    function substractFunction(this, f) result(g)
        class(FNumericalFunction), intent(in) :: this
        class(FNumericalFunction), intent(in) :: f
        type(FNumericalFunction) :: g
!------
        if (.not.this%isSimilar(f)) return
        g%range = this%range
        g%dx = this%dx
        g%y = this%y - f%y
        if (allocated(this%y_complex) .and. allocated(f%y_complex)) g%y_complex = this%y_complex - f%y_complex
    end function
end module