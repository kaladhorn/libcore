    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cfft1f(n, inc, c, lenc, wsave, lensav, work, lenwrk, ier)
        implicit none
        include 'kinds.inc'
        integer n, inc, lenc, lensav, lenwrk, ier
        complex (cwp) c(lenc)
        real (rwp) wsave(lensav), work(lenwrk)
        
        integer :: iw1
        
        ier = 0

        if (lenc<inc*(n-1)+1) then
            ier = 1
            call xerfft('CFFT1F ', 4)
        else if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('CFFT1F ', 6)
        else if (lenwrk<2*n) then
            ier = 3
            call xerfft('CFFT1F ', 8)
        end if

        if (n==1) return

        iw1 = n + n + 1
        call c1fm1f(n, inc, c, work, wsave, wsave(iw1), wsave(iw1+1))
    end subroutine
