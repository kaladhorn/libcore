!******************************************************************************!
!                          expression_eval
!------------------------------------------------------------------------------!
! Procedures used when using an expression's bytecode to evaluate its value.
!
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2016 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
    subroutine eval_simple(this, result, stat)
        class(FExpression), intent(inout) :: this
        real(wp), intent(out) :: result
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        ! Try to run the bytecode
        call this%bytecode%run(result, stat_)

        ! Handle errors
        if (stat_ /= 0) then

        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Evaluate an expression after setting a variable value. This is the function
!> version, with no error status. The program will abort if an error happens
!> when the bytecode is run.
!******************************************************************************!
    function evalf(this) result(result)
        class(FExpression), intent(inout) :: this
        real(wp) :: result
!------
        ! Try to run the bytecode
        call this%bytecode%run(result)

    end function
!******************************************************************************!
!> Evaluate an expression after setting a variable value.
!******************************************************************************!
    subroutine eval_var(this, name, value, result, stat)
        class(FExpression), intent(inout) :: this
        character(*), intent(in) :: name
        real(wp), intent(in) :: value
        real(wp), intent(out) :: result
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        integer :: i
!------
        ! Set the variable value
        setup: block
            do i=1, this%nVariables
                if (this%variable(i)%name == name) then
                    this%bytecode%data(i) = value
                    exit setup
                end if
            end do
            call stat_%raise("Variable '" // name // "' is not present in the expression.", MESSAGE_LEVEL_WARNING)
        end block setup

        ! Try to run the bytecode
        call this%bytecode%run(result, stat_)

        ! Handle errors
        if (stat_ /= 0) then

        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Evaluate a function. This version accepts values for one independent
!> variable. The expression is evaluated once for each input value, and the
!> results are stored in the output array. This subroutine is designed to have
!> better performances than the scalar versions.
!>
!> The optional parameter `blocksize` controls the number of iterations in
!> loops in the subroutine, which can affect performance.
!******************************************************************************!
    subroutine eval_vector(this, name, x, result, blockSize, stat)
        class(FExpression),     intent(inout) :: this !< Expression to evaluate
        character(*),           intent(in)    :: name !< Name of the independent variable
        real(wp), dimension(:), intent(in)    :: x    !< Initial value of the independent variable
        real(wp), dimension(:), allocatable, intent(out) :: result !< Results
        integer, intent(in), optional :: blockSize
        type(FException), intent(out), optional :: stat !< Error status
!------
        type(FException) :: stat_
        integer :: variable_index, i
!------

        ! Get the index of the variable
        setup: block
            variable_index = 0
            do i=1, size(this%variable)
                if (this%variable(i)%name == name) then
                    variable_index = i
                    exit setup
                end if
            end do
            ! Make sure the variable is part of the expression
            call stat_%raise("The variable " // name // &
                " is not present in expression " // this%string())
        end block setup

        ! Run the bytecode
        call this%bytecode%run_vector(variable_index, x, result, blockSize, stat_)

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine eval_vector_linear(this, name, x0, N, dx, result, blockSize, stat)
        class(FExpression), intent(inout) :: this !< Expression to evaluate
        character(*),       intent(in)    :: name !< Name of the independent variable
        real(wp),           intent(in)    :: x0   !< Values of the independent variable
        integer, intent(in) :: N
        real(wp),           intent(in)    :: dx   !<
        real(wp), dimension(:), allocatable, intent(out) :: result !< Results
        integer, intent(in), optional :: blockSize
        type(FException), intent(out), optional :: stat !< Error status
!------
        type(FException) :: stat_
        integer :: variable_index, i
!------

        ! Get the index of the variable
        setup: block
            variable_index = 0
            do i=1, size(this%variable)
                if (this%variable(i)%name == name) then
                    variable_index = i
                    exit setup
                end if
            end do
            ! Make sure the variable is part of the expression
            call stat_%raise("The variable " // name // &
                " is not present in expression " // this%string())
        end block setup

        ! Run the bytecode
        call this%bytecode%run_vector_linear(variable_index, x0, N, dx, result, blockSize, stat_)

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
