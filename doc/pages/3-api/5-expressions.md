title: Using mathematical expressions

# Purpose
This section describes how to use the `[[FExpression(type)]]` derived type from the `[[mod_expression(module)]]` 
module to parse and evaluate mathematical expressions at runtime.

This facility has been designed to be used when a program needs to evaluate mathematical 
expressions, that are not known at compile-time, and that can be provided by the user of the program.


# API description

## Creating expressions
An expression object is created using the constructor `[[FExpression(type)]]`:

```fortran
    type(FExpression) :: expression
    type(FDictionary) :: dict
    type(FString) :: names
    
    expression = FExpression("sin(2*x + p)")
```

In the constructor, the character string is parsed and transformed in an abstract syntax tree.
This tree is used to perform simple, safe mathematical "optimisations".
For example, all the multiplication by 1 are removed, and the branches of the tree might be rearranged.
The transformed tree is strictly equivalent to the initial one from a mathematical point of view.
In particular, no assumption is made about the possible value of the variables.

Then, the tree is transformed in a sequence of instructions (bytecode).
The bytecode undergoes a series of small optimisation passes, during which instructions are moved, and redundant instructions are removed.
The bytecode enables fast evaluation of the expression, whilst the syntax tree can be used for other operations, such as the calculation of derivatives.

## Using variables
Variables in an expression  can be set one at a time using `[[FExpression(type):setVariable(bound)]]`.
Several variables can be set at once if they are put in a `[[FDictionary]]` object, using `[[FExpression(type):setVariables(bound)]]`.
Whilst using a dictionary add one step if the variables are only set once, they are an easy way to set assign given values to variables in large sets of expressions.

```fortran
    call expression%setVariable("p", 2.0_wp)
    call expression%setVariable("x", PI/2)
    
    ! The following is strictly equivalent
    call dict%set("p", 2.0_wp)
    call dict%set("x", PI/2)
    call expression%setVariables(dict)
```

The values of the variables can be queried back using the `[[FExpression(type):getVariables(bound)]]` procedure, which returns a dictionary containing the names and values of the variables.
The names alone can be obtained using `[[FExpression(type):getVariableNames(bound)]]`.

```fortran
    call expression%getVariables(dict)
    call dict%print 
    ! Prints:
    ! {
    !     p : 2
    !     x : 1.5707963268
    ! }
    
    call expression%getVariableNames(names)
    do i=1, size(variableNames)
        write(*,*) names(i)%string()
    end do
    ! Prints:
    ! p
    ! x
```

## Evaluating expressions
An expression can be evaluated using its syntax tree, or its bytecode. Both methods should produce identical results.

The syntax tree is used when `[[FExpression(type):interpret(bound)]]` is called. This is slower than using the bytecode, and is mostly useful to check the validity of the bytecode or a derivative.

The `[[FExpression(type):eval(bound)]]` generic provides ways to evaluate an expression using its bytecode.
Its most basic specific subroutine simply evaluates the expression, and raises an error if at least one of the variables is not set.
There are other specific versions for convenience that allow to specify one variable or more when evaluating the expression.
Vector versions are also available, which usually provide much better performances when an expression ha to be evaluated several times. 

The following example shows the different methods, and can be used as a benchmark. Its expression comes from a test case for `FunctionParser`.

```fortran
    program expression_bench
        use core
        
        use mod_expression, only: FExpression
        use mod_timer, only: FTimer
        
        implicit none
        
        type(FExpression) :: f
        type(FTimer) :: timer
        integer :: i, Niter
        real(wp), dimension(:), allocatable :: res
        real(wp) :: x, y, z
        
        ! Parameters
        Niter = 5000000      ! Number of iterations
        allocate(res(Niter)) ! Array to store the results
        
        ! Test expression
        f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
            "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")
        
        ! Variable values
        x = 0.175_wp
        y = 0.110_wp
        z = 0.900_wp
        call f%setVariable("x", x)
        call f%setVariable("y", y)
        call f%setVariable("z", z)
        
        ! Test the compiler translation
        call timer%start
        do i = 1, Niter
        	res(i) = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+ &
                x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))
        end do
        call timer%stop
        write(*,*) "Direct evaluation:", timer%cpuTime()
        
        ! Test the interpreter
        call timer%reset
        call timer%start
        do i = 1, Niter
        	res(i) = f%interpret_f()
        end do
        call timer%stop
        write(*,*) "AST interpretation:", timer%cpuTime()
        
        ! Test the bytecode (scalar version)
        call timer%reset
        call timer%start
        do i = 1, Niter
            call f%eval(res(i))
        end do
        call timer%stop
        write(*,*) "Bytecode (scalar):", timer%cpuTime()
        
        ! Test the bytecode (vector version)
        call timer%reset
        call timer%start
        call f%eval("x", 0.175_wp, Niter, 0.0_e, res)
        call timer%stop
        write(*,*) "Bytecode (vector):", timer%cpuTime()
    end program
```

The results on a test machine (Intel Core i7 4870HQ using a build of GCC 6.0) are shown below.
The graphs show a comparison of the time needed to interpret the function `f` for different methods:

 - the direct evaluation (hard-coded function);
 - the AST interpretation using `[[FExpression(type):interpret(bound)]]`;
 - the bytecode evaluation using scalar values (`[[FExpression(type):eval_simple(bound)]]`);
 - the bytecode evaluation using array values (`[[FExpression(type):eval_vector_linear(bound)]]`);
 - the `FunctionParser` code (available under a BSD license at http://zeus.df.ufcg.edu.br/labfit/functionparser.htm);
 - the `fparser` code (available at http://fparser.sourceforge.net).

The metric is the CPU time for 5000000 evaluation, divided by the time the CPU time used in the hard-coded case with optimisation flags.
the first graph shows the performance of the different implementations without any optimisation flag.
For the second graph, the flags `-Ofast -mtune=native -march=native` were used.

![bench_unoptimised](|media|/expression_unoptimised.png)

![bench_optimised](|media|/expression_optimised.png)

## Calculating derivatives

# Example

This program demonstrates the basic features of the `[[FExpression(type)]]` type. It asks the user for an 
expression and a variable, calculates the derivative of the expression with respect to the variable, 
evaluates both and prints the results.

```fortran
    program basic_expression
        use core
        use ext_character
    
        use mod_expression, only: FExpression
    
        implicit none
    
        type(FExpression) :: expression, derivative
        integer, parameter :: BUFFER_LENGTH = 1000
        character(BUFFER_LENGTH) :: buffer, variable_name
        real(wp) :: variable_value, y, dy
    
        ! Ask the user to provide the definition of the expression
        write(*,*) "Type an expression and press Enter:"
        read(*,'(a)') buffer
    
        ! Setup the expression object
        expression = FExpression(buffer)
    
        ! Ask the user to specify the variable
        write(*,*) "Type the name of the variable and press Enter:"
        read(*,'(a)') variable_name
    
        ! Calculate the derivative
        call expression%getDerivative(variable_name, derivative)
    
        ! Print the derivative
        write(*,*) derivative%string()
    
        ! Ask the user to provide a value
        write(*,*) "Type a value for the variable and press Enter:"
        read(*,*) variable_value
    
        ! Evaluate the function, and print the result
        call expression%setVariable(variable_name, variable_value)
        call expression%eval(y)
        write(*,*) "Value for "//trim(variable_name)//"="//variable_value//": "//y
    
        ! Evaluate the derivative, and print the result
        call derivative%setVariable(variable_name, variable_value)
        call derivative%eval(dy)
        write(*,*) "Value of the derivative for "//trim(variable_name)//"="//variable_value//": "//dy
    
    end program
```
