title: Acknowledgments

This library contains code from:

  - the [JSON-Fortran](http://jacobwilliams.github.io/json-fortran) library to read and write JSON files;
  - the [SuperFastHash](http://www.azillionmonkeys.com/qed/hash.html) function for string hash calculations;
  - the [MurmurHash3](https://github.com/PeterScott/murmur3) function for string hash calculations.

It also uses the following libraries:

  - the [PCRE](http://www.pcre.org) library for regular expressions;
  - the [zlib](http://www.zlib.org) library to read and write gzip-compressed files;
  - the [libbz2](http://www.bzip.org) library to read and write bzip2-compressed files.

Included in this distribution is the Niffty library, which is a modified version of the 
[FFTPACK](http://www.netlib.org/fftpack) library for Fourier transform calculations.

This documentation is built using [FORD](https://github.com/cmacmackin/ford).
