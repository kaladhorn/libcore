#******************************************************************************#
#                              libCore strings                                 #
#______________________________________________________________________________#
#
#  Version 2.0
#
#  Written by Paul Fossati, <paul.fossati@gmail.com>
#  Copyright (c) 2009-2017 Paul Fossati
#------------------------------------------------------------------------------#
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
#     * Redistributions of source code must retain the above copyright notice, #
#       this list of conditions and the following disclaimer.                  #
#                                                                              #
#     * Redistributions in binary form must reproduce the above copyright      #
#       notice, this list of conditions and the following disclaimer in the    #
#       documentation and/or other materials provided with the distribution.   #
#                                                                              #
#     * The name of the author may not be used to endorse or promote products  #
#      derived from this software without specific prior written permission    #
#      from the author.                                                        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#******************************************************************************#
STRING_SOURCES= string/ext_character.f90 string/mod_string.f90                 \
	string/mod_formattedString.f90 string/libsoldout_wrapper.c string/fcre.f90 \
    string/mod_regex.f90 string/pcre_wrappers.c

#******************************************************************************!
# Included files
#******************************************************************************!
EXTRA_DIST+= string/regex_setup.inc string/regex_match.inc                     \
	string/regex_capture.inc  string/regex_replace.inc                         \
	string/characterWithReal.inc

AM_FCFLAGS+= -Istring

#******************************************************************************!
# Dependencies
#******************************************************************************!
string/ext_character.o: string/ext_character.f90 string/characterWithReal.inc  \
	core.mod
ext_character.mod: string/ext_character.o

string/mod_string.o: string/mod_string.f90 core.mod
mod_string.mod: string/mod_string.o

string/mod_formattedString.o: string/mod_formattedString.f90 mod_string.mod    \
	mod_regex.mod core.mod
mod_formattedstring.mod: string/mod_formattedString.o

string/fcre.o: string/fcre.f90
fcre.mod: string/fcre.o

string/mod_regex.o: string/mod_regex.f90 fcre.mod mod_string.mod               \
	mod_exception.mod ext_character.mod mod_dictionary.mod                     \
	mod_stringdictionary.mod string/regex_setup.inc string/regex_match.inc     \
	string/regex_capture.inc  string/regex_replace.inc
mod_regex.mod: string/mod_regex.o
