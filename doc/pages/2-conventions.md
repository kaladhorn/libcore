title: Coding conventions

# Coding conventions

The following principles are followed as much as possible in the source files throughout the library.

## Procedures
  - the subroutines should make error checking possible, usually by having a last argument `err` with the 
    description `type(FError), intent(out), optional`;
  - a function should exist only if it is important to be able to use it in I/O or expressions, and as such 
    it must be as pure as possible (strict purity being often impossible);
  - if an error occurs, functions should return a default value that is explicit in the documentation;
  - in general, side-effects should be minimised.

## Derived types
  - each derived type should have a simple and descriptive name, with a `F`prefix;
  - all the components of a derived type should be `private`;
  - the derived types are expected to have accessors with consistent names. For the getters, there should
    be a subroutine version with proper error handling, and a function version to be used in contexts where 
    purity is more important than error checking. For the component `foo_`, the setter should be `setFoo`, 
    the subroutine getter `getFoo` and the function getter `foo`.
  - a couple of procedures should be implemented for every type: a `string` function that returns a short 
    description of the object (usually a name); a `print` subroutine that prints a description of the object 
    on standard output (which can be longer, more complete and with more eye-candy than the string description).
  - it should be possible to save instances of a derived type to a file with a subroutine named `saveTo*`, and 
    read them with a subroutine named `open*`.

## Modules
  - each derived type should have a simple and descriptive name, with a `F` prefix, and should be defined in a module with
    the same name, but with a `mod_` prefix (for example, the type `FThing` would be defined in the `mod_thing` module);
  - the modules should have no module variables, but can have parameters;
  - only the derived type should be public, with as few exceptions as possible (mostly operators and parameters);
  - all the components of a derived type should be `private`;
  - some module define operations on a type (for example, implementations of the `//` operator to concatenate character 
    variables and variables with other types), in this case, the name of the module should be `ext_`, followed by the type that is being extended (/e.g./ `ext_character`).
