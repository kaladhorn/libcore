    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cfftmf(lot, jump, n, inc, c, lenc, wsave, lensav, work, lenwrk, &
      ier)
        include 'kinds.inc'
        integer lot, jump, n, inc, lenc, lensav, lenwrk, ier
        complex (cwp) c(lenc)
        real (rwp) wsave(lensav), work(lenwrk)
        logical xercon

        ier = 0

        if (lenc<(lot-1)*jump+inc*(n-1)+1) then
            ier = 1
            call xerfft('CFFTMF ', 6)
        else if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('CFFTMF ', 8)
        else if (lenwrk<2*lot*n) then
            ier = 3
            call xerfft('CFFTMF ', 10)
        else if (.not. xercon(inc,jump,n,lot)) then
            ier = 4
            call xerfft('CFFTMF ', -1)
        end if

        if (n==1) return

        iw1 = n + n + 1
        call cmfm1f(lot, jump, n, inc, c, work, wsave, wsave(iw1), &
          wsave(iw1+1))
    end subroutine
