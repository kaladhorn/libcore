!******************************************************************************!
!                           nodeOperations                                     !
!______________________________________________________________________________!
!> Part of the `[[mod_ast(module)]]` module.
!
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2015 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!

!******************************************************************************!
!> Add a real number to an AST node
!******************************************************************************!
    function add_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        call getAddNode(z)

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Add a real number to an AST node
!******************************************************************************!
    function real_add(x, y) result(z)
        real(e), intent(in) :: x
        class(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getAddNode(z)
        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        z%argument2 = getNumberNode(x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function minus(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getMinusNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Substract two AST nodes
!******************************************************************************!
    function substract(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        z = getSubstractNode()

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Substract a real number from an AST node
!******************************************************************************!
    function substract_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        z = getSubstractNode()

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Substract an AST node from a real number
!******************************************************************************!
    function real_substract(x, y) result(z)
        real(e), intent(in) :: x
        class(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        z = getSubstractNode()

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        z%argument2 = getNumberNode(x)
    end function
!******************************************************************************!
!> Multiply two AST nodes
!******************************************************************************!
    function multiply(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        z = getMultiplyNode()

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function  multiply_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        z = getMultiplyNode()

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function real_multiply(x, y) result(z)
        real(e), intent(in) :: x
        class(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        z = getMultiplyNode()

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        z%argument2 = getNumberNode(x)
    end function
!******************************************************************************!
!> Divide two AST nodes
!******************************************************************************!
    function divide(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getDivideNode(z)

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function divide_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        call getDivideNode(z)

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function real_divide(x, y) result(z)
        real(e), intent(in) :: x
        class(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getDivideNode(z)

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        z%argument2 = getNumberNode(x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function pow(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getPowNode(z)

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Return a power of an AST node
!******************************************************************************!
    function pow_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        call getPowNode(z)

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Return the exponential of an AST node
!******************************************************************************!
    function exp_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getExpNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the natural logarithm of an AST node
!******************************************************************************!
    function ln_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getLnNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the square root of an AST node
!******************************************************************************!
    function sqrt_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getSqrtNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the sine of an AST node
!******************************************************************************!
    function sin_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getSinNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the cosine of an AST node
!******************************************************************************!
    function cos_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getCosNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the tangent of an AST node
!******************************************************************************!
    function tan_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getTanNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the hyperbolic sine of an AST node
!******************************************************************************!
    function sinh_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getSinhNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the hyperbolic cosine of an AST node
!******************************************************************************!
    function cosh_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getCoshNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the hyperbolic tangent of an AST node
!******************************************************************************!
    function tanh_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getTanhNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the error function of an AST node
!******************************************************************************!
    function erf_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getErfNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the complementary error function of an AST node
!******************************************************************************!
    function erfc_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getErfcNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function