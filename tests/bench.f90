program expression_bench
    use core
    
    use mod_expression, only: FExpression
    use mod_timer, only: FTimer
    
    implicit none
    
    type(FExpression) :: f
    type(FTimer) :: timer
    integer :: i, Niter
    real(wp), dimension(:), allocatable :: res
    real(wp) :: x, y, z
    
    ! Parameters
    Niter = 5000000      ! Number of iterations
    allocate(res(Niter)) ! Array to store the results
    
    ! Test expression
    f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
        "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")
    
    ! Variable values
    x = 0.175_wp
    y = 0.110_wp
    z = 0.900_wp
    call f%setVariable("x", x)
    call f%setVariable("y", y)
    call f%setVariable("z", z)
    
    ! Test the compiler translation
    call timer%start
    do i = 1, Niter
        res(i) = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+ &
            x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))
    end do
    call timer%stop
    write(*,*) "Direct evaluation:", timer%cpuTime()
    
    ! Test the interpreter
    call timer%reset
    call timer%start
    do i = 1, Niter
        res(i) = f%interpret_f()
    end do
    call timer%stop
    write(*,*) "AST interpretation:", timer%cpuTime()
    
    ! Test the bytecode (scalar version)
    call timer%reset
    call timer%start
    do i = 1, Niter
        call f%eval("x", 0.175_wp, res(i))
    end do
    call timer%stop
    write(*,*) "Bytecode (scalar):", timer%cpuTime()
    
    ! Test the bytecode (vector version)
    call timer%reset
    call timer%start
    call f%eval("x", 0.175_wp, Niter, 0.0_e, res)
    call timer%stop
    write(*,*) "Bytecode (vector):", timer%cpuTime()
end program
