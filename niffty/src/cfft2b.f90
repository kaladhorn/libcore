    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cfft2b(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        integer l, m, ldim, lensav, lenwrk, ier
        complex (cwp) c(ldim, m)
        real (rwp) wsave(lensav), work(lenwrk)

        ! Initialize error return

        ier = 0

        if (l>ldim) then
            ier = 5
            call xerfft('CFFT2B', -2)
            go to 100
        else if (lensav<2*l+int(log(real(l, rwp))/log(2.0_rwp))+2*m+int(log( &
              real(m, rwp))/log(2.0_rwp))+8) then
            ier = 2
            call xerfft('CFFT2B', 6)
            go to 100
        else if (lenwrk<2*l*m) then
            ier = 3
            call xerfft('CFFT2B', 8)
            go to 100
        end if

        ! Transform X lines of C array
        iw = 2*l + int(log(real(l, rwp))/log(2.0_rwp)) + 3
        call cfftmb(l, 1, m, ldim, c, (l-1)+ldim*(m-1)+1, wsave(iw), &
          2*m+int(log(real(m, rwp))/log(2.0_rwp))+4, work, 2*l*m, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('CFFT2B', -5)
            go to 100
        end if

        ! Transform Y lines of C array
        iw = 1
        call cfftmb(m, ldim, l, 1, c, (m-1)*ldim+l, wsave(iw), 2*l+int(log( &
          real(l, rwp))/log(2.0_rwp))+4, work, 2*m*l, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('CFFT2B', -5)
        end if

100     continue
    end subroutine
