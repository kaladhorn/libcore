    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine c1f4kb(ido, l1, na, cc, in1, ch, in2, wa)
        implicit none
        include 'kinds.inc'
        integer :: ido, l1, na, in1, in2
        real(rwp) :: cc(in1, l1, ido, 4), ch(in2, l1, 4, ido), wa(ido, 3, 2)
        
        real(rwp) :: ci2, ci3, ci4, cr2, cr3, cr4, ti1, ti2, ti3, ti4, tr1, tr2, tr3, tr4
        integer :: i, k

        ! FFTPACK 5.1 auxiliary routine

        if (ido>1 .or. na==1) go to 100
        do k = 1, l1
            ti1 = cc(2, k, 1, 1) - cc(2, k, 1, 3)
            ti2 = cc(2, k, 1, 1) + cc(2, k, 1, 3)
            tr4 = cc(2, k, 1, 4) - cc(2, k, 1, 2)
            ti3 = cc(2, k, 1, 2) + cc(2, k, 1, 4)
            tr1 = cc(1, k, 1, 1) - cc(1, k, 1, 3)
            tr2 = cc(1, k, 1, 1) + cc(1, k, 1, 3)
            ti4 = cc(1, k, 1, 2) - cc(1, k, 1, 4)
            tr3 = cc(1, k, 1, 2) + cc(1, k, 1, 4)
            cc(1, k, 1, 1) = tr2 + tr3
            cc(1, k, 1, 3) = tr2 - tr3
            cc(2, k, 1, 1) = ti2 + ti3
            cc(2, k, 1, 3) = ti2 - ti3
            cc(1, k, 1, 2) = tr1 + tr4
            cc(1, k, 1, 4) = tr1 - tr4
            cc(2, k, 1, 2) = ti1 + ti4
            cc(2, k, 1, 4) = ti1 - ti4
        end do
        return
100     do k = 1, l1
            ti1 = cc(2, k, 1, 1) - cc(2, k, 1, 3)
            ti2 = cc(2, k, 1, 1) + cc(2, k, 1, 3)
            tr4 = cc(2, k, 1, 4) - cc(2, k, 1, 2)
            ti3 = cc(2, k, 1, 2) + cc(2, k, 1, 4)
            tr1 = cc(1, k, 1, 1) - cc(1, k, 1, 3)
            tr2 = cc(1, k, 1, 1) + cc(1, k, 1, 3)
            ti4 = cc(1, k, 1, 2) - cc(1, k, 1, 4)
            tr3 = cc(1, k, 1, 2) + cc(1, k, 1, 4)
            ch(1, k, 1, 1) = tr2 + tr3
            ch(1, k, 3, 1) = tr2 - tr3
            ch(2, k, 1, 1) = ti2 + ti3
            ch(2, k, 3, 1) = ti2 - ti3
            ch(1, k, 2, 1) = tr1 + tr4
            ch(1, k, 4, 1) = tr1 - tr4
            ch(2, k, 2, 1) = ti1 + ti4
            ch(2, k, 4, 1) = ti1 - ti4
        end do
        if (ido==1) return
        do i = 2, ido
            do k = 1, l1
                ti1 = cc(2, k, i, 1) - cc(2, k, i, 3)
                ti2 = cc(2, k, i, 1) + cc(2, k, i, 3)
                ti3 = cc(2, k, i, 2) + cc(2, k, i, 4)
                tr4 = cc(2, k, i, 4) - cc(2, k, i, 2)
                tr1 = cc(1, k, i, 1) - cc(1, k, i, 3)
                tr2 = cc(1, k, i, 1) + cc(1, k, i, 3)
                ti4 = cc(1, k, i, 2) - cc(1, k, i, 4)
                tr3 = cc(1, k, i, 2) + cc(1, k, i, 4)
                ch(1, k, 1, i) = tr2 + tr3
                cr3 = tr2 - tr3
                ch(2, k, 1, i) = ti2 + ti3
                ci3 = ti2 - ti3
                cr2 = tr1 + tr4
                cr4 = tr1 - tr4
                ci2 = ti1 + ti4
                ci4 = ti1 - ti4
                ch(1, k, 2, i) = wa(i, 1, 1)*cr2 - wa(i, 1, 2)*ci2
                ch(2, k, 2, i) = wa(i, 1, 1)*ci2 + wa(i, 1, 2)*cr2
                ch(1, k, 3, i) = wa(i, 2, 1)*cr3 - wa(i, 2, 2)*ci3
                ch(2, k, 3, i) = wa(i, 2, 1)*ci3 + wa(i, 2, 2)*cr3
                ch(1, k, 4, i) = wa(i, 3, 1)*cr4 - wa(i, 3, 2)*ci4
                ch(2, k, 4, i) = wa(i, 3, 1)*ci4 + wa(i, 3, 2)*cr4
            end do
        end do
    end subroutine
