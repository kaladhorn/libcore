!******************************************************************************!
!                              mod_ast module
!______________________________________________________________________________!
!> Derived type definition for Abstract Syntax Trees, and related type-bound
!> procedures. Designed only to be used in mod_expression.
!>
!>TODO: turn into a submodule when support is added to gfortran and NAG
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_ast
    use core
    use ext_character
    use iso_c_binding, only: c_int16_t

    implicit none
    private
    save

    ! Token types (for internal use only)
    integer, parameter, public :: TOKEN_TYPE_UNDEF       = 0
    integer, parameter, public :: TOKEN_TYPE_NAME        = 1
    integer, parameter, public :: TOKEN_TYPE_NUMBER      = 2
    integer, parameter, public :: TOKEN_TYPE_LEFT_PAREN  = 3
    integer, parameter, public :: TOKEN_TYPE_RIGHT_PAREN = 4
    integer, parameter, public :: TOKEN_TYPE_OPERATOR    = 5
    integer, parameter, public :: TOKEN_TYPE_FUNCTION    = 6
    integer, parameter, public :: TOKEN_TYPE_VARIABLE    = 7
    integer, parameter, public :: TOKEN_TYPE_NULL        = 8
    integer, parameter, public :: TOKEN_TYPE_CONSTANT    = 9

    include 'instruction_set.inc'

    ! Token precedence levels
    integer, parameter :: PRECEDENCE_FUNCTION = 11 ! Function names
    integer, parameter :: PRECEDENCE_NUMBER   = 10 ! Numbers
    integer, parameter :: PRECEDENCE_UNARY    = 8  ! Unary operators: +, -, .not.
    integer, parameter :: PRECEDENCE_POWER    = 9  ! Exponentiation: **
    integer, parameter :: PRECEDENCE_DIV      = 7  ! Division: /
    integer, parameter :: PRECEDENCE_MULT     = 6  ! Multiplication: *
    integer, parameter :: PRECEDENCE_ADD      = 5  ! Additive operations: +, -
    integer, parameter :: PRECEDENCE_COMP     = 4  ! Comparisons: <, <=, >, >=
    integer, parameter :: PRECEDENCE_EQUAL    = 3  ! Equality: ==, /=
    integer, parameter :: PRECEDENCE_AND      = 2  ! Logical and: .and.
    integer, parameter :: PRECEDENCE_OR       = 1  ! Logical or: .or.

!******************************************************************************!
!> Token derived type
!>
!>TODO Set components to private when possible
!******************************************************************************!
    type, public :: ast_node
        integer :: type = TOKEN_TYPE_UNDEF
        type(ast_node), pointer :: parent => null()
        type(ast_node), pointer :: argument1 => null()
        type(ast_node), pointer :: argument2 => null()
        real(e) :: value = 0
        logical :: changed = .true.

        integer :: id = -1
        integer :: precedence = 0
        integer :: nArg = 0

        procedure(ast_nodeFunction), pointer, nopass :: function1 => null()
        procedure(ast_nodeBinaryFunction), pointer, nopass :: function2 => null()

        character(:), allocatable :: name

        procedure(ast_getDeriv), pointer, nopass :: derive => null()
        procedure(ast_simplify), pointer :: simplify => null()
    contains
        procedure, public :: copy
        generic, public :: assignment(=) => copy

        procedure, public :: add
        procedure, public :: identity
        procedure, private :: add_real
        procedure, private, pass(y) :: real_add
        generic, public :: operator(+) => add, identity, add_real, real_add
        procedure, public :: substract
        procedure, public :: minus
        procedure, private :: substract_real
        procedure, private, pass(y) :: real_substract
        generic, public :: operator(-) => substract, minus, substract_real, real_substract
        procedure, public :: multiply
        procedure, private :: multiply_real
        procedure, private, pass(y) :: real_multiply
        generic, public :: operator(*) => multiply, multiply_real, real_multiply
        procedure, public :: divide
        procedure, public :: divide_real
        procedure, public, pass(y) :: real_divide
        generic, public :: operator(/) => divide, divide_real, real_divide
        procedure, public :: pow
        procedure, private :: pow_real
        generic, public :: operator(**) => pow, pow_real

        procedure, private :: evalf_
        procedure, private :: evalf_var
        generic, public :: evalf => evalf_, evalf_var
        procedure, public :: eval
        procedure, public :: calculate
        procedure, public :: printAST
        procedure, public :: getString

        final :: token_finalise

        procedure, public :: setChanged
        procedure, public :: setNeedsUpdate

        procedure, public :: checkParentPointers

        procedure, public :: substitute => insert

        procedure, public :: countNodes
    end type

    ! Interface for the internal representation of mathematical functions and operators
    abstract interface
        pure function ast_nodeFunction(x) result(y)
            import
            real(e), intent(in) :: x
            real(e) :: y
        end function
        pure function ast_nodeBinaryFunction(x,y) result(z)
            import
            real(e), intent(in) :: x
            real(e), intent(in) :: y
            real(e) :: z
        end function
        subroutine ast_getDeriv(this, d, variable)
            import
            class(ast_node), intent(in) :: this
            type(ast_node), intent(out) :: d
            character(*), intent(in) :: variable
        end subroutine
        subroutine ast_simplify(this)
            import
            class(ast_node), intent(inout), target :: this
        end subroutine
    end interface

    public :: getConstantNode
    public :: getNumberNode
    public :: getPiNode
    public :: getVariableNode
    public :: getAbsnode
    public :: getSqrtNode
    public :: getMinusNode
    public :: getSubstractNode
    public :: getIdNode
    public :: getAddNode
    public :: getMultiplyNode
    public :: getDivideNode
    public :: getPowNode

    public :: getCosNode
    public :: getSinNode
    public :: getTanNode
    public :: getCoshNode
    public :: getSinhNode
    public :: getTanhNode
    public :: getAcosNode
    public :: getAsinNode
    public :: getAtanNode
    public :: getAcoshNode
    public :: getAsinhNode
    public :: getAtanhNode
    public :: getExpNode
    public :: getLog10Node
    public :: getLnNode

    public :: getErfNode
    public :: getErfcNode

    public :: getEqualNode
    public :: getDifferentNode
    public :: getLessNode
    public :: getGreaterNode
    public :: getLessEqNode
    public :: getGreaterEqNode
    public :: getAndNode
    public :: getOrNode
    public :: getNotNode

! Interfaces
    interface ln
        module procedure ln_
    end interface
    interface sqrt
        module procedure sqrt_
    end interface
    interface sin
        module procedure sin_
    end interface
    interface cos
        module procedure cos_
    end interface
    interface sinh
        module procedure sinh_
    end interface
    interface cosh
        module procedure cosh_
    end interface
    interface exp
        module procedure exp_
    end interface

contains
!******************************************************************************!
!> Count the number of AST nodes in a tree, ignoring the nodes representing
!> variables.
!>
!> Used to build a flat representation of the AST for optimisation purposes.
!******************************************************************************!
    pure recursive subroutine countNodes(this, nInstructions, nConstants)
        class(ast_node), intent(in) :: this
        integer, intent(inout) :: nInstructions
        integer, intent(inout) :: nConstants
!------
        if (associated(this%argument1)) then
            call countNodes(this%argument1, nInstructions, nConstants)
        end if
        if (associated(this%argument2)) then
            call countNodes(this%argument2, nInstructions, nConstants)
        end if

        if (this%type /= TOKEN_TYPE_VARIABLE) then
            if (this%type == TOKEN_TYPE_CONSTANT .OR. THIS%TYPE == TOKEN_TYPE_NUMBER) then
                nConstants = nConstants + 1
            else
                nInstructions = nInstructions + 1
            end if
        end if
    end subroutine
!******************************************************************************!
!>
!******************************************************************************!
    function identity(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getIdNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Add two AST nodes
!******************************************************************************!
    function add(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getAddNode(z)

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Add a real number to an AST node
!******************************************************************************!
    function add_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        call getAddNode(z)

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Add a real number to an AST node
!******************************************************************************!
    function real_add(x, y) result(z)
        real(e), intent(in) :: x
        class(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getAddNode(z)
        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        z%argument2 = getNumberNode(x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function minus(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getMinusNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Substract two AST nodes
!******************************************************************************!
    function substract(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        z = getSubstractNode()

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Substract a real number from an AST node
!******************************************************************************!
    function substract_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        z = getSubstractNode()

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Substract an AST node from a real number
!******************************************************************************!
    function real_substract(x, y) result(z)
        real(e), intent(in) :: x
        class(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        z = getSubstractNode()

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        z%argument2 = getNumberNode(x)
    end function
!******************************************************************************!
!> Multiply two AST nodes
!******************************************************************************!
    function multiply(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        z = getMultiplyNode()

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function  multiply_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        z = getMultiplyNode()

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function real_multiply(x, y) result(z)
        real(e), intent(in) :: x
        class(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        z = getMultiplyNode()

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        z%argument2 = getNumberNode(x)
    end function
!******************************************************************************!
!> Divide two AST nodes
!******************************************************************************!
    function divide(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getDivideNode(z)

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function divide_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        call getDivideNode(z)

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function real_divide(x, y) result(z)
        real(e), intent(in) :: x
        class(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getDivideNode(z)

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        z%argument2 = getNumberNode(x)
    end function
!******************************************************************************!
!>
!******************************************************************************!
    function pow(x, y) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node), intent(in) :: y
        type(ast_node) :: z
!------
        call getPowNode(z)

        allocate(z%argument1)
        call copy(z%argument1, y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Return a power of an AST node
!******************************************************************************!
    function pow_real(x, y) result(z)
        class(ast_node), intent(in) :: x
        real(e), intent(in) :: y
        type(ast_node) :: z
!------
        call getPowNode(z)

        allocate(z%argument1)
        z%argument1 = getNumberNode(y)

        allocate(z%argument2)
        call copy(z%argument2, x)
    end function
!******************************************************************************!
!> Return the exponential of an AST node
!******************************************************************************!
    function exp_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getExpNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the natural logarithm of an AST node
!******************************************************************************!
    function ln_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getLnNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the square root of an AST node
!******************************************************************************!
    function sqrt_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getSqrtNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the sine of an AST node
!******************************************************************************!
    function sin_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getSinNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the cosine of an AST node
!******************************************************************************!
    function cos_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getCosNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the hyperbolic sine of an AST node
!******************************************************************************!
    function sinh_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getSinhNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
!******************************************************************************!
!> Return the hyperbolic cosine of an AST node
!******************************************************************************!
    function cosh_(x) result(z)
        class(ast_node), intent(in) :: x
        type(ast_node) :: z
!------
        z = getCoshNode()

        allocate(z%argument1)
        call copy(z%argument1, x)
    end function
    include 'math/nodeDefinitions.inc'
!******************************************************************************!
!> Mark an AST node and its parents as changed or up-to-date.
!******************************************************************************!
    pure recursive subroutine setChanged(this, changed)
        class(ast_node), intent(inout) :: this
        logical, intent(in) :: changed
!------
        this%changed = changed
        if (associated(this%parent)) then
            if (.not.this%parent%changed) &
                call this%parent%setChanged(changed)
        end if
    end subroutine
!******************************************************************************!
!> Mark an AST node and its children as changed or up-to-date.
!******************************************************************************!
    pure recursive subroutine setNeedsUpdate(this, changed)
        class(ast_node), intent(inout) :: this
        logical, intent(in) :: changed
!------
        this%changed = changed
        if (associated(this%argument1)) then
            call this%argument1%setNeedsUpdate(changed)
        end if
        if (associated(this%argument2)) then
            call this%argument2%setNeedsUpdate(changed)
        end if
    end subroutine
!******************************************************************************!
!> Evaluate the sub-tree starting from a given AST node. The function is pure,
!> so it can be used in I/O and contexts where side-effects can be problematic.
!> This can have some effects on performance if the AST contains many nodes
!> with values that do not change between successive calls to `evalf`. In this
!> case, it might help to call `calculate` before.
!******************************************************************************!
    pure recursive function evalf_(this) result(result)
        class(ast_node), intent(in) :: this
        real(e) :: result
!------
        real(e) :: x, y
!------
        if (this%changed) then
            select case(this%nArg)
                case (0)
                    result = this%value
                case (1)
                    x = this%argument1%evalf()
                    result = this%function1(x)
                case (2)
                    x = this%argument1%evalf()
                    y = this%argument2%evalf()
                    result = this%function2(x, y)
                case default
                    ! Unreachable
                    result = 0
            end select
        else
            result = this%value
        end if
    end function
!******************************************************************************!
!> Evaluate the sub-tree starting from a given AST node. The function is pure,
!> so it can be used in I/O and contexts where side-effects can be problematic.
!> It performs slower than `eval`, because it has to re-calculate the value of
!> every node in the AST.
!******************************************************************************!
    pure recursive function evalf_var(this, variable, value) result(result)
        class(ast_node), intent(in) :: this
        character(*), intent(in) :: variable
        real(e), intent(in) :: value
        real(e) :: result
!------
        real(e) :: x, y
!------
        select case(this%nArg)
            case (0)
                if (this%name == variable) then
                    result = value
                else
                    result = this%value
                end if
            case (1)
                x = this%argument1%evalf(variable, value)
                result = this%function1(x)
            case (2)
                x = this%argument1%evalf(variable, value)
                y = this%argument2%evalf(variable, value)
                result = this%function2(x, y)
            case default
                ! Unreachable
                result = 0
        end select
    end function
!******************************************************************************!
!> Evaluate the sub-tree starting from a given AST node. The values of the AST
!> nodes are updated if needed, so there is no need to call `calculate` before.
!******************************************************************************!
    pure recursive subroutine eval(this, result)
        class(ast_node), intent(inout) :: this
        real(e), intent(out) :: result
!------
        if (this%changed) call this%calculate

        result = this%value
    end subroutine
!******************************************************************************!
!> Update the value of the node in an AST. The nodes are marked as up-to-date
!> to avoid un-necessary calculations during later calls to `evalf`.
!******************************************************************************!
    pure recursive subroutine calculate(this)
        class(ast_node), intent(inout) :: this
!------
        real(e) :: x, y
!------
        if (this%changed) then
            select case(this%nArg)
                case (0)
                    ! No need to do anything
                case (1)
                    call calculate(this%argument1)
                    x = this%argument1%value
                    this%value = this%function1(x)
                case (2)
                    call calculate(this%argument1)
                    x = this%argument1%value
                    call calculate(this%argument2)
                    y = this%argument2%value
                    this%value = this%function2(x, y)
                case default
                    ! Unreachable
            end select
            this%changed = .false.
        end if
    end subroutine
!******************************************************************************!
!>
!******************************************************************************!
    recursive subroutine simplify_default(this)
        class(ast_node), intent(inout), target :: this
!------
        this%changed = .false.
        if (associated(this%argument1)) then
            if (this%argument1%changed) call this%argument1%simplify
            if (this%argument1%changed) call this%setChanged(.true.)
        end if
        if (associated(this%argument2)) then
            if (this%argument2%changed) call this%argument2%simplify
            if (this%argument2%changed) call this%setChanged(.true.)
        end if
    end subroutine
!******************************************************************************!
!> Get a string representation of an AST
!******************************************************************************!
    pure recursive subroutine getString(token, out)
        class(ast_node), intent(in) :: token
        character(:), allocatable, intent(inout) :: out
!------
        character(:), allocatable :: leftString, rightString
        integer :: id
!------
        if (.not.allocated(out)) out = ""

        ! The node is a number
        if (token%type == TOKEN_TYPE_NUMBER) then
            out = out // token%name
            return
        end if

        ! The node is a variable
        if (token%type == TOKEN_TYPE_VARIABLE) then
            out = out // token%name
            return
        end if

        if (token%type == TOKEN_TYPE_CONSTANT) then
            out = out // token%name
            return
        end if

        id = token%id

        ! The node has two arguments
        if (associated(token%argument2)) then
            call getString(token%argument2, leftString)
            if (token%argument2%precedence < token%precedence) &
                leftString = "("//leftString//")"
            if (leftSymbol(id) /= "") then
                leftString = trim(leftSymbol(id)) // leftString
            end if
        else
            if (leftSymbol(id) /= "") then
                leftString = trim(leftSymbol(id))
            else
                leftString = ""
            end if
        end if

        ! The node has one argument
        call getString(token%argument1, rightString)
        if (token%argument1%precedence < token%precedence) &
            rightString = "("//rightString//")"
        if (rightSymbol(id) /= "") then
            rightString = rightString // trim(rightSymbol(id))
        end if

        out = out // leftString // trim(symbol(id)) // rightString
    end subroutine
!******************************************************************************!
! Print an AST sub-tree, starting from its root node. For debug use.
!******************************************************************************!
!LCOV_EXCL_START
    recursive subroutine printAst(token, level)
        class(ast_node), intent(in) :: token
        integer, intent(in) :: level
!------
        character(:), allocatable :: line
        integer :: id
!------
        allocate(character(len(token%name)+level) :: line)
        line(:len(line)) = " "
        line(level+1:) = token%name

        id = token%id
!        associate(id => token%id)
!            if ((leftSymbol(id)/="") .and. (rightSymbol(id)/="") .and. (symbol(id)/="")) then
                write(*,'(a,g0,a,g0,1x,g0,1x,g0,1x,i0)') line // ",  ",  token%value, "_"//trim(leftSymbol(id))//"_"// &
                    trim(symbol(id))// &
                    "_"//trim(rightSymbol(id)), token%changed, token%precedence, associated(token%parent), id
!            else
!                write(*,'(a,g0,a,g0,1x,g0,1x,g0)') line // ",  ",  token%value, "___", &
!                    token%changed, token%precedence, associated(token%parent)
!            end if
            if (associated(token%argument1)) call printAST(token%argument1,level+1)
            if (associated(token%argument2)) call printAST(token%argument2,level+1)
!        end associate
    end subroutine
!LCOV_EXCL_STOP
!******************************************************************************!
!> Token destructor
!>
!>@todo crashes with gfortran
!******************************************************************************!
    recursive subroutine token_finalise(this)
        type(ast_node), intent(inout) :: this
!------
!bug with gfortran
!         if (associated(this%argument1)) deallocate(this%argument1)
!         this%argument1 => null()
!         if (associated(this%argument2)) deallocate(this%argument2)
!         this%argument2 => null()
    end subroutine
!******************************************************************************!
!> Insert an AST node in a tree
!******************************************************************************!
    recursive subroutine insert(this, from)
        class(ast_node), intent(inout), target :: this
        class(ast_node), intent(in) :: from
!------
        type(ast_node), pointer :: parent
!------

        parent => this%parent

        call copy(this, from)
        this%parent => parent
    end subroutine
!******************************************************************************!
!> Copy an AST node
!******************************************************************************!
    recursive subroutine copy(this, from)
        class(ast_node), intent(out), target :: this
        class(ast_node), intent(in) :: from
!------
        this%type = from%type
        if (allocated(from%name)) this%name = from%name
        this%nArg = from%nArg

        this%precedence = from%precedence
        this%value = from%value
        this%changed = from%changed
        if (associated(from%function1)) then
            this%function1 => from%function1
        else
            this%function1 => null()
        end if
        if (associated(from%function2)) then
            this%function2 => from%function2
        else
            this%function2 => null()
        end if
        if (associated(from%derive)) then
            this%derive => from%derive
        else
            this%derive => null()
        end if
        if (associated(from%simplify)) then
            this%simplify => from%simplify
        else
            this%simplify => simplify_default
        end if
        this%id = from%id
        if (associated(from%argument1)) then
            allocate(this%argument1)
            call copy(this%argument1, from%argument1)
            this%argument1%parent => this
        end if
        if (associated(from%argument2)) then
            allocate(this%argument2)
            call copy(this%argument2, from%argument2)
            this%argument2%parent => this
        end if
    end subroutine

    recursive subroutine checkParentPointers(this)
        class(ast_node), intent(inout), target :: this

        if (associated(this%argument1)) then
            if (associated(this%argument1%parent, this)) then
                call this%argument1%checkParentPointers
            else
                write(*,*) "Inconsistent parent pointer at node ", this%name
                write(*,*) " for child 1: ", this%argument1%name
            end if
        end if
        if (associated(this%argument2)) then
            if (associated(this%argument2%parent, this)) then
                call this%argument2%checkParentPointers
            else
                write(*,*) "Inconsistent parent pointer at node ", this%name
                write(*,*) " for child 2: ", this%argument2%name
            end if
        end if
    end subroutine
!******************************************************************************!
!> Calculate the derivative of an AST node
!******************************************************************************!
    recursive function derive(x, variable) result(y)
        class(ast_node), intent(in) :: x
        character(*), intent(in) :: variable
        type(ast_node) :: y
!------
        call x%derive(x, y, variable)
    end function
end module