
select case(this%instruction(i))
    case (CODE_INVALID);
    case (CODE_NONE);
    case (CODE_ID);         data(:l,i) =   data(:l,a1)
    case (CODE_MINUS);      data(:l,i) = - data(:l,a1)
    case (CODE_ADD);        data(:l,i) = data(:l,a2) + data(:l,a1)
    case (CODE_SUBSTRACT);  data(:l,i) = data(:l,a2) - data(:l,a1)
    case (CODE_MULTIPLY);   data(:l,i) = data(:l,a2) * data(:l,a1)
    case (CODE_DIVIDE);     data(:l,i) = data(:l,a2) / data(:l,a1)
    case (CODE_POW);        do d=1, l
                                associate(x=>data(d,a1), y=>data(d,a2))
                                    if (y < 0) then
                                        if (nint(x)*10 == nint(x*10)) then
                                            data(d,i) = y ** nint(x)
                                        else
                                            data(d,i) = y ** x
                                        end if
                                    else
                                        data(d,i) = y ** x
                                    end if
                                end associate
                            end do
    case (CODE_POW_INT);    data(:l,i) = data(:l,a2) ** nint(data(:l,a1))
    case (CODE_ABS);        data(:l,i) = abs(data(:l,a1))
    case (CODE_EXP);        data(:l,i) = exp(data(:l,a1))
    case (CODE_LN);         data(:l,i) = log(data(:l,a1))
    case (CODE_LOG10);      data(:l,i) = log10(data(:l,a1))
    case (CODE_SQRT);       data(:l,i) = sqrt(data(:l,a1))
    case (CODE_ERF);        data(:l,i) = erf(data(:l,a1))
    case (CODE_ERFC);       data(:l,i) = erfc(data(:l,a1))
    case (CODE_SIN);        data(:l,i) = sin(data(:l,a1))
    case (CODE_COS);        data(:l,i) = cos(data(:l,a1))
    case (CODE_TAN);        data(:l,i) = tan(data(:l,a1))
    case (CODE_ASIN);       data(:l,i) = asin(data(:l,a1))
    case (CODE_ACOS);       data(:l,i) = acos(data(:l,a1))
    case (CODE_ATAN);       data(:l,i) = atan(data(:l,a1))
    case (CODE_SINH);       data(:l,i) = sinh(data(:l,a1))
    case (CODE_COSH);       data(:l,i) = cosh(data(:l,a1))
    case (CODE_TANH);       data(:l,i) = tanh(data(:l,a1))
    case (CODE_ASINH);      data(:l,i) = asinh(data(:l,a1))
    case (CODE_ACOSH);      data(:l,i) = acosh(data(:l,a1))
    case (CODE_ATANH);      data(:l,i) = atanh(data(:l,a1))
    case (CODE_FMA);        data(:l,i) = data(:l,a3)*data(:l,a2) + &
                                data(:l,a1)
    case (CODE_EQUAL);     where (data(:l,a2) == data(:l,a1))
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    case (CODE_DIFFERENT); where (data(:l,a2) /= data(:l,a1))
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    case (CODE_LESS);      where (data(:l,a2) < data(:l,a1))
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    case (CODE_GREATER);   where (data(:l,a2) > data(:l,a1))
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    case (CODE_LESS_EQ);   where (data(:l,a2) <= data(:l,a1))
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    case (CODE_GREAT_EQ);  where (data(:l,a2) >= data(:l,a1))
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    case (CODE_NOT);       where (data(:l,a1) == 0)
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    case (CODE_AND);       where (data(:l,a2)==1 .and. data(:l,a1)==1)
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    case (CODE_OR);        where (data(:l,a2)==1 .or. data(:l,a1)==1)
                               data(:l,i) = 1.0_e
                           elsewhere
                               data(:l,i) = 0.0_e
                           end where
    !@todo un-comment the error stop once all the supported compilers allow error stop in pure procedures
    case default; !error stop
end select
