    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine costmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, lenwrk, &
      ier)
        include 'kinds.inc'
        integer lot, jump, n, inc, lenx, lensav, lenwrk, ier
        real (rwp) x(inc, *), wsave(lensav), work(lenwrk)
        logical xercon

        ier = 0

        if (lenx<(lot-1)*jump+inc*(n-1)+1) then
            ier = 1
            call xerfft('COSTMF', 6)
            go to 100
        else if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('COSTMF', 8)
            go to 100
        else if (lenwrk<lot*(n+1)) then
            ier = 3
            call xerfft('COSTMF', 10)
            go to 100
        else if (.not. xercon(inc,jump,n,lot)) then
            ier = 4
            call xerfft('COSTMF', -1)
            go to 100
        end if

        iw1 = lot + lot + 1
        call mcstf1(lot, jump, n, inc, x, wsave, work, work(iw1), ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('COSTMF', -5)
        end if

100     continue
    end subroutine
