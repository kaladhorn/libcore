    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine msntf1(lot, jump, n, inc, x, wsave, dsum, xh, work, ier)
        implicit none
        include 'kinds.inc'
        integer :: lot
        integer :: jump
        integer :: n
        integer :: inc
        real(rwp), intent(inout) :: x(inc, *)
        real(rwp) :: wsave(*)
        real(rwp) :: xh(lot, *)
        integer :: ier
        real(rwp) :: work
        
        double precision dsum(*)
        integer :: i, k, kc, lj, lnsv, lnwk, lnxh, m, m1, modn, np1, ns2, ier1
        real(rwp) :: sfnp1, ssqrt3, t1, t2, xhold
        

        ier = 0
        lj = (lot-1)*jump + 1
        if (n-2) 110, 100, 120
100     ssqrt3 = 1.0_rwp/sqrt(3.0_rwp)
        do m = 1, lj, jump
            xhold = ssqrt3*(x(m,1)+x(m,2))
            x(m, 2) = ssqrt3*(x(m,1)-x(m,2))
            x(m, 1) = xhold
        end do
110     go to 150
120     np1 = n + 1
        ns2 = n/2
        do k = 1, ns2
            kc = np1 - k
            m1 = 0
            do m = 1, lj, jump
                m1 = m1 + 1
                t1 = x(m, k) - x(m, kc)
                t2 = wsave(k)*(x(m,k)+x(m,kc))
                xh(m1, k+1) = t1 + t2
                xh(m1, kc+1) = t2 - t1
            end do
        end do
        modn = mod(n, 2)
        if (modn==0) go to 130
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            xh(m1, ns2+2) = 4.*x(m, ns2+1)
        end do
130     do m = 1, lot
            xh(m, 1) = 0.
        end do
        lnxh = lot - 1 + lot*(np1-1) + 1
        lnsv = np1 + int(log(real(np1, rwp))/log(2.0_rwp)) + 4
        lnwk = lot*np1

        call rfftmf(lot, 1, np1, lot, xh, lnxh, wsave(ns2+1), lnsv, work, &
          lnwk, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('MSNTF1', -5)
            go to 150
        end if

        if (mod(np1,2)/=0) go to 140
        do m = 1, lot
            xh(m, np1) = xh(m, np1) + xh(m, np1)
        end do
140     sfnp1 = 1.0_rwp/float(np1)
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            x(m, 1) = .5_rwp*xh(m1, 1)
            dsum(m1) = x(m, 1)
        end do
        do i = 3, n, 2
            m1 = 0
            do m = 1, lj, jump
                m1 = m1 + 1
                x(m, i-1) = .5_rwp*xh(m1, i)
                dsum(m1) = dsum(m1) + .5*xh(m1, i-1)
                x(m, i) = dsum(m1)
            end do
        end do
        if (modn/=0) go to 150
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            x(m, n) = .5_rwp*xh(m1, n+1)
        end do
150     continue
    end subroutine
