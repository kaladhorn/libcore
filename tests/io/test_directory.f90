module test_directory
    use mod_test
    use ext_character

    use config,        only: TEST_PATH
    use mod_exception, only: FException
    use mod_regex,     only: FRegex
    use mod_string,    only: FString
    use mod_directory, only: FDirectory

    use iso_c_binding

    implicit none
    private

    public :: testAll
contains
    subroutine testAll()
        call testUninitialised
        call testCreation
        call testLists
        call testArray
    end subroutine

    subroutine testCreation
        type(FDirectory) :: dir, dir2, dir3
        type(FException) :: err
        logical :: test

        call startTestGroup("directory creation")

        ! Test string constructor
        dir = FDirectory(FString("foo"))
        call check(dir%string() == "foo", "string description", &
            "wrong string description for directory foo: " // dir%string())

        ! Test character constructor
        dir2 = FDirectory("foo")
        test = (dir == dir2)
        call check(test, "directory equality")

        ! Test basic methods
        call check(.not.dir2%exists(), "non-existing directory", "non-existing directory ""foo"" exists")
        test = dir2%isEmpty(err)
        call check(err=="No such file or directory.", &
            "emptiness of non-existing directory", "error message: " // err%string())
        call err%discard
        call dir2%create(err)
        call check(err == 0, "directory creation", "directory ""foo"" could not be created")
        call err%discard
        call check(dir2%exists(), "directory existence", "directory ""foo"" does not exist")
        call check(dir%isEmpty(), "directory content", &
            "the directory """ // dir%string() // """ does not seem empty.")

        call dir2%remove(err)
        call check(err == 0, "removing directory", "directory ""foo"" could not be removed")
        call err%discard
        call dir2%remove(err)
        call check(err == "No such file or directory", "removing non-existing directory", &
            "directory ""foo"" could be removed")
        call err%discard

        call check(.not.dir2%exists(), "remove double-check", "directory ""foo"" still exists")
        dir = FDirectory(".")
        call check(.not.dir%isEmpty(), "content of a non-empty directory", &
            "the current working directory seems to be empty.")

        dir3 = FDirectory("")
        call dir3%create(err)
        call check(err == "No such file or directory", "creating directory with invalid name", &
            "wrong error status: """ // err%string() // """")
        call err%discard
    end subroutine

    subroutine testUninitialised
        type(FDirectory) :: dir
        type(FException) :: err
        logical :: empty

        call startTestGroup("uninitialised objects")

        ! Test un-initialised directory
        call check(.not.dir%exists(), "existence of uninitialised directory", &
            "non-existing directory exists")
        call dir%create(err)
        call check(err/=0, "creation of uninitialised directory", &
            "non-existing directory could be created")
        call err%discard
        call dir%remove(err)
        call check(err/=0, "removing of uninitialised directory", &
            "non-existing directory could be removed")
        call err%discard
        
        empty = dir%isEmpty(err)
        call check(err/=0, "emptiness of uninitialised directory", &
            "Trying to determine emptiness of an undefined directory.")
        call err%discard
    end subroutine

    subroutine testLists
        type(FDirectory) :: dir
        type(FString), dimension(:), allocatable :: filenames
        type(FRegex) :: re

        call startTestGroup("files lists")

        ! Get the list of files in the current directory
        dir = FDirectory(".")
        call dir%getFileList(filenames)

        ! Get a filtered list of files in the current directory
        re = FRegex(".*\.f90")
        call dir%getFileList(filenames, re)

        ! Get the list of directories in the current directory
        call dir%getDirectoryList(filenames)

        ! Get a filtered list of directories in the current directory
        re = FRegex(".*\.dSYM")
        call dir%getDirectoryList(filenames, re)
    end subroutine

    subroutine testArray
        type(FDirectory), dimension(:), allocatable :: dir, dir2
        type(FRegex) :: re

        call startTestGroup("directory arrays")

        re = FRegex(TEST_PATH//"/directory/.{3}")
        dir = FDirectory(re)
        write(*,*) TEST_PATH//"/directory/.{3}"
        call check(size(dir)==3, "short directory list", &
            "Wrong directory array size: " // size(dir) // " instead of 3.")
        dir2 = FDirectory(re)
        call check(all(dir==dir2), "simple directory array", "Wrong directory array")

        dir = FDirectory(FRegex(".*\.dSYM"))
        dir2 = FDirectory(FRegex("\./.*\.dSYM"))
        call check(size(dir)==size(dir2), "directory list size", &
            "wrong directory list size: " // size(dir) // " vs. " // size(dir2) // ".")
        if (size(dir)==size(dir2)) then
            call check(all(dir==dir2), "implicit ./", "wrong directory list")
        else
            call check(.false., "implicit ./", "wrong directory list")
        end if

        ! Test a long directory list
        dir = FDirectory(FRegex(TEST_PATH//"/directory/test_[0-9]+"))
        call check(size(dir)==150, "long directory list", &
            "Wrong directory array size: " // size(dir) // " instead of 150.")
    end subroutine
end module

program test
    use mod_test
    use test_directory

    call testAll
    call testsFinished
end program