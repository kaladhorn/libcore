    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mradf5(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, wa3, &
      wa4)
        include 'kinds.inc'
        real (rwp) cc(in1, ido, l1, 5), ch(in2, ido, 5, l1), wa1(ido), &
          wa2(ido), wa3(ido), wa4(ido)

        m1d = (m-1)*im1 + 1
        m2s = 1 - im2
        arg = 2.*4.*atan(1.0_rwp)/5.
        tr11 = cos(arg)
        ti11 = sin(arg)
        tr12 = cos(2.*arg)
        ti12 = sin(2.*arg)
        do k = 1, l1
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch(m2, 1, 1, k) = cc(m1, 1, k, 1) + (cc(m1,1,k,5)+cc(m1,1,k,2) &
                  ) + (cc(m1,1,k,4)+cc(m1,1,k,3))
                ch(m2, ido, 2, k) = cc(m1, 1, k, 1) + &
                  tr11*(cc(m1,1,k,5)+cc(m1,1,k,2)) + &
                  tr12*(cc(m1,1,k,4)+cc(m1,1,k,3))
                ch(m2, 1, 3, k) = ti11*(cc(m1,1,k,5)-cc(m1,1,k,2)) + &
                  ti12*(cc(m1,1,k,4)-cc(m1,1,k,3))
                ch(m2, ido, 4, k) = cc(m1, 1, k, 1) + &
                  tr12*(cc(m1,1,k,5)+cc(m1,1,k,2)) + &
                  tr11*(cc(m1,1,k,4)+cc(m1,1,k,3))
                ch(m2, 1, 5, k) = ti12*(cc(m1,1,k,5)-cc(m1,1,k,2)) - &
                  ti11*(cc(m1,1,k,4)-cc(m1,1,k,3))
            end do
        end do
        if (ido==1) return
        idp2 = ido + 2
        do k = 1, l1
            do i = 3, ido, 2
                ic = idp2 - i
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch(m2, i-1, 1, k) = cc(m1, i-1, k, 1) + ((wa1(i-2)*cc(m1, &
                      i-1,k,2)+wa1(i-1)*cc(m1,i,k,2))+(wa4(i-2)*cc(m1,i-1,k, &
                      5)+wa4(i-1)*cc(m1,i,k,5))) + ((wa2(i-2)*cc(m1,i-1,k, &
                      3)+wa2(i-1)*cc(m1,i,k,3))+(wa3(i-2)*cc(m1,i-1,k, &
                      4)+wa3(i-1)*cc(m1,i,k,4)))
                    ch(m2, i, 1, k) = cc(m1, i, k, 1) + ((wa1(i-2)*cc(m1,i,k, &
                      2)-wa1(i-1)*cc(m1,i-1,k,2))+(wa4(i-2)*cc(m1,i,k, &
                      5)-wa4(i-1)*cc(m1,i-1,k,5))) + ((wa2(i-2)*cc(m1,i,k, &
                      3)-wa2(i-1)*cc(m1,i-1,k,3))+(wa3(i-2)*cc(m1,i,k, &
                      4)-wa3(i-1)*cc(m1,i-1,k,4)))
                    ch(m2, i-1, 3, k) = cc(m1, i-1, k, 1) + &
                      tr11*(wa1(i-2)*cc(m1,i-1,k,2)+wa1(i-1)*cc(m1,i,k,2)+ &
                      wa4(i-2)*cc(m1,i-1,k,5)+wa4(i-1)*cc(m1,i,k,5)) + &
                      tr12*(wa2(i-2)*cc(m1,i-1,k,3)+wa2(i-1)*cc(m1,i,k,3)+ &
                      wa3(i-2)*cc(m1,i-1,k,4)+wa3(i-1)*cc(m1,i,k,4)) + &
                      ti11*(wa1(i-2)*cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k,2)- &
                      (wa4(i-2)*cc(m1,i,k,5)-wa4(i-1)*cc(m1,i-1,k, &
                      5))) + ti12*(wa2(i-2)*cc(m1,i,k,3)- &
                      wa2(i-1)*cc(m1,i-1,k,3)-(wa3(i-2)*cc(m1,i,k, &
                      4)-wa3(i-1)*cc(m1,i-1,k,4)))
                    ch(m2, ic-1, 2, k) = cc(m1, i-1, k, 1) + &
                      tr11*(wa1(i-2)*cc(m1,i-1,k,2)+wa1(i-1)*cc(m1,i,k,2)+ &
                      wa4(i-2)*cc(m1,i-1,k,5)+wa4(i-1)*cc(m1,i,k,5)) + &
                      tr12*(wa2(i-2)*cc(m1,i-1,k,3)+wa2(i-1)*cc(m1,i,k,3)+ &
                      wa3(i-2)*cc(m1,i-1,k,4)+wa3(i-1)*cc(m1,i,k,4)) - &
                      (ti11*(wa1(i-2)*cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k, &
                      2)-(wa4(i-2)*cc(m1,i,k,5)-wa4(i-1)*cc(m1,i-1,k,5)))+ti12 &
                      *(wa2(i-2)*cc(m1,i,k,3)-wa2(i-1)*cc(m1,i-1,k, &
                      3)-(wa3(i-2)*cc(m1,i,k,4)-wa3(i-1)*cc(m1,i-1,k,4))))
                    ch(m2, i, 3, k) = (cc(m1,i,k,1)+tr11*((wa1(i-2)* &
                      cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k,2))+(wa4(i-2)*cc(m1,i, &
                      k,5)-wa4(i-1)*cc(m1,i-1,k,5)))+tr12*((wa2(i-2)*cc(m1,i,k &
                      ,3)-wa2(i-1)*cc(m1,i-1,k,3))+(wa3(i-2)*cc(m1,i,k,4)- &
                      wa3(i-1)*cc(m1,i-1,k,4)))) + (ti11*((wa4(i-2)*cc(m1,i-1, &
                      k,5)+wa4(i-1)*cc(m1,i,k,5))-(wa1(i-2)*cc(m1,i-1,k,2)+ &
                      wa1(i-1)*cc(m1,i,k,2)))+ti12*((wa3(i-2)*cc(m1,i-1,k,4)+ &
                      wa3(i-1)*cc(m1,i,k,4))-(wa2(i-2)*cc(m1,i-1,k,3)+ &
                      wa2(i-1)*cc(m1,i,k,3))))
                    ch(m2, ic, 2, k) = (ti11*((wa4(i-2)*cc(m1,i-1,k,5)+ &
                      wa4(i-1)*cc(m1,i,k,5))-(wa1(i-2)*cc(m1,i-1,k,2)+ &
                      wa1(i-1)*cc(m1,i,k,2)))+ti12*((wa3(i-2)*cc(m1,i-1,k,4)+ &
                      wa3(i-1)*cc(m1,i,k,4))-(wa2(i-2)*cc(m1,i-1,k,3)+ &
                      wa2(i-1)*cc(m1,i,k,3)))) - (cc(m1,i,k,1)+tr11*((wa1(i-2) &
                      *cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k,2))+(wa4(i-2)*cc(m1,i &
                      ,k,5)-wa4(i-1)*cc(m1,i-1,k,5)))+tr12*((wa2(i-2)*cc(m1,i, &
                      k,3)-wa2(i-1)*cc(m1,i-1,k,3))+(wa3(i-2)*cc(m1,i,k,4)- &
                      wa3(i-1)*cc(m1,i-1,k,4))))
                    ch(m2, i-1, 5, k) = (cc(m1,i-1,k,1)+tr12*((wa1(i-2)* &
                      cc(m1,i-1,k,2)+wa1(i-1)*cc(m1,i,k,2))+(wa4(i-2)*cc(m1,i- &
                      1,k,5)+wa4(i-1)*cc(m1,i,k,5)))+tr11*((wa2(i-2)*cc(m1,i-1 &
                      ,k,3)+wa2(i-1)*cc(m1,i,k,3))+(wa3(i-2)*cc(m1,i-1,k,4)+ &
                      wa3(i-1)*cc(m1,i,k,4)))) + (ti12*((wa1(i-2)*cc(m1,i,k,2) &
                      -wa1(i-1)*cc(m1,i-1,k,2))-(wa4(i-2)*cc(m1,i,k,5)- &
                      wa4(i-1)*cc(m1,i-1,k,5)))-ti11*((wa2(i-2)*cc(m1,i,k,3)- &
                      wa2(i-1)*cc(m1,i-1,k,3))-(wa3(i-2)*cc(m1,i,k,4)- &
                      wa3(i-1)*cc(m1,i-1,k,4))))
                    ch(m2, ic-1, 4, k) = (cc(m1,i-1,k,1)+tr12*((wa1(i-2)* &
                      cc(m1,i-1,k,2)+wa1(i-1)*cc(m1,i,k,2))+(wa4(i-2)*cc(m1,i- &
                      1,k,5)+wa4(i-1)*cc(m1,i,k,5)))+tr11*((wa2(i-2)*cc(m1,i-1 &
                      ,k,3)+wa2(i-1)*cc(m1,i,k,3))+(wa3(i-2)*cc(m1,i-1,k,4)+ &
                      wa3(i-1)*cc(m1,i,k,4)))) - (ti12*((wa1(i-2)*cc(m1,i,k,2) &
                      -wa1(i-1)*cc(m1,i-1,k,2))-(wa4(i-2)*cc(m1,i,k,5)- &
                      wa4(i-1)*cc(m1,i-1,k,5)))-ti11*((wa2(i-2)*cc(m1,i,k,3)- &
                      wa2(i-1)*cc(m1,i-1,k,3))-(wa3(i-2)*cc(m1,i,k,4)- &
                      wa3(i-1)*cc(m1,i-1,k,4))))
                    ch(m2, i, 5, k) = (cc(m1,i,k,1)+tr12*((wa1(i-2)* &
                      cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k,2))+(wa4(i-2)*cc(m1,i, &
                      k,5)-wa4(i-1)*cc(m1,i-1,k,5)))+tr11*((wa2(i-2)*cc(m1,i,k &
                      ,3)-wa2(i-1)*cc(m1,i-1,k,3))+(wa3(i-2)*cc(m1,i,k,4)- &
                      wa3(i-1)*cc(m1,i-1,k,4)))) + (ti12*((wa4(i-2)*cc(m1,i-1, &
                      k,5)+wa4(i-1)*cc(m1,i,k,5))-(wa1(i-2)*cc(m1,i-1,k,2)+ &
                      wa1(i-1)*cc(m1,i,k,2)))-ti11*((wa3(i-2)*cc(m1,i-1,k,4)+ &
                      wa3(i-1)*cc(m1,i,k,4))-(wa2(i-2)*cc(m1,i-1,k,3)+ &
                      wa2(i-1)*cc(m1,i,k,3))))
                    ch(m2, ic, 4, k) = (ti12*((wa4(i-2)*cc(m1,i-1,k,5)+ &
                      wa4(i-1)*cc(m1,i,k,5))-(wa1(i-2)*cc(m1,i-1,k,2)+ &
                      wa1(i-1)*cc(m1,i,k,2)))-ti11*((wa3(i-2)*cc(m1,i-1,k,4)+ &
                      wa3(i-1)*cc(m1,i,k,4))-(wa2(i-2)*cc(m1,i-1,k,3)+ &
                      wa2(i-1)*cc(m1,i,k,3)))) - (cc(m1,i,k,1)+tr12*((wa1(i-2) &
                      *cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k,2))+(wa4(i-2)*cc(m1,i &
                      ,k,5)-wa4(i-1)*cc(m1,i-1,k,5)))+tr11*((wa2(i-2)*cc(m1,i, &
                      k,3)-wa2(i-1)*cc(m1,i-1,k,3))+(wa3(i-2)*cc(m1,i,k,4)- &
                      wa3(i-1)*cc(m1,i-1,k,4))))
                end do
            end do
        end do
    end subroutine
