    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cosqmi(n, wsave, lensav, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        integer n, lensav, ier
        real (rwp) wsave(lensav)

        ier = 0

        if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('COSQMI', 3)
            go to 100
        end if

        pih = 2.*atan(1.0_rwp)
        dt = pih/float(n)
        fk = 0.
        do k = 1, n
            fk = fk + 1.
            wsave(k) = cos(fk*dt)
        end do
        lnsv = n + int(log(real(n, rwp))/log(2.0_rwp)) + 4
        call rfftmi(n, wsave(n+1), lnsv, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('COSQMI', -5)
        end if
100     continue
    end subroutine
