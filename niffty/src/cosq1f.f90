    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cosq1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        integer n, inc, lenx, lensav, lenwrk, ier
        real (rwp) x(inc, *), wsave(lensav), work(lenwrk)

        ier = 0

        if (lenx<inc*(n-1)+1) then
            ier = 1
            call xerfft('COSQ1F', 6)
            go to 130
        else if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('COSQ1F', 8)
            go to 130
        else if (lenwrk<n) then
            ier = 3
            call xerfft('COSQ1F', 10)
            go to 130
        end if

        if (n-2) 110, 100, 120
100     ssqrt2 = 1.0_rwp/sqrt(2.0_rwp)
        tsqx = ssqrt2*x(1, 2)
        x(1, 2) = .5_rwp*x(1, 1) - tsqx
        x(1, 1) = .5_rwp*x(1, 1) + tsqx
110     return
120     call cosqf1(n, inc, x, wsave, work, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('COSQ1F', -5)
        end if

130     continue
    end subroutine

