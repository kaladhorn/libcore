    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine sinqmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, lenwrk, &
      ier)
        include 'kinds.inc'
        integer lot, jump, n, inc, lenx, lensav, lenwrk, ier
        real (rwp) x(inc, *), wsave(lensav), work(lenwrk)
        logical xercon

        ier = 0

        if (lenx<(lot-1)*jump+inc*(n-1)+1) then
            ier = 1
            call xerfft('SINQMF', 6)
            go to 100
        else if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('SINQMF', 8)
            go to 100
        else if (lenwrk<lot*n) then
            ier = 3
            call xerfft('SINQMF', 10)
            go to 100
        else if (.not. xercon(inc,jump,n,lot)) then
            ier = 4
            call xerfft('SINQMF', -1)
            go to 100
        end if

        if (n==1) return
        ns2 = n/2
        lj = (lot-1)*jump + 1
        do k = 1, ns2
            kc = n - k
            do m = 1, lj, jump
                xhold = x(m, k)
                x(m, k) = x(m, kc+1)
                x(m, kc+1) = xhold
            end do
        end do
        call cosqmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, lenwrk, &
          ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('SINQMF', -5)
            go to 100
        end if
        do k = 2, n, 2
            do m = 1, lj, jump
                x(m, k) = -x(m, k)
            end do
        end do
100     continue
    end subroutine
