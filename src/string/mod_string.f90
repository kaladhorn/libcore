!******************************************************************************!
!                             mod_string module
!______________________________________________________________________________!
!> `[[FString(type)]]` derived type and TBPs, to handle variable-length
!> character strings.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_string
    use core !LCOV_EXCL_LINE

    implicit none
    private
    save

!******************************************************************************!
!> Derived type containing a variable-length character scalar and related
!> type-bound procedures.
!******************************************************************************!
    type, public :: FString
        private
        character(:), allocatable :: string_ !< Internal representation
        procedure(FStringComparisonFunction), pointer :: compare => fortranComparisonFunction !< Function to use when comparing with another string object
    contains
        procedure, public :: string         !< Get the internal representation of the string
        procedure, public :: length         !< Get the (logical) length of the string
        procedure, public :: internalLength !< Get the length of the internal representation
        procedure, public :: substring      !< Get a substring

        ! Test the content of the string
        procedure, public :: isBlank        !< Check whether the string is empty
        procedure, public :: startsWith     !< Check whether the string starts with a given substring
        procedure, public :: endsWith       !< Check whether the string ends with a given substring

        ! Overloaded concatenation operator
        procedure, private :: appendToChar  !< Append the string to a character variable
        procedure, private :: appendString  !< Concatenate two strings
        procedure, private, pass(this) :: appendCharTo !< Append a character variable to the string
        generic, public :: operator(//) => &
            appendToChar, appendCharTo, appendString   !< Concatenate the string with
                                                       !< other strings or character
                                                       !< variables

        ! Overloaded == comparison operator
        procedure, private :: equalsChar   !< Check whether the string is equivalent to a character variable
        procedure, private :: equalsString !< Check whether two strings are equivalent
        procedure, private, pass(this) :: charEquals   !< Check whether a character variable is equivalent to the string
        !> Test whether the string is equal to another string or a character variable
        generic, public :: operator(==) => equalsChar, equalsString, charEquals

        ! Overloaded /= comparison operator
        procedure, private :: differsFromChar   !< Check whether the string differs from a character variable
        procedure, private :: differsFromString !< Check whether two strings differ
        procedure, private, pass(this) :: charDiffers !< Check whether a character variable differs from the string
        !> Test whether the string is equal to another string or a character variable
        generic, public :: operator(/=) => differsFromChar, charDiffers, differsFromString

        ! Overloaded > comparison operator
        procedure, private :: greaterThanChar   !< Check whether a string is greater than a character variable
        procedure, private :: greaterThanString !< Check whether the string is greater than another string
        !> Test whether the string is greater than another string or a character variable
        generic, public :: operator(>) => greaterThanChar, greaterThanString

        ! Overloaded < comparison operator
        procedure, private :: lessThanChar
        procedure, private :: lessThanString
        !> Test whether the string is smaller than another string or a character variable
        generic, public :: operator(<) => lessThanChar, lessThanString

        ! Overloaded assignment for scalars
        procedure, private :: string_assignCharacter               !< Conversion from character to string
        procedure, pass(this), private :: string_assignToCharacter !< Conversion from string to character
        !> Assign a character variable or another string to the string
        generic, public :: assignment(=) => string_assignCharacter, string_assignToCharacter
    end type

!******************************************************************************!
!> Constructor interface.
!******************************************************************************!
    interface FString
        module procedure string_initWithChar, string_initWithString
    end interface

!******************************************************************************!
!> Interface for comparison functions. The purpose of this interface is to
!> allow user-defined comparison functions to implement different ways of
!> comparing strings.
!******************************************************************************!
    abstract interface
       pure function FStringComparisonFunction(from, to) result(comp)
            import
            class(FString), intent(in) :: from !< String to compare
            character(*),   intent(in) :: to   !< String to compare to
            integer :: comp !< `0` if both strings are equal, `1` if `from` is
                            !< greater than `to`, and `-1` otherwise
        end function
    end interface
    public :: FStringComparisonFunction

contains
!******************************************************************************!
!> Check whether a string begins with a given substring.
!******************************************************************************!
    pure function isBlank(this) result(blank)
        class(FString), intent(in) :: this !< Subject string
        logical :: blank !< `.true.` if the string is empty, `.false.` otherwise
!------
        if (allocated(this%string_)) then
            blank = (this%string_ == "")
        else
            blank = .true.
        end if
    end function
!******************************************************************************!
!> Check whether a string begins with a given substring.
!******************************************************************************!
    pure function startsWith(this, pattern) result(match)
        class(FString), intent(in) :: this    !< Subject string
        character(*),   intent(in) :: pattern !< Pattern to test
        logical :: match !< `.true.` if the string `this` starts with `pattern`,
                         !< `.false.` otherwise
!------
        match = .false.
        if (len(pattern) > len(this%string_)) return

        if (allocated(this%string_)) then
            match = (this%string_(1:len(pattern)) == pattern)
        end if
    end function
!******************************************************************************!
!> Check whether a string begins with a given substring.
!******************************************************************************!
    pure function endsWith(this, pattern) result(match)
        class(FString), intent(in) :: this    !< Subject string
        character(*),   intent(in) :: pattern !< Pattern to test
        logical :: match !< `.true.` if the string `this` ends with `pattern`,
                         !< `.false.` otherwise
!------
        match = .false.
        if (len(pattern) > len(this%string_)) return

        if (allocated(this%string_)) then
            match = (this%string_(len(this%string_)-len(pattern)+1:) == pattern)
        end if
    end function
!******************************************************************************!
!> Initialise a string object with a character variable.
!******************************************************************************!
    pure function string_initWithChar(char, compare) result(this)
        character(*), intent(in) :: char !< Character variable with the string's content
        character(*), intent(in), optional :: compare !< Pointer to the comparison
                                                      !< function to be used with this string
        type(FString) :: this !< Initialised string
!------
        this%string_ = char

        ! Set the comparison function
        if (present(compare)) then
            ! Use a standard comparison function
            select case(compare)
                case ("fortran")
                    this%compare => fortranComparisonFunction
                case("natural")
                    this%compare => naturalComparisonFunction
                case default
                    this%compare => naturalComparisonFunction
            end select
        else
            ! Default comparison function: intrinsic comparisons
            this%compare => naturalComparisonFunction
        end if
    end function
!******************************************************************************!
!> Initialise a string object with another string.
!******************************************************************************!
    pure function string_initWithString(string, compare) result(this)
        type(FString), intent(in) :: string !< String object with the string's content
        character(*), intent(in), optional :: compare !< Pointer to the comparison
                                                      !< function to be used with this string
        type(FString) :: this !< Initialised string
!------
        if (present(compare)) then
            ! Use the provided comparison function if there is one
            this = string_initWithChar(string%string_, compare)
        else
            ! Otherwise, use the same comparison function as the model
            this = string_initWithChar(string%string_)
        end if
    end function
!******************************************************************************!
!> Get a character variable containing the representation of a string object.
!> This is mainly useful in contexts where character variables are accepted but
!> not `[[FString(type)]]` objects.
!******************************************************************************!
    elemental function string(this) result(char)
        class(FString), intent(in) :: this
        character(:), allocatable :: char  !< Content of the string
!------

        if (.not.allocated(this%string_)) then
            char = ""
        else
            char = this%string_
        end if
    end function
!******************************************************************************!
!> Get the length of a string object. The length returned is the number of
!> unicode glyphs, which might be different from the length if the internal
!> representation. It also ignore ANSI escape codes.
!******************************************************************************!
    elemental function length(this)
        class(FString), intent(in) :: this
        integer :: length !< Length of the string
!------
        integer :: i, code
!------
        length = 0
        if (allocated(this%string_)) then
            i = 1

            ! Process each character in the string
            do while (i <= len(this%string_))

                ! Get the code for the current character
                code = ichar(this%string_(i:i))

                if (code == 27) then
                    ! The current character is ESC

                    ! Make sure that it is not the last character
                    ! Exit if it is (the effective length does not need to be incremented)
                    if (i >= len(this%string_)-1) exit

                    ! Get the code of the character following ESC
                    code = ichar(this%string_(i+1:i+1))

                    if (this%string_(i+1:i+1) == "[") then
                        ! This is a multiple-character sequence

                        ! Look for the end of the sequence
                        i = i + 1
                        sequenceLoop: do while(i <= len(this%string_))
                            i = i + 1
                            code = ichar(this%string_(i:i))

                            if (code >= 64 .and. code <= 126) exit sequenceLoop
                        end do sequenceLoop
                    else
                        ! This is not the begining of an ANSI sequence, or
                        ! an unsupported two-character ANSI sequence
                        i = i + 1
                        cycle
                    end if

                else if (code < int(z'80') .or. code > int(z'bf')) then
                    ! The current character is not a continuation
                    ! The string effective length needs to be incremented
                    length = length + 1
                end if

                i = i + 1
            end do
        end if
    end function
!******************************************************************************!
!> Get the length of the internal representation of a string object. The length
!> does not take into account unicode glyphs or ANSI codes, and thus can be
!> greater than the number of glyphs (e.g. the length of the string when printed
!> to a terminal).
!******************************************************************************!
    pure function internalLength(this)
        class(FString), intent(in) :: this
        integer :: internalLength !< Length of the internal representation of the string
!------
        internalLength = 0
        if (allocated(this%string_)) internalLength = len(this%string_)
    end function
!******************************************************************************!
!> Comparison function using standard Fortran order.
!******************************************************************************!
    pure function fortranComparisonFunction(from, to) result(comp)
        class(FString), intent(in) :: from
        character(*),   intent(in) :: to
        integer :: comp !< `0` if both strings are equal, `1` if `from` is
                        !< greater than `to`, and `-1` otherwise
!------
        if (from%string_ < to) then
            comp = -1
        else if (from%string_ > to) then
            comp = 1
        else
            comp = 0
        end if
    end function
!******************************************************************************!
!> Internal comparison function.
!******************************************************************************!
    pure recursive function naturalComparison(from, to) result(comp)
        character(*), intent(in) :: from
        character(*),   intent(in) :: to
        integer :: comp !< `0` if both strings are equal, `1` if `from` is
                        !< greater than `to`, and `-1` otherwise
!------
        integer :: start_from, end_from, start_to, end_to, i
        integer :: s, ifrom, ito, n
        logical :: void_from, void_to
        character(1) :: c, d, p
        logical :: c_number, d_number, p_number
!------
        
        comp = -huge(comp)

!------ Handle simple cases
        ! Determine whether any of the strings is empty
        void_to = (to== "")
        void_from = (from== "")

        ! Both strings are empty
        if (void_from .and. void_to) then
            comp = 0
            return
        end if

        ! The reference string is empty
        if (void_to.and..not.void_from) then
            comp = +1
            return
        end if

        ! The other string is empty
        if (void_from.and..not.void_to) then
            comp = -1
            return
        end if

        ! Both strings are identical
        if (from == to) then
            comp = 0
            return
        end if

        ! One string is a substring of the other
        s = min(len(from), len(to))
        if (from(:s) == to(:s)) then
            ! The greatest string is the longer one
            if (len(from) > len(to)) then
                comp = +1
            else
                comp = -1
            end if
            return
        end if

!------ General case
        ! Look for the first different character
        do i=1, min(len(from), len(to))
            if (from(i:i) == to(i:i)) cycle
            s = i
            exit
        end do
    
        ! Check whether at least one of the different characters is not a number
        c = from(i:i)
        d = to(i:i)
        if (i > 1) then
            p = from(i-1:i-1)
        else
            p = " "
        end if
        c_number = (iachar(c) >= iachar("0") .and. iachar(c) <= iachar("9"))
        d_number = (iachar(d) >= iachar("0") .and. iachar(d) <= iachar("9"))
        p_number = (iachar(p) >= iachar("0") .and. iachar(p) <= iachar("9"))
        if (.not.c_number .and. .not. d_number) then
            if (c == d) then
                comp =  0
            else if (c > d) then
                comp =  1
            else
                comp = -1
            end if
            return
        else if (c_number .and. .not. d_number .and. .not. p_number) then
            comp = -1
            return
        else if (.not.c_number .and. d_number .and. .not. p_number) then
            comp =  1
            return
        end if
    
        ! Try to backtrack to get the index of the first digit
        if (.not.p_number .or. s==1) then
            start_from = s
            start_to = s
        else
            do i=s-1, 1, -1
                if (iachar(from(i:i)) >= iachar("0") .and. iachar(from(i:i)) <= iachar("9")) then
                    start_from = i
                else
                    exit
                end if
            end do
            do i=s-1, 1, -1
                if (iachar(to(i:i)) >= iachar("0") .and. iachar(to(i:i)) <= iachar("9")) then
                    start_to = i
                else
                    exit
                end if
            end do
        end if

        ! Get the last digit in the first string
        if (c_number) then
            end_from = s
            do i=s, len(from)
                if (iachar(from(i:i)) >= iachar("0") .and. iachar(from(i:i)) <= iachar("9")) then
                    end_from = i
                else
                    exit
                end if
            end do
        else if (p_number) then
            end_from = s-1
        else
!            error stop ! Should be unreachable
        end if

        ! Get the last digit in the second string
        if (d_number) then
            end_to = s
            do i=s, len(to)
                if (iachar(to(i:i)) >= iachar("0") .and. iachar(to(i:i)) <= iachar("9")) then
                    end_to = i
                else
                    exit
                end if
            end do
        else if (p_number) then
            end_to = s-1
        else
!            error stop ! Should be unreachable
        end if
    
        ifrom = 0
        n = -1
        do i=end_from, start_from, -1
            n = n + 1
            ifrom = ifrom + (iachar(from(i:i)) - 48) * 10**n
        end do
    
        ito = 0
        n = -1
        do i=end_to, start_to, -1
            n = n + 1
            ito = ito + (iachar(to(i:i)) - 48) * 10**n
        end do
    
        if (ifrom == ito) then
            comp = naturalComparison(from(end_from+1:), to(end_to+1:))
        else if (ifrom > ito) then
            comp =  1
        else if (ifrom < ito) then
            comp = -1
        end if
    end function
!******************************************************************************!
!> Comparison function using "natural" order.
!******************************************************************************!
    pure function naturalComparisonFunction(from, to) result(comp)
        class(FString), intent(in) :: from
        character(*),   intent(in) :: to
        integer :: comp !< `0` if both strings are equal, `1` if `from` is
                        !< greater than `to`, and `-1` otherwise
!------

        if (allocated(from%string_)) then
            comp = naturalComparison(from%string_, to)
        else
            comp = naturalComparison("", to) !LCOV_EXCL_LINE
        end if
    end function
!******************************************************************************!
!> Assign a string object to a character variable. In contrast with the
!> constructor `[[FString(interface)]]`, the comparison function cannot be
!> provided. The resulting `[[FString(type)]]` object will use the default
!> comparison function.
!******************************************************************************!
    pure subroutine string_assignToCharacter(char, this)
        character(:), allocatable, intent(inout) :: char
        class(FString), intent(in) :: this !< Initialised string
!------
        char = this%string_
    end subroutine
!******************************************************************************!
!> Assignment to a string object. In contrast with the constructor
!> `[[FString(interface)]]`, the comparison function cannot be provided. The
!> resulting `[[FString(type)]]` object will use the default comparison
!> function.
!******************************************************************************!
    pure subroutine string_assignCharacter(this, char)
        class(FString), intent(inout) :: this
        character(*), intent(in) :: char !< Initialised string
!------
        this%string_ = char
        this%compare => naturalComparisonFunction
    end subroutine
!******************************************************************************!
!> Concatenate a string object and a character scalar.
!******************************************************************************!
    pure function appendToChar(this, string) result(out)
        class(FString), intent(in) :: this
        character(*), intent(in) :: string
        character(:), allocatable :: out
!------
        out = this%string_ // string
    end function
!******************************************************************************!
!> Concatenate a character scalar and a string object.
!******************************************************************************!
    pure function appendCharTo(string, this) result(out)
        class(FString), intent(in) :: this
        character(*), intent(in) :: string
        character(:), allocatable :: out
!------
        out = string // this%string_
    end function
!******************************************************************************!
!> Concatenate two string objects.
!******************************************************************************!
    pure function appendString(this, string) result(out)
        class(FString), intent(in) :: this
        class(FString), intent(in) :: string
        character(:), allocatable :: out
!------
        out = this%string_ // string%string_
    end function
!******************************************************************************!
!> Compare a string object and a character variable.
!******************************************************************************!
    elemental function equalsChar(this, char) result(comp)
        class(FString), intent(in) :: this
        character(*), intent(in) :: char
        logical :: comp
!------
        integer :: i
!------
        ! Handle variables with a 0 length
        if (.not.allocated(this%string_)) then
            comp = (char == "")
        else
            ! Call the comparison function
            i = this%compare(char)
            if (i == 0) then
                comp = .true.
            else
                comp = .false.
            end if
        end if
    end function
!******************************************************************************!
!> Compare a string object and a character variable.
!******************************************************************************!
    elemental function charEquals(char, this) result(comp)
        character(*),   intent(in) :: char
        class(FString), intent(in) :: this
        logical :: comp
!------
        comp = equalsChar(this, char)
    end function
!******************************************************************************!
!> Compare two string objects.
!******************************************************************************!
    elemental function equalsString(this, string) result(comp)
        class(FString), intent(in) :: this
        class(FString), intent(in) :: string
        logical :: comp
!------
        if (allocated(string%string_)) then
            comp = equalsChar(this, string%string_)
        else
            comp = equalsChar(this, "")
        end if
    end function
!******************************************************************************!
!> Compare a string object and a character variable.
!******************************************************************************!
    elemental function differsFromChar(this, char) result(comp)
        class(FString), intent(in) :: this
        character(*), intent(in) :: char
        logical :: comp
!------
        comp = .not.equalsChar(this, char)
    end function
!******************************************************************************!
!> Compare a string object and a character variable.
!******************************************************************************!
    elemental function charDiffers(char, this) result(comp)
        character(*), intent(in) :: char
        class(FString), intent(in) :: this
        logical :: comp
!------
        comp = .not.equalsChar(this, char)
    end function
!******************************************************************************!
!> Compare two string objects.
!******************************************************************************!
    elemental function differsFromString(this, string) result(comp)
        class(FString), intent(in) :: this
        class(FString), intent(in) :: string
        logical :: comp
!------
        if (allocated(string%string_)) then
            comp = .not.equalsChar(this, string%string_)
        else
            comp = .not.equalsChar(this, "")
        end if
    end function
!******************************************************************************!
!> Compare a string object and a character variable.
!******************************************************************************!
    elemental function greaterThanChar(this, char) result(comp)
        class(FString), intent(in) :: this
        character(*), intent(in) :: char
        logical :: comp
!------
        integer :: i
!------

        ! Handle variables with a 0 length
        if (.not.allocated(this%string_)) then
            comp = .false.
        else
            ! Call the comparison function
            i = this%compare(char)
            if (i > 0) then
                comp = .true.
            else
                comp = .false.
            end if
        end if
    end function
!******************************************************************************!
!> Compare two string objects.
!******************************************************************************!
    elemental function greaterThanString(this, string) result(comp)
        class(FString), intent(in) :: this
        class(FString), intent(in) :: string
        logical :: comp
!------
        if (allocated(string%string_)) then
            comp = greaterThanChar(this, string%string_)
        else
            comp = greaterThanChar(this, "")
        end if
    end function
!******************************************************************************!
!> Compare a string object and a character variable.
!******************************************************************************!
    elemental function lessThanChar(this, char) result(comp)
        class(FString), intent(in) :: this
        character(*), intent(in) :: char
        logical :: comp
!------
        integer :: i
!------

        ! Handle variables with a 0 length
        if (.not.allocated(this%string_) .and. char == "") then
            comp = .false.
        else if (.not.allocated(this%string_) .and. char /= "") then
            comp = .true.
        else
            ! Call the comparison function
            i = this%compare(char)
            if (i < 0) then
                comp = .true.
            else
                comp = .false.
            end if
        end if
    end function
!******************************************************************************!
!> Compare two string objects.
!******************************************************************************!
    elemental function lessThanString(this, string) result(comp)
        class(FString), intent(in) :: this
        class(FString), intent(in) :: string
        logical :: comp
!------
        if (allocated(string%string_)) then
            comp = lessThanChar(this, string%string_)
        else
            comp = lessThanChar(this, "")
        end if
    end function
!******************************************************************************!
!> Get a substring defined by a beginning and an end indices. The substring
!> bounds corresponds to unicode glyphs, ANSI code are not supported yet.
!> If `from` is not present, the substring will start from the beginning of the
!> string. If `to` is not present, the substring will end at the end of the
!> string.
!******************************************************************************!
    pure function substring(this, from, to)
        type(FString) :: substring  !< Extracted substring; `""` if `from` is greater than `to`.
        class(FString), intent(in) :: this
        integer, intent(in), optional :: from !< Index of the first character of the substring
        integer, intent(in), optional :: to   !< Index of the last character of the substring
!------
        integer :: i, j, c, start, end, n, code
!------
        ! Get the lower bound of the substring
        if (present(from)) then
            i = from
        else
            i = 1
        end if

        ! Get the upper bound of the substring
        if (present(to)) then
            j = to
        else
            j = this%length()
        end if

        ! Special case: empty string
        if (j<i) then
            substring = ""
            return
        end if

        ! Calculate the indices corresponding to the bounds
        start = 0
        end = 0
        n = 0
        do c=1, len(this%string())
            ! Check whether the current character is a continuation
            code = ichar(this%string_(c:c))

            if (code < int(z'80') .or. code > int(z'bf')) then
                ! If not, increment the string length
                n = n + 1
                if (n == i) then
                    start = c
                end if
                if (n-1 == j) then
                    end = c-1
                end if
            end if
        end do
        if (j==this%length()) end = len(this%string_)

        ! Copy the substring
        substring = this%string_(start:end)
    end function
end module
