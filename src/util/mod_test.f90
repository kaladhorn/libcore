!******************************************************************************!
!                             mod_test module
!______________________________________________________________________________!
!> Basic infrastructure for unit testing.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_test
    use ext_character

    implicit none
    private
    save

    integer, private :: passed = 0
    integer, private :: failed = 0

    public :: testsFinished
    public :: check
    public :: xfail
    public :: startTestGroup
contains
!******************************************************************************!
!> Print the header for a test group
!******************************************************************************!
    subroutine startTestGroup(groupName, description)
        character(*), intent(in) :: groupName
        character(*), intent(in), optional :: description
!------
        write(*,*)
        write(*,*) "Testing "//groupName
        if (present(description)) then
            write(*,*) "  "//description
            write(*,*)
        end if
    end subroutine
!******************************************************************************!
!> Test a condition, and print a message indicating the test result
!******************************************************************************!
    subroutine check(test, name, description)
        logical, intent(in) :: test
        character(*), intent(in) :: name
        character(*), intent(in), optional :: description
!------
        if (test) then
            write(*,*) "["//colour("passed", "green")//"] "//name
            passed = passed + 1
            return
        end if

        failed = failed + 1
        write(*,*) "["//colour("failed", "red")//"] "//name
        if (present(description)) write(*,*) description
    end subroutine
!******************************************************************************!
!> Test a condition, and print a message indicating the test result, for tests
!> that are expected to fail
!******************************************************************************!
    subroutine xfail(test, name, description)
        logical, intent(in) :: test
        character(*), intent(in) :: name
        character(*), intent(in), optional :: description
!------
        if (.not.test) then
            write(*,*) "["//colour("xfail", "yellow")//"] "//name
            return
        end if

        failed = failed + 1
        write(*,*) "["//colour("xpass", "red")//"] "//name
        if (present(description)) write(*,*) description
    end subroutine
!******************************************************************************!
!> Print a summary of the tests
!******************************************************************************!
    subroutine testsFinished
        write(*,*)
        write(*,*) "Tests summary:"
        write(*,*) "Passed: ", passed
        write(*,*) "Failed: ", failed
        write(*,*) "Total:  ", (passed+failed)

        ! Return a non-zero error code if at least one test has failed
        if (failed /= 0) error stop
    end subroutine
end module