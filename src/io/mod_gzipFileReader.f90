!******************************************************************************!
!                         mod_gzipFileReader module
!------------------------------------------------------------------------------!
!> `[[FGzipFileReader(type)]]` derived type and TBPs, for gzip-compressed file
!> I/O.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!  Uses the zlib library written by Jean-Loup Gailly and Mark Adler,
!  Copyright (c) 1995-2015 Jean-Loup Gailly and Mark Adler,
!  available at http://www.zlib.net.
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_gzipFileReader
    use iso_c_binding

    use core
    use ext_character

    use mod_string,     only: FString
    use mod_exception,  only: FException, MESSAGE_LEVEL_LOG
    use mod_fileReader, only: FFileReader

    implicit none
    private
    save

    type, public, extends(FFileReader) :: FGzipFileReader
        private
        character(:), allocatable :: buffer
        type(c_ptr) :: file = C_NULL_PTR
    contains
        procedure, public :: canRead
        procedure, public :: canWrite

        procedure, public :: openFile
        procedure, public :: closeFile

        procedure, public :: getNextLine_character
        procedure, public :: getNextLine_string
        procedure, public :: getLines

        procedure, public :: writeLine
        procedure, public :: writeLines
    end type

    integer, parameter :: BUFFER_LENGTH = 65536

    interface
        function gzopen(path, mode) bind(C, name='gzopen')
            use iso_c_binding
            character(kind=c_char), dimension(*) :: path, mode
            type (c_ptr) :: gzopen
        end function
        function gzread(filehandle, buf, len) bind(C, name='gzread')
            use iso_c_binding
            integer (c_int) :: gzread
            type (c_ptr),   value :: filehandle
            type (c_ptr),   value :: buf
            integer(c_int), value :: len
        end function
        function gzwrite(filehandle, buf, len) bind(C, name='gzwrite')
            use iso_c_binding
            integer (c_int) :: gzwrite
            type (c_ptr),   value :: filehandle
            type (c_ptr),   value :: buf
            integer(c_int), value :: len
        end function
        function gzclose(filehandle) bind(C, name='gzclose')
            use iso_c_binding
            integer(c_int) :: gzclose
            type (c_ptr), value :: filehandle
        end function
    end interface
contains

    function canRead(this)
        class(FGzipFileReader), intent(in) :: this
        logical :: canRead

        canRead = c_associated(this%file)
    end function

    function canWrite(this)
        class(FGzipFileReader), intent(in) :: this
        logical :: canWrite

        canWrite = c_associated(this%file)
    end function

    subroutine openFile(this, filename, mode, stat)
        class(FGzipFileReader), intent(inout), target :: this
        character(*), intent(in) :: filename
        character(*), intent(in) :: mode
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        if (c_associated(this%file)) then
            call stat_%raise("The file "//filename//" is already open")
        else
            select case(mode)
                case ("r", "read")
                    this%file = gzopen(filename//C_NULL_CHAR, "rb"//C_NULL_CHAR)
                case ("w", "write")
                    this%file = gzopen(filename//C_NULL_CHAR, "wb"//C_NULL_CHAR)
                case ("a", "append")
                    this%file = gzopen(filename//C_NULL_CHAR, "ab+"//C_NULL_CHAR)
            end select

            if (.not.c_associated(this%file)) then
                call stat_%raise("Error while trying to open file "//filename)
            end if
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine closeFile(this, stat)
        class(FGzipFileReader), intent(inout):: this
        type(FException), intent(out), optional :: stat

        integer :: ierr
        type(FException) :: stat_
!------
        if (c_associated(this%file)) then
            ierr = gzclose(this%file)
            if (ierr /= 0) call stat_%raise("Error while closing file: "//ierr)
            this%file = C_NULL_PTR
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine getNextLine_character(this, line, stat)
        class(FGzipFileReader), intent(inout):: this
        character(*), intent(out) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: i, rsize
        character(BUFFER_LENGTH,kind=C_CHAR), target :: lbuf
        character(:), allocatable :: temp
        type(FException) :: stat_
!------
        ! Check whether a complete line is already in the buffer
        i = -1
        if (allocated(this%buffer)) i = index(this%buffer, new_line(""))

        ! If not, read from the file until there is one
        do while (i == 0 .or. .not.allocated(this%buffer))
            lbuf = ""
            rsize = gzread(this%file, c_loc(lbuf(1:1)), len(lbuf))

            ! Test if the file is empty
            if (.not.allocated(this%buffer) .and. rsize == 0) then
                line = ""
                call stat_%raise("End of file", MESSAGE_LEVEL_LOG)
                exit
            end if

            ! Add the local buffer to the file buffer
            if (.not.allocated(this%buffer)) then
                this%buffer = lbuf(:rsize)
            else
                this%buffer = this%buffer // lbuf(:rsize)
            end if

            ! Check whether there is a line break in the file buffer
            i = index(this%buffer, new_line(this%buffer))
            if (rsize < len(lbuf)) then
                exit
            end if
        end do

        ! Get the line from the buffer
        if (stat_ == 0) then
            if (rsize < len(lbuf) .and. this%buffer=="") then
                ! End of file
                line = ""
                call stat_%raise("End of file")
            else if (i == 0) then
                ! If there is no line termination, return the whole buffer
                line = this%buffer
                this%buffer = ""
            else
                ! If there is a line termination, return the next line and update the buffer
                line = this%buffer(:i-1)
                temp = this%buffer
                this%buffer = temp(i+1:)
            end if
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine getNextLine_string(this, line, stat)
        class(FGzipFileReader), intent(inout):: this
        type(FString), intent(out) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: i, rsize
        character(BUFFER_LENGTH,kind=C_CHAR), target :: lbuf
        character(:), allocatable :: temp
        type(FException) :: stat_
!------
        ! Check whether a complete line is already in the buffer
        i = -1
        if (allocated(this%buffer)) i = index(this%buffer, new_line(""))

        ! If not, read from the file until there is one
        do while (i == 0 .or. .not.allocated(this%buffer))
            lbuf = ""
            rsize = gzread(this%file, c_loc(lbuf(1:1)), len(lbuf))

            ! Test if the file is empty
            if (.not.allocated(this%buffer) .and. rsize == 0) then
                line = ""
                call stat_%raise("End of file", MESSAGE_LEVEL_LOG)
                exit
            end if

            ! Add the local buffer to the file buffer
            if (.not.allocated(this%buffer)) then
                this%buffer = lbuf(:rsize)
            else
                this%buffer = this%buffer // lbuf(:rsize)
            end if

            ! Check whether there is a line break in the file buffer
            i = index(this%buffer, new_line(this%buffer))
            if (rsize < len(lbuf)) then
                exit
            end if
        end do

        ! Get the line from the buffer
        if (stat_ == 0) then
            if (rsize < len(lbuf) .and. this%buffer=="") then
                ! End of file
                line = ""
                call stat_%raise("End of file")
            else if (i == 0) then
                ! If there is no line termination, return the whole buffer
                line = this%buffer
                this%buffer = ""
            else
                ! If there is a line termination, return the next line and update the buffer
                line = this%buffer(:i-1)
                temp = this%buffer
                this%buffer = temp(i+1:)
            end if
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine getLines(this, lines, stat)
        class(FGzipFileReader), intent(inout) :: this
        type(FString), dimension(:), allocatable, intent(out) :: lines
        type(FException), intent(out), optional :: stat
!------
        integer :: i, rsize, j
        character(BUFFER_LENGTH,kind=C_CHAR), target :: lbuf
        type(FException) :: stat_
        character(:), allocatable :: buffer
        integer :: nLines
        character(1) :: nl
!------

        nl = new_line(c_char_"")
        if (c_associated(this%file)) then

            ! Get the content of the file in the buffer
            lbuf = ""
            buffer = ""
            rsize = gzread(this%file, c_loc(lbuf(1:1)), len(lbuf))
            do while (rsize == len(lbuf))
                buffer = buffer // lbuf
                lbuf = ""
                rsize = gzread(this%file, c_loc(lbuf(1:1)), len(lbuf))
            end do
            buffer = buffer//lbuf(:rsize)

            ! Count the number of lines in the buffer
            i = index(buffer, nl)
            j = 1
            nLines = 0
            do while (i /= 0)
                nLines = nLines + 1
                j = j + i
                i = index(buffer(j:), new_line(buffer))
            end do

            ! Count one more line if the last character of the file is not a new line
            if (buffer(len(buffer):) /= new_line(buffer)) nLines = nLines + 1

            ! Fill the lines array
            allocate(lines(nLines))
            i = index(buffer, new_line(buffer))
            nLines = 1
            j = 1
            lines(nLines) = buffer(:i-1)
            do while (i /= 0)
                nLines = nLines + 1
                j = j + i

                ! Get the index of the next new line
                i = index(buffer(j:), new_line(buffer))

                ! Set the nLines-th line
                if (i == 0 .and. buffer(len(buffer):) /= new_line(buffer)) then
                    lines(nLines) = buffer(j:)
                else if (i == 0) then
                    exit
                else
                    lines(nLines) = buffer(j:j+i-2)
                end if
            end do
        else

        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine writeLine(this, line, stat)
        class(FGzipFileReader), intent(inout):: this
        character(*), intent(in) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: i
        type(FException) :: stat_
        character(:,c_char), allocatable, target :: buff
!------

        if (c_associated(this%file)) then
            buff = line // new_line(c_char_"")
            i = gzwrite(this%file, c_loc(buff(1:1)), len(buff))
            if (i/=len(buff)) call stat_%raise("I/O error.")
        else

        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine writeLines(this, lines, stat)
        class(FGzipFileReader), intent(inout) :: this
        type(FString), dimension(:), intent(in) :: lines
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        character(:,c_char), allocatable, target :: buff
        integer :: i, l
!------

        if (c_associated(this%file)) then
            do i=1, size(lines)
                buff = lines(i) // new_line(c_char_"")
                l = gzwrite(this%file, c_loc(buff(1:1)), len(buff))
                if (l/=len(buff)) call stat_%raise("I/O error.")
            end do
        else

        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
end module