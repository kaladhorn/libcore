    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cfft2i(l, m, wsave, lensav, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        integer l, m, ier
        real (rwp) wsave(lensav)

        ! Initialize error return

        ier = 0

        if (lensav<2*l+int(log(real(l, rwp))/log(2.0_rwp))+2*m+int(log(real(m, rwp))/ &
          log(2.0_rwp))+8) then
            ier = 2
            call xerfft('CFFT2I', 4)
            go to 100
        end if

        call cfftmi(l, wsave(1), 2*l+int(log(real(l, rwp))/log(2.0_rwp))+4, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('CFFT2I', -5)
            go to 100
        end if
        call cfftmi(m, wsave(2*l+int(log(real(l, rwp))/log(2.0_rwp))+3), 2*m+int(log( &
          real(m, rwp))/log(2.0_rwp))+4, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('CFFT2I', -5)
        end if

100     continue
    end subroutine
