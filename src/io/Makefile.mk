#******************************************************************************#
#                                libCore io                                    #
#______________________________________________________________________________#
#
#  Version 2.0
#
#  Written by Paul Fossati, <paul.fossati@gmail.com>
#  Copyright (c) 2009-2016 Paul Fossati
#------------------------------------------------------------------------------#
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
#     * Redistributions of source code must retain the above copyright notice, #
#       this list of conditions and the following disclaimer.                  #
#                                                                              #
#     * Redistributions in binary form must reproduce the above copyright      #
#       notice, this list of conditions and the following disclaimer in the    #
#       documentation and/or other materials provided with the distribution.   #
#                                                                              #
#     * The name of the author may not be used to endorse or promote products  #
#      derived from this software without specific prior written permission    #
#      from the author.                                                        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#******************************************************************************#
IO_SOURCES= io/com_io.f90 io/mod_directory_wrapper.c io/mod_directory.f90      \
	io/mod_fileReader.f90 io/mod_bzip2FileReader.f90 io/mod_gzipFileReader.f90 \
	io/mod_textFileReader.f90 io/mod_file.f90

#******************************************************************************!
# Dependencies
#******************************************************************************!
io/mod_textFileReader.o: io/mod_textFileReader.f90 core.mod ext_character.mod  \
	mod_exception.mod mod_string.mod mod_filereader.mod mod_directory.mod      \
    com_io.mod
mod_textfilereader.mod: io/mod_textFileReader.o

io/mod_gzipFileReader.o: io/mod_gzipFileReader.f90 core.mod ext_character.mod  \
	mod_exception.mod mod_string.mod mod_filereader.mod
mod_gzipfilereader.mod: io/mod_gzipFileReader.o

io/mod_fileReader.o: io/mod_fileReader.f90 core.mod mod_exception.mod          \
	mod_string.mod
mod_filereader.mod: io/mod_fileReader.o

io/mod_file.o: io/mod_file.f90 core.mod ext_character.mod mod_exception.mod    \
	mod_regex.mod mod_string.mod mod_directory.mod mod_filereader.mod          \
	mod_textfilereader.mod mod_gzipfilereader.mod mod_bzip2filereader.mod      \
    mod_stringdictionary.mod com_io.mod
mod_file.mod: io/mod_file.o

io/mod_directory.o: io/mod_directory.f90 com_io.mod fcre.mod mod_string.mod   \
    ext_character.mod mod_regex.mod  mod_exception.mod
mod_directory.mod: io/mod_directory.o

io/mod_bzip2FileReader.o: io/mod_bzip2FileReader.f90 core.mod mod_string.mod  \
    ext_character.mod mod_exception.mod  mod_filereader.mod mod_directory.mod \
    com_io.mod
mod_bzip2filereader.mod: io/mod_bzip2FileReader.o

io/com_io.o: ext_character.mod
com_io.mod: io/com_io.o
