!******************************************************************************!
!                           ext_character module
!------------------------------------------------------------------------------!
!> Operators for character variables.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module ext_character
    use iso_c_binding

    use core

    implicit none
    private
    save

!******************************************************************************!
!> Overloaded assignment operator to create character representation of
!> numerical variables
!******************************************************************************!
    interface assignment(=)
        module procedure characterWithInteger, characterWithIntegerArray, &
            characterWithSingle, characterWithSingleArray, &
            characterWithDouble, characterWithDoubleArray, &
            characterWithQuad, characterWithQuadArray, &
            characterWithLogical
    end interface
    public :: assignment(=)

!******************************************************************************!
!> Overloaded concatenation for character and numerical variables
!******************************************************************************!
    interface operator(//)
        module procedure character_appendInteger, integer_appendCharacter, &
            character_appendSingle,  single_appendCharacter, &
            character_appendDouble,  double_appendCharacter, &
            character_appendQuad,    quad_appendCharacter,   &
            character_appendLogical, logical_appendCharacter, &
            character_appendIntegerArray, integerArray_appendCharacter, &
            character_appendSingleArray, singleArray_appendCharacter, &
            character_appendDoubleArray, doubleArray_appendCharacter, &
            character_appendQuadArray, quadArray_appendCharacter
    end interface
    public :: operator(//)

    ! ANSI colour codes
    character(*), parameter :: ANSI_COLOUR_DEFAULT = achar(27)//"[0m"
    character(*), parameter :: ANSI_COLOUR_RED     = achar(27)//"[31m"
    character(*), parameter :: ANSI_COLOUR_GREEN   = achar(27)//"[32m"
    character(*), parameter :: ANSI_COLOUR_YELLOW  = achar(27)//"[33m"
    character(*), parameter :: ANSI_COLOUR_BLUE    = achar(27)//"[34m"
    character(*), parameter :: ANSI_COLOUR_CYAN    = achar(27)//"[35m"
    character(*), parameter :: ANSI_COLOUR_MAGENTA = achar(27)//"[36m"

    ! Set the colour of a character variable
    public :: colour
    public :: bold
    public :: apparentLength
    public :: c_f_string

    ! Interface to grisu3
    interface
        ! dtoa_grisu3(double v, char *dst);
        pure function grisu3(v, dst) bind(C, name="dtoa_grisu3")
            use iso_c_binding
            real(c_double), value :: v ! number to write
            type(c_ptr), value :: dst ! Character string; length should be 25
            integer(c_int) :: grisu3 ! Number of characters written
        end function
    end interface
    public :: grisu3
contains
!******************************************************************************!
!> Conversion from C to Fortran strings.
!******************************************************************************!
    subroutine c_f_string(stringptr, fstring)
        type(c_ptr), intent(in) :: stringptr
        character(:), allocatable, intent(out) :: fstring
!------
        character(1,c_char), dimension(:), pointer :: buffer
        integer(c_size_t) :: i
        integer(c_size_t), dimension(1) :: l

        interface
            function strlen(string) bind(C)
                import
                type(c_ptr), value :: string
                integer(c_size_t) :: strlen
            end function
        end interface
!------
        if (.not.c_associated(stringptr)) return
        l(1) = strlen(stringptr)
        call c_f_pointer(stringptr, buffer, l)
        allocate(character(l(1)) :: fstring)
        do i=1, l(1)
            fstring(i:i) = buffer(i)
        end do
    end subroutine
!******************************************************************************!
!> Return the length of a character string as it would be printed in a
!> terminal. The ANSI escape sequences are ignored.
!******************************************************************************!
    pure function apparentLength(string) result(length)
        character(*), intent(in) :: string
        integer :: length
!------
        character(:), allocatable :: string_, newString
        integer :: i, j
!------
        string_ = string
        i = index(string_, char(27)//"[")
        do while(i /= 0)
            j = index(string_(i:), "m")
            if (j == 0) exit
            if (i == 1) then
                allocate(newString, source=string_(i+j:))
            else
                allocate(newString, source=string_(:i-1)//string_(i+j:))
            end if
            string_ = newString
            i = index(string_, char(27)//"[")
        end do

        length = len(string_)
    end function
!******************************************************************************!
!> Format a string by turning it into bold characters using an ANSI escape
!> sequence.
!******************************************************************************!
    pure function bold(string)
        character(*), intent(in) :: string
        character(:), allocatable :: bold
!------
        bold = char(27)//"[1m"//string//char(27)//"[0m"
    end function
!******************************************************************************!
!> Set the colour of a character variable for terminal output
!******************************************************************************!
    pure function colour(string, colourName) result(this)
        character(*), intent(in)  :: string
        character(*), intent(in)  :: colourName
        character(:), allocatable :: this
!------
        select case(colourName)
            case ("red")
                this = ANSI_COLOUR_RED//string//ANSI_COLOUR_DEFAULT
            case ("green")
                this = ANSI_COLOUR_GREEN//string//ANSI_COLOUR_DEFAULT
            case ("yellow")
                this = ANSI_COLOUR_YELLOW//string//ANSI_COLOUR_DEFAULT
            case ("blue")
                this = ANSI_COLOUR_BLUE//string//ANSI_COLOUR_DEFAULT
            case ("cyan")
                this = ANSI_COLOUR_CYAN//string//ANSI_COLOUR_DEFAULT
            case ("magenta")
                this = ANSI_COLOUR_MAGENTA//string//ANSI_COLOUR_DEFAULT
        end select
    end function
!******************************************************************************!
!> Concatenate a character variable and a representation of an integer array
!******************************************************************************!
    pure function character_appendIntegerArray(string, x) result(out)
        character(*), intent(in) :: string
        integer, dimension(:), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Concatenate a representation of an integer array and a character variable
!******************************************************************************!
    pure function integerArray_appendCharacter(x, string) result(out)
        character(*), intent(in) :: string
        integer, dimension(:), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Concatenate a representation of a single-precision real array and a
!> character variable
!******************************************************************************!
    pure function character_appendSingleArray(string, x) result(out)
        character(*), intent(in) :: string
        real(sp), dimension(:), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Concatenate a character variable with a representation of a single-precision
!> real array
!******************************************************************************!
    pure function singleArray_appendCharacter(x, string) result(out)
        character(*), intent(in) :: string
        real(sp), dimension(:), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Concatenate a representation of a double-precision real array and a
!> character variable
!******************************************************************************!
    pure function character_appendDoubleArray(string, x) result(out)
        character(*), intent(in) :: string
        real(dp), dimension(:), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Concatenate a character variable with a representation of a double-precision
!> real array
!******************************************************************************!
    pure function doubleArray_appendCharacter(x, string) result(out)
        character(*), intent(in) :: string
        real(dp), dimension(:), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Concatenate a representation of a quadruple-precision real array and a
!> character variable
!******************************************************************************!
    pure function character_appendQuadArray(string, x) result(out)
        character(*), intent(in) :: string
        real(qp), dimension(:), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Concatenate a character variable with a representation of a quadruple-
!> precision real array
!******************************************************************************!
    pure function quadArray_appendCharacter(x, string) result(out)
        character(*), intent(in) :: string
        real(qp), dimension(:), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Append an integer value to a character variable
!******************************************************************************!
    pure function character_appendInteger(string, x) result(out)
        character(*), intent(in) :: string
        integer, intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Append an integer value to a character variable
!******************************************************************************!
    pure function integer_appendCharacter(x, string) result(out)
        integer, intent(in) :: x
        character(*), intent(in) :: string
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Append a single-precision value to a character variable
!******************************************************************************!
    pure function character_appendSingle(string, x) result(out)
        character(*), intent(in) :: string
        real(sp), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Append a character variable to a single-precision value
!******************************************************************************!
    pure function single_appendCharacter(x, string) result(out)
        real(sp), intent(in) :: x
        character(*), intent(in) :: string
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Append a double-precision value to a character variable
!******************************************************************************!
    pure function character_appendDouble(string, x) result(out)
        character(*), intent(in) :: string
        real(dp), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Append a character variable to a doube-precision value
!******************************************************************************!
    pure function double_appendCharacter(x, string) result(out)
        real(dp), intent(in) :: x
        character(*), intent(in) :: string
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Append a quad-precision value to a character variable
!******************************************************************************!
    pure function character_appendQuad(string, x) result(out)
        character(*), intent(in) :: string
        real(qp), intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Append a character variable to a quad-precision value
!******************************************************************************!
    pure function quad_appendCharacter(x, string) result(out)
        real(qp), intent(in) :: x
        character(*), intent(in) :: string
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Append a logical value to a character variable
!******************************************************************************!
    pure function character_appendLogical(string, x) result(out)
        character(*), intent(in) :: string
        logical, intent(in) :: x
        character(:), allocatable :: out
!------
        out = x
        out = string // out
    end function
!******************************************************************************!
!> Append a character variable to a logical value
!******************************************************************************!
    pure function logical_appendCharacter(x, string) result(out)
        logical, intent(in) :: x
        character(*), intent(in) :: string
        character(:), allocatable :: out
!------
        out = x
        out = out // string
    end function
!******************************************************************************!
!> Create a character variable representing an integer number
!******************************************************************************!
    pure subroutine characterWithInteger(this, i)
        character(:), allocatable, intent(out) :: this
        integer, intent(in) :: i
!------
        integer :: n
!------
        if (i == 0) then
            n = 1
        else if (i > 0) then
            n = floor(log10(i*1.0_e)) + 1
        else if (i < 0) then
            n = floor(log(-1.0_e*i)) + 2
        end if
        allocate(character(n) :: this)

        write(this,'(i0)') i
    end subroutine
!******************************************************************************!
!> Create a character variable representing an integer array
!******************************************************************************!
    pure subroutine characterWithIntegerArray(this, x)
        character(:), allocatable, intent(out) :: this
        integer, dimension(:), intent(in) :: x
!------
        integer :: n
!------
        ! Get the number list
        this = x(1)
        do n=2, size(x)
            this = this // ", " // x(n)
        end do

        ! Add the brackets
        if (size(x) <= 4) then
            this = "[" // this // "]"
        else
            this = "(" // this // ")"
        end if
    end subroutine
!******************************************************************************!
!> Create a string object representing a single-precision number
!******************************************************************************!
    pure subroutine characterWithSingle(this, number)
        character(:), allocatable, intent(out) :: this
        real(sp), intent(in) :: number
!------
        include 'string/characterWithReal.inc'
    end subroutine

    pure subroutine characterWithSingleArray(this, x)
        character(:), allocatable, intent(out) :: this
        real(sp), dimension(:), intent(in) :: x
!------
        integer :: n
!------
        ! Get the number list
        this = x(1)
        do n=2, size(x)
            this = this // ", " // x(n)
        end do

        ! Add the brackets
        if (size(x) <= 4) then
            this = "[" // this // "]"
        else
            this = "(" // this // ")"
        end if
    end subroutine
!******************************************************************************!
!> Create a string object representing a double-precision real number
!******************************************************************************!
    pure subroutine characterWithDouble(this, number)
        use iso_c_binding

        character(:), allocatable, intent(out) :: this
        real(dp), intent(in) :: number
!------
        character(25,c_char), target :: buf
        integer :: i
!------
        i = grisu3(number, c_loc(buf))
        this = buf(:i)
    end subroutine
!******************************************************************************!
!> Create a string object representing a double-precision real array
!******************************************************************************!
    pure subroutine characterWithDoubleArray(this, x)
        character(:), allocatable, intent(out) :: this
        real(dp), dimension(:), intent(in) :: x
!------
        integer :: n
!------
        ! Get the number list
        this = x(1)
        do n=2, size(x)
            this = this // ", " // x(n)
        end do

        ! Add the brackets
        if (size(x) <= 4) then
            this = "[" // this // "]"
        else
            this = "(" // this // ")"
        end if
    end subroutine
!******************************************************************************!
!> Create a string object representing a quadruple-precision number
!******************************************************************************!
    pure subroutine characterWithQuad(this, number)
        character(:), allocatable, intent(out) :: this
        real(qp), intent(in) :: number
!------
        include 'string/characterWithReal.inc'
    end subroutine
!******************************************************************************!
!> Create a string object representing a quadruple-precision real array
!******************************************************************************!
    pure subroutine characterWithQuadArray(this, x)
        character(:), allocatable, intent(out) :: this
        real(qp), dimension(:), intent(in) :: x
!------
        integer :: n
!------
        ! Get the number list
        this = x(1)
        do n=2, size(x)
            this = this // ", " // x(n)
        end do

        ! Add the brackets
        if (size(x) <= 4) then
            this = "[" // this // "]"
        else
            this = "(" // this // ")"
        end if
    end subroutine
!******************************************************************************!
!> Create a string object representing a logical variable
!******************************************************************************!
    pure subroutine characterWithLogical(this, value)
        character(:), allocatable, intent(out) :: this
        logical, intent(in) :: value
!------
        if (value) then
            this = YES
        else
            this = NO
        end if
    end subroutine
end module
