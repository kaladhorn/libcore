!******************************************************************************!
!                         mod_textFileReader module
!------------------------------------------------------------------------------!
!> `[[FTextFileReader(type)]]` derived type and TBPs, for text file I/O.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_textFileReader
    use iso_c_binding

    use core
    use ext_character
    use com_io

    use mod_string,     only: FString
    use mod_exception,  only: FException, FExceptionDescription
    use mod_fileReader, only: FFileReader

    implicit none
    private
    save

!******************************************************************************!
!> File reader object for text file I/O
!******************************************************************************!
    type, public, extends(FFileReader) :: FTextFileReader
        private
        type(c_ptr) :: stream = C_NULL_PTR !< Pointer to the C stream
        logical :: readMode = .false.  !< Indicates whether the file has been opened for reading
        logical :: writeMode = .false. !< Indicates whether the file has been opened for writing
    contains
        procedure, public :: canRead   !< Check whether the file can be read from
        procedure, public :: canWrite  !< Check whether the file can be written to

        procedure, public :: openFile  !< Open the file
        procedure, public :: closeFile !< Close the file

        procedure, public :: getNextLine_character
        procedure, public :: getNextLine_string
        procedure, public :: getLines  !< Read from the file

        procedure, public :: writeLine
        procedure, public :: writeLines

        final :: textFileReader_finalise !< Free ressources
    end type

    integer, parameter :: BUFFER_LENGTH = 65536
contains
!******************************************************************************!
!> Final subroutine, to make sure that the file is closed when the reader
!> disappears.
!******************************************************************************!
    elemental impure subroutine textFileReader_finalise(this)
        type(FTextFileReader), intent(inout) :: this
!------
        call closeFile(this)
    end subroutine
!******************************************************************************!
!> Check whether the reader can be used to read data.
!******************************************************************************!
    function canRead(this)
        class(FTextFileReader), intent(in) :: this
        logical :: canRead
!------
        canRead = (c_associated(this%stream) .and. this%readMode)
    end function
!******************************************************************************!
!> Check whether the reader can be used to write data.
!******************************************************************************!
    function canWrite(this)
        class(FTextFileReader), intent(in) :: this
        logical :: canWrite
!------
        canWrite = (c_associated(this%stream) .and. this%writeMode)
    end function
!******************************************************************************!
!> Open a text file.
!******************************************************************************!
    subroutine openFile(this, filename, mode, stat)
        class(FTextFileReader), intent(inout), target :: this
        character(*), intent(in) :: filename
        character(*), intent(in) :: mode
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        character(:), allocatable :: message
        type(c_ptr) :: c_message
!------
        body: block

            ! Check whether the file is already open
            if (c_associated(this%stream)) then

                ! If yes, raise an error
                call stat_%raise("File already open.")
                exit body
            end if

            select case(mode)

                ! Open the file for reading
                case ("r", "read")

                    ! If not, open it for reading
                    this%stream = fopen(filename//C_NULL_CHAR, &
                        "r"//C_NULL_CHAR)

                    ! Error check
                    if (.not.c_associated(this%stream)) then
                        write(*,*) "Error code:", errno()
                        c_message = strerror(errno())
                        call c_f_string(c_message, message)
                        write(*,*) "Error message:", message
                        call stat_%raise("Error while trying to open file " // filename // " for reading.")
                        exit body
                    end if

                    ! Set the mode
                    this%readMode = .true.
                    this%writeMode = .false.

                ! Open the file for writing
                case ("w", "write")
                    ! Check whether the file is already open

                    ! Call library C functions
                    this%stream = fopen(filename//C_NULL_CHAR, &
                        "w"//C_NULL_CHAR)

                    ! Error check
                    if (.not.c_associated(this%stream)) then

                        call stat_%raise("Error while trying to open file " // filename // " for writing.")
                        exit body
                    end if

                    ! Set the mode
                    this%readMode = .false.
                    this%writeMode = .true.

                ! Handle unknown modes
                case default
                    call stat_%raise("Error while trying to open file " // filename // &
                        ": '"//mode//" is not a valid mode.")
                    exit body
            end select
        end block body

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Close a text file.
!******************************************************************************!
    subroutine closeFile(this, stat)
        class(FTextFileReader), intent(inout):: this
        type(FException), intent(out), optional :: stat
!------
        integer :: err
!------
        if (c_associated(this%stream)) then
            err = fclose(this%stream)
            this%stream = C_NULL_PTR
        end if
    end subroutine
!******************************************************************************!
!> Read the next line from a text file into a character scalar.
!******************************************************************************!
    subroutine getNextLine_character(this, line, stat)
        class(FTextFileReader), intent(inout):: this
        character(*), intent(out) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: i
        character(1,c_char), dimension(BUFFER_LENGTH), target :: cbuff
        type(FException) :: stat_
        type(c_ptr) :: out
        integer :: size_
!------

        body: block

            ! Stop here if the file is not open
            if (.not.c_associated(this%stream)) then
                call stat_%raise(FExceptionDescription("File not open.", &
                    "while trying to read from file :"))
                exit body
            end if

            ! Get the line from the file
            out = fgets(cbuff, size(cbuff)-1, this%stream)

            ! Handle errors
            if (.not.c_associated(out)) then
                line = ""
                if (errno() == 0) then
                    call stat_%raise(FExceptionDescription("End of file.", &
                    "while trying to read from file :"))
                else
                    call stat_%raise(FExceptionDescription(ioErrorMessage(), &
                    "while trying to read from file :"))
                end if
                exit body
            end if

            ! Get the index of the last useful character (ignoring t
            ! railing C_NULL_CHARs and new line)
            size_ = 0
            do i=1, size(cbuff)
                if (cbuff(i) == new_line(cbuff) .or. cbuff(i) == C_NULL_CHAR) then
                    size_ = i-1
                    exit
                end if
            end do

            ! Copy the useful string
            do i=1, min(size_, len(line))
                line(i:i) = cbuff(i)
            end do
            if (size_ < len(line)) then
                line(size_+1:) = ""
            else
                call stat_%raise("Line too long for the buffer.")
            end if
        end block body

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Read the next line from a text file into a string object.
!******************************************************************************!
    subroutine getNextLine_string(this, line, stat)
        class(FTextFileReader), intent(inout):: this
        type(FString), intent(out) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: i
        character(BUFFER_LENGTH) :: lbuf
        character(1,c_char), dimension(BUFFER_LENGTH) :: cbuff
        type(FException) :: stat_
        type(c_ptr) :: out
        integer :: size_
!------

        body: block

            ! Stop here if the file is not open
            if (.not.c_associated(this%stream)) then
                call stat_%raise("File not open.")
                exit body
            end if

            ! Get the line from the file
            out = fgets(cbuff, size(cbuff)-1, this%stream)

            ! Handle errors
            if (.not.c_associated(out)) then
                line = ""
                if (errno() == 0) then
                    call stat_%raise("End of file.")
                else
                    call stat_%raise(ioErrorMessage())
                end if
                exit body
            end if

            ! Get the index of the last useful character (ignoring t
            ! railing C_NULL_CHARs and new line)
            size_ = 0
            do i=1, size(cbuff)
                if (cbuff(i) == new_line(cbuff) .or. cbuff(i) == C_NULL_CHAR) then
                    size_ = i-1
                    exit
                end if
            end do

            ! Copy the useful string
            do i=1, size_
                lbuf(i:i) = cbuff(i)
            end do
            line = lbuf(:size_)
        end block body

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Read a whole file into a string object array.
!******************************************************************************!
    subroutine getLines(this, lines, stat)
        class(FTextFileReader), intent(inout):: this
        type(FString), dimension(:), allocatable, intent(out) :: lines
        type(FException), intent(out), optional :: stat
!------
        integer(c_long) :: pos
        integer(c_int) :: n, err
        integer :: i
        type(FException) :: stat_
!------
        ! Save the position within the file
        pos = ftell(this%stream)

        ! Count the number of lines
        n = countLines(this%stream)

        ! Rewind to the initial position in file
        err = fseek(this%stream, pos, 0_c_int)

        if (err == 0) then
            ! Allocate the output array
            allocate(lines(n))

            ! Fill the output array
            do i=1, n
                call getNextLine_string(this, lines(i), stat_)
                if (stat_ /= 0) exit
            end do
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Write a character string to the file.
!******************************************************************************!
    subroutine writeLine(this, line, stat)
        class(FTextFileReader), intent(inout):: this
        character(*), intent(in) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: iostat, i
        type(FException) :: stat_
        character(1,c_char), dimension(1000) :: buffer
        character(1,c_char), dimension(:), allocatable :: abuff
!------

        if (c_associated(this%stream) .and. this%writeMode) then
            if (len(line) < size(buffer)) then
                ! Use the buffer if it is large enough
                do i=1, len(line)
                    buffer(i) = line(i:i)
                end do
                buffer(len(line)+1) = new_line(buffer)
                buffer(len(line)+2) = C_NULL_CHAR

                ! Call library C functions
                iostat = fputs(buffer, this%stream)
            else
                ! Otherwise, allocate a bigger one (more generic, but slower)
                allocate(abuff(len(line)+2))
                do i=1, len(line)
                    abuff(i) = line(i:i)
                end do
                abuff(len(line)+1) = new_line(buffer)
                abuff(len(line)+2) = C_NULL_CHAR

                ! Call library C functions
                iostat = fputs(abuff, this%stream)
            end if

            ! Report errors
            if (iostat < 0) then
                call stat_%raise(ioErrorMessage())
            end if
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Write an array of character strings to the file.
!******************************************************************************!
    subroutine writeLines(this, lines, stat)
        class(FTextFileReader), intent(inout) :: this
        type(FString), dimension(:), intent(in) :: lines
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        integer :: i, l, iostat
        character(1000,c_char) :: buffer
!------
        if (c_associated(this%stream) .and. this%writeMode) then
            do i=1, size(lines)
                ! Fill the buffer
                buffer = lines(i)%string()
                l = lines(i)%length()
                buffer(l+1:l+1) = new_line(buffer)
                buffer(l+2:l+2) = C_NULL_CHAR

                ! Call library C functions
                iostat = fputs(buffer, this%stream)

                ! Report errors
                if (iostat /= 0) then
                    call stat_%raise(ioErrorMessage())
                    exit
                end if
            end do
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
end module
