!******************************************************************************!
!                            regex_capture module
!______________________________________________________________________________!
!> Procedures used when using a regex object to capture substrings.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2016 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!


!******************************************************************************!
!> Get captured substrings from a character variable. For convenience reasons,
!> the captured subtrings are put into a string array.
!******************************************************************************!
    subroutine capture_all_character(this, subject, capture, stat)
        class(FRegex), intent(inout) :: this
        character(*), intent(in) :: subject
        type(FString), dimension(:), allocatable, intent(out) :: capture
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        call capture_internal(this, subject, capture, stat_)

        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine capture_single_character(this, subject, index, capture, stat)
        class(FRegex), intent(inout) :: this
        character(*), intent(in) :: subject
        integer, intent(in) :: index
        character(:), allocatable, intent(out) :: capture
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
        type(FString), dimension(:), allocatable :: captures
!------
        call capture_internal(this, subject, captures, istat)

        if (allocated(captures)) then
            if (index <= size(captures)) capture = captures(index)
        end if

        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get captured substrings from a string object.
!******************************************************************************!
    subroutine capture_all_string(this, subject, capture, stat)
        class(FRegex), intent(inout) :: this
        type(FString), intent(in) :: subject
        type(FString), dimension(:), allocatable, intent(out) :: capture
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        call capture_internal(this, subject%string(), capture, stat_)

        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine capture_single_string(this, subject, index, capture, stat)
        class(FRegex), intent(inout) :: this
        type(FString), intent(in) :: subject
        integer, intent(in) :: index
        type(FString), allocatable, intent(out) :: capture
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        type(FString), dimension(:), allocatable :: captures
!------
        call capture_internal(this, subject%string(), captures, stat_)

        if (allocated(captures)) then
            if (size(captures) <= index) capture = captures(index)
        end if

        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get captured substrings (for internal use only).
!******************************************************************************!
    subroutine capture_internal(this, subject, captures, stat)
        class(FRegex), intent(in) :: this
        character(*),  intent(in) :: subject                !< input character variable
        type(FString), dimension(:), allocatable, intent(out) :: captures !< array to store the captured substrings
        type(FException), intent(out), optional :: stat        !< error code
!------
        integer(c_int) :: rc
        integer :: i, s
        type(FException) :: istat
        integer, dimension(:), allocatable :: ovector
!------
        body: block

            ! Make sure the regex is useable
            if (.not.c_associated(this%re) .or. &
                .not.c_associated(this%study)) then

                call istat%raise("trying to use an uninitialised regex object.")
                exit body
            end if

            ! Try to execute the regex
            allocate(ovector(this%ovecsize))
            ovector = 0
            rc = pcre_exec(        &
                this%re,           &
                this%study,        &
                subject,           &
                len(subject),      &
                this%offset,       &
                this%execOptions,  &
                ovector,           &
                size(ovector))

            ! Error check
            if (rc < 0) then
                call istat%raise(fcre_error_message(rc))
                exit body
            end if

!------ Extract all the captured substrings
            if (rc > 1 ) then

                ! Some substrings were captured
                ! Allocate the array containing the captured substrings
                call this%getPatternInfo(PCRE_INFO_CAPTURECOUNT, s)
                allocate(captures(s))

                ! Extract the substrings
                do i=1, rc-1
                    captures(i) = subject(ovector((i+1)*2-1)+1:ovector((i+1)*2))
                end do

                ! Set the empty strings
                if (rc-1<s) then
                    do i=rc, s
                        captures(i) = ""
                    end do
                end if
            else
                ! No substring was captured
                allocate(captures(0))
            end if
        end block body

        ! Report any error
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get named captured substrings from a character variable. For convenience
!> reasons, the captured subtrings are put into a string dictionary.
!******************************************************************************!
    subroutine capture_all_named_character(this, subject, capture, stat)
        class(FRegex), intent(inout) :: this
        character(*), intent(in) :: subject
        type(FStringDictionary), intent(out) :: capture
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
!------
        call capture_named_internal(this, subject, capture, stat_)

        ! Report any error
        if (present(stat)) call stat%transfer(stat_)
    end subroutine

    subroutine capture_single_named_character(this, subject, name, capture, stat)
        class(FRegex), intent(inout) :: this
        character(*), intent(in) :: subject
        character(*), intent(in) :: name
        character(:), allocatable, intent(out) :: capture
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
        type(FStringDictionary) :: captures
!------
        call capture_named_internal(this, subject, captures, istat)

        if (istat == 0) call captures%getValue(name, capture, istat)

        ! Report any error
        if (present(stat)) call stat%transfer(istat)
    end subroutine

    subroutine capture_single_named_string(this, subject, name, capture, stat)
        class(FRegex), intent(inout) :: this
        type(FString), intent(in) :: subject
        type(FString), intent(in) :: name
        type(FString), allocatable, intent(out) :: capture
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
        type(FStringDictionary) :: captures
!------
        call capture_named_internal(this, subject%string(), captures, istat)

        if (istat == 0) call captures%getValue(name, capture, istat)

        ! Report any error
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get named captured substrings from a string object. For convenience
!> reasons, the captured subtrings are put into a string dictionary.
!******************************************************************************!
    subroutine capture_all_named_string(this, subject, capture, stat)
        class(FRegex), intent(inout) :: this
        type(FString), intent(in) :: subject
        type(FStringDictionary), intent(out) :: capture
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: istat
!------
        call capture_named_internal(this, subject%string(), capture, istat)

        ! Report any error
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Get named captured substrings (for internal use only).
!******************************************************************************!
    subroutine capture_named_internal(this, subject, captures, stat)
        class(FRegex), intent(in) :: this
        character(*),  intent(in) :: subject                !< input character variable
        type(FStringDictionary), intent(out) :: captures !< array to store the captured substrings
        type(FException), intent(out), optional :: stat        !< error code
!------
        integer(c_int) :: rc
        integer :: i
        type(FException) :: istat
        integer, dimension(:), allocatable :: ovector
        type(c_ptr) :: tablePtr, namePtr
        character(c_char), dimension(:,:), pointer :: tableString
        integer :: nameCount, entrySize, captureIndex, err
        character(:), allocatable :: name
!------
        body: block

            ! Make sure the regex is useable
            if (.not.c_associated(this%re) .or. &
                .not.c_associated(this%study)) then

                call istat%raise("trying to use an uninitialised regex object.")
                exit body
            end if

            ! Try to execute the regex
            allocate(ovector(this%ovecsize))
            ovector = 0
            rc = pcre_exec(        &
                this%re,           &
                this%study,        &
                subject,           &
                len(subject),      &
                this%offset,       &
                this%execOptions,  &
                ovector,           &
                size(ovector))

            ! Error check
            if (rc < 0) then
                call istat%raise(fcre_error_message(rc))
                exit body
            end if

!------ Extract all the captured substrings
            if (rc > 1 ) then

                ! Get the number of cnamed aptures
                call this%getPatternInfoShort(PCRE_INFO_NAMECOUNT, nameCount)
                if (nameCount == 0) exit body

                ! Get the size of the name array
                call this%getPatternInfo(PCRE_INFO_NAMEENTRYSIZE, entrySize)

                ! Get the name array
                err = fcre_nametable(this%re, this%study, PCRE_INFO_NAMETABLE, tablePtr)

                ! Get nice character variables
                call c_f_pointer(tablePtr, tableString, [entrySize, nameCount])

                ! Process each named capture
                do i=1, nameCount

                    ! Get the name of the i-th capture
                    namePtr = c_loc(tableString(3,i))
                    call c_f_string(namePtr, name)

                    ! Get the number
                    captureIndex = pcre_get_stringnumber(this%re, name//C_NULL_CHAR)

                    ! Get the value
                    call captures%set(name, &
                        subject(ovector((captureIndex+1)*2-1)+1:ovector((captureIndex+1)*2)))
                end do

            else
                ! No substring was captured
            end if
        end block body

        ! Report any error
        if (present(stat)) call stat%transfer(istat)
    end subroutine
