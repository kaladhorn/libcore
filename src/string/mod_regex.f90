!******************************************************************************!
!                              mod_regex module
!______________________________________________________________________________!
!> `[[FRegex(type)]]` derived type and TBPs, to handle regexes. This is a
!> higher-level interface to the FCRE module.
!
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_regex
    use iso_c_binding

    use fcre

    use ext_character,  only: c_f_string
    use com_parameters, only: e

    use mod_string,     only: FString
    use mod_exception,  only: FException, FExceptionDescription
    use mod_stringDictionary, only: FStringDictionary

    implicit none
    private
    save

!******************************************************************************!
!> Type describing a regular expression.
!******************************************************************************!
    type, public, extends(FString) :: FRegex
        private

        character(:), allocatable :: substitute     !< Pattern to be used for substitutions (not allocated if no substitution is defined)
        character(:,c_char), allocatable :: pattern !< Computer-friendly version of the pattern (null-terminated)

        integer :: id = 0                 !< Unique identifier
        type(c_ptr) :: re = C_NULL_PTR    !< Pointer to the internal representation of the regex
        type(c_ptr) :: study = C_NULL_PTR !< Pointer to the study data
        integer(c_int)  :: execOptions        = 0   !< Options to be passed when executing the regex
        integer(c_int)  :: compilationOptions = 0   !< Options to be passed when compiling the pattern

        integer, dimension(:), allocatable :: backReferenceIndex    !<
        integer, dimension(:), allocatable :: backReferencePosition !<
        integer :: backReferenceCount = 0 !< Number of back references in the substitute string

        integer :: ovecsize = 0
        integer(c_int) :: offset = 0
    contains

        ! Split a string into substrings
        procedure, private :: split_string
        procedure, private :: split_character
        generic, public :: split => split_string, split_character

        ! Matching procedures
        procedure, private :: match_single_position_string
        procedure, private :: match_single_substring_string
        procedure, private :: match_single_both_string
        procedure, private :: match_single_position_character
        procedure, private :: match_single_substring_character
        procedure, private :: match_single_both_character
        procedure, private :: match_all_position_string
        procedure, private :: match_all_substring_string
        procedure, private :: match_all_both_string
        procedure, private :: match_all_position_character
        procedure, private :: match_all_substring_character
        procedure, private :: match_all_both_character
        generic, public :: match => match_single_position_string, match_single_substring_string, &
            match_single_both_string, match_single_position_character, &
            match_single_substring_character, match_single_both_character, &
            match_all_position_string, match_all_substring_string, &
            match_all_both_string, match_all_position_character, &
            match_all_substring_character, match_all_both_character

        ! Capture procedures
        procedure, private :: capture_all_character
        procedure, private :: capture_all_string
        procedure, private :: capture_single_character
        procedure, private :: capture_single_string
        procedure, private :: capture_all_named_string
        procedure, private :: capture_all_named_character
        procedure, private :: capture_single_named_string
        procedure, private :: capture_single_named_character
        generic, public :: capture => capture_all_string, capture_all_character, &
            capture_single_string, capture_single_character, &
            capture_all_named_string, capture_all_named_character, &
            capture_single_named_string, capture_single_named_character
        procedure, public :: getCaptureNames

        procedure, private :: doesMatch_string
        procedure, private :: doesMatch_character
        generic, public :: doesMatch => doesMatch_character, doesMatch_string
        procedure, public :: replace
        procedure, public :: replace_all

        ! Options
        procedure, private  :: setCaseSensitive
        procedure, private  :: setMultiline
        procedure, private  :: setExtended
        procedure, private  :: setReluctant

        ! Internal procedures
        procedure, private :: setCompilationOption
        procedure, private :: unsetCompilationOption
        procedure, private :: setPattern
        procedure, private :: setSubstitution
        procedure, private :: compilePattern
        procedure, private :: studyPattern
        procedure, private :: getPatternInfoShort
        procedure, private :: getPatternInfoLong
        generic, private :: getPatternInfo => getPatternInfoShort, getPAtternInfoLong

        ! Final subroutine
        final :: regex_finalise

        procedure, private :: regex_copy
        generic, public :: assignment(=) => regex_copy
    end type

    ! Constructor
    interface FRegex
        module procedure regex_initWithCharacters, regex_initWithStrings
    end interface
    public :: regex_initWithCharacters

    interface
        function copy_regex(from) bind(C, name="copy_regex")
            use iso_c_binding, only: c_ptr
            type(c_ptr), value :: from
            type(c_ptr) :: copy_regex
        end function
    end interface

contains
    include 'string/regex_setup.inc'
    include 'string/regex_match.inc'
    include 'string/regex_replace.inc'
    include 'string/regex_capture.inc'
!******************************************************************************!
!> Create a copy of a regex object.
!******************************************************************************!
    subroutine regex_copy(to, from)
        class(FRegex), intent(out) :: to
        type(FRegex), intent(in) :: from
!------
        if (allocated(from%pattern)) then

            to%FString = from%FString
            if (allocated(from%substitute)) to%substitute = from%substitute
            if (allocated(from%pattern)) to%pattern = from%pattern
            to%id = from%id
            to%re = from%re
            to%study = from%study
            to%execOptions = from%execOptions
            to%compilationOptions = from%compilationOptions
            if (allocated(from%backReferenceIndex)) &
                to%backReferenceIndex = from%backReferenceIndex
            if (allocated(from%backReferencePosition)) &
                to%backReferencePosition = from%backReferencePosition
            to%backReferenceCount = from%backReferenceCount
            to%ovecsize = from%ovecsize
            to%offset = from%offset
        end if
    end subroutine
!******************************************************************************!
!> Cleanup the internal representation of a regex object.
!******************************************************************************!
    elemental impure subroutine regex_finalise(this)
        type(FRegex), intent(inout) :: this
!------
        ! Remove the compiled regex
        if (c_associated(this%re)) then
!            call pcre_free(this%re)
!            this%re = C_NULL_PTR
        end if

        ! Invalidate the study
        if (c_associated(this%study)) then
!            call pcre_free_study(this%study)
!            this%study = C_NULL_PTR
        end if
    end subroutine
!******************************************************************************!
!> Initialise a regex with a pattern and optionally a replacement string,
!> provided as character scalars.
!******************************************************************************!
    recursive function regex_initWithCharacters(pattern, replacement, options) result(this)
        character(*), intent(in) :: pattern
        character(*), intent(in), optional :: replacement
        character(*), intent(in), optional :: options
        type(FRegex) :: this
!------
        integer :: i
        real(e) :: n
        type(FException) :: istat
!------

        body: block

            this%FString = pattern

            ! Get a unique ID
            call random_number(n)
            this%id = nint(n*10000)

            ! Set the pattern
            call this%setPattern(pattern, istat)
            if (istat /= 0) exit body

            ! Set the substitution pattern if present
            if (present(replacement)) then
                call this%setSubstitution(replacement, istat)
                if (istat /= 0) exit body
            end if

            ! Support Unicode strings by default
            call this%setCompilationOption(PCRE_UCP)
            call this%setCompilationOption(PCRE_UTF8)

            ! Change the options if needed
            if (present(options)) then
                do i=1, len(options)
                    select case(options(i:i))
                        case("i")
                            call this%setCompilationOption(PCRE_CASELESS)
                        case("r")
                            call this%setCompilationOption(PCRE_UNGREEDY)
                        case("x")
                            call this%setCompilationOption(PCRE_EXTENDED)
                            this%execOptions = ibset(this%execOptions,fcre_option_position(PCRE_EXTENDED))
                        case("m")
                            call this%setCompilationOption(PCRE_MULTILINE)
                    end select
                end do
            end if

            ! Compile the pattern
            call this%compilePattern
        end block body
    end function
!******************************************************************************!
!> Initialise a regex with a pattern and optionally a replacement string,
!> provided as `[[FString(type)]]` objects. Thin wrapper around
!> `[[regex_initWithCharacters(function)]]`.
!******************************************************************************!
    function regex_initWithStrings(pattern, replacement, options) result(this)
        type(FString), intent(in) :: pattern
        type(FString), intent(in), optional :: replacement
        character(*), intent(in), optional :: options
        type(FRegex) :: this
!------
        if (present(replacement) .and. present(options)) then
            this = FRegex(pattern%string(), replacement=replacement%string(), options=options)
        else if (present(options)) then
            this = FRegex(pattern%string(), options=options)
        else if (present(replacement)) then
            this = FRegex(pattern%string(), replacement=replacement%string())
        else
            this = FRegex(pattern%string())
        end if
    end function
!******************************************************************************!
!> Get information about the pattern of a regular expression (default
!> integer version)
!>
!> Get information about the pattern of a regular expression. An overflow may
!> occur depending on the information type and the value of the result. If
!> status is provided and the information can not be obtained, its value is set
!> to the error number returned by pcre.
!******************************************************************************!
    subroutine getPatternInfoShort(this, info, result, status)
        class(FRegex),  intent(in)  :: this
        integer(c_int), intent(in)     :: info      !< requested information
        integer,        intent(out)    :: result    !< request result
        integer, intent(out), optional :: status    !< error number
!------
        integer(c_long) :: result_
!------
        call this%getPatternInfoLong(info, result_, status)
        result = int(result_, kind(result))
    end subroutine
!******************************************************************************!
!> Get information about the pattern of a regular expression
!>
!> Get information about the pattern of a regular expression. If status is
!> provided and the information can not be obtained, it is set to the error
!> number returned by pcre.
!> The result is a c_ptr variable that should be interpreted differently
!> depending on the requested information.
!>
!> For the information for which the result is not an integer, other specific
!> procedures must be used:
!> - getCaptureNames for PCRE_INFO_NAMETABLE;
!> - getFirstTable for PCRE_INFO_FIRSTTABLE (not yet implemented);
!> - getDefaultTabkes for PCRE_INFO_DEFAULT_TABLES (not yet implemented).
!******************************************************************************!
    subroutine getPatternInfoLong(this, info, result, status)
        class(FRegex), intent(in)   :: this
        integer(c_int), intent(in)     :: info      !< requested information
        integer(c_long),    intent(out):: result    !< request result
        integer, intent(out), optional :: status    !< error number
!------
        integer(c_int) :: errorCode
        integer(c_size_t), target :: size_t_
        integer(c_int), target :: int_
        integer(c_int32_t), target :: int32_
        integer(c_long), target :: long_
        integer(c_signed_char), target :: unit_t_
        type(c_ptr) resultPtr
!------
        ! Some info are not supported in this procedure
        if (info == PCRE_INFO_FIRSTTABLE .or. info == PCRE_INFO_NAMETABLE .or. &
            info == PCRE_INFO_DEFAULT_TABLES) then
            if (present(status)) then
                status = FCRE_ERROR_UNSUPPORTED_OPTION
            else
                write(*,*) "fcre error: " // fcre_error_message(FCRE_ERROR_UNSUPPORTED_OPTION)
            end if
            return
        end if

        resultPtr = C_NULL_PTR ! No-op, removes a warning about uninitialised resultPtr with some compilers
        select case(info)
            case (PCRE_INFO_OPTIONS)
                resultPtr = c_loc(long_)
            case (PCRE_INFO_SIZE)
                resultPtr = c_loc(size_t_)
            case (PCRE_INFO_CAPTURECOUNT)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_BACKREFMAX)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_FIRSTBYTE) ! Also matches PCRE_INFO_FIRSTCHAR
                resultPtr = c_loc(int_)
            case (PCRE_INFO_LASTLITERAL)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_NAMEENTRYSIZE)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_NAMECOUNT)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_STUDYSIZE)
                resultPtr = c_loc(size_t_)
            case (PCRE_INFO_OKPARTIAL)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_JCHANGED)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_HASCRORLF)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_MINLENGTH)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_JIT)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_JITSIZE)
                resultPtr = c_loc(size_t_)
            case (PCRE_INFO_MAXLOOKBEHIND)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_FIRSTCHARACTER)
                resultPtr = c_loc(unit_t_)
            case (PCRE_INFO_FIRSTCHARACTERFLAGS)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_REQUIREDCHAR)
                resultPtr = c_loc(unit_t_)
            case (PCRE_INFO_REQUIREDCHARFLAGS)
                resultPtr = c_loc(int_)
            case (PCRE_INFO_MATCHLIMIT)
                resultPtr = c_loc(int32_)
            case (PCRE_INFO_RECURSIONLIMIT)
                resultPtr = c_loc(int32_)
            case (PCRE_INFO_MATCH_EMPTY)
                resultPtr = c_loc(int_)
            case default
        end select

        errorCode = pcre_fullinfo(code=this%re,    &
                                 extra=this%study, &
                                  what=info,       &
                                 where=resultPtr)

        ! Set the error number if a problem happened
        if (errorCode /= 0) then
            if (present(status)) status = errorCode
            return
        end if

        select case(info)
            case (PCRE_INFO_OPTIONS)
                result = long_
            case (PCRE_INFO_SIZE)
                result = size_t_
            case (PCRE_INFO_CAPTURECOUNT)
                result = int_
            case (PCRE_INFO_BACKREFMAX)
                result = int_
            case (PCRE_INFO_FIRSTBYTE) ! Also matches PCRE_INFO_FIRSTCHAR
                result = int_
            case (PCRE_INFO_LASTLITERAL)
                result = int_
            case (PCRE_INFO_NAMEENTRYSIZE)
                result = int_
            case (PCRE_INFO_NAMECOUNT)
                result = int_
            case (PCRE_INFO_STUDYSIZE)
                result = size_t_
            case (PCRE_INFO_OKPARTIAL)
                result = int_
            case (PCRE_INFO_JCHANGED)
                result = int_
            case (PCRE_INFO_HASCRORLF)
                result = int_
            case (PCRE_INFO_MINLENGTH)
                result = int_
            case (PCRE_INFO_JIT)
                result = int_
            case (PCRE_INFO_JITSIZE)
                result = size_t_
            case (PCRE_INFO_MAXLOOKBEHIND)
                result = int_
            case (PCRE_INFO_FIRSTCHARACTER)
                result = unit_t_
            case (PCRE_INFO_FIRSTCHARACTERFLAGS)
                result = int_
            case (PCRE_INFO_REQUIREDCHAR)
                result = unit_t_
            case (PCRE_INFO_REQUIREDCHARFLAGS)
                result = int_
            case (PCRE_INFO_MATCHLIMIT)
                result = int32_
            case (PCRE_INFO_RECURSIONLIMIT)
                result = int32_
            case (PCRE_INFO_MATCH_EMPTY)
                result = int_
            case default
        end select
        if (present(status)) status = 0
    end subroutine
!******************************************************************************!
!> Get the list of named captures in a pattern.
!******************************************************************************!
    subroutine getCaptureNames(this, names, status)
        class(FRegex), intent(inout)   :: this
        type(FString), dimension(:), allocatable, intent(out) :: names !< capture names
        integer, intent(out), optional :: status          !< error number
!------
        type(c_ptr) :: tablePtr, namePtr
        character(c_char), dimension(:,:), pointer :: tableString
        integer :: nameCount, entrySize
        integer, dimension(2) :: shape
        integer :: errorCode, i
        character(:), allocatable :: tmp
!------
        ! Make sure the pattern has been set
        if (.not.allocated(this%pattern)) then
            if (present(status)) then
                status = FCRE_ERROR_UNSET_PATTERN
            else
                write(*,*) "fcre error: " //fcre_error_message(FCRE_ERROR_UNSET_PATTERN)
            end if
            return
        end if

        ! Make sure the pattern has been compiled
        if (.not.c_associated(this%re)) then
            call this%compilePattern
        end if

        ! Get the number of captured substrings
        call this%getPatternInfoShort(PCRE_INFO_NAMECOUNT, nameCount)

        if (nameCount > 0) then

            ! Get the size of the name array
            call this%getPatternInfo(PCRE_INFO_NAMEENTRYSIZE, entrySize)

            ! Get the name array
            errorCode = fcre_nametable(this%re, this%study, PCRE_INFO_NAMETABLE, tablePtr)

            ! Set the error number if a problem happened
            if (errorCode /= 0) then
                if (present(status)) status = errorCode
                return
            end if

            ! Get nice character variables
            allocate(names(nameCount))
            shape(1) = entrySize
            shape(2) = nameCount
            call c_f_pointer(tablePtr, tableString, shape)
            do i=1, nameCount
                namePtr = c_loc(tableString(3,i))
                call c_f_string(namePtr, tmp)
                names(i) = tmp
            end do
        end if
    end subroutine
!******************************************************************************!
!> Check whether a regex matches a given `[[FString(type)]]` object.
!******************************************************************************!
    function doesMatch_string(this, subject) result(test)
        class(FRegex), intent(in) :: this
        type(FString), intent(in) :: subject
        logical :: test
!------
        test = doesMatch_character(this, subject%string())
    end function
!******************************************************************************!
!> Check whether a regex matches a given character scalar.
!******************************************************************************!
    function doesMatch_character(this, subject) result(test)
        class(FRegex), intent(in) :: this
        character(*), intent(in) :: subject
        logical :: test
!------
        integer :: rc
        integer(c_int), dimension(:), allocatable :: ovector
!------
        ! Try to execute the regular expression
        allocate(ovector(this%ovecsize))
        ovector = 0
        rc = pcre_exec(        &
            this%re,           &
            this%study,        &
            subject,           &
            len(subject),      &
            this%offset,       &
            this%execOptions,  &
            ovector,           &
            size(ovector))

        ! Check whether at least one match has been found
        test = (rc > 0)
    end function
!******************************************************************************!
!> Split a `[[FString(type)]]` object into an array of strings.
!******************************************************************************!
    subroutine split_string(this, subject, split)
        class(FRegex), intent(inout) :: this
        type(FString), intent(in) :: subject
        type(FString), dimension(:), allocatable, intent(out) :: split
!------
        call split_character(this, subject%string(), split)
    end subroutine
!******************************************************************************!
!> Split a character scalar into an array of strings.
!******************************************************************************!
    subroutine split_character(this, subject, split)
        class(FRegex), intent(inout) :: this
        character(*), intent(in) :: subject
        type(FString), dimension(:), allocatable, intent(out) :: split
!------
        type(FString), dimension(:), allocatable :: tmp
        integer, dimension(:), allocatable :: ind
        integer :: i, n
        type(FException) :: istat
!------

        ! Get the instances of the separator
        call this%match(subject=subject, position=ind, substring=tmp, stat=istat)

        if (size(ind) == 0) then
            allocate(split(1))
            split(1) = subject
        else
            ! Count the number of substrings
            n = 0
            if (ind(1) /= 1) n = n + 1
            if (ind(size(ind)) + tmp(size(ind))%length() - 1 == len(subject)) then
                n = n + size(ind) - 1
            else
                n = n + size(ind)
            end if

            ! Set the elements
            allocate(split(n))
            n = 0
            if (ind(1) /= 1) then
                n = n + 1
                split(1) = subject(:ind(1)-1)
            end if
            do i=1, size(ind)-1
                n = n + 1
                split(n) = subject(ind(i)+tmp(size(ind))%length():ind(i+1)-1)
            end do
            i = size(ind)
            if (ind(i) + tmp(i)%length() - 1 /= len(subject)) then
                n = n + 1
                split(n) = subject(ind(i)+tmp(size(ind))%length():)
            end if
        end if
        call istat%discard
    end subroutine
end module
