#******************************************************************************#
#                              libCore support                                 #
#______________________________________________________________________________#
#
#  Version 2.0
#
#  Written by Paul Fossati, <paul.fossati@gmail.com>
#  Copyright (c) 2009-2017 Paul Fossati
#------------------------------------------------------------------------------#
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
#     * Redistributions of source code must retain the above copyright notice, #
#       this list of conditions and the following disclaimer.                  #
#                                                                              #
#     * Redistributions in binary form must reproduce the above copyright      #
#       notice, this list of conditions and the following disclaimer in the    #
#       documentation and/or other materials provided with the distribution.   #
#                                                                              #
#     * The name of the author may not be used to endorse or promote products  #
#      derived from this software without specific prior written permission    #
#      from the author.                                                        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#******************************************************************************#
SUPPORT_SOURCES= support/com_parameters.f90 support/core.f90                   \
    support/mod_exception.f90 support/com_hash.f90

AM_FCFLAGS+= -Isupport

EXTRA_DIST+= support/almostEqual.inc support/almostEqual_wrapper.inc
#******************************************************************************!
# Dependencies
#******************************************************************************!

support/com_parameters.o: support/com_parameters.f90
com_parameters.mod: support/com_parameters.o

support/core.o: support/core.f90 support/almostEqual.inc com_parameters.mod    \
    config.mod
core.mod: support/core.o

support/mod_exception.o: support/mod_exception.f90 core.mod
mod_exception.mod: support/mod_exception.o

support/com_hash.o: support/com_hash.f90 core.mod
com_hash.mod: support/com_hash.o
