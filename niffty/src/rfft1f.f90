    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine rfft1f(n, inc, r, lenr, wsave, lensav, work, lenwrk, ier)
        implicit none
        include 'kinds.inc'
        integer n, inc, lenr, lensav, lenwrk, ier
        real(rwp) :: r(lenr), wsave(lensav), work(lenwrk)

        ier = 0

        if (lenr<inc*(n-1)+1) then
            ier = 1
            call xerfft('RFFT1F ', 6)
        else if (lensav<n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('RFFT1F ', 8)
        else if (lenwrk<n) then
            ier = 3
            call xerfft('RFFT1F ', 10)
        end if

        if (n==1) return

        call rfftf1(n, inc, r, work, wsave, wsave(n+1))
    end subroutine
