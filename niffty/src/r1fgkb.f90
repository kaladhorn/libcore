    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine r1fgkb(ido, ip, l1, idl1, cc, c1, c2, in1, ch, ch2, in2, wa)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
          c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), wa(ido)

        tpi = 2.*4.*atan(1.0_rwp)
        arg = tpi/float(ip)
        dcp = cos(arg)
        dsp = sin(arg)
        idp2 = ido + 2
        nbd = (ido-1)/2
        ipp2 = ip + 2
        ipph = (ip+1)/2
        if (ido<l1) go to 100
        do k = 1, l1
            do i = 1, ido
                ch(1, i, k, 1) = cc(1, i, 1, k)
            end do
        end do
        go to 110
100     do i = 1, ido
            do k = 1, l1
                ch(1, i, k, 1) = cc(1, i, 1, k)
            end do
        end do
110     do j = 2, ipph
            jc = ipp2 - j
            j2 = j + j
            do k = 1, l1
                ch(1, 1, k, j) = cc(1, ido, j2-2, k) + cc(1, ido, j2-2, k)
                ch(1, 1, k, jc) = cc(1, 1, j2-1, k) + cc(1, 1, j2-1, k)
            end do
        end do
        if (ido==1) go to 130
        if (nbd<l1) go to 120
        do j = 2, ipph
            jc = ipp2 - j
            do k = 1, l1
                do i = 3, ido, 2
                    ic = idp2 - i
                    ch(1, i-1, k, j) = cc(1, i-1, 2*j-1, k) + &
                      cc(1, ic-1, 2*j-2, k)
                    ch(1, i-1, k, jc) = cc(1, i-1, 2*j-1, k) - &
                      cc(1, ic-1, 2*j-2, k)
                    ch(1, i, k, j) = cc(1, i, 2*j-1, k) - cc(1, ic, 2*j-2, k)
                    ch(1, i, k, jc) = cc(1, i, 2*j-1, k) + cc(1, ic, 2*j-2, k)
                end do
            end do
        end do
        go to 130
120     do j = 2, ipph
            jc = ipp2 - j
            do i = 3, ido, 2
                ic = idp2 - i
                do k = 1, l1
                    ch(1, i-1, k, j) = cc(1, i-1, 2*j-1, k) + &
                      cc(1, ic-1, 2*j-2, k)
                    ch(1, i-1, k, jc) = cc(1, i-1, 2*j-1, k) - &
                      cc(1, ic-1, 2*j-2, k)
                    ch(1, i, k, j) = cc(1, i, 2*j-1, k) - cc(1, ic, 2*j-2, k)
                    ch(1, i, k, jc) = cc(1, i, 2*j-1, k) + cc(1, ic, 2*j-2, k)
                end do
            end do
        end do
130     ar1 = 1.
        ai1 = 0.
        do l = 2, ipph
            lc = ipp2 - l
            ar1h = dcp*ar1 - dsp*ai1
            ai1 = dcp*ai1 + dsp*ar1
            ar1 = ar1h
            do ik = 1, idl1
                c2(1, ik, l) = ch2(1, ik, 1) + ar1*ch2(1, ik, 2)
                c2(1, ik, lc) = ai1*ch2(1, ik, ip)
            end do
            dc2 = ar1
            ds2 = ai1
            ar2 = ar1
            ai2 = ai1
            do j = 3, ipph
                jc = ipp2 - j
                ar2h = dc2*ar2 - ds2*ai2
                ai2 = dc2*ai2 + ds2*ar2
                ar2 = ar2h
                do ik = 1, idl1
                    c2(1, ik, l) = c2(1, ik, l) + ar2*ch2(1, ik, j)
                    c2(1, ik, lc) = c2(1, ik, lc) + ai2*ch2(1, ik, jc)
                end do
            end do
        end do
        do j = 2, ipph
            do ik = 1, idl1
                ch2(1, ik, 1) = ch2(1, ik, 1) + ch2(1, ik, j)
            end do
        end do
        do j = 2, ipph
            jc = ipp2 - j
            do k = 1, l1
                ch(1, 1, k, j) = c1(1, 1, k, j) - c1(1, 1, k, jc)
                ch(1, 1, k, jc) = c1(1, 1, k, j) + c1(1, 1, k, jc)
            end do
        end do
        if (ido==1) go to 150
        if (nbd<l1) go to 140
        do j = 2, ipph
            jc = ipp2 - j
            do k = 1, l1
                do i = 3, ido, 2
                    ch(1, i-1, k, j) = c1(1, i-1, k, j) - c1(1, i, k, jc)
                    ch(1, i-1, k, jc) = c1(1, i-1, k, j) + c1(1, i, k, jc)
                    ch(1, i, k, j) = c1(1, i, k, j) + c1(1, i-1, k, jc)
                    ch(1, i, k, jc) = c1(1, i, k, j) - c1(1, i-1, k, jc)
                end do
            end do
        end do
        go to 150
140     do j = 2, ipph
            jc = ipp2 - j
            do i = 3, ido, 2
                do k = 1, l1
                    ch(1, i-1, k, j) = c1(1, i-1, k, j) - c1(1, i, k, jc)
                    ch(1, i-1, k, jc) = c1(1, i-1, k, j) + c1(1, i, k, jc)
                    ch(1, i, k, j) = c1(1, i, k, j) + c1(1, i-1, k, jc)
                    ch(1, i, k, jc) = c1(1, i, k, j) - c1(1, i-1, k, jc)
                end do
            end do
        end do
150     continue
        if (ido==1) return
        do ik = 1, idl1
            c2(1, ik, 1) = ch2(1, ik, 1)
        end do
        do j = 2, ip
            do k = 1, l1
                c1(1, 1, k, j) = ch(1, 1, k, j)
            end do
        end do
        if (nbd>l1) go to 160
        is = -ido
        do j = 2, ip
            is = is + ido
            idij = is
            do i = 3, ido, 2
                idij = idij + 2
                do k = 1, l1
                    c1(1, i-1, k, j) = wa(idij-1)*ch(1, i-1, k, j) - &
                      wa(idij)*ch(1, i, k, j)
                    c1(1, i, k, j) = wa(idij-1)*ch(1, i, k, j) + &
                      wa(idij)*ch(1, i-1, k, j)
                end do
            end do
        end do
        go to 170
160     is = -ido
        do j = 2, ip
            is = is + ido
            do k = 1, l1
                idij = is
                do i = 3, ido, 2
                    idij = idij + 2
                    c1(1, i-1, k, j) = wa(idij-1)*ch(1, i-1, k, j) - &
                      wa(idij)*ch(1, i, k, j)
                    c1(1, i, k, j) = wa(idij-1)*ch(1, i, k, j) + &
                      wa(idij)*ch(1, i-1, k, j)
                end do
            end do
        end do
170     return
    end subroutine
