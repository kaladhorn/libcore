    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine r1f2kf(ido, l1, cc, in1, ch, in2, wa1)
        implicit none
        include 'kinds.inc'
        integer :: ido, l1, in1, in2
        real(rwp) :: ch(in2, ido, 2, l1)
        real(rwp) :: cc(in1, ido, l1, 2)
        real(rwp) :: wa1(ido)
        
        integer :: i, k, ic, idp2

        do k = 1, l1
            ch(1, 1, 1, k) = cc(1, 1, k, 1) + cc(1, 1, k, 2)
            ch(1, ido, 2, k) = cc(1, 1, k, 1) - cc(1, 1, k, 2)
        end do
        if (ido-2) 120, 110, 100
100     idp2 = ido + 2
        do k = 1, l1
            do i = 3, ido, 2
                ic = idp2 - i
                ch(1, i, 1, k) = cc(1, i, k, 1) + (wa1(i-2)*cc(1,i,k,2)-wa1(i- &
                  1)*cc(1,i-1,k,2))
                ch(1, ic, 2, k) = (wa1(i-2)*cc(1,i,k,2)-wa1(i-1)*cc(1,i-1,k,2) &
                  ) - cc(1, i, k, 1)
                ch(1, i-1, 1, k) = cc(1, i-1, k, 1) + &
                  (wa1(i-2)*cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k,2))
                ch(1, ic-1, 2, k) = cc(1, i-1, k, 1) - &
                  (wa1(i-2)*cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k,2))
            end do
        end do
        if (mod(ido,2)==1) return
110     do k = 1, l1
            ch(1, 1, 2, k) = -cc(1, ido, k, 2)
            ch(1, ido, 1, k) = cc(1, ido, k, 1)
        end do
120     return
    end subroutine
