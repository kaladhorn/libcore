    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine sintf1(n, inc, x, wsave, xh, work, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) x(inc, *), wsave(*), xh(*)
        double precision dsum

        ier = 0
        if (n-2) 140, 100, 110
100     ssqrt3 = 1.0_rwp/sqrt(3.0_rwp)
        xhold = ssqrt3*(x(1,1)+x(1,2))
        x(1, 2) = ssqrt3*(x(1,1)-x(1,2))
        x(1, 1) = xhold
        go to 140
110     np1 = n + 1
        ns2 = n/2
        do k = 1, ns2
            kc = np1 - k
            t1 = x(1, k) - x(1, kc)
            t2 = wsave(k)*(x(1,k)+x(1,kc))
            xh(k+1) = t1 + t2
            xh(kc+1) = t2 - t1
        end do
        modn = mod(n, 2)
        if (modn==0) go to 120
        xh(ns2+2) = 4.*x(1, ns2+1)
120     xh(1) = 0.
        lnxh = np1
        lnsv = np1 + int(log(real(np1, rwp))/log(2.0_rwp)) + 4
        lnwk = np1

        call rfft1f(np1, 1, xh, lnxh, wsave(ns2+1), lnsv, work, lnwk, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('SINTF1', -5)
            go to 140
        end if

        if (mod(np1,2)/=0) go to 130
        xh(np1) = xh(np1) + xh(np1)
130     sfnp1 = 1.0_rwp/float(np1)
        x(1, 1) = .5_rwp*xh(1)
        dsum = x(1, 1)
        do i = 3, n, 2
            x(1, i-1) = .5_rwp*xh(i)
            dsum = dsum + .5*xh(i-1)
            x(1, i) = dsum
        end do
        if (modn/=0) go to 140
        x(1, n) = .5_rwp*xh(n+1)
140     return
    end subroutine
