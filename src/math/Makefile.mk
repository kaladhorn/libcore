#******************************************************************************#
#                               libCore math                                   #
#______________________________________________________________________________#
#
#  Version 2.0
#
#  Written by Paul Fossati, <paul.fossati@gmail.com>
#  Copyright (c) 2009-2016 Paul Fossati
#------------------------------------------------------------------------------#
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
#     * Redistributions of source code must retain the above copyright notice, #
#       this list of conditions and the following disclaimer.                  #
#                                                                              #
#     * Redistributions in binary form must reproduce the above copyright      #
#       notice, this list of conditions and the following disclaimer in the    #
#       documentation and/or other materials provided with the distribution.   #
#                                                                              #
#     * The name of the author may not be used to endorse or promote products  #
#      derived from this software without specific prior written permission    #
#      from the author.                                                        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#******************************************************************************#
MATH_SOURCES= math/mod_bytecode.f90 math/mod_ast.f90 math/mod_expression.f90   \
    math/mod_function.f90 math/mod_niffty.f90 math/fft_niffty.f90              \
	math/mod_numericalFunction.f90 math/mod_ast.f90 math/mod_bytecode.f90      \
    math/com_math.f90

EXTRA_DIST+= math/nodeOperations.inc math/nodeDefinitions.inc                  \
	math/expression_parse.inc math/expression_compile.inc                      \
	math/expression_eval.inc math/instruction_set.inc                          \
    math/bytecode_run_kernel.inc

#******************************************************************************#
# Dependencies
#******************************************************************************#
math/com_math.o: math/com_math.f90 core.mod
com_math.mod: math/com_math.o

math/fft_niffty.o: math/fft_niffty.f90 core.mod mod_niffty.mod
com_fft.mod: math/fft_niffty.o

math/mod_function.o: math/mod_function.f90 core.mod mod_exception.mod          \
	mod_string.mod mod_dictionary.mod mod_expression.mod
mod_function.mod: math/mod_function.o

math/mod_niffty.o: math/mod_niffty.f90
mod_niffty.mod: math/mod_niffty.o

math/mod_bytecode.o: math/mod_bytecode.f90 core.mod mod_ast.mod
mod_bytecode.mod: math/mod_bytecode.o

math/mod_expression.o: math/mod_expression.f90 core.mod mod_exception.mod      \
	mod_regex.mod mod_string.mod mod_dictionary.mod mod_ast.mod                \
	ext_character.mod mod_numericaldictionary.mod math/expression_parse.inc    \
    math/expression_eval.inc math/expression_compile.inc mod_bytecode.mod
mod_expression.mod: math/mod_expression.o

math/mod_numericalFunction.o: math/mod_numericalFunction.f90 core.mod          \
	ext_character.mod com_fft.mod mod_string.mod mod_file.mod                  \
	mod_exception.mod mod_expression.mod
mod_numericalfunction.mod: math/mod_numericalFunction.o

math/mod_ast.o:	math/mod_ast.f90 core.mod ext_character.mod                    \
	math/nodeOperations.inc math/nodeDefinitions.inc math/instruction_set.inc
mod_ast.mod: math/mod_ast.o
