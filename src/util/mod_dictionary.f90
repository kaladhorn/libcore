!******************************************************************************!
!                             mod_dictionary module
!------------------------------------------------------------------------------!
!> Simple dictionary data structure. This defines the common interfaces for
!> dictionary objects, and is supposed to be extended to store different types
!> of values.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_dictionary

    use mod_string,     only: FString
    use mod_exception,  only: FException

    implicit none
    private
    save

!******************************************************************************!
!> Dictionary description
!******************************************************************************!
    type, public, abstract :: FDictionary
        type(FString), dimension(:), allocatable :: key_
        integer :: initialSize = 10
        integer :: sizeIncrement = 10
        integer :: size_ = 0
    contains
        procedure, private :: setCharacterScalar
        procedure, private :: setStringScalar
        procedure, private :: setCharacterArray
        procedure, private :: setStringArray
        generic, public :: set => setCharacterScalar, &
            setStringScalar, &
            setCharacterArray, &
            setStringArray

        procedure, public :: expandArrays

        procedure, public :: key
        procedure, public :: getKey
        procedure, public :: getKeys

        procedure, private :: isSet_c
        procedure, private :: isSet_s
        generic, public    :: isSet =>isSet_c, isSet_s

        procedure, public :: size => dictionary_size

        procedure(printProcedure), public, deferred :: print
    end type

    interface
        subroutine printProcedure(this)
            import
            class(FDictionary), intent(in) :: this
        end subroutine
    end interface

contains
!******************************************************************************!
!> Check whether a key has a value in a `FDictionary` object.
!******************************************************************************!
    function isSet_c(this, name) result(set)
        class(FDictionary), intent(in) :: this
        character(*), intent(in) :: name
        logical :: set
!------
        integer :: i
!------
        set = .false.
        do i=1, this%size_
            if (this%key_(i) == name) then
                set = .true.
                return
            end if
        end do
    end function
!******************************************************************************!
!> Check whether a key has a value in a `FDictionary` object.
!******************************************************************************!
    function isSet_s(this, name) result(set)
        class(FDictionary), intent(in) :: this
        type(FString), intent(in) :: name
        logical :: set
!------
        integer :: i
!------
        set = .false.
        do i=1, this%size_
            if (this%key_(i) == name) then
                set = .true.
                return
            end if
        end do
    end function
!******************************************************************************!
!> Add keys to a dictionary object (Character version).
!******************************************************************************!
    subroutine setCharacterArray(this, key)
        class(FDictionary), intent(inout) :: this
        character(*), dimension(:), intent(in) :: key
!------
        integer :: i
!------
        do i=1, size(key)

            ! Set the key with a default value
            call this%set(key(i))
        end do
    end subroutine
!******************************************************************************!
!> Add keys to a dictionary object (FString version).
!******************************************************************************!
    subroutine setStringArray(this, key)
        class(FDictionary), intent(inout) :: this
        type(FString), dimension(1:), intent(in) :: key
!------
        integer :: i
!------
        do i=1, size(key)

            ! Set the key with a default value
            call this%set(key(i))
        end do
    end subroutine
!******************************************************************************!
!> Add a key to a dictionary object (Character version). Simply calls the
!> FString version.
!******************************************************************************!
    subroutine setCharacterScalar(this, key)
        class(FDictionary), intent(inout) :: this
        character(*), intent(in) :: key
!------
        call setStringScalar(this, FString(key))
    end subroutine
!******************************************************************************!
!> Add a key to a dictionary object (FString version).
!******************************************************************************!
    subroutine setStringScalar(this, key)
        class(FDictionary), intent(inout) :: this
        type(FString), intent(in) :: key
!------
        integer :: i
!------
        ! Make sure the key is not already defined
        do i=1, this%size()
            if (this%key(i) == key) then
                ! If it is, simply return
                return
            end if
        end do

        ! Increase the size of the internal arrays if needed
        this%size_ = this%size_ + 1
        if (.not.allocated(this%key_)) then
            call expandArrays(this)
        else
            if (this%size() > size(this%key_)) &
                call expandArrays(this)
        end if

        ! Set the value
        this%key_(this%size()) = key
    end subroutine
!******************************************************************************!
!> Get a key defined in a dictionary object.
!******************************************************************************!
    pure subroutine getKey(this, i, key)
        class(FDictionary), intent(in) :: this
        integer, intent(in) :: i
        type(FString), intent(out) :: key
!------
        if (.not.allocated(this%key_) .or. this%size_ == 0) return
        if (i > size(this%key_)) return

        key = this%key_(i)
    end subroutine
!******************************************************************************!
!> Get a key defined in a dictionary object.
!******************************************************************************!
    pure function key(this, i)
        class(FDictionary), intent(in) :: this
        integer, intent(in) :: i
        character(:), allocatable :: key
!------
        if (.not.allocated(this%key_) .or. this%size_ == 0) return
        if (i > size(this%key_)) return

        key = this%key_(i)
    end function
!******************************************************************************!
!> Get the keys defined in a dictionary object.
!******************************************************************************!
    pure subroutine getKeys(this, key)
        class(FDictionary), intent(in) :: this
        type(FString), dimension(:), allocatable, intent(out) :: key
!------
        integer :: i
!------
        if (.not.allocated(this%key_) .or. this%size_ == 0) then
            allocate(key(0))
            return
        end if

        allocate(key(this%size_))
        do i=1, this%size_
            key(i) = this%key_(i)%string()
        end do
    end subroutine
!******************************************************************************!
!> Get the number of defined keys in a dictionary.
!******************************************************************************!
    elemental function dictionary_size(this)
        class(FDictionary), intent(in) ::this
        integer :: dictionary_size
!------
        dictionary_size = this%size_
    end function
!******************************************************************************!
!> Reallocate the internal arrays of a dictionary object to mare space
!> for more elements. For internal use only.
!******************************************************************************!
    pure subroutine expandArrays(this)
        class(FDictionary), intent(inout) :: this
!------
        type(FString), dimension(:), allocatable :: tmps ! Temporary array for the keys
!------
        if (.not.allocated(this%key_)) then
            allocate(this%key_(this%initialSize))
        else
            call move_alloc(from=this%key_, to=tmps)
            allocate(this%key_(size(tmps)+this%sizeIncrement))
            this%key_(:size(tmps)) = tmps
        end if
    end subroutine
end module !LCOV_EXCL_LINE
