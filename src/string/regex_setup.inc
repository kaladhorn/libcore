!******************************************************************************!
!                              regex_setup module
!______________________________________________________________________________!
!> Procedures used when initialising a FRegex variable.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!


!******************************************************************************!
!> Set the pattern of a regex. This should be called only from constructors.
!******************************************************************************!
    subroutine setPattern(this, pattern, stat)
        class(FRegex), intent(inout)  :: this
        character(*),  intent(in)  :: pattern !< pattern to be compiled
        type(FException), intent(out), optional :: stat
!------
        logical :: reset
!------

        if (pattern /= "") then
            reset = .false.

            ! Set the human-friendly pattern
            if (pattern(len(pattern):len(pattern)) == C_NULL_CHAR) then
                this%FString = FString(pattern(:len(pattern)-1))
                this%pattern = pattern
            else
                this%FString = FString(pattern)
                this%pattern = pattern // C_NULL_CHAR
            end if
        else
            this%FString = FString("")
            this%pattern = C_NULL_CHAR

            ! Remove the compiled regex
            if (c_associated(this%re)) then
                call pcre_free(this%re)
                this%re = C_NULL_PTR
            end if

            ! Invalidate the study
            if (c_associated(this%study)) then
                call pcre_free(this%study)
                this%study = C_NULL_PTR
            end if
        end if
    end subroutine
!******************************************************************************!
!> Prepare a regex for substitution. The pattern must have been set
!> previously, and will be compiled if needed, as the substitution informations
!> are required.
!>
!> Informations about the substitution are stored in the FRegex object:
!> backReferenceCount: number of back references in the substitution string
!> backReferenceIndex: index of each back reference
!> backReferencePosition: position in the substitution string at which each back
!>   reference should be added
!> substitution: substitution string without the back references.
!>
!>FIXME: add consistency checks for the substitution string
!>FIXME: add support for named back references
!******************************************************************************!
    subroutine setSubstitution(this, substitution, stat)
        class(FRegex), intent(inout) :: this
        character(*), intent(in) :: substitution
        type(FException), intent(out), optional :: stat
!------
        type(FRegex) :: backReferenceRegex
        integer, dimension(:), allocatable :: index!, length
        type(FString), dimension(:), allocatable :: substrings
        integer :: c, n, iostat, slength
        character(:), allocatable :: buffer
        type(FException) :: stat_
!------
        ! Detect the back references in the substitution string
        backReferenceRegex = FRegex("\$([0-9]+|\`|\'|\+|\&)")
        call backReferenceRegex%match_all_both_character(substitution, index, substrings, stat_)
        if (stat_ == 0) then

            ! There is at least one back reference

            ! Prepare the regex object
            if (allocated(this%backReferenceIndex)) deallocate(this%backReferenceIndex)
            allocate(this%backReferenceIndex(size(index)))
            this%backReferenceIndex = 0
            if (allocated(this%backReferencePosition)) deallocate(this%backReferencePosition)
            allocate(this%backReferencePosition(size(index)))
            this%backReferencePosition = 0

            allocate(character(len(substitution)) :: buffer)
            buffer = substitution
            do c=1, size(index)
                read(buffer(index(c)+1:index(c)+substrings(c)%length()-1),*,iostat=iostat) n
                if (iostat == 0) then

                    ! The character after the dollar sign is an integer
                    buffer(index(c):) = buffer(index(c)+substrings(c)%length():)
                    this%backReferenceIndex(c) = n
                    this%backReferencePosition(c) = index(c)
                    index(:) = index(:) - substrings(c)%length()
                else

                    ! Special cases
                    select case(buffer(index(c)+1:index(c)+1))
                        case ("`") ! $`
                            buffer(index(c):) = buffer(index(c)+substrings(c)%length():)
                            this%backReferenceIndex(c) = FCRE_REF_BEFORE
                            this%backReferencePosition(c) = index(c)
                            index(:) = index(:) - substrings(c)%length()
                        case ("'") ! $'
                            buffer(index(c):) = buffer(index(c)+substrings(c)%length():)
                            this%backReferenceIndex(c) = FCRE_REF_AFTER
                            this%backReferencePosition(c) = index(c)
                            index(:) = index(:) - substrings(c)%length()
                        case ("+") ! $+
                            buffer(index(c):) = buffer(index(c)+substrings(c)%length():)
                            this%backReferenceIndex(c) = FCRE_REF_LAST
                            this%backReferencePosition(c) = index(c)
                            index(:) = index(:) - substrings(c)%length()
                        case ("&") ! $&
                            buffer(index(c):) = buffer(index(c)+substrings(c)%length():)
                            this%backReferenceIndex(c) = FCRE_REF_MATCH
                            this%backReferencePosition(c) = index(c)
                            index(:) = index(:) - substrings(c)%length()
                        case default
                    end select
                end if
            end do

            this%backReferenceCount = size(index) ! Number of back references
            sLength = len(substitution) - sum(substrings%length())

            this%substitute = buffer(:sLength) ! Cleaned substitution string
        else

            ! Constant substitution (without back references)
            this%substitute = substitution
        end if
        call stat_%discard
    end subroutine
!******************************************************************************!
!> Compile the pattern of a regex. If the pattern is an
!> empty string, status is set to FCRE_ERROR_EMPTY_PATTERN if present, otherwise
!> the subroutine returns. If another compilation error occurs, status is set
!> to the error code returned by the PCRE library if it is present, otherwise
!> the program aborts.
!******************************************************************************!
    subroutine compilePattern(this, stat)
        class(FRegex), intent(inout)  :: this
        type(FException), intent(out), optional :: stat
!------
        type(c_ptr) :: errorPointer
        integer(c_int) :: erroffset, errorCode
        integer :: captureCount
        character(:), allocatable :: errorMessage_
        type(FString),dimension(:), allocatable :: captureName
        type(FException) :: stat_
!------
        ! Try to compile the regex
        errorCode = 0
        this%re = C_NULL_PTR
        errorPointer = C_NULL_PTR

        ! Special case: empty pattern
        if (this%isBlank()) then
            if (present(stat)) call stat%raise(fcre_error_message(FCRE_ERROR_EMPTY_PATTERN))
            return
        end if

        ! Call the PCRE function
        this%re = pcre_compile2(pattern=this%pattern,            &
                                options=this%compilationOptions, &
                                  error=errorCode,               &
                                 errptr=errorPointer,            &
                              erroffset=erroffset,               &
                               tableptr=C_NULL_PTR)

        ! Compilation failed: print the error message and exit
        if (.not.c_associated(this%re)) then
            call c_f_string(errorPointer, errorMessage_)
            call stat_%raise(FExceptionDescription(errorMessage_, "when trying to compile regex '" // this%string() // "':"))

            ! Return and let the caller handle the failure if status or errorMessage is present
            if (present(stat)) then
                call stat%transfer(stat_)
            end if

            return
        end if

        ! Study the pattern by default
        call this%studyPattern

        ! Get the number of captured substrings
        call this%getPatternInfo(PCRE_INFO_CAPTURECOUNT, captureCount, errorCode)
        if (errorCode == 0) then

            ! Prepare the result array
            this%ovecsize = (captureCount + 1) * 3
        else
            call stat_%raise("Error while getting the number of captured substrings")

            ! This really should not happen
            ! Nonetheless, if it does, return with an error status if possible
            if (present(stat)) then
                call stat%transfer(stat_)
            end if

            return
        end if

        ! Get the named captures
        call this%getCaptureNames(captureName)
    end subroutine
!******************************************************************************!
!> Study the pattern of a regex. If status is provided and the
!> compilation fails, it is set to a non-zero value.
!******************************************************************************!
    subroutine studyPattern(this, stat)
        class(FRegex), intent(inout)  :: this
        type(FException), intent(out), optional :: stat
!------
        integer(c_int) :: options
        type(c_ptr) :: errorPointer
        character(:), allocatable :: errorMessage_
        type(FException) :: stat_
!------
        ! Try to compile the pattern if needed
        if (.not.c_associated(this%re)) then
            if (present(stat)) then
                if (allocated(this%pattern)) call this%compilePattern(stat)
            else
                if (allocated(this%pattern)) call this%compilePattern
            end if

            ! Test whether the pattern was compiled
            if (.not.c_associated(this%re)) then
                call stat_%raise("The pattern "//this%pattern//" could not be compiled: " // errorMessage_)
                if (present(stat)) call stat%transfer(stat_)
                return
            end if
        end if

        ! Use the JIT compiler if available
        options = PCRE_STUDY_JIT_COMPILE

        ! Deallocate the previous study if needed
        if (c_associated(this%study)) then
            call pcre_free_study(this%study)
            this%study = C_NULL_PTR
        end if

        ! Study the pattern
        this%study = pcre_study(code=this%re, &
                             options=options, &
                              errptr=errorPointer)

        ! Something failed
        if (.not.c_associated(this%study)) then
            call c_f_string(errorPointer, errorMessage_)

            ! Return and let the caller handle the failure if status or errorMessage is present
            call stat_%raise("Study failed with error:" // errorMessage_)
            if (present(stat)) call stat%transfer(stat_)
            return
        end if
    end subroutine
!******************************************************************************!
!> Set the pattern matching mode to be switch between case sensitive and
!> insensitive. This affects only the compilation options. The pattern will
!> need to be recompiled for the change to take effect.
!> If the mode variable is not present, the option will be set to case
!> insensitive.
!>
!>see: setCompilationOption, unsetCompilationOption
!******************************************************************************!
    subroutine setCaseSensitive(this, mode)
        class(FRegex), intent(inout) :: this
        logical, intent(in), optional :: mode
!------
        logical :: mode_
!------
        if (present(mode)) then
            mode_ = mode
        else
            mode_ = .true.
        end if

        ! Case sensitivity is the first bit of the compilation options
        if (mode_) then
            call this%unsetCompilationOption(PCRE_CASELESS)
        else
            call this%setCompilationOption(PCRE_CASELESS)
        end if
    end subroutine
!******************************************************************************!
!> Set the pattern matching mode to be switch between single-line and
!> multi-line. This affects only the compilation options. The pattern will
!> need to be recompiled for the change to take effect.
!> If the mode variable is not present, the option will be set to multiline.
!>
!>see: setCompilationOption, unsetCompilationOption
!******************************************************************************!
    subroutine setMultiline(this, mode)
        class(FRegex), intent(inout) :: this
        logical, intent(in), optional :: mode
!------
        logical :: mode_
!------
        if (present(mode)) then
            mode_ = mode
        else
            mode_ = .true.
        end if

        ! Multiline mode is the second bit of the compilation options
        if (mode_) then
            call this%setCompilationOption(PCRE_MULTILINE)
        else
            call this%unsetCompilationOption(PCRE_MULTILINE)
        end if
    end subroutine
!******************************************************************************!
!> Set the pattern matching mode to be switch between simple and extended. This
!> affects only the compilation options. The pattern will need to be recompiled
!> for the change to take effect.
!> If the mode variable is not present, the option will be set to extended.
!>
!>see: setCompilationOption, unsetCompilationOption
!******************************************************************************!
    subroutine setExtended(this, mode)
        class(FRegex), intent(inout) :: this
        logical, intent(in), optional :: mode
!------
        logical :: mode_
!------
        if (present(mode)) then
            mode_ = mode
        else
            mode_ = .true.
        end if

        ! Extended mode is the fourth bit of the compilation options
        if (mode_) then
            call this%setCompilationOption(PCRE_EXTENDED)
            this%execOptions = ibset(this%execOptions,fcre_option_position(PCRE_EXTENDED))
        else
            call this%unsetCompilationOption(PCRE_EXTENDED)
            this%execOptions = ibclr(this%execOptions,fcre_option_position(PCRE_EXTENDED))
        end if
    end subroutine
!******************************************************************************!
!> Set the pattern matching mode to be switch between greedy and not greedy.
!> This affects only the compilation options. The pattern will need to be
!> recompiled for the change to take effect.
!> If the mode variable is not present, the option will be set to greedy.
!>
!>see: setCompilationOption, unsetCompilationOption
!******************************************************************************!
    subroutine setReluctant(this, mode)
        class(FRegex), intent(inout) :: this
        logical, intent(in), optional :: mode
!------
        logical :: mode_
!------
        if (present(mode)) then
            mode_ = mode
        else
            mode_ = .true.
        end if

        ! Greedy mode is the tenth bit of the compilation options
        if (mode_) then
            call this%setCompilationOption(PCRE_UNGREEDY)
        else
            call this%unsetCompilationOption(PCRE_UNGREEDY)
        end if
    end subroutine
!******************************************************************************!
!> Set a regex compilation options. Valid options are defined in fcre.f90. This
!> gives a direct access to all the compilation options accepted by the PCRE
!> library, and should generally not be used from outside.
!******************************************************************************!
    subroutine setCompilationOption(this, option, stat)
        class(FRegex), intent(inout) :: this
        integer, intent(in) :: option               !< option to be set (valid values are defined in fcre.f90)
        type(FException), intent(out), optional :: stat
!-------
        integer :: p
        type(FException) :: stat_
!------
        ! Get the position of the bit corresponding to the option to set
        p = fcre_option_position(option)

        ! Test the option validity
        if (p < 0) then
            call stat_%raise("Error while setting compilation options: " // fcre_error_message(PCRE_ERROR_BADOPTION))
            if (present(stat)) call stat%transfer(stat_)
            return
        end if

        ! Set the option bit
        this%compilationOptions = ibset(this%compilationOptions, p)

        ! The extra data is no longer valid once the compilation options have been changed
        if (c_associated(this%re)) then
            call pcre_free(this%re)
            this%re = C_NULL_PTR
        end if
        if (c_associated(this%study)) then
            call pcre_free(this%study)
            this%study = C_NULL_PTR
        end if
        this%offset = 0
    end subroutine
!******************************************************************************!
!> Unset regex compilation options. Valid options are defined in fcre.f90.
!> This gives a direct access to all the compilation options accepted by the
!> PCRE library, other procedures provide simpler access to the most commonly-
!> used options.
!>
!>see: setCompilationOption
!******************************************************************************!
    subroutine unsetCompilationOption(this, option, status)
        class(FRegex), intent(inout) :: this
        integer, intent(in) :: option         !< option to be unset (valid values are defined in fcre.f90)
        integer, intent(out), optional :: status  !< Error code if something goes wrong
!------
        integer :: p
!------
        ! Get the position of the bit corresponding to the option to un-set
        p = fcre_option_position(option)

        ! Test the option validity
        if (p < 0) then
            if (present(status)) then
                status = PCRE_ERROR_BADOPTION
                return
            else
                write(*,*) "Error while changing compilation options: ", fcre_error_message(PCRE_ERROR_BADOPTION)
                stop
            end if
        end if

        ! Set the option bit
        this%compilationOptions = ibclr(this%compilationOptions, fcre_option_position(option))

        ! The extra data is no longer valid once the compilation options have been changed
        if (c_associated(this%re)) then
            call pcre_free(this%re)
            this%re = C_NULL_PTR
        end if
        if (c_associated(this%study)) then
            call pcre_free(this%study)
            this%study = C_NULL_PTR
        end if
        this%offset = 0
    end subroutine
