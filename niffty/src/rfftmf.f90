    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine rfftmf(lot, jump, n, inc, r, lenr, wsave, lensav, work, lenwrk, &
      ier)
        include 'kinds.inc'
        integer lot, jump, n, inc, lenr, lensav, lenwrk, ier
        real (rwp) r(lenr), wsave(lensav), work(lenwrk)
        logical xercon

        ier = 0

        if (lenr<(lot-1)*jump+inc*(n-1)+1) then
            ier = 1
            call xerfft('RFFTMF ', 6)
        else if (lensav<n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('RFFTMF ', 8)
        else if (lenwrk<lot*n) then
            ier = 3
            call xerfft('RFFTMF ', 10)
        else if (.not. xercon(inc,jump,n,lot)) then
            ier = 4
            call xerfft('RFFTMF ', -1)
        end if

        if (n==1) return

        call mrftf1(lot, jump, n, inc, r, work, wsave, wsave(n+1))
    end subroutine
