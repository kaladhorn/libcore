#******************************************************************************#
#                               libCore json                                   #
#______________________________________________________________________________#
#
#  Version 2.0
#
#  Written by Paul Fossati, <paul.fossati@gmail.com>
#  Copyright (c) 2009-2016 Paul Fossati
#------------------------------------------------------------------------------#
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
#     * Redistributions of source code must retain the above copyright notice, #
#       this list of conditions and the following disclaimer.                  #
#                                                                              #
#     * Redistributions in binary form must reproduce the above copyright      #
#       notice, this list of conditions and the following disclaimer in the    #
#       documentation and/or other materials provided with the distribution.   #
#                                                                              #
#     * The name of the author may not be used to endorse or promote products  #
#      derived from this software without specific prior written permission    #
#      from the author.                                                        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#******************************************************************************#
JSON_SOURCES= json/json_kinds.F90 json/json_parameters.F90                     \
	json/json_string_utilities.F90 json/json_value_module.F90                  \
	json/json_file_module.F90 json/json_module.F90

#******************************************************************************!
# Dependencies
#******************************************************************************!
json/json_module.o: json/json_module.F90 json_file_module.mod                  \
	json_value_module.mod json_kinds.mod
json_module.mod: json/json_module.o

json/json_file_module.o: json/json_file_module.F90 json_kinds.mod              \
	json_parameters.mod json_string_utilities.mod json_value_module.mod
json_file_module.mod: json/json_file_module.o

json/json_value_module.o: json/json_value_module.F90 json_kinds.mod            \
	json_parameters.mod json_string_utilities.mod
json_value_module.mod: json/json_value_module.o

json/json_string_utilities.o: json/json_string_utilities.F90 json_kinds.mod    \
	json_parameters.mod
json_string_utilities.mod: json/json_string_utilities.o

json/json_kinds.o: json/json_kinds.F90
json_kinds.mod: json/json_kinds.o

json/json_parameters.o: json/json_parameters.F90 json_kinds.mod
json_parameters.mod: json/json_parameters.o
