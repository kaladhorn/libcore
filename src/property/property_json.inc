!******************************************************************************!
!                             property_json                                    !
!______________________________________________________________________________!
!> I/O procedure for JSON files. Part of the `[[mod_property(module)]]`
!> module.
!
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2015 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!

!******************************************************************************!
!> Read properties from a JSON file
!******************************************************************************!
    subroutine readJson(this, file, stat)
        class(FProperty), intent(inout) :: this
        type(FFile),      intent(inout) :: file
        type(FException), intent(out), optional :: stat
!------
        type(json_file) :: json
        type(json_core) :: core
        type(FException) :: stat_
        type(json_value), pointer :: value
!------
        body: block
            ! Read the Json file
            call json%load_file(file%fullName())

            ! Return if the file could not be read
            if (json%failed()) then    !if there was an error reading the file
                call stat_%raise("Error while trying to read JSON file "//file%fullName())
                exit body
            end if

            ! Get the JSON object from the file
            call json%get(core)
            call json%get(value)

            call propertyWithJsonValue(this, value, core)

            ! Set the property name to the name of the file if it is empty
            if (.not.allocated(this%name)) then
                this%name = file%fullName()
            else if (this%name == "") then
                this%name = file%fullName()
            end if

            ! The root property has an empty key
            this%key_ = ""
        end block body

        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Transform a json_value object into a FProperty object
!******************************************************************************!
    recursive subroutine propertyWithJsonValue(this, value, core)
        type(FProperty), intent(out) :: this
        type(json_value), pointer, intent(inout) :: value
        type(json_core), intent(inout) :: core
!------
        integer :: type, i
        type(json_value), pointer :: v
        real(e) :: doublevalue
        integer :: integerValue
        logical :: logicalValue
        character(:), allocatable :: characterValue
!------

        ! Get the description of the value
        call core%info(value, type, this%size, this%key_)

        ! Get the rank of the property
        if (this%size == 0) then
            this%rank_ = RANK_SCALAR
        else
            this%rank_ = RANK_LIST
        end if

        ! Get the value of the property
        select case(type)
            case (json_object)
            case (json_array)
                this%rank_ = RANK_VECTOR
            case (json_logical)
                call core%get(value, logicalValue)
                this%value_ = logicalValue
            case (json_integer)
                call core%get(value, integerValue)
                this%value_ = integerValue
            case (json_double)
                call core%get(value, doublevalue)
                this%value_ = doublevalue
            case (json_string)
                call core%get(value, characterValue)
                this%value_ = characterValue
        end select

        ! Set the children if any
        if (this%size > 0) then
            allocate(this%children(this%size))
            do i=1, this%size
                call core%get_child(value, i, v)
                call propertyWithJsonValue(this%children(i), v, core)
            end do
        end if
    end subroutine
!******************************************************************************!
!> Print the representation of a property list in a JSON file
!******************************************************************************!
    subroutine saveToJson(this, file, stat)
        class(FProperty), intent(in) :: this
        type(FFile), intent(inout) :: file
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        type(json_value), pointer :: value
        character(:), allocatable :: json_string
        type(json_core) :: json
!------
        body: block
            !
            call json%initialize
            if (json%failed()) then
                call stat_%raise("Unable to save properties to file "//file%fullName()//": could not create JSON object")
                exit body
            end if

            ! Get the json_value object
            call jsonWithProperty(value, json, this)

            ! Transform the json_value into a character variable
            call json%print_to_string(value, json_string)

            ! Free memory used by the json_value object
            call json%destroy

            ! Print the character variable to the file
            call file%writeLine(json_string, stat_)
        end block body

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get a `[[json_value(type)]]` from a `[[FProperty(type)]]` object.
!******************************************************************************!
    recursive subroutine jsonWithProperty(value, json, property)
        type(json_value), pointer, intent(inout) :: value
        type(json_core), intent(inout) :: json
        class(FProperty), intent(in) :: property
!------
        integer :: i
!------

        ! Create the root JSON value
        call json%create_object(value, '')

        ! Populate it
        if (property%size == 0) then
            call json%add(value, property%key_, property%value_)
        else
            do i=1, property%size
                call setProperties(value, property%children(i))
            end do
        end if
    contains
        recursive subroutine setProperties(parent, property)
            type(json_value), pointer, intent(inout) :: parent
            class(FProperty), intent(in) :: property
!------
            type(json_value), pointer :: child
            integer :: i
!------

            if (property%size == 0) then
                call json%create_string(child, property%value_, property%key_)
            else
                if (property%rank_ == RANK_VECTOR) then
                    call json%create_array(child, property%key_)
                else
                    call json%create_object(child, property%key_)
                end if
                do i=1, property%size
                    call setProperties(child, property%children(i))
                end do
            end if
            call json%add(parent, child)
        end subroutine
    end subroutine