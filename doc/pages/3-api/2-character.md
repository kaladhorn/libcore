title: Procedures for the character intrinsic type

It is often necessary to mix character and numbers in a `write`statement when doing human-readable output.
The procedures and overloaded operators in the `[[ext_character(module)]]` module aim at making this easier.
They allow to create character variables from numbers or arrays of numbers and concatenate numbers and character variables.

# Assignments to character variables
The assignment (`=`) is overloaded to allow assignment of integers, reals and logicals to assumed-length, allocatable character variables.

For logicals, the result is `"true"` if the input is `.true.`, `"false"` otherwise.

For integers, the result is the same as if the integer were written to the character variable using the `i0` format. 
The result will have exactly the right size, without leading or trailing blanks.

For real numbers, the output is supposed to be human-readable. To this intent, scientific notation is used for numbers greater 
than 999 or smaller than 0.001 in magnitude. The number of figures on the right side of the decimal point is equal to the 
precision of the number, except for the trailing zeros that are removed for clarity.

Arrays are enclosed in square brackets, with their elements separated by commas and spaces.


```fortran
    program simple_character_assignment
        use core ! Defines the number kinds
        use ext_character ! Defines operations with the character intrinsic type
        
        character(:), allocatable :: c
        logical :: l
        integer :: i
        real(wp) :: x
        real(wp), dimension(3) :: v
        
        c = .true.
        write(*,*) c ! Equivalent to write(*,*) "yes"
        
        c = 42
        write(*,*) c ! Equivalent to write(*,*) "42"
        
        c = PI
        write(*,*) c ! Equivalent to write(*,*) ""
        
        c = 4.0_wp
        write(*,*) c ! Equivalent to write(*,*) "4"
        
        c = 2.5_wp
        write(*,*) c ! Equivalent to write(*,*) "2.5"
        
        c = [1.0_wp, 2.5_wp, 3.87321_wp]
        write(*,*) c ! Equivalent to write(*,*) "[1, 2.5, 3.87321]"
    end program
```

# Concatenation
The `//` operator is overloaded to handle the combination of character variables with integers, logicals and reals.
It is based on the overloaded assignment described in the previous section.

```fortran
    program simple_concatenation
        use core ! Defines the number kinds
        use ext_character ! Defines operations with the character intrinsic type
        
        character(:), allocatable :: base
        integer :: i
        real(wp) :: x
        real(wp), dimension(3) :: v
        
        i = 42
        x = PI
        base = "This is a nice answer:"
        
        write(*,'(a)') base // i ! Equivalent to write(*,'(a,i0)') base, i
        write(*,'(a)') base // i // x ! Similar to write(*,'(a,i0,g0)') base, i, x
        
        v = [1.0_wp, 2.5_wp, 3.87321_wp]
        write(*,*) "This is a real array: " // v ! Prints "This is a real array: [1, 2.5, 3.87321]"
    end program
```
