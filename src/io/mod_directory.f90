!******************************************************************************!
!                         mod_directory module
!------------------------------------------------------------------------------!
!> FDirectory derived type and type-bound procedures, for interaction with file
!> systems.
!>
!> For now, only POSIX systems are supported. The system/dependent stuff
!> should only happen in `mod_directory_wrapper.c`.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_directory
    use iso_c_binding

    use fcre
    use ext_character
    use com_io

    use mod_regex,      only: FRegex
    use mod_string,     only: FString
    use mod_exception,  only: FException, FExceptionDescription

    implicit none
    private
    save

!******************************************************************************!
!> Directory description for interaction with the file system.
!******************************************************************************!
    type, public :: FDirectory
        private
        character(:,c_char), allocatable :: path
    contains
        ! Basic operations
        procedure, public :: exists  => directory_exists      !< Check whether the directory exists.
        procedure, public :: isEmpty => directory_isEmpty     !< Test whether the directory is empty.
        procedure, public :: create  => directory_create      !< Create the directory if it does not exist already.
        procedure, public :: remove  => directory_remove      !< Remove the directory.
        procedure, public :: string  => directory_string      !< Get the full path of a directory.

        ! Read the directory content
        procedure, public :: getFileList      => directory_getFileList      !< Get the list of files in the directory.
        procedure, public :: getDirectoryList => directory_getDirectoryList !< Get the list of subdirectories.

        ! Comparison
        procedure, private :: directory_equals
        generic, public :: operator(==) => directory_equals !< Compare two directories
    end type

!******************************************************************************!
!> Constructor interface.
!******************************************************************************!
    interface FDirectory
        module procedure directory_initWithChar, directory_initWithString, directoriesWithRegex
    end interface

    integer, parameter :: PATH_LENGTH    = 1024
    integer, parameter :: FILE_TYPE      = 0
    integer, parameter :: DIRECTORY_TYPE = 1

contains
!******************************************************************************!
!> Get the list of directories whose names match a regex.
!******************************************************************************!
    function directoriesWithRegex(regex, err) result(this)
        type(FRegex), intent(in) :: regex !< Regex that the directories must match
        type(FException), intent(out),  optional    :: err  !< Error status
        type(FDirectory), dimension(:), allocatable :: this !< List of directories
!------
        type(FRegex) :: pathRegex
        type(FRegex), dimension(:), allocatable :: regexArray
        type(FString), dimension(:), allocatable :: files, token
        type(FException) :: err_
        integer :: i, nFiles, n
        type(FString) :: pattern
        character(:), allocatable :: start
!------
        body: block
            nFiles = 0

            pattern = regex%FString

            ! Split the path into its components
            pathRegex = FRegex("(?<!\\)\/")
            call pathRegex%split(pattern, token)

            ! Allocate the work arrays
            allocate(files(100))

            if (pattern%startsWith("/")) then
                allocate(regexArray(size(token)+1))
                regexArray(1) = ""
                start = ""
                n  = 1
            else if (.not.pattern%startsWith("\./").and..not.pattern%startsWith("./")) then
                allocate(regexArray(size(token)+1))
                regexArray(1) = "."
                start = "."
                n = 1
            else
                allocate(regexArray(size(token)))
                n = 0
                start = "."
            end if

            do i=1, size(token)
                n = n + 1
                regexArray(n) = FRegex("^"//token(i)//"$")
            end do

            if (size(regexArray) == 1) then
                nFiles = 1
                files(1) = start
            else
                call walk(start, regexArray(2:))
            end if

        end block body

        allocate(this(nFiles))
        do i=1, nFiles
            call simplifyPath(files(i))
            this(i) = FDirectory(files(i))
        end do

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    contains
        recursive subroutine walk(start, regex)
            character(*), intent(in) :: start
            type(FRegex), dimension(:), intent(inout) :: regex
!------
            type(FDirectory) :: cwd
            type(FString), dimension(:), allocatable :: string
            integer :: i
!------
            cwd = FDirectory(start//"/")
            call cwd%getDirectoryList(string, regex(1))

            if (size(regex) > 1) then
                do i=1, size(string)
                    call walk(start//"/"//string(i), regex(2:))
                end do
            else if (size(regex) == 1) then
                do i=1, size(string)

                    ! Make sure there is some space to add another name
                    if (nFiles == size(files)) call reallocateDirectoryList

                    ! Increment the name counter
                    nFiles = nFiles + 1

                    ! Add the name
                    files(nFiles) = start//"/"//string(i)
                end do
            end if
        end subroutine
        ! Increase the size of an array of directory objects
        subroutine reallocateDirectoryList
            type(FString), dimension(:), allocatable :: tmpf

            allocate(tmpf(size(files)+100))
            tmpf(:size(files)) = files
            deallocate(files)
            call move_alloc(from=tmpf, to=files)
        end subroutine
    end function
!******************************************************************************!
!> Initialise a `[[FDirectory(type)]]` object with a path in a character
!> variable.
!******************************************************************************!
    pure function directory_initWithChar(path) result(this)
        type(FDirectory) :: this
        character(*), intent(in) :: path !< Directory path
!------
        ! Set the directory path
        this%path = path//C_NULL_CHAR
    end function
!******************************************************************************!
!> Initialise a `[[FDirectory(type)]]` object with a path in a
!> `[[FString(type)]]` object.
!******************************************************************************!
    pure function directory_initWithString(path) result(this)
        type(FString), intent(in) :: path !< Directory path
        type(FDirectory) :: this
!------
        ! Call the character constructor
        this = directory_initWithChar(path%string())
    end function
!******************************************************************************!
!> Check whether a directory correspdonding to a `[[FDirectory(type)]]`
!> object exists in the file system.
!******************************************************************************!
    function directory_exists(this) result(exists)
        class(FDirectory), intent(in) :: this
        logical :: exists !< `.true.` if the directory exists, `.false.` otherwise
!------
        type(c_ptr) :: dir
        integer :: res
!------
        if (allocated(this%path)) then
            dir = opendir(this%path)
            if (c_associated(dir)) then
                exists = .true.
                res = closedir(dir)
            else
                exists = .false.
            end if
        else
            ! The directory has not been properly initialised
            exists = .false.
        end if
    end function
!******************************************************************************!
!> Create a directory correspdonding to a `[[FDirectory(type)]]` object. No 
!> error is returned if the directory already exists.
!>
!>@todo handle error conditions properly: access denied
!>@todo create the missing parent directories if needed, as in mkdir -r
!******************************************************************************!
    subroutine directory_create(this, err)
        class(FDirectory), intent(in) :: this
        type(FException), intent(out), optional :: err !< Error status
!------
        integer(c_int) :: res
        type(FException) :: err_
!------
        ! Try to create the directory
        if (allocated(this%path)) then
            res = mkdir(this%path)
            if (res /= 0) then
                if (ioErrorMessage() /= "File exists") &
                    call err_%raise(FExceptionDescription(ioErrorMessage(), &
                        "when trying to create directory " // this%path // ":"))
            end if
        else
            call err_%raise("Trying to create directory with empty path.")
        end if

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Remove from the file system a directory correspdonding to a
!> `[[FDirectory(type)]]` object.
!>
!>@todo handle error conditions properly: directory does not exist, access
!>    denied
!******************************************************************************!
    subroutine directory_remove(this, err)
        class(FDirectory), intent(in) :: this
        type(FException), intent(out), optional :: err !< Error status
!------
        integer :: res
        type(FException) :: err_
!------
        if (allocated(this%path)) then
            res = rmdir(this%path)
            if (res /= 0) &
                call err_%raise(FExceptionDescription(ioErrorMessage(), &
                    "when trying to remove directory " // this%path // ":"))
        else
            call err_%raise("Trying to remove directory with empty path.")
        end if

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Check whether a directory is empty.
!>
!> Stop the program if the directory does not exist, or if its content connot
!> be read properly.
!******************************************************************************!
    function directory_isEmpty(this, stat) result(test)
        class(FDirectory), intent(in) :: this
        logical :: test !< `.true.` it the directory is empty, `.false.` otherwise
        type(FException), intent(out), optional :: stat
!------
        integer(c_int) :: filetype, l
        type(c_ptr) :: dir
        character(PATH_LENGTH,c_char) :: buffer
        type(FException) :: err_
!------
        test = .true.
        body: block
            if (.not.allocated(this%path)) then
                call err_%raise("Trying to determine emptiness of an undefined directory.")
                exit body
            end if

            ! Open the directory
            dir = opendir(this%path)

            test = .true.
            if (.not.c_associated(dir)) then
                call err_%raise(FExceptionDescription("No such file or directory.", &
                    "when trying to determine emptiness of '" // this%path // "'"))
                exit body
            end if

            ! Read the first file or subdirectory
            l = readdir(dir, buffer, PATH_LENGTH, filetype)

            ! If there is no subdirectory, then something wrong happened, as every directory should contain at least . and ..
            if (l == 0) call err_%raise("Error while trying to read "//this%string())

            ! The directory is not empty if it has at least one element that is neither . nor ..
            do while(l /= 0)
                if (buffer(:l) /= "." .and. buffer(:l) /= "..") then
                    test = .false.
                    exit
                end if
                l = readdir(dir, buffer, PATH_LENGTH, filetype)
            end do
        end block body
        
        if (present(stat)) call stat%transfer(err_)
    end function
!******************************************************************************!
!> Get the list of files in a directory. If `regex` is present, the list will be
!> filtered and contain only files with matching names. File names are sorted 
!> using natural alphabetical order (see the `[[mod_string(module)]]`module).
!******************************************************************************!
    subroutine directory_getFileList(this, filenames, regex, stat)
        class(FDirectory), intent(in) :: this !< Directory to be searched
        type(FString), dimension(:), allocatable, intent(out) :: filenames !< List of file names
        type(FRegex),     intent(inout), optional :: regex !< Regex for filtering
        type(FException), intent(out),   optional :: stat  !< Error status
!------
        integer :: l, i, nFiles, err, j, n
        integer(c_int) :: filetype
        type(c_ptr) :: dir
        character(PATH_LENGTH,c_char) :: buffer
        type(FString), dimension(:), allocatable :: filenames_
        type(FException) :: stat_
        logical, dimension(:), allocatable :: processed
!------

        ! Open the directory
        dir = opendir(this%path)

        ! Get the number of files
        l = readdir(dir, buffer, PATH_LENGTH, filetype)
        nFiles = 0
        do while(l /= 0)
            if (fileType == FILE_TYPE) nFiles = nFiles + 1
            l = readdir(dir, buffer, PATH_LENGTH, filetype)
        end do

        ! Close the directory
        err = closedir(dir)

        ! Allocate the file name array
        allocate(filenames_(nFiles))

        ! Open the directory
        dir = opendir(this%path)

        ! Get the file names
        buffer = ""
        l = readdir(dir, buffer, PATH_LENGTH, filetype)
        nFiles = 0
        do while(l /= 0)
            if (fileType == FILE_TYPE) then
                if (present(regex)) then
                    call regex%match(buffer(:l), i, stat=stat_)

                    ! Add a file to the list if it matches
                    if (i > 0 .and. stat_ == 0) then
                        nFiles = nFiles + 1
                        filenames_(nFiles) = FString(buffer(:l), "natural")
                    else
                        call stat_%discard
                    end if
                else
                    nFiles = nFiles + 1
                    filenames_(nFiles) = FString(buffer(:l), "natural")
                end if
            end if
            buffer = ""
            l = readdir(dir, buffer, PATH_LENGTH, filetype)
        end do

        ! Sort files by name
        allocate(filenames(nFiles))
        allocate(processed(nFiles))
        processed = .false.
        do i=1, nFiles

            n = 0
            do j=1, nFiles
                if (processed(j)) cycle

                if (n == 0) then
                    n = j
                    cycle
                end if
                if (filenames_(j) < filenames_(n)) then
                    n = j
                end if
            end do

            filenames(i) = filenames_(n)
            processed(n) = .true.
        end do

        ! Close the directory
        err = closedir(dir)
    end subroutine
!******************************************************************************!
!> Get the list of subdirectories in a directory. If `regex` is present, the
!> list will be filtered and contain only subdirectories with matching names.
!>
!>@todo handle error conditions properly: directory does not exist, access
!>    denied
!******************************************************************************!
    subroutine directory_getDirectoryList(this, directoryNames, regex, stat)
        class(FDirectory), intent(in) :: this !< Directory to be searched
        type(FString), dimension(:), allocatable, intent(out) :: directoryNames !< List of subdirectories
        type(FRegex),     intent(inout), optional :: regex !< Regex for filtering
        type(FException), intent(out),   optional :: stat  !< Error status
!------
        integer :: l, err, nDirs
        integer(c_int) :: filetype
        type(c_ptr) :: dir
        character(PATH_LENGTH,c_char) :: buffer
        type(FException) :: stat_
!------
        ! Open the directory
        dir = opendir(this%path)

        ! Get the number of files
        l = readdir(dir, buffer, PATH_LENGTH, filetype)
        nDirs = 0
        do while(l > 0)
            if (fileType == DIRECTORY_TYPE) then
                if (present(regex)) then
                    if (regex%doesMatch(buffer(:l))) nDirs = nDirs + 1
                else
                    nDirs = nDirs + 1
                end if
            end if
            l = readdir(dir, buffer, PATH_LENGTH, filetype)
        end do

        ! Close the directory
        err = closedir(dir)

        ! Allocate the file name array
        allocate(directoryNames(nDirs))

        ! Open the directory
        dir = opendir(this%path)

        ! Get the file names
        l = readdir(dir, buffer, PATH_LENGTH, filetype)
        nDirs = 0
        do while(l > 0)
            if (fileType == DIRECTORY_TYPE) then
                if (present(regex)) then
                    if (regex%doesMatch(buffer(:l))) then
                        nDirs = nDirs + 1
                        directoryNames(nDirs) = buffer(:l)
                    end if
                else
                    nDirs = nDirs + 1
                    directoryNames(nDirs) = buffer(:l)
                end if
            end if
            l = readdir(dir, buffer, PATH_LENGTH, filetype)
        end do

        ! Close the directory
        err = closedir(dir)

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Get the full path of a directory.
!******************************************************************************!
    pure function directory_string(this) result(string)
        class(FDirectory), intent(in) :: this
        character(:), allocatable :: string  !< Path of the directory
!------
        if (allocated(this%path)) string = this%path(:len(this%path)-1)
    end function
!******************************************************************************!
!> Simplify a path in a `[[FString(type)]]` object.
!******************************************************************************!
    subroutine simplifyPath(path)
        type(FString), intent(inout) :: path !< Path to be simplified
!------
        type(FRegex) :: re, re2
        character(:), allocatable :: path_
        integer :: i
        type(FException) :: stat
!------
        path_ = path

        ! Replace each instance of "./" by ""
        re2 = FRegex("(?<!\.)\.\/", "")
        do while(stat == 0)
            call re2%replace(path_, i, stat=stat)
        end do
        call stat%discard

        ! Replace each instance of "//" by "/"
        re = FRegex("\/\/", "\/")
        do while(stat == 0)
            call re%replace(path_, i, stat=stat)
        end do
        call stat%discard

        path = path_
    end subroutine
!******************************************************************************!
!> Compare two directories.
!>
!> They are identical if they have the same path.
!******************************************************************************!
    elemental impure function directory_equals(this, to) result(equals)
        class(FDirectory), intent(in) :: this
        class(FDirectory), intent(in) :: to
        logical :: equals !< `.true.` if both directories have equivalent paths, `.false.` otherwise.
!------
        type(FString) :: path1, path2
!------
        ! Get the simplified paths
        path1 = this%path
        call simplifyPath(path1)
        path2 = to%path
        call simplifyPath(path2)

        ! Compare the simplified paths
        equals = (path1 == path2)
    end function
end module !LCOV_EXCL_LINE
