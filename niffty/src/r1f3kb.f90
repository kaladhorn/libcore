    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine r1f3kb(ido, l1, cc, in1, ch, in2, wa1, wa2)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) cc(in1, ido, 3, l1), ch(in2, ido, l1, 3), wa1(ido), &
          wa2(ido)

        arg = 2.*4.*atan(1.0_rwp)/3.
        taur = cos(arg)
        taui = sin(arg)
        do k = 1, l1
            ch(1, 1, k, 1) = cc(1, 1, 1, k) + 2.*cc(1, ido, 2, k)
            ch(1, 1, k, 2) = cc(1, 1, 1, k) + (2.*taur)*cc(1, ido, 2, k) - &
              (2.*taui)*cc(1, 1, 3, k)
            ch(1, 1, k, 3) = cc(1, 1, 1, k) + (2.*taur)*cc(1, ido, 2, k) + &
              2.*taui*cc(1, 1, 3, k)
        end do
        if (ido==1) return
        idp2 = ido + 2
        do k = 1, l1
            do i = 3, ido, 2
                ic = idp2 - i
                ch(1, i-1, k, 1) = cc(1, i-1, 1, k) + &
                  (cc(1,i-1,3,k)+cc(1,ic-1,2,k))
                ch(1, i, k, 1) = cc(1, i, 1, k) + (cc(1,i,3,k)-cc(1,ic,2,k))
                ch(1, i-1, k, 2) = wa1(i-2)*((cc(1,i-1,1, &
                  k)+taur*(cc(1,i-1,3,k)+cc(1,ic-1,2,k)))-(taui*(cc(1,i,3,k)+ &
                  cc(1,ic,2,k)))) - wa1(i-1)*((cc(1,i,1, &
                  k)+taur*(cc(1,i,3,k)-cc(1,ic,2,k)))+(taui*(cc(1,i-1,3,k)- &
                  cc(1,ic-1,2,k))))
                ch(1, i, k, 2) = wa1(i-2)*((cc(1,i,1, &
                  k)+taur*(cc(1,i,3,k)-cc(1,ic,2,k)))+(taui*(cc(1,i-1,3,k)- &
                  cc(1,ic-1,2,k)))) + wa1(i-1)*((cc(1,i-1,1, &
                  k)+taur*(cc(1,i-1,3,k)+cc(1,ic-1,2,k)))-(taui*(cc(1,i,3,k)+ &
                  cc(1,ic,2,k))))
                ch(1, i-1, k, 3) = wa2(i-2)*((cc(1,i-1,1, &
                  k)+taur*(cc(1,i-1,3,k)+cc(1,ic-1,2,k)))+(taui*(cc(1,i,3,k)+ &
                  cc(1,ic,2,k)))) - wa2(i-1)*((cc(1,i,1, &
                  k)+taur*(cc(1,i,3,k)-cc(1,ic,2,k)))-(taui*(cc(1,i-1,3,k)- &
                  cc(1,ic-1,2,k))))
                ch(1, i, k, 3) = wa2(i-2)*((cc(1,i,1, &
                  k)+taur*(cc(1,i,3,k)-cc(1,ic,2,k)))-(taui*(cc(1,i-1,3,k)- &
                  cc(1,ic-1,2,k)))) + wa2(i-1)*((cc(1,i-1,1, &
                  k)+taur*(cc(1,i-1,3,k)+cc(1,ic-1,2,k)))+(taui*(cc(1,i,3,k)+ &
                  cc(1,ic,2,k))))
            end do
        end do
    end subroutine
