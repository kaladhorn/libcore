!******************************************************************************!
!                            mod_function module
!______________________________________________________________________________!
!> `[[FFunction(type)]]` derived type and type-bound procedures, for arbitrary
!> mathematical function evaluation.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_function
    use core

    use mod_string,     only: FString
    use mod_exception,  only: FException
    use mod_expression, only: FExpression
    use mod_numericalDictionary, only: FNumericalDictionary

    implicit none
    private
    save

!******************************************************************************!
!> Mathematical function of one independent variable.
!******************************************************************************!
    type, public :: FFunction
        private
        character(:), allocatable :: variable
        type(FExpression), public :: expr
    contains
        procedure, public :: eval       !< Evaluate the function (subroutine version)
        procedure, public :: evalf      !< Evaluate the function (pure function version)

        procedure, public :: printAST   !< Print the AST
        procedure, public :: string     !< Get a character variable representing the function
        procedure, public :: print      !< Print a representation on standard output
    end type

!******************************************************************************!
!> Constructor interface.
!******************************************************************************!
    interface FFunction
        module procedure function_init
    end interface
contains
!******************************************************************************!
!> Initialise a FFunction object with an expression.
!******************************************************************************!
    function function_init(expression, variable, arg, err) result(this)
        type(FFunction), target          :: this               !< the resulting FFunction object
        character(*), intent(in) :: expression         !< Function expression
        character(*), intent(in) :: variable           !< Name of the independent variable
        type(FNumericalDictionary), intent(in), optional :: arg !< Name and value of parameters
        type(FException),     intent(out), optional :: err !< Error status
!------
        type(FException) :: err_
        type(FString), dimension(:), allocatable :: variables
        integer :: i
!------
        body: block
            ! Set the independent variable
            this%variable = variable

            ! Set the interpreter
            this%expr = FExpression(expression, err_)
            if (err_ /= 0) exit body

            ! Make sure there is only one independent variable
            call this%expr%getVariableNames(variables)
            do i=1, size(variables)
                if (variables(i) == variable) cycle
                if (present(arg)) then
                    if (.not.arg%isSet(variables(i))) then
                        call err_%raise("The parameter "//variables(i)//" does not have a value")
                        exit body
                    end if
                else
                    call err_%raise("The parameter "//variables(i)//" does not have a value")
                end if
            end do

            ! Set the parameters
            if (present(arg)) call this%expr%setVariables(arg)

            ! Optimise the expression
            call this%expr%simplify
            call this%expr%optimise
            call this%expr%setVariable(this%variable, 0.0_e)
        end block body

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    end function
!******************************************************************************!
!> Evaluate a `FFunction` object (subroutine version).
!******************************************************************************!
    subroutine eval(this, x, y, err)
        class(FFunction), intent(inout) :: this !< Function to evaluate
        real(e), intent(in)  :: x !< Value of the independent variable
        real(e), intent(out) :: y !< Result value
        type(FException), intent(out), optional :: err !< Error status
!------
        type(FException) :: err_
!------
        call this%expr%eval(this%variable, x, y)

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Evaluate a `FFunction` object (function version). This  function would be
!> pure if it were not for ieee_* calls in `FExpression::evalf`, so it should be
!> fine in most contexts.
!******************************************************************************!
    function evalf(this, x) result(y)
        class(FFunction), intent(inout) :: this !< Function to evaluate
        real(e), intent(in)  :: x            !< Value of the independent variable
        real(e) :: y !< Result value

        call this%expr%eval(this%variable, x, y)
    end function
!******************************************************************************!
!> Get a character variable containing a description of a function.
!******************************************************************************!
    function string(this)
        class(FFunction), intent(in) :: this
        character(:), allocatable :: string

        string = this%expr%string()
    end function
!******************************************************************************!
!> Print a description of a function on standard output.
!******************************************************************************!
    subroutine print(this)
        class(FFunction), intent(in) :: this

        call this%expr%print
    end subroutine
!******************************************************************************!
!> Print the AST of a function on standard output.
!******************************************************************************!
    subroutine printAST(this)
        class(FFunction), intent(in) :: this

        call this%expr%printAST
    end subroutine
end module
