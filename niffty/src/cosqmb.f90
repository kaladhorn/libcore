    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cosqmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, lenwrk, &
      ier)
        include 'kinds.inc'
        integer lot, jump, n, inc, lenx, lensav, lenwrk, ier
        real (rwp) x(inc, *), wsave(lensav), work(lenwrk)
        logical xercon

        ier = 0

        if (lenx<(lot-1)*jump+inc*(n-1)+1) then
            ier = 1
            call xerfft('COSQMB', 6)
            go to 130
        else if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('COSQMB', 8)
            go to 130
        else if (lenwrk<lot*n) then
            ier = 3
            call xerfft('COSQMB', 10)
            go to 130
        else if (.not. xercon(inc,jump,n,lot)) then
            ier = 4
            call xerfft('COSQMB', -1)
            go to 130
        end if

        lj = (lot-1)*jump + 1
        if (n-2) 100, 110, 120
100     do m = 1, lj, jump
            x(m, 1) = x(m, 1)
        end do
        return
110     ssqrt2 = 1.0_rwp/sqrt(2.0_rwp)
        do m = 1, lj, jump
            x1 = x(m, 1) + x(m, 2)
            x(m, 2) = ssqrt2*(x(m,1)-x(m,2))
            x(m, 1) = x1
        end do
        return
120     call mcsqb1(lot, jump, n, inc, x, wsave, work, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('COSQMB', -5)
        end if

130     continue
    end subroutine
