!******************************************************************************!
!                            mod_property module
!------------------------------------------------------------------------------!
!> `[[FProperty(type)]]` derived type and type-bound procedures.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_property
    use core
    use ext_character

    use mod_file,       only: FFile
    use mod_regex,      only: FRegex
    use mod_string,     only: FString
    use mod_exception,  only: FException, MESSAGE_LEVEL_WARNING, &
        MESSAGE_LEVEL_ERROR, FExceptionDescription
    use mod_expression, only: FExpression
    use mod_formattedString,     only: FFormattedString
    use mod_numericalDictionary, only: FNumericalDictionary

    use json_module

    implicit none
    private
    save

!******************************************************************************!
!> Type containing a property tree
!******************************************************************************!
    type, public :: FProperty
        private
        type(FProperty), dimension(:), pointer :: children => null()
        integer :: size = 0                   !< Number of children
        logical :: default_ = .false.         !< Default value
        character(:), allocatable :: name     !< Name of the property list
        character(:), allocatable :: key_     !< Key
        character(:), allocatable :: value_   !< Value
        character(:), allocatable :: rank_    !< Type (scalar, list or vector)
        integer :: accessed = 0               !< Number of times a property has been accessed to
    contains
        ! Comparison
        procedure, private :: compare_equal
        procedure, private :: compare_different
        generic, public :: operator(==) => compare_equal
        generic, public :: operator(/=) => compare_different

        ! Assignment
        procedure, private :: property_copy
        generic, public :: assignment(=) => property_copy

        ! Get values
        
        procedure, public :: value !< Get the value of a property (function version)

        ! Set values

        ! Test value types

        ! Set default values
        procedure, private :: setDefault_scalar_real
        procedure, private :: setDefault_scalar_string
        procedure, private :: setDefault_scalar_char
        procedure, private :: setDefault_scalar_i
        procedure, private :: setDefault_scalar_l
        generic, public :: setDefault => setDefault_scalar_real, setDefault_scalar_string, setDefault_scalar_char,  &
            setDefault_scalar_i, setDefault_scalar_l !< Get the value of a property (subroutine version)

        ! Metadata
        procedure, public :: isSet       !< Check whether a property is defined
        procedure, public :: isTrue
        procedure, public :: isFalse
        procedure, public :: isList
        procedure, public :: listSize
        procedure, public :: isStructure
        procedure, public :: structureSize
        procedure, public :: isVector
        procedure, public :: isMatrix

        ! String interpolation
        procedure, private :: interpolate_character
        procedure, private :: interpolate_string
        generic, public :: interpolate => interpolate_character, interpolate_string

        ! Descriptions
        procedure, public :: string      !< Get a short description of the object
        procedure, public :: print       !< Print the property tree on standard output
        procedure, public :: prettyPrint !< Print the property tree on standard output

        ! File I/O
        procedure, public :: saveToJson      !< Save the property tree to a JSON file
        procedure, public :: readJson        !< Load properties from a JSON file
        procedure, public :: saveToFile      !< Save the property tree to a property file
        procedure, public :: readFile        !< Load properties from a property file
        procedure, public :: readCommandLine !< Load properties from the command line

        ! Helper procedures for internal use
        procedure, private :: addChild
        procedure, public  :: merge
        procedure, public  :: clear
        procedure, private :: setDefaultStatus
        procedure, private :: defineStructure
        procedure, private :: defineVector

        ! Destructor
        final :: property_finalise

        procedure, public :: getDictionary
        
        
        include 'generated_accessors_bindings.inc'
        include 'generated_getter_generic_binding.inc'
        include 'generated_setter_generic_binding.inc'
        
    end type

!******************************************************************************!
!> Property constructor.
!******************************************************************************!
    interface FProperty
        module procedure property_init, property_initWithFile
    end interface

!******************************************************************************!
!> Property error description.
!******************************************************************************!
    type, public, extends(FExceptionDescription) :: FPropertyError
        private
        character(:), allocatable :: filename
        integer :: line
        character(:), allocatable :: content
    end type

!******************************************************************************!
!> Property error constructor.
!******************************************************************************!
    interface FPropertyError
        module procedure :: propertyError_initWithFile
    end interface

    character(*), parameter, public :: RANK_SCALAR  = "scalar"
    character(*), parameter, public :: RANK_VECTOR  = "vector"
    character(*), parameter, public :: RANK_LIST    = "list"

    character(1), dimension(*), parameter :: vectortag = ["x", "y", "z", "t"]

    ! Paramters when resizing the children array of a property
    integer, parameter, private :: INITIAL_SIZE   = 10
    integer, parameter, private :: SIZE_INCREMENT = 10

    integer, parameter :: TOKEN_LEFT_CURLY_BRACE = 11
    integer, parameter :: TOKEN_RIGHT_CURLY_BRACE = 12
    integer, parameter :: TOKEN_LEFT_SQUARE_BRACKET = 13
    integer, parameter :: TOKEN_RIGHT_SQUARE_BRACKET = 14
    integer, parameter :: TOKEN_EQUALS = 15
    integer, parameter :: TOKEN_COMMA = 16
    integer, parameter :: TOKEN_LEFT_PAREN = 17
    integer, parameter :: TOKEN_RIGHT_PAREN = 18
    integer, parameter :: TOKEN_COMMENT = 19
    integer, parameter :: TOKEN_IDENTIFIER = 20
    integer, parameter :: TOKEN_NUMBER_REAL = 21
    integer, parameter :: TOKEN_NUMBER_INTEGER = 22
    integer, parameter :: TOKEN_STRING = 23
    integer, parameter :: TOKEN_VARIABLE = 24
    integer, parameter :: TOKEN_LINE_BREAK = 25
    integer, parameter :: TOKEN_WHITESPACE = 26
    integer, parameter :: TOKEN_FUNCTION = 28
    integer, parameter :: TOKEN_QUOTE = 29

    integer, parameter :: MODE_INITIAL    = 0
    integer, parameter :: MODE_NAME       = 1
    integer, parameter :: MODE_DEFINITION = 2
    integer, parameter :: MODE_VALIDATION = 3
    integer, parameter :: MODE_STRUCT     = 4
    integer, parameter :: MODE_VECTOR     = 5
    integer, parameter :: MODE_STRUCT_DEFINITION = 6
    integer, parameter :: MODE_QUOTED_STRING = 7

contains
    
    include 'property/generated_functions.inc'
    
    ! Include JSON I/O procedures
    include 'property/property_json.inc'

    ! Include property files I/O procedures
    include 'property/property_io.inc'

    ! Include property error
    include 'property/property_error.inc'

!******************************************************************************!
!> Get a dictionary with values from the properties.
!******************************************************************************!
    recursive subroutine getDictionary(this, dict, root)
        class(FProperty), intent(in), target :: this
        type(FNumericalDictionary), intent(out) :: dict
        character(*), intent(in), optional :: root
!------
        real(e) :: x
        integer :: iostat, i
!------
        do i=1, this%size
            if (this%children(i)%size == 0) then
                read(this%children(i)%value_,*,iostat=iostat) x
                if (iostat==0) then
                    if (present(root)) then
                        call dict%set(root//"-"//this%name//"-"//this%children(i)%name, x)
                    else
                        call dict%set(this%name//"-"//this%children(i)%name, x)
                    end if
                end if
            else
                if (present(root)) then
                    call getDictionary(this%children(i), dict, this%name)
                else
                    call getDictionary(this%children(i), dict, root//"-"//this%name)
                end if
            end if
        end do
    end subroutine
!******************************************************************************!
!> Copy a `[[FProperty(type)]]` objects.
!******************************************************************************!
    recursive subroutine property_copy(this, from)
        class(FProperty), intent(out) :: this
        type(FProperty), intent(in) :: from
!------
        integer :: i
!------
        this%size = from%size
        this%default_ = from%default_
        if (allocated(from%name)) this%name = from%name
        if (allocated(from%key_)) this%key_ = from%key_
        if (allocated(from%value_)) this%value_ = from%value_
        if (allocated(from%rank_)) this%rank_ = from%rank_
        this%accessed = 0

        if (this%size > 0) then
            allocate(this%children(this%size))
            do i=1, this%size
                this%children(i) = from%children(i)
            end do
        end if
    end subroutine
!******************************************************************************!
!> Destructor for `[[FProperty(type)]]` objects
!******************************************************************************!
    recursive subroutine property_finalise(this)
        type(FProperty), intent(inout) :: this

        !FIXME: to be enabled when possible to avoid memory leak, bug with gfortran
!        if (associated(this%children)) deallocate(this%children)
    end subroutine
!******************************************************************************!
!> Create a `[[FProperty(type)]]` object representing a scalar value
!******************************************************************************!
    function property_init(key, value) result(this)
        character(*), intent(in) :: key
        character(*), intent(in), optional :: value
        type(FProperty) :: this
!------
        this%key_ = key
        if (present(value)) this%value_ = value
        this%rank_ = RANK_SCALAR
    end function
!******************************************************************************!
!> Create a `[[FProperty(type)]]` object with the content of a JSON file
!******************************************************************************!
    function property_initWithFile(file, format, default, name) result(this)
        type(FFile), intent(inout) :: file
        character(*), intent(in), optional :: format
        logical, intent(in), optional :: default
        character(*), intent(in), optional :: name
        type(FProperty) :: this
!------

        ! Read the file (for now only Json files are supported)
        call this%readJson(file)

        ! The root property is always a list
        this%rank_ = RANK_LIST

        ! Set the property list as default values if requested
        if (present(default)) then
            if (default) call this%setDefaultStatus(.true.)
        end if

        ! Set the root property name if needed
        if (present(name)) this%name = name
    end function
!******************************************************************************!
!> Return a simple description of a `[[FProperty(type)]]` object
!******************************************************************************!
    pure function string(this) result(out)
        class(FProperty), intent(in) :: this
        character(:), allocatable :: out
!------
        if (allocated(this%name)) then
            out = "Un-named property"
        else
            out = this%name
        end if
    end function

    subroutine print(this)
        class(FProperty), intent(in) :: this
!------
        integer :: i
!------
        do i=1, this%size
            call print_internal(this%children(i), this%children(i)%key_)
        end do
    contains
        recursive subroutine print_internal(this, prefix)
            class(FProperty), intent(in) :: this
            character(*), intent(in) :: prefix
!------
            integer :: i
            character(:), allocatable :: childString
!------
            if (associated(this%children)) then
                do i=1, size(this%children)
                    if (prefix /= "") then
                        if (this%rank_ == RANK_LIST .and. this%children(i)%key_=="") then
                            call print_internal(this%children(i), prefix//"-"//i)
                        else if (this%rank_ == RANK_VECTOR .and. this%children(i)%key_=="" .and. i<size(vectortag)) then
                            call print_internal(this%children(i), prefix//"-"//vectortag(i))
                        else
                            call print_internal(this%children(i), prefix//"-"//this%children(i)%key_)
                        end if
                    else
                        if (this%rank_ == RANK_LIST .and. this%key_=="") then
                            call print_internal(this%children(i), ""//i)
                        else if (this%rank_ == RANK_VECTOR .and. this%key_=="" .and. i<size(vectortag)) then
                            call print_internal(this%children(i), ""//vectortag(i))
                        else
                            call print_internal(this%children(i), this%key_)
                        end if
                    end if
                end do
            end if
            if (this%value_ /= "") then
                if (prefix /= "") then
                    write(*,'(a)') prefix//" = "//this%value_
                else
                    write(*,'(a)') this%key_//" = "//this%value_
                end if
            end if
        end subroutine
    end subroutine
!******************************************************************************!
!> Print a representation fo a property list on standard output
!******************************************************************************!
    subroutine prettyPrint(this)
        class(FProperty), intent(in) :: this
!------
        character(:), allocatable :: open, close
        integer :: i
!------
        ! Get the correct enclosing symbol pair
        if (allocated(this%rank_)) then
            if (this%rank_ == RANK_VECTOR) then
                open = "["
                close = "]"
            else
                open = "{"
                close = "}"
            end if
        else
            open = ""
            close = ""
        end if

       if (this%size > 0) then

            ! Print the key and opening symbol
            if (allocated(this%key_)) then
                if (this%key_ /= "") then
                    write(*,'(a)') this%key_//" = "//open
                end if
            end if

            ! Print the contents
            do i=1, this%size
                if (.not.allocated(this%key_)) then
                    call prettyPrint_internal(this%children(i), 0)
                else if (this%key_ /= "") then
                    call prettyPrint_internal(this%children(i), 1)
                else
                    call prettyPrint_internal(this%children(i), 0)
                end if
            end do

            ! Print the closing symbol
            if (allocated(this%key_)) then
                if (this%key_ /= "") then
                    write(*,'(a)') close
                end if
            end if

        else
            if (allocated(this%value_)) then
                ! The property is a scalar
                if (index(this%value_, "{") /= 0 .or. index(this%value_, "}") /= 0 .or. &
                    index(this%value_, "[") /= 0 .or. index(this%value_, "]") /= 0 .or. &
                    index(this%value_, ",") /= 0 .or. index(this%value_, "=") /= 0) then

                    write(*,'(a)') this%key_//" = """//this%value_//""""
                else
                    write(*,'(a)') this%key_//" = "//this%value_
                end if
            end if
        end if
    contains
        recursive subroutine prettyPrint_internal(this, level)
            class(FProperty), intent(in) :: this
            integer, value :: level
!------
            integer :: i
            character(:), allocatable :: format
            character(:), allocatable :: nameString, childString
            character(1) :: open, close
!------
            if (level > 0) then
                format = '(' // level*4 // 'x,a)'
            else
                format = '(a)'
            end if

            ! Get the colorised name string
            if (this%key_ /= "") then
                if (this%default_) then
                    nameString = FFormattedString("{"//this%key_//"}{yellow}")
                else if (this%accessed > 0) then
                    nameString = FFormattedString("{"//this%key_//"}{green}")
                else
                    nameString = FFormattedString("{"//this%key_//"}{blue}")
                end if
            end if

            ! Get the correct enclosing symbol pair
            if (this%rank_ == RANK_VECTOR) then
                open = "["
                close = "]"
            else
                open = "{"
                close = "}"
            end if

            if (this%size > 0) then
                if (allocated(this%key_)) then
                    if (this%key_ /= "") then
                        write(*,format) nameString//" = "//open
                    else
                        write(*,format) open
                    end if
                else
                    write(*,format) open
                end if
            else
                if (index(this%value_, "{") /= 0 .or. index(this%value_, "}") /= 0 .or. &
                    index(this%value_, "[") /= 0 .or. index(this%value_, "]") /= 0 .or. &
                    index(this%value_, ",") /= 0 .or. index(this%value_, "=") /= 0) then

                    write(*,format) nameString//" = """//this%value_//""""
                else
                    write(*,format) nameString//" = "//this%value_
                end if
            end if

            if (this%size > 0) then

                do i=1, this%size
                    if (this%children(i)%size > 0) then
                        call prettyPrint_internal(this%children(i), level+1)
                    else
                        if (this%children(i)%key_ /= "") then
                            if (this%children(i)%default_) then
                                childString = FFormattedString("{"//this%children(i)%key_//"}{yellow}")
                            else if (this%children(i)%accessed > 0) then
                                childString = FFormattedString("{"//this%children(i)%key_//"}{green}")
                            else
                                childString = FFormattedString("{"//this%children(i)%key_//"}{blue}")
                            end if
                        end if
                        if (i < this%size) then
                            if (this%rank_ == RANK_VECTOR) then
                                if (index(this%children(i)%value_, "{") /= 0 .or. index(this%children(i)%value_, "}") /= 0 .or. &
                                    index(this%children(i)%value_, "[") /= 0 .or. index(this%children(i)%value_, "]") /= 0 .or. &
                                    index(this%children(i)%value_, ",") /= 0 .or. index(this%children(i)%value_, "=") /= 0) then

                                    write(*,format) "    """//this%children(i)%value_//""","
                                else
                                    write(*,format) "    "//this%children(i)%value_//","
                                end if
                            else
                                if (index(this%children(i)%value_, "{") /= 0 .or. index(this%children(i)%value_, "}") /= 0 .or. &
                                    index(this%children(i)%value_, "[") /= 0 .or. index(this%children(i)%value_, "]") /= 0 .or. &
                                    index(this%children(i)%value_, ",") /= 0 .or. index(this%children(i)%value_, "=") /= 0) then

                                    write(*,format) "    "//childString//" = """//this%children(i)%value_//""","
                                else
                                    write(*,format) "    "//childString//" = "//this%children(i)%value_//","
                                end if
                            end if
                        else
                            if (this%rank_ == RANK_VECTOR) then
                                if (index(this%children(i)%value_, "{") /= 0 .or. index(this%children(i)%value_, "}") /= 0 .or. &
                                    index(this%children(i)%value_, "[") /= 0 .or. index(this%children(i)%value_, "]") /= 0 .or. &
                                    index(this%children(i)%value_, ",") /= 0 .or. index(this%children(i)%value_, "=") /= 0) then

                                    write(*,format) "    """//this%children(i)%value_//""""
                                else
                                    write(*,format) "    "//this%children(i)%value_//""
                                end if
                            else
                                if (index(this%children(i)%value_, "{") /= 0 .or. index(this%children(i)%value_, "}") /= 0 .or. &
                                    index(this%children(i)%value_, "[") /= 0 .or. index(this%children(i)%value_, "]") /= 0 .or. &
                                    index(this%children(i)%value_, ",") /= 0 .or. index(this%children(i)%value_, "=") /= 0) then

                                    write(*,format) "    "//childString//" = """//this%children(i)%value_//""""
                                else
                                    write(*,format) "    "//childString//" = "//this%children(i)%value_//""
                                end if
                            end if
                        end if
                    end if
                end do
            end if

            if (this%size > 0) then
                if (level > 0) then
                    write(*,format) close//","
                else
                    write(*,format) close
                end if
            end if
        end subroutine
    end subroutine
!******************************************************************************!
!>
!******************************************************************************!
    subroutine defineVector(this, key)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
!------
        call setValue_internal(this, key, YES, rank=RANK_VECTOR)
    end subroutine
!******************************************************************************!
!>
!******************************************************************************!
    subroutine defineStructure(this, key)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
!------
        call setValue_internal(this, key, YES, rank=RANK_LIST)
    end subroutine
!******************************************************************************!
!> Set a double-precision real default value for a property
!******************************************************************************!
    subroutine setDefault_scalar_real(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        real(dp), intent(in) :: value
!------
        call setValue_internal(this, key, ""//value, default=.true.)
    end subroutine
!******************************************************************************!
!> Set a integer default value for a property
!******************************************************************************!
    subroutine setDefault_scalar_i(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        integer, intent(in) :: value
!------
        call setValue_internal(this, key, ""//value, default=.true.)
    end subroutine
!******************************************************************************!
!> Set a logical default value for a property
!******************************************************************************!
    subroutine setDefault_scalar_l(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        logical, intent(in) :: value
!------
        call setValue_internal(this, key, ""//value, default=.true.)
    end subroutine
!******************************************************************************!
!> Set a string default value for a property
!******************************************************************************!
    subroutine setDefault_scalar_string(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        class(FString), intent(in) :: value
!------
        call setValue_internal(this, key, value%string(), default=.true.)
    end subroutine
!******************************************************************************!
!> Set a character default value for a property
!******************************************************************************!
    subroutine setDefault_scalar_char(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        character(*), intent(in) :: value
!------
        call setValue_internal(this, key, value, default=.true.)
    end subroutine
!******************************************************************************!
!> Set a property with a double-precision real value
!******************************************************************************!
    subroutine setValue_scalar_real(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        real(dp), intent(in) :: value
!------
        call setValue_internal(this, key, ""//value)
    end subroutine
!******************************************************************************!
!> Set a property with a integer value
!******************************************************************************!
    subroutine setValue_scalar_integer(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        integer, intent(in) :: value
!------
        call setValue_internal(this, key, ""//value)
    end subroutine
!******************************************************************************!
!> Set a property with a logical value
!******************************************************************************!
    subroutine setValue_scalar_logical(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        logical, intent(in) :: value
!------
        call setValue_internal(this, key, ""//value)
    end subroutine
!******************************************************************************!
!> Set a property with a string value
!******************************************************************************!
    subroutine setValue_scalar_string(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        class(FString), intent(in) :: value
!------
        call setValue_internal(this, key, value%string())
    end subroutine
!******************************************************************************!
!> Set a property with a character value
!******************************************************************************!
    subroutine setValue_scalar_character(this, key, value)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        character(*), intent(in) :: value
!------
        call setValue_internal(this, key, value)
    end subroutine
!******************************************************************************!
!> Set a property value, for internal use only
!******************************************************************************!
    recursive subroutine setValue_internal(this, key, value, default, rank)
        class(FProperty), intent(inout) :: this
        character(*), intent(in) :: key
        character(*), intent(in) :: value
        logical, intent(in), optional :: default
        character(*), intent(in), optional :: rank
!------
        integer :: i, a
        logical :: default_
!------
        body: block

            ! By default, the value is not the default one
            if (present(default)) then
                default_ = default
            else
                default_ = .false.
            end if

            i = index(key, "-")
            if (i == 0) then

                ! Check whether there is already a child property with the right key
                do a=1, this%size
                    if (this%children(a)%key_ == key) then
                        this%children(a)%value_ = value
                        this%children(a)%default_ = default_
                        if (present(rank)) this%children(a)%rank_ = rank
                        exit body
                    end if
                end do

                ! If not, add it
                call this%addChild(FProperty(key, value))
                this%children(this%size)%default_ = default_
                if (present(rank)) this%children(this%size)%rank_ = rank
            else

                ! Check whether there is already a child property with the first part of the key
                do a=1, this%size
                    if (this%children(a)%key_ == key(:i-1)) then
                        if (present(rank)) then
                            call setValue_internal(this%children(a), key(i+1:), value, default_, rank)
                        else
                            call setValue_internal(this%children(a), key(i+1:), value, default_)
                        end if
                        exit body
                    end if
                end do

                ! If not, add it
                call this%addChild(FProperty(key(:i-1)))
                if (present(rank)) this%children(this%size)%rank_ = rank
                do a=1, this%size
                    if (this%children(a)%key_ == key(:i-1)) then
                        if (present(rank)) then
                            call setValue_internal(this%children(a), key(i+1:), value, default_, rank)
                        else
                            call setValue_internal(this%children(a), key(i+1:), value, default_)
                        end if
                        exit body
                    end if
                end do
            end if
        end block body
    end subroutine
!******************************************************************************!
!> Check whether a property is set.
!******************************************************************************!
    function isSet(this, key)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: key
        logical :: isSet
!------
        character(:), allocatable :: v
!------
        call getValue_scalar_character(this, key, v)
        isSet = (v /= UNDEF)
    end function
!******************************************************************************!
!> Check whether a property is true.
!******************************************************************************!
    function isTrue(this, name)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        logical :: isTrue
!------
        logical :: t
        type(FException) :: err_
!------
        call getValue_scalar_logical(this, name, t, err_)

        if (err_ /= 0) then
            isTrue = .false.
            call err_%discard
        else
            isTrue = t
        end if
    end function
!******************************************************************************!
!> Check whether a property is false.
!******************************************************************************!
    function isFalse(this, name)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        logical :: isFalse
!------
        isFalse = .not.isTrue(this, name)
    end function
!******************************************************************************!
!> Check whether a property is a valid list.
!******************************************************************************!
    function isList(this, name)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        logical :: isList
!------
        integer :: i
        type(FProperty), pointer :: p
!------
        isList = .false.
        call getPointerToProperty(this, name, p)
        if (associated(p)) then
            if (p%rank_ == RANK_LIST) then
                do i=1, p%size
                    if (p%children(i)%key_ /= "" // i) return
                end do
                isList = .true.
            end if
        end if
    end function
!******************************************************************************!
!> Get the size of a list property.
!******************************************************************************!
    function listSize(this, name)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        integer :: listSize
!------
        integer :: i
        type(FProperty), pointer :: p
!------
        listSize = 0
        call getPointerToProperty(this, name, p)
        if (associated(p)) then
            if (p%rank_ == RANK_LIST) then
                do i=1, p%size
                    if (p%children(i)%key_ /= "" // i) return
                end do
                listSize = p%size
            end if
        end if
    end function
!******************************************************************************!
!> Check whether a property is a valid vector.
!******************************************************************************!
    function isVector(this, name)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        logical :: isVector
!------
        type(FProperty), pointer :: p
!------
        isVector = .false.
        call getPointerToProperty(this, name, p)
        if (associated(p)) isVector = (p%rank_ == RANK_VECTOR)
    end function
!******************************************************************************!
!> Check whether a property is a valid structure.
!******************************************************************************!
    function isStructure(this, name)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        logical :: isStructure
!------
        type(FProperty), pointer :: p
!------
        isStructure = .false.
        call getPointerToProperty(this, name, p)
        if (associated(p)) isStructure = (p%rank_ == RANK_LIST)
    end function
!******************************************************************************!
!> Check whether a property is a valid matrix.
!******************************************************************************!
    function isMatrix(this, name) result(test)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        logical :: test
!------
        type(FProperty), pointer :: property
        integer :: i
!------
        call getPointerToProperty(this, name, property)
        test = .false.
        if (associated(property)) then
            if (property%rank_ /= RANK_VECTOR) return

            do i=1, property%size
                if (property%children(i)%rank_ /= RANK_VECTOR) return
            end do

            test = .true.
        end if
    end function
!******************************************************************************!
!> Get the size of a structure property.
!******************************************************************************!
    function structureSize(this, name)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        integer :: structureSize

        type(FProperty), pointer :: p
!------
        structureSize = 0
        call getPointerToProperty(this, name, p)
        if (associated(p)) then
            if (p%rank_ == RANK_LIST) structureSize = p%size
        end if
    end function
!******************************************************************************!
!> Check whether a value is a valid string object.
!******************************************************************************!
    function isSDefinition(value) result(test)
        character(*), intent(in) :: value
        logical :: test
!------
        test = .true.
    end function
!******************************************************************************!
!> Check whether a value is a valid character scalar.
!******************************************************************************!
    function isCDefinition(value) result(test)
        character(*), intent(in) :: value
        logical :: test
!------
        test = .true.
    end function
!******************************************************************************!
!> Check whether a value is a valid real number.
!******************************************************************************!
    function isRDefinition(value) result(test)
        character(*), intent(in) :: value
        logical :: test
!------
        integer :: iostat
        real(e) :: v
!------
        read(value,*,iostat=iostat) v
        test = (iostat == 0)
    end function
!******************************************************************************!
!> Check whether a value is a valid logical value.
!******************************************************************************!
    function isLDefinition(value) result(test)
        character(*), intent(in) :: value
        logical :: test
!------
        integer :: iostat
        type(FRegex) :: yre, nre
        logical :: v
!------
        read(value,*,iostat=iostat) v
        if (iostat == 0) then
            test = .true.
        else
            yre = FRegex("^\s*[yY][eE][sS]")
            nre = FRegex("^\s*[nN][oO]")
            if (yre%doesMatch(value) .or. nre%doesMatch(value)) then
                test = .true.
            else
                test = .false.
            end if
        end if
    end function
!******************************************************************************!
!> Check whether a value is a valid integer number.
!******************************************************************************!
    function isIDefinition(value) result(test)
        character(*), intent(in) :: value
        logical :: test
!------
        integer :: iostat
        integer :: v
!------
        read(value,*,iostat=iostat) v
        test = (iostat == 0)
    end function
!******************************************************************************!
!> Get the value of a property as a standard-precision scalar.
!******************************************************************************!
    subroutine getValue_scalar_real(this, name, value, err)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        real(dp), intent(out) :: value
        type(FException), intent(out), optional :: err
!------
        character(:), allocatable :: v
        integer :: iostat
        type(FException) :: err_
!------
        body: block
            ! Get the string value of the property
            v = this%value(name)

            ! Check that the property is set
            if (v == UNDEF) then
                call err_%raise("The property "//name//" is not set", MESSAGE_LEVEL_WARNING)
                exit body
            end if

            ! Convert the value
            read(v,*,iostat=iostat) value

            ! Check that the conversion worked
            if (iostat /= 0) then
                call err_%raise("The property "//name//" is not a valid double-precision real", MESSAGE_LEVEL_WARNING)
                exit body
            end if

            ! Increment the access counter
!            this%accessed = this%accessed + 1
        end block body

        ! Report any issue if needed
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the value of a property as an integer scalar.
!******************************************************************************!
    subroutine getValue_scalar_integer(this, name, value, err)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        integer, intent(out) :: value
        type(FException), intent(out), optional :: err
!------
        character(:), allocatable :: v
        integer :: iostat
        type(FException) :: err_
!------
        body: block
            ! Get the string value of the property
            v = this%value(name)

            ! Check that the property is sets
            if (v == UNDEF) then
                call err_%raise("The property "//name//" is not set", MESSAGE_LEVEL_WARNING)
                exit body
            end if

            ! Convert the value
            read(v,*,iostat=iostat) value

            ! Check that the conversion worked
            if (iostat /= 0) then
                call err_%raise("The property "//name//" is not a valid integer", MESSAGE_LEVEL_WARNING)
                exit body
            end if

            ! Increment the access counter
!            this%accessed = this%accessed + 1
        end block body

        ! Report any issue if needed
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the value of a property as a logical scalar.
!******************************************************************************!
    subroutine getValue_scalar_logical(this, name, value, err)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        logical, intent(out) :: value
        type(FException), intent(out), optional :: err
!------
        character(:), allocatable :: v
        integer :: iostat
        type(FException) :: err_
        type(FRegex) :: yre, nre
!------
        body: block
            ! Get the string value of the property
            v = this%value(name)

            ! Check that the property is sets
            if (v == UNDEF) then
                call err_%raise("The property "//name//" is not set", MESSAGE_LEVEL_WARNING)
                exit body
            end if

            ! Convert the value
            read(v,*,iostat=iostat) value

            ! Check that the conversion worked
            if (iostat /= 0) then
                yre = FRegex("^\s*[yY][eE][sS]")
                nre = FRegex("^\s*[nN][oO]")
                if (yre%doesMatch(v)) then
                    value = .true.
                else if (nre%doesMatch(v)) then
                    value = .false.
                else
                    call err_%raise("The property "//name//" is not a valid logical", MESSAGE_LEVEL_WARNING)
                    exit body
                end if
            end if

            ! Increment the access counter
!            this%accessed = this%accessed + 1
        end block body

        ! Report any issue if needed
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the value of a property as a character scalar.
!******************************************************************************!
    subroutine getValue_scalar_character(this, key, val)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: key
        character(:), allocatable, intent(out) :: val
!------
!------
        body: block
            ! Get the un-interpolated value
            call value_internal(this, key, val)

            ! Interpolate the result string
            call interpolate_character(this, val)
        end block body
    end subroutine
!******************************************************************************!
!> Get the value of a property as a `[[FString(type)]]` scalar.
!******************************************************************************!
    subroutine getValue_scalar_string(this, name, value, err)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: name
        type(FString), intent(out) :: value
        type(FException), intent(out), optional :: err
!------
        value = FString(this%value(name))
    end subroutine
!******************************************************************************!
!> Apply substitutions in a string object.
!******************************************************************************!
    subroutine interpolate_string(this, string)
        class(FProperty), intent(in) :: this
        type(FString), intent(inout) :: string
!------
        character(:), allocatable :: val
!------
        val = string
        call interpolate_character(this, val)
        string = val
    end subroutine
!******************************************************************************!
!> Apply substitutions in a character variable.
!******************************************************************************!
    subroutine interpolate_character(this, string)
        class(FProperty), intent(in) :: this
        character(:), allocatable, intent(inout) :: string
!------
        character(:), allocatable :: val
        type(FRegex) :: var1Regex, var2Regex, exp1Regex, exp2Regex
        character(:), allocatable :: iv, name, tmp
        integer :: i!, length
        logical :: changed
        type(FExpression) :: exp
        character(:), allocatable :: substring
!------

        body: block
            ! Work on a local copy of the input string
            val = string

            ! Do not try interpolating if there is no $ sign in the val
            if (index(val, "$") == 0 .and. index(val, "((") == 0) then
                exit body
            end if

            ! Setup the regular expressions to detect variables and expressions
            var1Regex = FRegex("\$\{[a-zA-Z][a-zA-Z_\-0-9]*\}")
            var2Regex = FRegex("\$[a-zA-Z][a-zA-Z_\-0-9]*")

            exp1Regex = FRegex("\$(\(([^()$]|(?1))*\))")
            exp2Regex = FRegex("\((\(([^()$]|(?1))*\))\)")

!------ Process variables
            do
                changed = .false.

                ! Detect ${} variables
                if (var1Regex%doesMatch(val)) then
                    call var1Regex%match(val, i, substring)

                    name = val(i+2:i+len(substring)-2)
                    call value_internal(this, name, iv)
                    if (iv /= UNDEF) then
                        tmp = val(:i-1) // iv // val(i+len(substring):)
                    else
                        tmp = val(:i-1) // val(i+len(substring):)
                    end if
                    val = tmp
                    changed = .true.
                end if

                ! Detect $ variables
                if (var2Regex%doesMatch(val)) then
                    call var2Regex%match(val, i, substring)

                    name = val(i+1:i+len(substring)-1)
                    call value_internal(this, name, iv)
                    if (iv /= UNDEF) then
                        tmp = val(:i-1) // iv // val(i+len(substring):)
                    else
                        tmp = val(:i-1) // val(i+len(substring):)
                    end if
                    val = tmp
                    changed = .true.
                end if

                if (.not.changed) exit
            end do

!------ Process expressions
            do
                changed = .false.

                ! Detect $() expressions
                if (exp1Regex%doesMatch(val)) then
                    call exp1Regex%match(val, i, substring)

                    ! Get a FExpression obect
                    exp = FExpression(val(i+2:i+len(substring)-2))

                    tmp = val(:i-1) // exp%evalf() // val(i+len(substring):)
                    val = tmp
                    changed = .true.
                end if

                ! Detect (()) expressions
                if (exp2Regex%doesMatch(val)) then
                    call exp2Regex%match(val, i, substring)

                    ! Get a FExpression obect
                    exp = FExpression(val(i+2:i+len(substring)-3))

                    tmp = val(:i-1) // exp%evalf() // val(i+len(substring):)
                    val = tmp
                    changed = .true.
                end if
                if (.not.changed) exit
            end do
            
            string = val
        end block body
    end subroutine
!******************************************************************************!
!> Merge a `[[FProperty(type)]]` into another.
!******************************************************************************!
    recursive subroutine merge(this, from)
        class(FProperty), intent(inout) :: this
        type(FProperty), intent(in) :: from
!------
        integer :: i, j
!------
        this%value_ = from%value_
        this%rank_ = from%rank_

        iloop: do i=1, from%size
            do j=1, from%size
                if (this%children(i)%key_ == from%children(j)%key_) then
                    call this%children(i)%merge(from%children(j))
                    cycle iloop
                end if
            end do

            ! At this point, from%children(i) is not defined in this
            call this%addChild(from%children(i))
        end do iloop
    end subroutine
!******************************************************************************!
!> Merge a `[[FProperty(type)]]` into another.
!******************************************************************************!
    recursive subroutine clear(this)
        class(FProperty), intent(inout) :: this
!------
        integer :: i
!------
        if (associated(this%children)) then
            do i=1, size(this%children)
                call this%children(i)%clear
            end do
            deallocate(this%children)
        end if
        this = FProperty()
    end subroutine
!******************************************************************************!
!> Add a child to a `[[FProperty(type)]]` object.
!******************************************************************************!
    subroutine addChild(this, node)
        class(FProperty), intent(inout) :: this
        type(FProperty), intent(in) :: node
!------
        type(FProperty), dimension(:), pointer :: tmp
!------

        if (.not.associated(this%children)) then
            ! Allocate the children array if needed
            allocate(this%children(INITIAL_SIZE))
        else
            ! Resize the children array if needed
            if (this%size < size(this%children)) then
                tmp => this%children
                allocate(this%children(size(tmp) + SIZE_INCREMENT))
                this%children(:size(tmp)) = tmp(:)
                deallocate(tmp)
            end if
        end if

        ! Incrment the number of children
        this%size = this%size + 1

        ! Set the new children
        this%children(this%size) = node
    end subroutine
!******************************************************************************!
!> Get the value of a `[[FProperty(type)]]` object.
!******************************************************************************!
    function value(this, key) result(val)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: key
        character(:), allocatable :: val
!------
        call this%get(key, val)
    end function
!******************************************************************************!
!> Get the value of a `[[FProperty(type)]]` object.
!******************************************************************************!
    recursive subroutine value_internal(this, key, value)
        class(FProperty), intent(in) :: this
        character(*), intent(in) :: key
        character(:), allocatable, intent(out) :: value
!------
        integer :: i, a
!------
        value = UNDEF
        i = index(key, "-")

        if (i == 0) then
            do a=1, this%size
                if (this%children(a)%key_ == key) then
                    value = this%children(a)%value_
                    exit
                end if
            end do
        else
            do a=1, this%size
                if (this%children(a)%key_ == key(:i-1)) then
                    call value_internal(this%children(a), (key(i+1:)), value)
                    exit
                end if
            end do
        end if
    end subroutine
!******************************************************************************!
!> Find a property with a given name.
!******************************************************************************!
    recursive subroutine getPointerToProperty(this, key, ptr)
        class(FProperty), target, intent(in) :: this
        character(*), intent(in) :: key
        type(FProperty), pointer, intent(out) :: ptr
!------
        integer :: i, a
!------
        ptr => null()
        i = index(key, "-")

        if (i == 0) then
            do a=1, this%size
                if (this%children(a)%key_ == key) then
                    ptr => this%children(a)
                    exit
                end if
            end do
        else
            do a=1, this%size
                if (this%children(a)%key_ == key(:i-1)) then
                    call getPointerToProperty(this%children(a), (key(i+1:)), ptr)
                    if (associated(ptr)) return
                    exit
                end if
            end do
        end if
    end subroutine
!******************************************************************************!
!> Set the default status.
!******************************************************************************!
    recursive subroutine setDefaultStatus(this, status)
        class(FProperty), intent(inout) :: this
        logical, intent(in) :: status
!------
        integer :: i
!------
        this%default_ = status
        if (this%size > 0) then
            do i=1, this%size
                call this%children(i)%setDefaultStatus(status)
            end do
        end if
    end subroutine
!******************************************************************************!
!> Compare two property lists.
!******************************************************************************!
    recursive function compare_equal(this, to) result(identical)
        class(FProperty), intent(in) :: this
        class(FProperty), intent(in) :: to
        logical :: identical
!------
        integer :: i
!------
        identical = .true.
        if (allocated(this%key_) .neqv. allocated(to%key_)) then
            identical = .false.
            return
        end if
        if (allocated(this%value_) .neqv. allocated(to%value_)) then
            identical = .false.
            return
        end if
        if (this%size /= to%size) then
            identical = .false.
            return
        end if
        if (allocated(this%rank_) .neqv. allocated(to%rank_)) then
            identical = .false.
            return
        end if
        if (allocated(this%rank_)) then
            if (this%rank_ /= to%rank_) then
                identical = .false.
                return
            end if
        end if


        if (allocated(this%key_) .and. allocated(this%value_)) then
            if (this%key_ /= to%key_ .or. this%value_ /= to%value_) then
                identical = .false.
            else
                do i=1, this%size
                    identical = (this%children(i) == to%children(i))
                    if (.not. identical) then
                        exit
                    end if
                end do
            end if
        else
            do i=1, this%size
                identical = (this%children(i) == to%children(i))
                if (.not. identical) then
                    exit
                end if
            end do
        end if
    end function
!******************************************************************************!
!> Compare two property lists.
!******************************************************************************!
    function compare_different(this, to) result(different)
        class(FProperty), intent(in) :: this
        class(FProperty), intent(in) :: to
        logical :: different
!------
!------
        different = .not.compare_equal(this, to)
    end function
end module
