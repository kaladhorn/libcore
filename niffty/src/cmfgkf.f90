    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cmfgkf(lot, ido, ip, l1, lid, na, cc, cc1, im1, in1, ch, ch1, &
      im2, in2, wa)
        include 'kinds.inc'
        real (rwp) ch(2, in2, l1, ido, ip), cc(2, in1, l1, ip, ido), &
          cc1(2, in1, lid, ip), ch1(2, in2, lid, ip), wa(ido, ip-1, 2)

        ! FFTPACK 5.0 auxiliary routine

        m1d = (lot-1)*im1 + 1
        m2s = 1 - im2
        ipp2 = ip + 2
        ipph = (ip+1)/2
        do ki = 1, lid
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch1(1, m2, ki, 1) = cc1(1, m1, ki, 1)
                ch1(2, m2, ki, 1) = cc1(2, m1, ki, 1)
            end do
        end do
        do j = 2, ipph
            jc = ipp2 - j
            do ki = 1, lid
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch1(1, m2, ki, j) = cc1(1, m1, ki, j) + cc1(1, m1, ki, jc)
                    ch1(1, m2, ki, jc) = cc1(1, m1, ki, j) - &
                      cc1(1, m1, ki, jc)
                    ch1(2, m2, ki, j) = cc1(2, m1, ki, j) + cc1(2, m1, ki, jc)
                    ch1(2, m2, ki, jc) = cc1(2, m1, ki, j) - &
                      cc1(2, m1, ki, jc)
                end do
            end do
        end do
        do j = 2, ipph
            do ki = 1, lid
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    cc1(1, m1, ki, 1) = cc1(1, m1, ki, 1) + ch1(1, m2, ki, j)
                    cc1(2, m1, ki, 1) = cc1(2, m1, ki, 1) + ch1(2, m2, ki, j)
                end do
            end do
        end do
        do l = 2, ipph
            lc = ipp2 - l
            do ki = 1, lid
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    cc1(1, m1, ki, l) = ch1(1, m2, ki, 1) + &
                      wa(1, l-1, 1)*ch1(1, m2, ki, 2)
                    cc1(1, m1, ki, lc) = -wa(1, l-1, 2)*ch1(1, m2, ki, ip)
                    cc1(2, m1, ki, l) = ch1(2, m2, ki, 1) + &
                      wa(1, l-1, 1)*ch1(2, m2, ki, 2)
                    cc1(2, m1, ki, lc) = -wa(1, l-1, 2)*ch1(2, m2, ki, ip)
                end do
            end do
            do j = 3, ipph
                jc = ipp2 - j
                idlj = mod((l-1)*(j-1), ip)
                war = wa(1, idlj, 1)
                wai = -wa(1, idlj, 2)
                do ki = 1, lid
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        cc1(1, m1, ki, l) = cc1(1, m1, ki, l) + &
                          war*ch1(1, m2, ki, j)
                        cc1(1, m1, ki, lc) = cc1(1, m1, ki, lc) + &
                          wai*ch1(1, m2, ki, jc)
                        cc1(2, m1, ki, l) = cc1(2, m1, ki, l) + &
                          war*ch1(2, m2, ki, j)
                        cc1(2, m1, ki, lc) = cc1(2, m1, ki, lc) + &
                          wai*ch1(2, m2, ki, jc)
                    end do
                end do
            end do
        end do
        if (ido>1) go to 110
        sn = 1.0_rwp/real(ip*l1, rwp)
        if (na==1) go to 100
        do ki = 1, lid
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                cc1(1, m1, ki, 1) = sn*cc1(1, m1, ki, 1)
                cc1(2, m1, ki, 1) = sn*cc1(2, m1, ki, 1)
            end do
        end do
        do j = 2, ipph
            jc = ipp2 - j
            do ki = 1, lid
                do m1 = 1, m1d, im1
                    chold1 = sn*(cc1(1,m1,ki,j)-cc1(2,m1,ki,jc))
                    chold2 = sn*(cc1(1,m1,ki,j)+cc1(2,m1,ki,jc))
                    cc1(1, m1, ki, j) = chold1
                    cc1(2, m1, ki, jc) = sn*(cc1(2,m1,ki,j)-cc1(1,m1,ki,jc))
                    cc1(2, m1, ki, j) = sn*(cc1(2,m1,ki,j)+cc1(1,m1,ki,jc))
                    cc1(1, m1, ki, jc) = chold2
                end do
            end do
        end do
        return
100     do ki = 1, lid
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch1(1, m2, ki, 1) = sn*cc1(1, m1, ki, 1)
                ch1(2, m2, ki, 1) = sn*cc1(2, m1, ki, 1)
            end do
        end do
        do j = 2, ipph
            jc = ipp2 - j
            do ki = 1, lid
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch1(1, m2, ki, j) = sn*(cc1(1,m1,ki,j)-cc1(2,m1,ki,jc))
                    ch1(2, m2, ki, j) = sn*(cc1(2,m1,ki,j)+cc1(1,m1,ki,jc))
                    ch1(1, m2, ki, jc) = sn*(cc1(1,m1,ki,j)+cc1(2,m1,ki,jc))
                    ch1(2, m2, ki, jc) = sn*(cc1(2,m1,ki,j)-cc1(1,m1,ki,jc))
                end do
            end do
        end do
        return
110     do ki = 1, lid
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch1(1, m2, ki, 1) = cc1(1, m1, ki, 1)
                ch1(2, m2, ki, 1) = cc1(2, m1, ki, 1)
            end do
        end do
        do j = 2, ipph
            jc = ipp2 - j
            do ki = 1, lid
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch1(1, m2, ki, j) = cc1(1, m1, ki, j) - cc1(2, m1, ki, jc)
                    ch1(2, m2, ki, j) = cc1(2, m1, ki, j) + cc1(1, m1, ki, jc)
                    ch1(1, m2, ki, jc) = cc1(1, m1, ki, j) + &
                      cc1(2, m1, ki, jc)
                    ch1(2, m2, ki, jc) = cc1(2, m1, ki, j) - &
                      cc1(1, m1, ki, jc)
                end do
            end do
        end do
        do i = 1, ido
            do k = 1, l1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    cc(1, m1, k, 1, i) = ch(1, m2, k, i, 1)
                    cc(2, m1, k, 1, i) = ch(2, m2, k, i, 1)
                end do
            end do
        end do
        do j = 2, ip
            do k = 1, l1
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    cc(1, m1, k, j, 1) = ch(1, m2, k, 1, j)
                    cc(2, m1, k, j, 1) = ch(2, m2, k, 1, j)
                end do
            end do
        end do
        do j = 2, ip
            do i = 2, ido
                do k = 1, l1
                    m2 = m2s
                    do m1 = 1, m1d, im1
                        m2 = m2 + im2
                        cc(1, m1, k, j, i) = wa(i, j-1, 1)* &
                          ch(1, m2, k, i, j) + wa(i, j-1, 2)*ch(2, m2, k, i, j &
                          )
                        cc(2, m1, k, j, i) = wa(i, j-1, 1)* &
                          ch(2, m2, k, i, j) - wa(i, j-1, 2)*ch(1, m2, k, i, j &
                          )
                    end do
                end do
            end do
        end do
    end subroutine
