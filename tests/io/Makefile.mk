#******************************************************************************#
#                            libCore io tests                              #
#______________________________________________________________________________#
#
#  Version 2.0
#
#  Written by Paul Fossati, <paul.fossati@gmail.com>
#  Copyright (c) 2009-2016 Paul Fossati
#------------------------------------------------------------------------------#
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
#     * Redistributions of source code must retain the above copyright notice, #
#       this list of conditions and the following disclaimer.                  #
#                                                                              #
#     * Redistributions in binary form must reproduce the above copyright      #
#       notice, this list of conditions and the following disclaimer in the    #
#       documentation and/or other materials provided with the distribution.   #
#                                                                              #
#     * The name of the author may not be used to endorse or promote products  #
#      derived from this software without specific prior written permission    #
#      from the author.                                                        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#******************************************************************************#
check_PROGRAMS += test_directory test_file test_gzipFileReader                \
    test_bzip2FileReader

#******************************************************************************!
# Sources and dependencies
#******************************************************************************!
test_directory_SOURCES= io/test_directory.f90
io/test_directory.o: ../src/mod_directory.mod io/test_directory.f90 directory
directory: directory.tbz2
	tar -xjf $(top_srcdir)/tests/directory.tbz2

test_file_SOURCES= io/test_file.f90
io/test_file.o: ../src/mod_file.mod io/test_file.f90 file
file: file.tbz2
	tar -xjf $(top_srcdir)/tests/file.tbz2

test_gzipFileReader_SOURCES= io/test_gzipFileReader.f90
io/test_gzipFileReader.o: io/test_gzipFileReader.f90 gzipFileReader
gzipFileReader: gzipFileReader.tbz2
	tar -xjf $(top_srcdir)/tests/gzipFileReader.tbz2

test_bzip2FileReader_SOURCES= io/test_bzip2FileReader.f90
io/test_bzip2FileReader.o: bzip2FileReader io/test_bzip2FileReader.f90 \
	../src/io/mod_bzip2FileReader.o
bzip2FileReader: bzip2FileReader.tbz2
	tar -xjf $(top_srcdir)/tests/bzip2FileReader.tbz2
