module test_function
    use core
    use mod_test
    use ext_character
    
    use mod_function,   only: FFunction
    use mod_exception,  only: FException
    use mod_expression, only: FExpression
    use mod_numericalDictionary, only: FNumericalDictionary
    
    implicit none
    private
    
    public :: testAll
contains
    subroutine testAll()
        call testBasic
        call testErrors
        call testIO
    end subroutine
    
    subroutine testBasic
        type(FFunction), target :: f, g
        type(FExpression) :: i
        type(FNumericalDictionary) :: arg
        logical :: identical
        type(FException) :: stat
        real(e) :: y
        
        call startTestGroup("basic functions")
        ! Get a basic function
        f = FFunction("sin(x)", "x", err=stat)
        call check(stat == 0, "creating sin(x)", stat%string())
        
        ! Check a couple of values
        call f%eval(0.0_e, y)
        call check(y == 0.0_e, "evaluating sin(0)", "wrong result: "//y)
        call f%eval(PI, y)
        call check(y .approx. 0.0_e, "evaluating sin(pi)", "wrong result: "//y)
        
        ! Get a function with parameters
        call arg%set("n", 2.0_e)
        f = FFunction("sin(n*x)", "x", arg, err=stat)
        call check(stat == 0, "creating sin(n*x)", stat%string())
        
        ! Check a couple of values
        f = FFunction("sin(n*x)", "x", arg, err=stat)
        call f%eval(0.0_e, y)
        call check(y == 0.0_e, "evaluating sin(2*0)", "wrong result: "//y)
        call f%eval(PI/4, y)
        call check(y == 1.0_e, "evaluating sin(2*pi/4)", "wrong result: "//y)
        
        ! Test the evalf function
        f = FFunction("sin(n*x)", "x", arg, err=stat)
        y = f%evalf(PI/4)
        call check(y == 1.0_e, "evaluating sin(2*pi/4)", "wrong result: "//y)
        f = FFunction("42", "x", arg, err=stat)
        y = f%evalf(PI/4)
        call check(y == 42.0_e, "evaluating 42", "wrong result: "//y)
    end subroutine
    
    subroutine testErrors
        type(FFunction) :: f
        type(FException) :: err
        type(FNumericalDictionary) :: arg
        
        call startTestGroup("error conditions")
        
        ! Try creating a parameterised function without a parameter dictionary
        f = FFunction("2*n*x", "x", err=err)
        call check(err /= 0, "missing parameter dictionary", "failed to detect error")
        call err%discard
        
        ! Try creating a parameterised function with a parameter dictionary that 
        ! does not have a value for the parameter
        f = FFunction("2*n*x", "x", arg, err=err)
        call check(err /= 0, "missing parameter", "failed to detect error")
        call err%discard
        
        ! Syntax error
        f = FFunction("sin(x", "x", err=err)
        call check(err /= 0, "syntax error", "failed to detect error")
        call err%discard
    end subroutine
    
    subroutine testIO
        type(FFunction) :: f
        character(:), allocatable :: str
        
        call startTestGroup("input/output")
        f = FFunction("(2/r^8) * (1+erf(20*(r-1.5)))/2", "r")
        str = f%string()
        call check(str == "2/r**8*(1+erf(20*(r-1.5)))/2", &
            "representation of (2/r^8) * (1+erf(20*(r-1.5)))/2", "wrong string: "//str)
        call f%print
    end subroutine
end module

program test
    use test_function
    use mod_test
    
    call testAll
    call testsFinished
end program