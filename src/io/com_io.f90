!******************************************************************************!
!                            com_io module
!------------------------------------------------------------------------------!
!> Low-level I/O related procedures and interfaces to POSIX C libraries.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module com_io
    use iso_c_binding

    use ext_character, only: c_f_string

    implicit none
    private
    save

!******************************************************************************!
! Bindings to standard POSIX file functions.
!******************************************************************************!
    interface
        function fopen(name, mode) bind(C, name="fopen")
            use iso_c_binding
            type(c_ptr) :: fopen
            character(1,c_char), dimension(*) :: name
            character(1,c_char), dimension(*) :: mode
        end function

        function fgets(str, n, stream) bind(C, name="fgets")
            use iso_c_binding
            type(c_ptr) :: fgets
            character(c_char), dimension(*), intent(out) :: str
            integer(c_int), intent(in), value :: n
            type(c_ptr), value :: stream
        end function

        function fputs(str, stream) bind(C, name="fputs")
            use iso_c_binding
            integer(c_int) :: fputs
            character(1,c_char), dimension(*), intent(in) :: str
            type(c_ptr), value :: stream
        end function

        function fclose(stream) bind(C, name="fclose")
            use iso_c_binding
            integer(c_int) :: fclose
            type(c_ptr), value :: stream
        end function

        function mktemp(template) bind(C, name="mktemp")
            use iso_c_binding
            character(c_char), dimension(*), intent(inout) :: template
            type(c_ptr) :: mktemp
        end function

        function ftell(stream) bind(C, name="ftell")
            use iso_c_binding
            integer(c_int) :: ftell
            type(c_ptr), value :: stream
        end function

        function fseek(stream, offset, whence) bind(C, name="fseek")
            use iso_c_binding
            integer(c_int) :: fseek
            type(c_ptr), value :: stream
            integer(c_long), value :: offset
            integer(c_int), value :: whence
        end function

        function countLines(stream) bind(C, name="countLines")
            use iso_c_binding
            integer(c_int) :: countLines
            type(c_ptr), value :: stream
        end function

        function feof(stream) bind(C, name="feof")
            use iso_c_binding
            integer(c_int) :: feof
            type(c_ptr), value :: stream
        end function
    end interface
    public :: fgets
    public :: fopen
    public :: fclose
    public :: fputs
    public :: feof
    public :: fseek
    public :: ftell
    public :: mktemp
    public :: countLines
!******************************************************************************!
! Interfaces to the wrapper functions in mod_directory_wrapper.c
!******************************************************************************!
    interface
        function strerror(code) bind(C, name="strerror")
            use iso_c_binding
            type(c_ptr) :: strerror
            integer(c_int), value :: code
        end function
        function errno() bind(C, name="errno_wrapper")
            use iso_c_binding
            integer(c_int) :: errno
        end function
        function mkdir(path) bind(C, name="mkdir_wrapper")
            use iso_c_binding
            character(1,c_char), dimension(*), intent(in) :: path
            integer(c_int) :: mkdir
        end function
        function rmdir(path) bind(C, name="rmdir_wrapper")
            use iso_c_binding
            character(1,c_char), dimension(*), intent(in) :: path
            integer(c_int) :: rmdir
        end function
        function closedir(dirp) bind(C, name="closedir_wrapper")
            use iso_c_binding
            type(c_ptr), value :: dirp
            integer(c_int) :: closedir
        end function
        function opendir(dirname) bind(C, name="opendir_wrapper")
            use iso_c_binding
            character(1,c_char), dimension(*), intent(in) :: dirname
            type(c_ptr) :: opendir
        end function
        function readdir(dirp, filename, maxlength, filetype) bind(C, name="readdir_wrapper")
            use iso_c_binding
            type(c_ptr), value :: dirp
            character(1,c_char), dimension(*), intent(out) :: filename
            integer(c_int), value :: maxlength
            integer(c_int), intent(out) :: filetype
            integer(c_int) :: readdir
        end function
    end interface

    public :: strerror
    public :: opendir
    public :: errno
    public :: mkdir
    public :: closedir
    public :: rmdir
    public :: readdir
    public :: ioErrorMessage
contains
!******************************************************************************!
!> Get the message corresponding to the latest error condition from the C
!> library.
!******************************************************************************!
    function ioErrorMessage() result(message)
        character(:), allocatable :: message
!------
        type(c_ptr) :: str
        integer(c_int) :: errnum
!------
        errnum = errno()
        if (errnum == 0) then
            message = ""
        else
            str = strerror(errno())
            call c_f_string(str, message)
        end if
    end function

end module