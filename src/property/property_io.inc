!******************************************************************************!
!                              property_io                                     !
!______________________________________________________________________________!
!> I/O procedure for property files. Part of the `[[mod_property(module)]]`
!> module.
!
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2015 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!


!******************************************************************************!
!> Create a `FProperty` object from the command line
!******************************************************************************!
    subroutine readCommandLine(this, stat)
        class(FProperty), intent(out), target :: this
        type(FException), intent(out), optional :: stat
!------
        type(FRegex), dimension(:), allocatable :: re
        integer, dimension(:), allocatable :: id, pos
        integer :: i, index, n, j, cursor
        character(2000) :: buffer
        character(:), allocatable :: line, symbol, previous, value, tmp, oline
        integer :: level
        type(FString), dimension(:), allocatable :: nameStack
        integer, dimension(:), allocatable :: modeStack
        type(FRegex) :: tabRegex
        type(FException) :: istat, pstat
!------
        body: block
            ! Setup the regular expressions for the parser
            allocate(re(18))
            allocate(id(18))
            re(1) = FRegex("^\s*$", "")
            id(1) = TOKEN_WHITESPACE
            re(2) = FRegex("^\!(?<symbol>.*)$", "")
            id(2) = TOKEN_COMMENT
            re(3) = FRegex("^\#(?<symbol>.*)$", "")
            id(3) = TOKEN_COMMENT
            re(4) = FRegex("^(?<![\\\""])(?<symbol>[\""])(?!\"")")
            id(4) = TOKEN_QUOTE
            re(5) = FRegex("^(?<![\\\'])(?<symbol>[\'])(?!\')")
            id(5) = TOKEN_QUOTE
            re(6) = FRegex("^(?<![\\\$])(?<symbol>\{)", "")
            id(6) = TOKEN_LEFT_CURLY_BRACE
            re(7) = FRegex("^(?<!\\)(?<symbol>\})", "")
            id(7) = TOKEN_RIGHT_CURLY_BRACE
            re(8) = FRegex("^(?<!\\)(?<symbol>\[)", "")
            id(8) = TOKEN_LEFT_SQUARE_BRACKET
            re(9) = FRegex("^(?<!\\)(?<symbol>\])", "")
            id(9) = TOKEN_RIGHT_SQUARE_BRACKET
            re(10) = FRegex("^(?<!\\)(?<symbol>\,)", "")
            id(10) = TOKEN_COMMA
            re(11) = FRegex("^(?<!\\)(?<symbol>=)", "")
            id(11) = TOKEN_EQUALS
            re(12) = FRegex("^(?<symbol>[a-zA-Z\-0-9][a-zA-Z0-9_\-]*)\b", "")
            id(12) = TOKEN_IDENTIFIER
            re(13) = FRegex("^(?<symbol>\$(\{[a-zA-Z\-0-9][a-zA-Z0-9_-]*\}|[a-zA-Z][a-zA-S0-9_-]*))", "")
            id(13) = TOKEN_STRING
            re(14) = FRegex("^\b(?<symbol>[0-9]+\.[0-9]*([eE][+\-]?[0-9]+)?|\.[0-9]+([eE][+-]?[0-9]+)?)\b", "")
            id(14) = TOKEN_STRING
            re(15) = FRegex("^\b(?<symbol>[0-9]+)\b", "")
            id(15) = TOKEN_STRING
            re(16) = FRegex("^(?<symbol>\\[\{\}\[\]\=\,])", "")
            id(16) = TOKEN_STRING
            re(17) = FRegex("^(?<symbol>\s+)", "")
            id(17) = TOKEN_WHITESPACE
            re(18) = FRegex("^(?<symbol>[^\!\#\s\;\{\}\[\]\,]*)", "")
            id(18) = TOKEN_STRING

            ! Regex to replace tabs with spaces
            tabRegex = FRegex("(\t)", "    ")

            n = command_argument_count()

            level = 1
            allocate(modeStack(100))
            allocate(nameStack(100))
            modeStack(level) = MODE_INITIAL
            value = ""

            do j=1, n
                call get_command_argument(j, buffer)
                if (len_trim(buffer) == 0) exit

                ! Ignore strings that do not start with -
                if (buffer(1:1) /= "-") cycle

                ! Remove the leading -
                line = trim(buffer(2:))
                oline = line
                cursor = 1

                ! Turn tabs into spaces
                call tabRegex%replace_all(line, pos, stat=istat)
                if (istat == "No match found.") call istat%discard

                do while (line /= "")
                    ! Try to consume all the tokens in the buffer
                    do i=1, size(re)
                        if (re(i)%doesMatch(line)) then
                             previous = line
                             call re(i)%match(line, index)
                             call re(i)%capture(line, "symbol", symbol)
                             call parse(this, modeStack, nameStack, value, level, symbol, id(i), pstat)

                             ! Handle parse errors
                             if (pstat /= 0) then
                                 call istat%raise(FPropertyError(&
                                     filename="standard input", &
                                     linenumber=j, &
                                     position=cursor, &
                                     context=oline, &
                                     message=pstat%string()))
                                 call pstat%discard
                                 exit body
                             end if

                             cursor = cursor + len(symbol)
                             tmp = line(:index-1)//line(index+len(symbol):)
                             line = tmp
                             exit
                        end if
                    end do
                end do
                call parse(this, modeStack, nameStack, value, level, "<EOL>", TOKEN_LINE_BREAK, pstat)

                ! Handle parse errors
                if (pstat /= 0) then
                    call istat%raise(FPropertyError(&
                        filename="standard input", &
                        linenumber=j, &
                        position=cursor, &
                        context=oline, &
                        message=pstat%string()))
                    call pstat%discard
                    exit body
                end if

            end do
        end block body

        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Read properties from a property file
!******************************************************************************!
    subroutine readFile(this, file, default, stat)
        class(FProperty), intent(out), target :: this
        type(FFile), intent(inout) :: file
        logical, intent(in), optional :: default
        type(FException), intent(out), optional :: stat
!------
        type(FRegex), dimension(:), allocatable :: re
        type(FRegex) :: tabRegex
        integer, dimension(:), allocatable :: id, pos
        character(:), allocatable :: line, symbol, previous, value, tmp, oline
        integer :: i, j, index, cursor
        type(FString), dimension(:), allocatable :: nameStack
        integer, dimension(:), allocatable :: modeStack
        integer :: level
        type(FString), dimension(:), allocatable :: lines
        type(FException) :: istat, pstat
!------
        body: block
            ! Setup the regular expressions for the parser
            allocate(re(18))
            allocate(id(18))
            re(1) = FRegex("^\s*$", "")
            id(1) = TOKEN_WHITESPACE
            re(2) = FRegex("^\!(?<symbol>.*)$", "")
            id(2) = TOKEN_COMMENT
            re(3) = FRegex("^\#(?<symbol>.*)$", "")
            id(3) = TOKEN_COMMENT
            re(4) = FRegex("^(?<![\\\""])(?<symbol>[\""])(?!\"")")
            id(4) = TOKEN_QUOTE
            re(5) = FRegex("^(?<![\\\'])(?<symbol>[\'])(?!\')")
            id(5) = TOKEN_QUOTE
            re(6) = FRegex("^(?<![\\\$])(?<symbol>\{)", "")
            id(6) = TOKEN_LEFT_CURLY_BRACE
            re(7) = FRegex("^(?<!\\)(?<symbol>\})", "")
            id(7) = TOKEN_RIGHT_CURLY_BRACE
            re(8) = FRegex("^(?<!\\)(?<symbol>\[)", "")
            id(8) = TOKEN_LEFT_SQUARE_BRACKET
            re(9) = FRegex("^(?<!\\)(?<symbol>\])", "")
            id(9) = TOKEN_RIGHT_SQUARE_BRACKET
            re(10) = FRegex("^(?<!\\)(?<symbol>\,)", "")
            id(10) = TOKEN_COMMA
            re(11) = FRegex("^(?<!\\)(?<symbol>=)", "")
            id(11) = TOKEN_EQUALS
            re(12) = FRegex("^(?<symbol>[a-zA-Z\-0-9][a-zA-Z0-9_\-]*)\b", "")
            id(12) = TOKEN_IDENTIFIER
            re(13) = FRegex("^(?<symbol>\$(\{[a-zA-Z\-0-9][a-zA-Z0-9_-]*\}|[a-zA-Z][a-zA-S0-9_-]*))", "")
            id(13) = TOKEN_STRING
            re(14) = FRegex("^\b(?<symbol>[0-9]+\.[0-9]*([eE][+\-]?[0-9]+)?|\.[0-9]+([eE][+-]?[0-9]+)?)\b", "")
            id(14) = TOKEN_STRING
            re(15) = FRegex("^\b(?<symbol>[0-9]+)\b", "")
            id(15) = TOKEN_STRING
            re(16) = FRegex("^(?<symbol>\\[\{\}\[\]\=\,])", "")
            id(16) = TOKEN_STRING
            re(17) = FRegex("^(?<symbol>\s+)", "")
            id(17) = TOKEN_WHITESPACE
            re(18) = FRegex("^(?<symbol>[^\!\#\s\;\{\}\[\]\,""']*)", "")
            id(18) = TOKEN_STRING

            ! Regex to replace tabs with spaces
            tabRegex = FRegex("(\t)", "    ")

            call file%getLines(lines)
            j = 0
            level = 1
            allocate(modeStack(100))
            allocate(nameStack(100))
            modeStack(level) = MODE_INITIAL
            value = ""
            do j=1, size(lines)
                line = lines(j)%string()
                oline = line
                cursor = 1

                call tabRegex%replace_all(line, pos, stat=istat)
                if (istat == "No match found.") call istat%discard

                do while (line /= "")
                    ! Try to consume all the tokens in the buffer
                    do i=1, size(re)
                        if (re(i)%doesMatch(line)) then
                         previous = line
                         call re(i)%match(line, index)
                         call re(i)%capture(line, "symbol", symbol)
                         call parse(this, modeStack, nameStack, value, level, symbol, id(i), pstat)

                         ! Handle parse errors
                         if (pstat /= 0) then
                             call istat%raise(FPropertyError(&
                                 filename=file%fullName(), &
                                 linenumber=j, &
                                 position=cursor, &
                                 context=oline, &
                                 message=pstat%string()))
                             call pstat%discard
                             exit body
                         end if

                         cursor = cursor + len(symbol)
                         tmp = line(:index-1)//line(index+len(symbol):)
                         line = tmp
                         exit
                        end if
                    end do
                end do

                call parse(this, modeStack, nameStack, value, level, "<EOL>", TOKEN_LINE_BREAK, pstat)

                ! Handle parse errors
                if (pstat /= 0) then
                    call istat%raise(FPropertyError(&
                        filename=file%fullName(), &
                        linenumber=j, &
                        position=cursor, &
                        context=oline, &
                        message=pstat%string()))
                    call pstat%discard
                    exit body
                end if

            end do

            ! Make sure all the structures and vectors are finished properly
            if (level > 1) then; block
                character(:), allocatable :: name

                ! Get the name of the last vector or structure
                name = nameStack(level)%string()
                do i=level, 1, -1
                    name = nameStack(i)%string() // "-" // name
                end do

                select case(modeStack(level))
                    case (MODE_VECTOR)
                        call istat%raise(FPropertyError(&
                            filename=file%fullName(), &
                            linenumber=j, &
                            position=cursor, &
                            context=oline, &
                            message="Incomplete vector definition."))
                    case default
                        call istat%raise(FPropertyError(&
                            filename=file%fullName(), &
                            linenumber=j, &
                            position=cursor, &
                            context=oline, &
                            message="Incomplete structure definition."))
                end select
            end block; end if

            if (present(default)) &
                call this%setDefaultStatus(default)
        end block body

        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Low-level parsing procedure
!******************************************************************************!
    subroutine parse(root, modeStack, nameStack, value, level, symbol, tokenType, stat)
        type(FProperty), intent(inout) :: root
        integer, dimension(:), allocatable, intent(inout) :: modeStack
        type(FString), dimension(:), allocatable, intent(inout) :: nameStack
        character(:), allocatable, intent(inout) :: value
        integer, intent(inout) :: level
        character(*), intent(in) :: symbol
        integer, intent(in) :: tokenType
        type(FException), intent(out) :: stat
!------
        type(FProperty), pointer :: p
!------
        ! Ignore the comments in any case
        if (tokenType == TOKEN_COMMENT) return

        select case (modeStack(level))

            ! We are at the root level
            case (MODE_INITIAL)
                select case (tokenType)
                    case (TOKEN_LINE_BREAK, TOKEN_WHITESPACE)
                        ! Ignore the line breaks and white space outside property definitions
                    case (TOKEN_IDENTIFIER)
                        ! Identifiers start property definitions
                        level = level + 1
                        modeStack(level) = MODE_NAME
                        nameStack(level) = symbol
                    case default
                        ! Everything else is a syntax error
                        call stat%raise("Expected a property name")
                        return
                end select

            ! We are reading a property name
            case (MODE_NAME)
                select case (tokenType)
                    case (TOKEN_WHITESPACE)
                        ! Ignore white space outside property definitions
                    case (TOKEN_LINE_BREAK)
                        ! No definition implies implicit boolean property
                        if (level == 2) then
                            ! Validate the property if its name is valid
                            if (nameStack(level) /= UNDEF) then
                                call root%set(nameStack(level)%string(), YES)
                            end if
                            ! Go down one level
                            modeStack(level) = 0
                            nameStack(level) = ""
                            level = level - 1
                        end if
                    case (TOKEN_EQUALS)
                        ! A = sign indicates the begining of a definition
                        modeStack(level) = MODE_DEFINITION
                    case (TOKEN_COMMA)

                    case default
                        ! Everything else is a syntax error
                        call stat%raise("Bad symbol in property name")
                        nameStack(level) = UNDEF
                        return
                end select

            ! We are reading a string value
            case (MODE_QUOTED_STRING)

                select case(tokenType)
                    case (TOKEN_LINE_BREAK)
                        ! Error: there is an end of line before the closing quote
                        call stat%raise("Unterminated character string")
                        return

                    case (TOKEN_QUOTE)

                        ! Go down one level
                        modeStack(level) = 0
                        nameStack(level) = ""
                        level = level - 1
                        value = ""

                    case default
                        value = value // symbol
                end select

            ! We are reading a property definition
            case (MODE_DEFINITION)
                select case (tokenType)
                    case (TOKEN_WHITESPACE)
                        ! Append the white space only if it is not at the beginning of the definition
                        if (value /= "") value = value // symbol

                    case (TOKEN_QUOTE)
                        if (value /= "") then
                            ! Error: there should be nothing before opening quotes
                            call stat%raise("Unexpected quotes")
                            return
                        else
                            modeStack(level) = MODE_QUOTED_STRING
                        end if

                    case (TOKEN_IDENTIFIER, TOKEN_STRING)
                        ! Append successive strings to the property definition
                        value = value // symbol

                    case (TOKEN_COMMA)
                        ! In structures and vectors, comma separate components
                        if (level > 2) then
                            if (nameStack(level) /= UNDEF) then
                                call root%set(join(nameStack(2:level), "-"), trim(value))
                            end if
                            ! Go down one level
                            modeStack(level) = 0
                            nameStack(level) = ""
                            level = level - 1
                            value = ""
                        else
                            ! Otherwise, this is a syntax error
                            call stat%raise("Invalid character in property value")
                            return
                        end if

                    case (TOKEN_LINE_BREAK)
                        ! Line breaks outside structures or arrays indicate the end of the definition
                        if (level == 2) then
                            if (value == "") then
                                ! A line break after the = is invalid
                                call stat%raise("Expected a property value")
                                return
                            else if (nameStack(level) /= UNDEF) then
                                call root%set(nameStack(level)%string(), trim(value))
                            end if
                            ! Go down one level
                            modeStack(level) = 0
                            nameStack(level) = ""
                            level = level - 1
                            value = ""
                        end if

                    case (TOKEN_LEFT_CURLY_BRACE)
                        ! A { sign indicates the begining of a structure
                        modeStack(level) = MODE_STRUCT

                    case (TOKEN_LEFT_SQUARE_BRACKET)
                        ! A [ sign indicates the begining of a vector
                        modeStack(level) = MODE_VECTOR
                        nameStack(level+1) = "x"

                    case (TOKEN_RIGHT_CURLY_BRACE)
                        ! A } sign indicates the end of a structure
                        if (level > 2 .and. modeStack(level-1) == MODE_STRUCT) then
                            if (value /= "") then
                                ! Validate the current component if it has a value
                                call root%set(join(nameStack(2:level), "-"), trim(value))

                                ! Go down one level
                                modeStack(level) = 0
                                nameStack(level) = ""
                                level = level - 1
                                value = ""
                            end if

                            ! Validate the structure
                            call defineStructure(root, join(nameStack(2:level), "-"))
                            modeStack(level) = 0
                            nameStack(level) = ""
                            level = level - 1
                            value = ""
                        else
                            ! Otherwise, this is a syntax error
                            call stat%raise("Invalid character in property value")
                            return
                        end if

                    case default
                        ! Everything else is a syntax error
                        call stat%raise("Invalid character in property value")
                        return
                end select

            case (MODE_STRUCT)
                select case (tokenType)
                    case (TOKEN_LINE_BREAK, TOKEN_WHITESPACE)
                        ! Ignore the line breaks and white space in structure definitions

                    case (TOKEN_IDENTIFIER)
                        ! Structure component
                        level = level + 1
                        modeStack(level) = MODE_STRUCT_DEFINITION
                        nameStack(level) = symbol

                    case (TOKEN_RIGHT_CURLY_BRACE)
                        call getPointerToProperty(root, join(nameStack(2:level), "-"), p)
                        if (associated(p)) then
                            call defineStructure(root, join(nameStack(2:level), "-"))
                        else
                            call stat%raise("Empty structure definition")
                            return
                        end if
                        modeStack(level) = 0
                        nameStack(level) = ""
                        level = level - 1
                        value = ""

                    case (TOKEN_COMMA)
                        ! Ignore superfluous commas in structure definitions

                    case (TOKEN_LEFT_CURLY_BRACE)
                        ! The component is an anonymous sub-structure

                        ! Get a pointer to the current structure
                        call getPointerToProperty(root, join(nameStack(2:level), "-"), p)
                        if (associated(p)) then
                            nameStack(level+1) = "" // (p%size + 1)
                        else
                            nameStack(level+1) = "1"
                        end if
                        call root%set(join(nameStack(2:level+1), "-"), trim(value))
                        value = ""
                        level = level + 1
                        modeStack(level) = MODE_STRUCT

                    case default
                        call stat%raise("Invalid symbol in structure definition")
                        return
                end select

            case (MODE_STRUCT_DEFINITION)
                select case (tokenType)
                    case (TOKEN_WHITESPACE, TOKEN_LINE_BREAK)
                        ! Ignore white space

                    case (TOKEN_EQUALS)
                        ! A = sign indicates the begining of a definition
                        modeStack(level) = MODE_DEFINITION

                    case (TOKEN_COMMA)
                        if (nameStack(level) /= "") then
                            value = nameStack(level)
                            call getPointerToProperty(root, join(nameStack(2:level-1), "-"), p)
                            if (associated(p)) then
                                nameStack(level) = "" // (p%size + 1)
                            else
                                nameStack(level) = "1"
                            end if
                            call root%set(join(nameStack(2:level), "-"), trim(value))
                            value = ""
                            nameStack(level) = ""
                            modeStack(level) = 0
                            level = level - 1
                        end if

                    case (TOKEN_RIGHT_CURLY_BRACE)
                        if (nameStack(level) /= "") then
                            value = nameStack(level)
                            call getPointerToProperty(root, join(nameStack(2:level-1), "-"), p)
                            if (associated(p)) then
                                nameStack(level) = "" // (p%size + 1)
                            else
                                nameStack(level) = "1"
                            end if
                            call root%set(join(nameStack(2:level), "-"), trim(value))
                            value = ""
                            nameStack(level) = ""
                            modeStack(level) = 0
                            level = level - 1
                        end if
                        if (root%isSet(join(nameStack(2:level), "-"))) then
                            call defineStructure(root, join(nameStack(2:level), "-"))
                        else
                            call stat%raise("Empty structure definition")
                            nameStack(level) = UNDEF
                            return
                        end if
                        modeStack(level) = 0
                        nameStack(level) = ""
                        level = level - 1
                        value = ""

                    case default
                        ! Everything else is a syntax error
                        call stat%raise("Bad symbol in property name")
                        nameStack(level) = UNDEF
                        return
                end select


            case (MODE_VECTOR)
                select case (tokenType)
                    case (TOKEN_LINE_BREAK, TOKEN_WHITESPACE)
                        ! Ignore the line breaks and white space in structure definitions

                    case (TOKEN_IDENTIFIER, TOKEN_STRING)
                        ! Vector component
                        value = value//symbol

                    case (TOKEN_COMMA)
                        if (value /= "") then
                            call root%set(join(nameStack(2:level+1), "-"), trim(value))
                            value = ""
                        end if
                        select case (nameStack(level+1)%string())
                            case ("x")
                                nameStack(level+1) = "y"
                            case ("y")
                                nameStack(level+1) = "z"
                            case ("z")
                                nameStack(level+1) = "t"
                            case ("t")
                                ! Everything else is a syntax error
                                call stat%raise("Too many vector components")
                                return
                            case default
                                nameStack(level+1) = "x"
                        end select

                    case (TOKEN_RIGHT_SQUARE_BRACKET)
                        ! Validate the last component if defined
                        if (value /= "") then
                            call root%set(join(nameStack(2:level+1), "-"), trim(value))
                            value = ""
                            nameStack(level+1) = ""
                        end if

                        if (root%isSet(join(nameStack(2:level), "-"))) then
                            ! Validate the vector
                            call defineVector(root, join(nameStack(2:level), "-"))
                        else
                            ! Everything else is a syntax error
                            call stat%raise("Empty vector definition")
                            return
                        end if
                        modeStack(level) = 0
                        nameStack(level) = ""
                        level = level - 1
                        value = ""

                    case (TOKEN_LEFT_SQUARE_BRACKET)
                        ! A [ symbol indicates a nested vector
                        level = level + 1
                        nameStack(level+1) = "x"
                        modeStack(level) = MODE_VECTOR
                        value = ""

                    case default
                        call stat%raise("Wrong symbol in vector definition")
                        return
                end select
            case default
        end select
    end subroutine
!******************************************************************************!
!> Helper procedure
!******************************************************************************!
    function join(array, char)
        type(FString), dimension(:), intent(in) :: array
        character(*), intent(in) :: char
        character(:), allocatable :: join
!------
        integer :: i, n
!------
        if (size(array) == 1) then
            join = array(1)
            return
        end if

        n = -len(char)
        do i=1, size(array)
            n = n + array(i)%internalLength() + len(char)
        end do
        allocate(character(n) :: join)

        n = 0
        do i=1, size(array)
            join(n+1:) = array(i)%string() // char
            n = n + array(i)%internalLength() + len(char)
        end do
    end function
!******************************************************************************!
!> Print the representation of a property list in a property file
!******************************************************************************!
    subroutine saveToFile(this, file, err)
        class(FProperty), intent(in) :: this
        type(FFile), intent(inout) :: file
        type(FException), intent(out), optional :: err
!------
        character(:), allocatable :: open, close
        integer :: i
!------
        ! Get the correct enclosing symbol pair
        if (allocated(this%rank_)) then
            if (this%rank_ == RANK_VECTOR) then
                open = "["
                close = "]"
            else
                open = "{"
                close = "}"
            end if
        else
            open = ""
            close = ""
        end if

       if (this%size > 0) then

            ! Print the key and opening symbol
            if (allocated(this%key_)) then
                if (this%key_ /= "") then
                    call file%writeLine(this%key_//" = "//open)
                end if
            end if

            ! Print the contents
            do i=1, this%size
                if (.not.allocated(this%key_)) then
                    call saveToFile_internal(this%children(i), 0)
                else if (this%key_ /= "") then
                    call saveToFile_internal(this%children(i), 1)
                else
                    call saveToFile_internal(this%children(i), 0)
                end if
            end do

            ! Print the closing symbol
            if (allocated(this%key_)) then
                if (this%key_ /= "") then
                    call file%writeLine(close)
                end if
            end if

        else

            ! The property is a scalar
            if (index(this%value_, "{") /= 0 .or. index(this%value_, "}") /= 0 .or. &
                index(this%value_, "[") /= 0 .or. index(this%value_, "]") /= 0 .or. &
                index(this%value_, ",") /= 0 .or. index(this%value_, "=") /= 0) then

                call file%writeLine(this%key_//" = """//this%value_//"""")
            else
                call file%writeLine(this%key_//" = "//this%value_)
            end if
        end if

    contains
        recursive subroutine saveToFile_internal(this, level)
            class(FProperty), intent(in) :: this
            integer, value :: level
!------
            integer :: i
            character(:), allocatable :: header
            character(1) :: open, close
!------
            allocate(character(level*4) :: header)
            header(:) = " "

            ! Get the correct enclosing symbol pair
            if (this%rank_ == RANK_VECTOR) then
                open = "["
                close = "]"
            else
                open = "{"
                close = "}"
            end if

            if (this%size > 0) then
                if (allocated(this%key_)) then
                    if (this%key_ /= "") then
                        call file%writeLine(header//this%key_//" = "//open)
                    else
                        call file%writeLine(header//open)
                    end if
                else
                    call file%writeLine(header//open)
                end if
            else
                if (index(this%value_, "{") /= 0 .or. index(this%value_, "}") /= 0 .or. &
                    index(this%value_, "[") /= 0 .or. index(this%value_, "]") /= 0 .or. &
                    index(this%value_, ",") /= 0 .or. index(this%value_, "=") /= 0) then

                    call file%writeLine(header//this%key_//" = """//this%value_//"""")
                else
                    call file%writeLine(header//this%key_//" = "//this%value_)
                end if
            end if

            if (this%size > 0) then

                do i=1, this%size
                    if (this%children(i)%size > 0) then
                        call saveToFile_internal(this%children(i), level+1)
                    else
                        if (i < this%size) then
                            if (this%rank_ == RANK_VECTOR) then
                                if (index(this%children(i)%value_, "{") /= 0 .or. index(this%children(i)%value_, "}") /= 0 .or. &
                                    index(this%children(i)%value_, "[") /= 0 .or. index(this%children(i)%value_, "]") /= 0 .or. &
                                    index(this%children(i)%value_, ",") /= 0 .or. index(this%children(i)%value_, "=") /= 0) then

                                    call file%writeLine(header//"    """//this%children(i)%value_//""",")
                                else
                                    call file%writeLine(header//"    "//this%children(i)%value_//",")
                                end if
                            else
                                if (index(this%children(i)%value_, "{") /= 0 .or. index(this%children(i)%value_, "}") /= 0 .or. &
                                    index(this%children(i)%value_, "[") /= 0 .or. index(this%children(i)%value_, "]") /= 0 .or. &
                                    index(this%children(i)%value_, ",") /= 0 .or. index(this%children(i)%value_, "=") /= 0) then

                                    call file%writeLine(header//"    "//this%children(i)%key_//" = """//&
                                        this%children(i)%value_//""",")
                                else
                                    call file%writeLine(header//"    "//this%children(i)%key_//" = "//&
                                        this%children(i)%value_//",")
                                end if
                            end if
                        else
                            if (this%rank_ == RANK_VECTOR) then
                                if (index(this%children(i)%value_, "{") /= 0 .or. index(this%children(i)%value_, "}") /= 0 .or. &
                                    index(this%children(i)%value_, "[") /= 0 .or. index(this%children(i)%value_, "]") /= 0 .or. &
                                    index(this%children(i)%value_, ",") /= 0 .or. index(this%children(i)%value_, "=") /= 0) then

                                    call file%writeLine(header//"    """//this%children(i)%value_//"""")
                                else
                                    call file%writeLine(header//"    "//this%children(i)%value_//"")
                                end if
                            else
                                if (index(this%children(i)%value_, "{") /= 0 .or. index(this%children(i)%value_, "}") /= 0 .or. &
                                    index(this%children(i)%value_, "[") /= 0 .or. index(this%children(i)%value_, "]") /= 0 .or. &
                                    index(this%children(i)%value_, ",") /= 0 .or. index(this%children(i)%value_, "=") /= 0) then

                                    call file%writeLine(header//"    "//this%children(i)%key_//" = """//&
                                        this%children(i)%value_//"""")
                                else
                                    call file%writeLine(header//"    "//this%children(i)%key_//" = "//&
                                        this%children(i)%value_//"")
                                end if
                            end if
                        end if
                    end if
                end do
            end if

            if (this%size > 0) then
                if (level > 0) then
                    call file%writeLine(header//close//",")
                else
                    call file%writeLine(header//close)
                end if
            end if
        end subroutine
    end subroutine
