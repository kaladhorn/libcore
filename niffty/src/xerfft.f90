    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine xerfft(srname, info)
        implicit none
        include 'kinds.inc'

        ! .. Scalar Arguments ..
        character *6 srname
        integer info

        ! ..

        ! Purpose
        ! =======

        ! XERFFT  is an error handler for library FFTPACK version 5.1
        ! routines.
        ! It is called by an FFTPACK 5.1 routine if an input parameter has an
        ! invalid value.  A message is printed and execution stops.

        ! Installers may consider modifying the STOP statement in order to
        ! call system-specific exception-handling facilities.

        ! Arguments
        ! =========

        ! SRNAME  (input) CHARACTER*6
        ! The name of the routine which called XERFFT.

        ! INFO    (input) INTEGER
        ! When a single  invalid parameter in the parameter list of
        ! the calling routine has been detected, INFO is the position
        ! of that parameter.  In the case when an illegal combination
        ! of LOT, JUMP, N, and INC has been detected, the calling
        ! subprogram calls XERFFT with INFO = -1.

        ! ====================================================================
        !=

        ! .. Executable Statements ..

        if (info>=1) then
            write (*, '(A,A,A,I3,A)') ' ** On entry to ', srname, &
              ' parameter number ', info, ' had an illegal value'
        else if (info==-1) then
            write (*, '(A,A,A,A)') ' ** On entry to ', srname, &
              ' parameters LOT, JUMP, N and INC are inconsistent'
        else if (info==-2) then
            write (*, '(A,A,A,A)') ' ** On entry to ', srname, &
              ' parameter L is greater than LDIM'
        else if (info==-3) then
            write (*, '(A,A,A,A)') ' ** On entry to ', srname, &
              ' parameter M is greater than MDIM'
        else if (info==-5) then
            write (*, '(A,A,A,A)') ' ** Within ', srname, &
              ' input error returned by lower level routine'
        else if (info==-6) then
            write (*, '(A,A,A,A)') ' ** On entry to ', srname, &
              ' parameter LDIM is less than 2*(L/2+1)'
        end if

        stop

        ! End of XERFFT

    end subroutine
