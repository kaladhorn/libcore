!******************************************************************************!
!                          mod_expression module
!------------------------------------------------------------------------------!
!> `[[Finterpreter(type)]]` derived type and type-bound procedures, for parsing
!> and evaluation of mathematical expressions. For examples see
!> !(this documentation page)[pages/3-api/5-expressions.html].
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_expression
    use core
    use ext_character

    use mod_regex,     only: FRegex
    use mod_string,    only: FString
    use mod_bytecode,  only: FBytecode
    use mod_exception, only: FException, MESSAGE_LEVEL_WARNING, FExceptionDescription, MESSAGE_LEVEL_IPE
    use mod_numericalDictionary, only: FNumericalDictionary

    use mod_ast

    implicit none
    private
    save

    type, private :: nodePointer
        type(ast_node), pointer, private :: node => null()
    end type

    type, private :: variable
        type(nodePointer), dimension(:), allocatable :: instance
        character(:), allocatable :: name
        real(e) :: value = 0.0_e
        integer :: nInstances = 0
        logical :: constant = .false.
    end type

!******************************************************************************!
!> Interpreter object type
!******************************************************************************!
    type, public :: FExpression
        private
        type(ast_node) , allocatable :: root        !< AST root node
        character(:),    allocatable :: expression  !< Character representation of the expression
        type(variable), dimension(:), allocatable :: variable !< Variable table
        integer :: nVariables = 0 !< Number of variables in the expression

        type(FBytecode), public :: bytecode !< Bytecode representation
    contains
        procedure, public :: setConstant      !< Set the value of a constant
        procedure, public :: setConstants     !< Set the values of several constants

        ! Variable accessors
        procedure, private :: setVariable_string !< Set the value of a variable
        procedure, private :: setVariable_char   !< Set the value of a variable
        generic, public :: setVariable => setVariable_string, setVariable_char

        procedure, public :: setVariables     !< Set the values of several variables
        procedure, public :: getVariables     !< Get the names and values of the variables in the expression

        procedure, private :: setVariableTables !< Update the variable table
        procedure, public :: getVariableNames !< Get the list of variables in the expression

        procedure, public :: getDerivative !< Calculate the derivative of the expression
        procedure, public :: optimise      !< Optimise the internal AST to improve efficiency

        ! Bytecode-related procedures
        procedure, private :: generateBytecode !< Generate bytecode from the AST

        ! Evaluation (function version)
        procedure, public  :: evalf

        ! Evaluation (subroutine version)
        procedure, private :: eval_simple
        procedure, private :: eval_var
        procedure, private :: eval_vector_linear
        procedure, private :: eval_vector
        generic,   public  :: eval => eval_simple, eval_var, eval_vector, eval_vector_linear

        ! I/O-related procedures
        procedure, public :: print         !< Print the expression to standard output
        procedure, public :: string        !< Get a character representation of the expression
        procedure, private :: expression_appendString
        procedure, private, pass(this) :: string_appendExpression
        generic, public :: operator(//) => string_appendExpression, expression_appendString

        procedure, public :: simplify      !< Simplify the internal AST
        procedure, private :: interpret_f_
        procedure, private :: interpret_f_var
        generic, public :: interpret_f => interpret_f_, interpret_f_var !< Evaluate the expression
        procedure, public :: interpret       !< Evaluate the expression

        procedure, public :: setDefaultVariables !< Set common mathematical constants

        ! Comparison
        procedure, private :: equal
        generic :: operator(==) => equal !< Compare two expressions

        ! Copy
        procedure, public :: copy
        generic, public :: assignment(=) => copy

        ! Debugging procedures
        procedure, public :: printAST       !< Print the AST
        procedure, public :: printVariables !< Print the internal representation of the variables
    end type

    interface FExpression
        module procedure expression_init
    end interface



    integer, parameter :: LIST_SIZE_INCREMENT = 10
contains
    include 'math/expression_parse.inc'
    include 'math/expression_compile.inc'
    include 'math/expression_eval.inc'

    pure function expression_appendString(this, char) result(out)
        class(FExpression), intent(in) :: this
        character(*), intent(in) :: char
        character(:), allocatable :: out
!------
        out = this%string() // char
    end function

    pure function string_appendExpression(char, this) result(out)
        character(*), intent(in) :: char
        class(FExpression), intent(in) :: this
        character(:), allocatable :: out
!------
        out = this%string() // char
    end function

    subroutine copy(this, from)
        class(FExpression), intent(out), target :: this
        class(FExpression), intent(in),  target :: from
!------

        if (allocated(from%expression)) this%expression = from%expression

        if (allocated(from%root)) then

            ! Copy the AST
            allocate(this%root)
            call this%root%substitute(from%root)

            ! Re-build the variables index
            call this%setVariableTables
        end if

        ! Copy the bytecode
        this%bytecode = from%bytecode

        this%nVariables = from%nVariables
    end subroutine

!LCOV_EXCL_START
    subroutine printAST(this)
        class(FExpression), intent(in), target :: this
!------
        call this%root%printAST(0)
    end subroutine
!LCOV_EXCL_STOP

!LCOV_EXCL_START
    subroutine printVariables(this)
        class(FExpression), intent(in) :: this
!------
        integer :: i
!------
        write(*,*) "Expression: "//this%string()
        write(*,*) "Variables: "//this%nVariables
        do i=1, this%nVariables
            write(*,*) this%variable(i)%name, this%variable(i)%value, this%variable(i)%nInstances
        end do
    end subroutine
!LCOV_EXCL_STOP
!******************************************************************************!
!> Set some common variables in a `FExpression` object.
!******************************************************************************!
    subroutine setDefaultVariables(this)
        class(FExpression), intent(inout) :: this
!------
        call this%setVariable("pi", PI)
        call this%setVariable("e", EE)
    end subroutine
!******************************************************************************!
!> Compare two `FExpression` objects. The objects are identical if their string
!> representations are identical.
!******************************************************************************!
    function equal(this, to) result(similar)
        class(FExpression), intent(in) :: this
        class(FExpression), intent(in) :: to
        logical :: similar !< `.true.` if the expressions are identical, `.false.` otherwise
!------
        similar = (to%string() == this%string())
    end function
!******************************************************************************!
!> Initialise a `FExpression` object from a mathematical expression in a
!> character variable. The AST is built from the expression for future
!> evaluation.
!******************************************************************************!
    function expression_init(expression, err) result(this)
        type(FExpression), target :: this
        character(*), intent(in) :: expression     !< Character variable containing the mathematical expression
        type(FException), intent(out), optional :: err !< Error status
!------
        type(ast_node), dimension(:), allocatable :: tokenList
        integer :: status
        logical, dimension(:), allocatable :: visited
        type(FException) :: err_
!------
        body: block
            !Set the expression
            this%expression = expression
            ! Get the token list from the expression
            call tokenise(expression, tokenList, status)

            if (status /= 0) then
                call err_%raise("error while parsing expression "//expression)
                exit body
            end if

            call reverse(tokenList, err_)
            if (err_ /= 0) exit body

            ! An error happened
            if (status /= 0) then
                call err_%raise("error while parsing expression "//expression)
                exit body
            end if

            ! The expression was empty
            if (size(tokenList) == 0) then
                exit body
            end if

            ! Make sure each token is added only once to the AST
            allocate(visited(size(tokenList)))
            visited = .false.

            ! Build the AST
            allocate(this%root)
            call buildAst(tokenList, this%root, visited, size(tokenList))

            ! Set the internal representation of the variables in the AST
            call this%setVariableTables

            ! Set the value for some common constants
            call this%setDefaultVariables

            ! Calculate the bytecode
            call this%generateBytecode
        end block body

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    end function
!******************************************************************************!
!> Get a string representation of a function
!******************************************************************************!
    pure function string(this)
        class(FExpression), intent(in) :: this
!------
        character(:), allocatable :: string !< Character representation of the expression
!------
        if (allocated(this%root)) then
            call this%root%getString(string)
        else
            string = ""
        end if
    end function
!******************************************************************************!
!> Print a string representation of a function
!******************************************************************************!
    subroutine print(this)
        class(FExpression), intent(in) :: this
!------
        character(:), allocatable :: expression
!------
        call this%root%getString(expression)
        write(*,'(a)') expression
    end subroutine

!******************************************************************************!
!> Get the list of variable names from an expression
!******************************************************************************!
    subroutine getVariableNames(this, name)
        class(FExpression), intent(in) :: this
        type(FString), dimension(:), allocatable, intent(out) :: name
!------
        integer :: i
        type(FException) :: err_
!------
        if (allocated(this%variable)) then
            allocate(name(size(this%variable)))
            do i=1, size(this%variable)
                if (allocated(this%variable(i)%name)) then
                    name(i) = FString(this%variable(i)%name)
                else
                    call err_%raise("Unallocated name of variable "//i//" in expression "//this%string())
                end if
            end do
        else
            allocate(name(0))
        end if
    end subroutine
!******************************************************************************!
!> Get the list of variable names and values from an expression
!******************************************************************************!
    subroutine getVariables(this, variable)
        class(FExpression), intent(in) :: this
        type(FNumericalDictionary), intent(out) :: variable !< Variable names and values
!------
        integer :: i
!------
        do i=1, size(this%variable)
            call variable%set(this%variable(i)%name, this%variable(i)%value)
        end do
    end subroutine
!******************************************************************************!
!> Set a variable (character version).
!******************************************************************************!
    subroutine setVariable_char(this, name, value)
        class(FExpression), intent(inout), target :: this
        character(*), intent(in) :: name  !< Name of the variable
        real(e),      intent(in) :: value !< Value of the variable
!------
        integer :: i, a
!------
        ! Set the value in the variable array
        do i=1, this%nVariables
            if (this%variable(i)%name == name) then

                ! Mark the symbol as a variable if it was a constant
                this%variable(i)%constant = .false.

                ! Set the constant status in the bytecode
                if (allocated(this%bytecode%instruction)) then
                    this%bytecode%instruction(i) = CODE_VAR
                end if

                ! Set the new value in the variable table
                this%variable(i)%value = value

                ! Set the value in the bytecode
                if (allocated(this%bytecode%data)) then
                    this%bytecode%data(i) = value
                end if

                ! Set the value to all the instances of the variable
                do a=1, this%variable(i)%nInstances
                    this%variable(i)%instance(a)%node%value = value
                    call this%variable(i)%instance(a)%node%setChanged(.true.)
                end do
            end if
        end do
    end subroutine
!******************************************************************************!
!> Set a variable (string version).
!******************************************************************************!
    subroutine setVariable_string(this, name, value)
        class(FExpression), intent(inout), target :: this
        type(FString), intent(in) :: name  !< Name of the variable
        real(e),       intent(in) :: value !< Value of the variable
!------
        integer :: i, a
!------
        ! Set the value in the variable array
        do i=1, this%nVariables
            if (this%variable(i)%name == name) then

                ! Mark the symbol as a variable if it was a constant
                this%variable(i)%constant = .false.

                ! Set the constant status in the bytecode
                if (allocated(this%bytecode%instruction)) then
                    this%bytecode%instruction(i) = CODE_VAR
                end if

                ! Set the new value in the variable table
                this%variable(i)%value = value

                ! Set the value in the bytecode
                if (allocated(this%bytecode%data)) then
                    this%bytecode%data(i) = value
                end if

                ! Set the value to all the instances of the variable
                do a=1, this%variable(i)%nInstances
                    this%variable(i)%instance(a)%node%value = value
                    call this%variable(i)%instance(a)%node%setChanged(.true.)
                end do
            end if
        end do
    end subroutine
!******************************************************************************!
!> Set the variables contained in a dictionary object.
!******************************************************************************!
    subroutine setVariables(this, dict)
        class(FExpression), intent(inout) :: this
        type(FNumericalDictionary), intent(in) :: dict !< Variable names and values
!------
        integer :: i
        type(FString), dimension(:), allocatable :: name
!------
        call dict%getKeys(name)

        do i=1, size(name)
            call setVariable_string(this, name(i), dict%value(name(i)))
        end do
    end subroutine
!******************************************************************************!
!> Set a constant.
!******************************************************************************!
    subroutine setConstant(this, name, value)
        class(FExpression), intent(inout), target :: this
        character(*), intent(in) :: name  !< Name of the constant
        real(e),      intent(in) :: value !< Value of the constant
!------
        integer :: i, a
!------
        ! Set the value in the variable array
        do i=1, this%nVariables
            if (this%variable(i)%name == name) then

                ! Mark the symbol as a variable if it was a constant
                this%variable(i)%constant = .true.

                ! Set the constant status in the bytecode
                if (allocated(this%bytecode%instruction)) then
                    this%bytecode%instruction(i) = CODE_CONSTANT
                end if

                ! Set the new value in the variable table
                this%variable(i)%value = value

                ! Set the value in the bytecode
                if (allocated(this%bytecode%data)) then
                    this%bytecode%data(i) = value
                end if

                ! Set the value to all the instances of the variable
                do a=1, this%variable(i)%nInstances
                    this%variable(i)%instance(a)%node%value = value
                    call this%variable(i)%instance(a)%node%setChanged(.true.)
                end do
            end if
        end do
    end subroutine
!******************************************************************************!
!> Set the variables contained in a dictionary object.
!******************************************************************************!
    subroutine setConstants(this, dict)
        class(FExpression), intent(inout) :: this
        type(FNumericalDictionary), intent(in) :: dict !< Variable names and values
!------
        integer :: i
        type(FString), dimension(:), allocatable :: name
        character(:), allocatable :: tmp
        real(e) :: tmpval
!------
        call dict%getKeys(name)
        do i=1, size(name)
            tmp = name(i)
            tmpval = dict%value(tmp)
            call setConstant(this, tmp, dict%value(tmp))
        end do
    end subroutine
!******************************************************************************!
!> Evaluate an expression in a FExpression object
!>
!>@todo check the IEEE flags after the calculation
!******************************************************************************!
    subroutine interpret(this, result, err)
        class(FExpression), intent(inout) :: this
        real(e), intent(out) :: result
        type(FException), intent(out), optional :: err
!------

        ! Nothing can be done if the root node is not allocated
        if (.not.allocated(this%root)) then
            if (allocated(this%expression)) then
                if (present(err)) call err%raise("invalid expression: "//this%expression)
            else
                if (present(err)) call err%raise("invalid expression")
            end if
            return
        end if

        if (this%root%changed) then
            call this%root%eval(result)
        else
            result = this%root%value
        end if
    end subroutine
!******************************************************************************!
!> Evaluate an expression in a `FExpression` object (function version). This
!> function would be pure if it were not for the ieee_* calls, so it should be
!> fine in most contexts.
!******************************************************************************!
    function interpret_f_(this) result(result)
        class(FExpression), intent(in) :: this
        real(e) :: result
!------
        ! Re-calculate the value only if the expression has changed
        if (this%root%changed) then
            result = this%root%evalf()
        else
            result = this%root%value
        end if
    end function
!******************************************************************************!
!> Evaluate an expression in a `FExpression` object (function version with one
!> variable). The function would be pure if it were not for the ieee_* calls,
!> so it should be fine in most contexts.
!******************************************************************************!
    function interpret_f_var(this, variable, value) result(result)
        class(FExpression), intent(in) :: this
        character(*), intent(in) :: variable
        real(e), intent(in) :: value
        real(e) :: result
!------
        ! Re-calculate the value only if the expression has changed
        if (this%root%changed) then
            result = this%root%evalf(variable, value)
        else
            result = this%root%value
        end if
    end function
!******************************************************************************!
!>
!******************************************************************************!
    subroutine getDerivative(this, variable, deriv)
        class(FExpression), intent(in) :: this
        character(*), intent(in) :: variable
        type(FExpression), intent(out) :: deriv
!------

        ! Calculate the derivative
        if (allocated(this%root)) then

            ! Get the derivative AST
            allocate(deriv%root)
            call this%root%derive(this%root, deriv%root, variable)

            ! Simplify the derivative
            call deriv%simplify

            ! Set the internal arrays to access variables in the AST
            call deriv%setVariableTables

            ! Make sure the expression will be re-calculated
            call deriv%root%setNeedsUpdate(.true.)

            ! Compile the derivative
            call deriv%generateBytecode
        end if
    end subroutine
!******************************************************************************!
!> Simplify an expression. The original expression and the simplified one must
!> be mathematically equivalent.
!******************************************************************************!
    subroutine simplify(this)
        class(FExpression), intent(inout) :: this
!------
        integer :: i
!------
        if (allocated(this%root)) then
            ! Make sure simplification will b eattempted for each node
            call this%root%setNeedsUpdate(.true.)

            ! Try to simplify the AST up to 10 times
            do i=1, 10
                call this%root%simplify
                if (.not.this%root%changed) then
                    exit
                end if
            end do

            ! Make sure the parent pointers are correct
            call this%root%checkParentPointers

            ! Rebuild the variable tables
            call this%setVariableTables

            ! Make sure the expression will be re-calculated
            call this%root%setNeedsUpdate(.true.)

            ! Re-compile the expression
            call this%generateBytecode
        end if
    end subroutine
!******************************************************************************!
!> Build a variable table from an expression object
!******************************************************************************!
    subroutine setVariableTables(this)
        class(FExpression), intent(inout), target :: this
!------
        integer :: i, n
        type(variable), dimension(:), allocatable :: tmpv
!------
        if (.not.allocated(this%variable)) then

            ! Get the maximum number of variables
            call countVariables(this%root, this%nVariables)
            allocate(this%variable(this%nVariables))

            this%nVariables = 0
        end if

        ! Get the number of instances of each variable
        call countVariableInstances(this%root, this, this%variable)

        ! Count the number of unnecessary elements in the variable array (i.e. those whose name has not been set)
        n = 0
        do i=1, size(this%variable)
            if (allocated(this%variable(i)%name)) n = n + 1
        end do

        ! Remove the unnecessary elements
        if (n /= size(this%variable)) then
            call move_alloc(from=this%variable, to=tmpv)
            this%variable = tmpv(:n)
            deallocate(tmpv)
        end if

        ! Remove unnecessary elements from the variable array
        do i=1, this%nVariables
            if (allocated(this%variable(i)%instance)) deallocate(this%variable(i)%instance)
            allocate(this%variable(i)%instance(this%variable(i)%nInstances))
        end do

        ! Re-set the instance counter
        this%variable(:)%nInstances = 0

        ! Set the instance pointers
        call setVariableInstances(this, this%root, this%variable)
    end subroutine
!******************************************************************************!
!> Walk an expression tree to set the instances pointers of a variable list
!>
!> For use in the `mod_expression` module only.
!******************************************************************************!
    recursive subroutine setVariableInstances(expr, node, var)
        class(FExpression), intent(inout), target :: expr
        type(ast_node), intent(in), target :: node
        type(variable), dimension(:), intent(inout) :: var
!------
        integer :: i
!------
        if (node%type == TOKEN_TYPE_VARIABLE .or. node%type == TOKEN_TYPE_CONSTANT) then
            do i=1, expr%nVariables
                if (expr%variable(i)%name == node%name) then
                    expr%variable(i)%nInstances = expr%variable(i)%nInstances + 1
                    expr%variable(i)%instance(expr%variable(i)%nInstances)%node => node
                    return
                end if
            end do
        else
            if (associated(node%argument1)) then
                call setVariableInstances(expr, node%argument1, var)
            end if
            if (associated(node%argument2)) then
                call setVariableInstances(expr, node%argument2, var)
            end if
        end if
    end subroutine
!******************************************************************************!
!> Walk an expression tree to count the number of instances of each variable
!>
!> For use in the `mod_expression` module only.
!******************************************************************************!
    recursive subroutine countVariableInstances(node, this, var)
        type(ast_node), intent(in) :: node
        class(FExpression), intent(inout), target :: this
        type(variable), dimension(:), intent(inout) :: var
!------
        integer :: i
!------
        if (node%type == TOKEN_TYPE_VARIABLE .or. node%type == TOKEN_TYPE_CONSTANT) then
            do i=1, size(var)
                if (allocated(var(i)%name)) then
                    if (var(i)%name == node%name) then
                        var(i)%nInstances = var(i)%nInstances + 1
                        return
                    end if
                end if
            end do
            this%nVariables = this%nVariables + 1
            var(this%nVariables)%name = node%name
            var(this%nVariables)%nInstances = 1
            if (node%type == TOKEN_TYPE_CONSTANT) then
                var(this%nVariables)%value = node%value
                var(this%nVariables)%constant = .true.
            end if
        else
            if (associated(node%argument1)) then
                call countVariableInstances(node%argument1, this, var)
            end if
            if (associated(node%argument2)) then
                call countVariableInstances(node%argument2, this, var)
            end if
        end if
    end subroutine
!******************************************************************************!
!> Walk an expression tree to count the number of different variables in it.
!> For use in the `mod_expression` module only.
!******************************************************************************!
    recursive subroutine countVariables(node, n)
        type(ast_node), intent(in) :: node
        integer, intent(out) :: n
!------
        integer :: m
!------
        n = 0
        if (node%type == TOKEN_TYPE_VARIABLE .or. node%type == TOKEN_TYPE_CONSTANT) then
            n = 1
        else
            if (associated(node%argument1)) then
                call countVariables(node%argument1, m)
                n = n + m
            end if
            if (associated(node%argument2)) then
                call countVariables(node%argument2, m)
                n = n + m
            end if
        end if
    end subroutine
!******************************************************************************!
!> Calculate the node values of the AST.
!******************************************************************************!
    subroutine optimise(this)
        class(FExpression), intent(inout) :: this
!------
        if (allocated(this%root)) then
            call this%root%calculate
        end if
        call this%bytecode%optimise
    end subroutine
end module
