    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mcstb1(lot, jump, n, inc, x, wsave, dsum, work, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) x(inc, *), wsave(*)
        double precision dsum(*)

        ier = 0
        nm1 = n - 1
        np1 = n + 1
        ns2 = n/2
        lj = (lot-1)*jump + 1
        if (n-2) 150, 100, 110
100     do m = 1, lj, jump
            x1h = x(m, 1) + x(m, 2)
            x(m, 2) = x(m, 1) - x(m, 2)
            x(m, 1) = x1h
        end do
        return
110     if (n>3) go to 120
        do m = 1, lj, jump
            x1p3 = x(m, 1) + x(m, 3)
            x2 = x(m, 2)
            x(m, 2) = x(m, 1) - x(m, 3)
            x(m, 1) = x1p3 + x2
            x(m, 3) = x1p3 - x2
        end do
        return
120     do m = 1, lj, jump
            x(m, 1) = x(m, 1) + x(m, 1)
            x(m, n) = x(m, n) + x(m, n)
        end do
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            dsum(m1) = x(m, 1) - x(m, n)
            x(m, 1) = x(m, 1) + x(m, n)
        end do
        do k = 2, ns2
            m1 = 0
            do m = 1, lj, jump
                m1 = m1 + 1
                kc = np1 - k
                t1 = x(m, k) + x(m, kc)
                t2 = x(m, k) - x(m, kc)
                dsum(m1) = dsum(m1) + wsave(kc)*t2
                t2 = wsave(k)*t2
                x(m, k) = t1 - t2
                x(m, kc) = t1 + t2
            end do
        end do
        modn = mod(n, 2)
        if (modn==0) go to 130
        do m = 1, lj, jump
            x(m, ns2+1) = x(m, ns2+1) + x(m, ns2+1)
        end do
130     continue
        lenx = (lot-1)*jump + inc*(nm1-1) + 1
        lnsv = nm1 + int(log(real(nm1, rwp))/log(2.0_rwp)) + 4
        lnwk = lot*nm1

        call rfftmf(lot, jump, nm1, inc, x, lenx, wsave(n+1), lnsv, work, &
          lnwk, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('MCSTB1', -5)
            go to 150
        end if

        fnm1s2 = float(nm1)/2.
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            dsum(m1) = .5_rwp*dsum(m1)
            x(m, 1) = fnm1s2*x(m, 1)
        end do
        if (mod(nm1,2)/=0) go to 140
        do m = 1, lj, jump
            x(m, nm1) = x(m, nm1) + x(m, nm1)
        end do
140     fnm1s4 = float(nm1)/4.
        do i = 3, n, 2
            m1 = 0
            do m = 1, lj, jump
                m1 = m1 + 1
                xi = fnm1s4*x(m, i)
                x(m, i) = fnm1s4*x(m, i-1)
                x(m, i-1) = dsum(m1)
                dsum(m1) = dsum(m1) + xi
            end do
        end do
        if (modn/=0) return
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            x(m, n) = dsum(m1)
        end do
150     continue
    end subroutine
