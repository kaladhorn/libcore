module test_stringDictionary
    use core
    use mod_test
    use ext_character

    use config,         only: TEST_PATH
    use mod_string,     only: FString
    use mod_exception,  only: FException
    use mod_stringDictionary, only: FStringDictionary

    implicit none
    private

    public :: testAll
contains
    subroutine testAll
        call testConstructor
        call testScalarAccessors
        call testsFinished
    end subroutine

    subroutine testConstructor
        type(FStringDictionary) :: d, d2
        type(FString), dimension(:), allocatable :: keys, values
        type(FException) :: stat
        logical :: identical

        call startTestGroup("constructor")
        allocate(keys(10))
        allocate(values(10))
        keys(1) = "a"
        keys(2) = "b"
        keys(3) = "c"
        keys(4) = "d"
        keys(5) = "e"
        keys(6) = "f"
        keys(7) = "g"
        keys(8) = "h"
        keys(9) = "i"
        keys(10) = "j"
        values(1) = "1"
        values(2) = "2"
        values(3) = "3"
        values(4) = "4"
        values(5) = "5"
        values(6) = "6"
        values(7) = "7"
        values(8) = "8"
        values(9) = "9"
        values(10) = "10"

        d = FStringDictionary(keys, values, stat)
        call check(stat == 0, "string array constructor", &
            "error message from constructor: " // stat%string() // ".")


        call d2%set("a", "1")
        call d2%set("b", "2")
        call d2%set("c", "3")
        call d2%set("d", "4")
        call d2%set("e", "5")
        call d2%set("f", "6")
        call d2%set("g", "7")
        call d2%set("h", "8")
        call d2%set("i", "9")
        call d2%set("j", "10")
        identical = (d==d2)
        call check(identical, "identical dictionary comparison", "")

        call d%set("a", "42")
        identical = (d==d2)
        call check(.not.identical, "different dictionary comparison", "")

        call d%set("a", "1")
        call d%set("k", "11")
        identical = (d==d2)
        call check(.not.identical, "different dictionary comparison", "")

        call d2%set("k", "11")
        call d2%set("l", "12")
        identical = (d==d2)
        call check(.not.identical, "different dictionary comparison 2", "")

        deallocate(values)
        allocate(values(1))
        d = FStringDictionary(keys, values, stat)
        call check(stat == "Keys and values arrays do not match.", &
            "string constructor with mismatched arrays", &
            "wrong error message from constructor: """ // &
            stat%string() // """ instead of ""Keys and values arrays do not match.""")
        call stat%discard
    end subroutine

    subroutine testScalarAccessors
        type(FStringDictionary) :: d
        character(:), allocatable :: vc
        type(FString) :: vs
        type(FException) :: stat

        call startTestGroup("scalar accessors")
        call d%set("a", "1")
        call d%set("b", "2")
        call d%set("c", "3")
        call d%set("d", "4")
        call d%set("e", "5")
        call d%set("f", "6")
        call d%set("g", "7")
        call d%set("h", "8")
        call d%set("i", "9")
        call d%set("j", "10")

        call d%getValue("a", vc)
        call check(vc=="1", "character getter", &
            "wrong value: """ // vc // "instead of ""1""")
        call d%getValue("z", vc, stat)
        call check(stat=="the key 'z' is not defined.", "character getter (undefined key)", &
            "wrong status: """ // stat%string() // "instead of ""the key 'z' is not defined.""")
        call stat%discard

        call d%getValue(FString("a"), vs)
        call check(vs=="1", "string getter", &
            "wrong value: """ // vs // "instead of ""1""")
        call d%getValue(FString("z"), vs, stat)
        call check(stat=="the key 'z' is not defined.", "string getter (undefined key)", &
            "wrong status: """ // stat%string() // "instead of ""the key 'z' is not defined.""")
        call stat%discard

        call d%getValue("a", vs)
        call check(vs=="1", "character/string getter", &
            "wrong value: """ // vs // "instead of ""1""")
        call d%getValue("z", vs, stat)
        call check(stat=="the key 'z' is not defined.", "character/string getter (undefined key)", &
            "wrong status: """ // stat%string() // "instead of ""the key 'z' is not defined.""")
        call stat%discard

        call d%getValueScalarSC(FString("a"), vc)
        call check(vc=="1", "string/character getter", &
            "wrong value: """ // vc // """ instead of ""1""")
        call d%getValue(FString("z"), vc, stat)
        call check(stat=="the key 'z' is not defined.", "string/character getter (undefined key)", &
            "wrong status: """ // stat%string() // """ instead of ""the key 'z' is not defined.""")
        call stat%discard

        call check(d%value("a")=="1", "character function getter", &
            "wrong value: """ // d%value("a") // "instead of ""1""")
        call check(d%value("z")==UNDEF, "character function getter (undefined key)", &
            "wrong value: """ // d%value("z") // "instead of "" // UNDEF // """)

        call check(d%value(FString("a"))=="1", "string function getter", &
            "wrong value: """ // d%value(FString("a")) // "instead of ""1""")
        call check(d%value(FString("z"))==UNDEF, "string function getter (undefined key)", &
            "wrong value: """ // d%value(FString("z")) // "instead of "" // UNDEF // """)

        call d%set("a", "42")
        call check(d%value("a")=="42", "character setter", &
            "wrong value: """ // d%value("a") // "instead of ""42""")

        call d%set(FString("a"), FString("24"))
        call check(d%value("a")=="24", "string setter", &
            "wrong value: """ // d%value("a") // "instead of ""24""")


    end subroutine
end module

program test
    use test_stringDictionary

    call testAll
end program