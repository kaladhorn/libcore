module mod_niffty
  integer, parameter :: rwp=kind(1.0d0)
  integer, parameter :: cwp=kind(1.0d0)
  ! interface module generated on 2015-11-21 at 18:51:51 +0100.
  interface
    subroutine c1f2kb(ido, l1, na, cc, in1, ch, in2, wa)
        import
      ! interface, source="c1f2kb.f90".
      real (rwp) :: cc(in1, l1, ido, 2), ch(in2, l1, 2, ido), &
        wa(ido, 1, 2)
    end subroutine
    subroutine c1f2kf(ido, l1, na, cc, in1, ch, in2, wa)
        import
      ! interface, source="c1f2kf.f90".
      real (rwp) :: cc(in1, l1, ido, 2), ch(in2, l1, 2, ido), &
        wa(ido, 1, 2)
    end subroutine
    subroutine c1f3kb(ido, l1, na, cc, in1, ch, in2, wa)
        import
      ! interface, source="c1f3kb.f90".
      real (rwp) :: cc(in1, l1, ido, 3), ch(in2, l1, 3, ido), &
        wa(ido, 2, 2)
    end subroutine
    subroutine c1f3kf(ido, l1, na, cc, in1, ch, in2, wa)
        import
      ! interface, source="c1f3kf.f90".
      real (rwp) :: cc(in1, l1, ido, 3), ch(in2, l1, 3, ido), &
        wa(ido, 2, 2)
    end subroutine
    subroutine c1f4kb(ido, l1, na, cc, in1, ch, in2, wa)
        import
      ! interface, source="c1f4kb.f90".
      real (rwp) :: cc(in1, l1, ido, 4), ch(in2, l1, 4, ido), &
        wa(ido, 3, 2)
    end subroutine
    subroutine c1f4kf(ido, l1, na, cc, in1, ch, in2, wa)
        import
      ! interface, source="c1f4kf.f90".
      real (rwp) :: cc(in1, l1, ido, 4), ch(in2, l1, 4, ido), &
        wa(ido, 3, 2)
    end subroutine
    subroutine c1f5kb(ido, l1, na, cc, in1, ch, in2, wa)
        import
      ! interface, source="c1f5kb.f90".
      real (rwp) :: cc(in1, l1, ido, 5), ch(in2, l1, 5, ido), &
        wa(ido, 4, 2)
    end subroutine
    subroutine c1f5kf(ido, l1, na, cc, in1, ch, in2, wa)
        import
      ! interface, source="c1f5kf.f90".
      real (rwp) :: cc(in1, l1, ido, 5), ch(in2, l1, 5, ido), &
        wa(ido, 4, 2)
    end subroutine
    subroutine c1fgkb(ido, ip, l1, lid, na, cc, cc1, in1, ch, ch1, in2, &
      wa)
      import
      ! interface, source="c1fgkb.f90".
      real (rwp) :: ch(in2, l1, ido, ip), cc(in1, l1, ip, ido), &
        cc1(in1, lid, ip), ch1(in2, lid, ip), wa(ido, ip-1, 2)
    end subroutine
    subroutine c1fgkf(ido, ip, l1, lid, na, cc, cc1, in1, ch, ch1, in2, &
      wa)
      import
      ! interface, source="c1fgkf.f90".
      real (rwp) :: ch(in2, l1, ido, ip), cc(in1, l1, ip, ido), &
        cc1(in1, lid, ip), ch1(in2, lid, ip), wa(ido, ip-1, 2)
    end subroutine
    subroutine c1fm1b(n, inc, c, ch, wa, fnf, fac)
      import
      ! interface, source="c1fm1b.f90".
      Complex (cwp) :: c(*)
      real (rwp) :: ch(*), wa(*), fac(*)
    end subroutine
    subroutine c1fm1f(n, inc, c, ch, wa, fnf, fac)
      import
      ! interface, source="c1fm1f.f90".
      Complex (cwp) :: c(*)
      real (rwp) :: ch(*), wa(*), fac(*)
    end subroutine
    subroutine cfft1b(n, inc, c, lenc, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="cfft1b.f90".
      Integer :: n, inc, lenc, lensav, lenwrk, ier
      Complex (cwp) :: c(lenc)
      real (rwp) :: wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cfft1f(n, inc, c, lenc, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="cfft1f.f90".
      Integer :: n, inc, lenc, lensav, lenwrk, ier
      Complex (cwp) :: c(lenc)
      real (rwp) :: wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cfft1i(n, wsave, lensav, ier)
      import
      ! interface, source="cfft1i.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine cfft2b(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="cfft2b.f90".
      Integer :: l, m, ldim, lensav, lenwrk, ier
      Complex (cwp) :: c(ldim, m)
      real (rwp) :: wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cfft2f(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="cfft2f.f90".
      Integer :: l, m, ldim, lensav, lenwrk, ier
      Complex (cwp) :: c(ldim, m)
      real (rwp) :: wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cfft2i(l, m, wsave, lensav, ier)
      import
      ! interface, source="cfft2i.f90".
      Integer :: l, m, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine cfftmb(lot, jump, n, inc, c, lenc, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="cfftmb.f90".
      Integer :: lot, jump, n, inc, lenc, lensav, lenwrk, ier
      Complex (cwp) :: c(lenc)
      real (rwp) :: wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cfftmf(lot, jump, n, inc, c, lenc, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="cfftmf.f90".
      Integer :: lot, jump, n, inc, lenc, lensav, lenwrk, ier
      Complex (cwp) :: c(lenc)
      real (rwp) :: wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cfftmi(n, wsave, lensav, ier)
      import
      ! interface, source="cfftmi.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine cmf2kb(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
      import
      ! interface, source="cmf2kb.f90".
      real (rwp) :: cc(2, in1, l1, ido, 2), ch(2, in2, l1, 2, ido), &
        wa(ido, 1, 2)
    end subroutine
    subroutine cmf2kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
      import
      ! interface, source="cmf2kf.f90".
      real (rwp) :: cc(2, in1, l1, ido, 2), ch(2, in2, l1, 2, ido), &
        wa(ido, 1, 2)
    end subroutine
    subroutine cmf3kb(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
      import
      ! interface, source="cmf3kb.f90".
      real (rwp) :: cc(2, in1, l1, ido, 3), ch(2, in2, l1, 3, ido), &
        wa(ido, 2, 2)
    end subroutine
    subroutine cmf3kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
      import
      ! interface, source="cmf3kf.f90".
      real (rwp) :: cc(2, in1, l1, ido, 3), ch(2, in2, l1, 3, ido), &
        wa(ido, 2, 2)
    end subroutine
    subroutine cmf4kb(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
      import
      ! interface, source="cmf4kb.f90".
      real (rwp) :: cc(2, in1, l1, ido, 4), ch(2, in2, l1, 4, ido), &
        wa(ido, 3, 2)
    end subroutine
    subroutine cmf4kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
      import
      ! interface, source="cmf4kf.f90".
      real (rwp) :: cc(2, in1, l1, ido, 4), ch(2, in2, l1, 4, ido), &
        wa(ido, 3, 2)
    end subroutine
    subroutine cmf5kb(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
      import
      ! interface, source="cmf5kb.f90".
      real (rwp) :: cc(2, in1, l1, ido, 5), ch(2, in2, l1, 5, ido), &
        wa(ido, 4, 2)
    end subroutine
    subroutine cmf5kf(lot, ido, l1, na, cc, im1, in1, ch, im2, in2, wa)
      import
      ! interface, source="cmf5kf.f90".
      real (rwp) :: cc(2, in1, l1, ido, 5), ch(2, in2, l1, 5, ido), &
        wa(ido, 4, 2)
    end subroutine
    subroutine cmfgkb(lot, ido, ip, l1, lid, na, cc, cc1, im1, in1, ch, &
      ch1, im2, in2, wa)
      import
      ! interface, source="cmfgkb.f90".
      real (rwp) :: ch(2, in2, l1, ido, ip), cc(2, in1, l1, ip, ido), &
        cc1(2, in1, lid, ip), ch1(2, in2, lid, ip), wa(ido, ip-1, 2)
    end subroutine
    subroutine cmfgkf(lot, ido, ip, l1, lid, na, cc, cc1, im1, in1, ch, &
      ch1, im2, in2, wa)
      import
      ! interface, source="cmfgkf.f90".
      real (rwp) :: ch(2, in2, l1, ido, ip), cc(2, in1, l1, ip, ido), &
        cc1(2, in1, lid, ip), ch1(2, in2, lid, ip), wa(ido, ip-1, 2)
    end subroutine
    subroutine cmfm1b(lot, jump, n, inc, c, ch, wa, fnf, fac)
      import
      ! interface, source="cmfm1b.f90".
      Complex (cwp) :: c(*)
      real (rwp) :: ch(*), wa(*), fac(*)
    end subroutine
    subroutine cmfm1f(lot, jump, n, inc, c, ch, wa, fnf, fac)
      import
      ! interface, source="cmfm1f.f90".
      Complex (cwp) :: c(*)
      real (rwp) :: ch(*), wa(*), fac(*)
    end subroutine
    subroutine cosq1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="cosq1b.f90".
      Integer :: n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cosq1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="cosq1f.f90".
      Integer :: n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cosq1i(n, wsave, lensav, ier)
      import
      ! interface, source="cosq1i.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine cosqb1(n, inc, x, wsave, work, ier)
      import
      ! interface, source="cosqb1.f90".
      real (rwp) :: x, wsave, work
      Dimension :: x(inc, *), wsave(*), work(*)
    end subroutine
    subroutine cosqf1(n, inc, x, wsave, work, ier)
      import
      ! interface, source="cosqf1.f90".
      real (rwp) :: x, wsave, work
      Dimension :: x(inc, *), wsave(*), work(*)
    end subroutine
    subroutine cosqmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="cosqmb.f90".
      Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cosqmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="cosqmf.f90".
      Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cosqmi(n, wsave, lensav, ier)
      import
      ! interface, source="cosqmi.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine cost1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="cost1b.f90".
      Integer :: n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cost1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="cost1f.f90".
      Integer :: n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine cost1i(n, wsave, lensav, ier)
      import
      ! interface, source="cost1i.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine costb1(n, inc, x, wsave, work, ier)
      import
      ! interface, source="costb1.f90".
      real (rwp) :: x(inc, *), wsave(*)
    end subroutine
    subroutine costf1(n, inc, x, wsave, work, ier)
      import
      ! interface, source="costf1.f90".
      real (rwp) :: x(inc, *), wsave(*)
    end subroutine
    subroutine costmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="costmb.f90".
      Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine costmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="costmf.f90".
      Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine costmi(n, wsave, lensav, ier)
      import
      ! interface, source="costmi.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine factor(n, nf, fac)
      import
      ! interface, source="factor.f90".
      real (rwp) :: fac(*)
    end subroutine
    subroutine mcfti1(n, wa, fnf, fac)
      import
      ! interface, source="mcfti1.f90".
      real (rwp) :: wa(*), fac(*)
    end subroutine
    subroutine mcsqb1(lot, jump, n, inc, x, wsave, work, ier)
      import
      ! interface, source="mcsqb1.f90".
      real (rwp) :: x, wsave, work
      Dimension :: x(inc, *), wsave(*), work(lot, *)
    end subroutine
    subroutine mcsqf1(lot, jump, n, inc, x, wsave, work, ier)
      import
      ! interface, source="mcsqf1.f90".
      real (rwp) :: x, wsave, work
      Dimension :: x(inc, *), wsave(*), work(lot, *)
    end subroutine
    subroutine mcstb1(lot, jump, n, inc, x, wsave, dsum, work, ier)
      import
      ! interface, source="mcstb1.f90".
      real (rwp) :: x(inc, *), wsave(*)
      Double Precision :: dsum(*)
    end subroutine
    subroutine mcstf1(lot, jump, n, inc, x, wsave, dsum, work, ier)
      import
      ! interface, source="mcstf1.f90".
      real (rwp) :: x(inc, *), wsave(*)
      Double Precision :: dsum(*)
    end subroutine
    subroutine mradb2(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1)
      import
      ! interface, source="mradb2.f90".
      real (rwp) :: cc(in1, ido, 2, l1), ch(in2, ido, l1, 2), wa1(ido)
    end subroutine
    subroutine mradb3(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2)
      import
      ! interface, source="mradb3.f90".
      real (rwp) :: cc(in1, ido, 3, l1), ch(in2, ido, l1, 3), wa1(ido), &
        wa2(ido)
    end subroutine
    subroutine mradb4(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, &
      wa3)
      import
      ! interface, source="mradb4.f90".
      real (rwp) :: cc(in1, ido, 4, l1), ch(in2, ido, l1, 4), wa1(ido), &
        wa2(ido), wa3(ido)
    end subroutine
    subroutine mradb5(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, &
      wa3, wa4)
      import
      ! interface, source="mradb5.f90".
      real (rwp) :: cc(in1, ido, 5, l1), ch(in2, ido, l1, 5), wa1(ido), &
        wa2(ido), wa3(ido), wa4(ido)
    end subroutine
    subroutine mradbg(m, ido, ip, l1, idl1, cc, c1, c2, im1, in1, ch, ch2, &
      im2, in2, wa)
      import
      ! interface, source="mradbg.f90".
      real (rwp) :: ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
        c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), &
        wa(ido)
    end subroutine
    subroutine mradf2(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1)
      import
      ! interface, source="mradf2.f90".
      real (rwp) :: ch(in2, ido, 2, l1), cc(in1, ido, l1, 2), wa1(ido)
    end subroutine
    subroutine mradf3(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2)
      import
      ! interface, source="mradf3.f90".
      real (rwp) :: ch(in2, ido, 3, l1), cc(in1, ido, l1, 3), wa1(ido), &
        wa2(ido)
    end subroutine
    subroutine mradf4(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, &
      wa3)
      import
      ! interface, source="mradf4.f90".
      real (rwp) :: cc(in1, ido, l1, 4), ch(in2, ido, 4, l1), wa1(ido), &
        wa2(ido), wa3(ido)
    end subroutine
    subroutine mradf5(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2, &
      wa3, wa4)
      import
      ! interface, source="mradf5.f90".
      real (rwp) :: cc(in1, ido, l1, 5), ch(in2, ido, 5, l1), wa1(ido), &
        wa2(ido), wa3(ido), wa4(ido)
    end subroutine
    subroutine mradfg(m, ido, ip, l1, idl1, cc, c1, c2, im1, in1, ch, ch2, &
      im2, in2, wa)
      import
      ! interface, source="mradfg.f90".
      real (rwp) :: ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
        c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), &
        wa(ido)
    end subroutine
    subroutine mrftb1(m, im, n, in, c, ch, wa, fac)
      import
      ! interface, source="mrftb1.f90".
      real (rwp) :: ch(m, *), c(in, *), wa(n), fac(15)
    end subroutine
    subroutine mrftf1(m, im, n, in, c, ch, wa, fac)
      import
      ! interface, source="mrftf1.f90".
      real (rwp) :: ch(m, *), c(in, *), wa(n), fac(15)
    end subroutine
    subroutine mrfti1(n, wa, fac)
      import
      ! interface, source="mrfti1.f90".
      real (rwp) :: wa(n), fac(15)
    end subroutine
    subroutine msntb1(lot, jump, n, inc, x, wsave, dsum, xh, work, ier)
      import
      ! interface, source="msntb1.f90".
      real (rwp) :: x(inc, *), wsave(*), xh(lot, *)
      Double Precision :: dsum(*)
    end subroutine
    subroutine msntf1(lot, jump, n, inc, x, wsave, dsum, xh, work, ier)
      import
      ! interface, source="msntf1.f90".
      real (rwp) :: x(inc, *), wsave(*), xh(lot, *)
      Double Precision :: dsum(*)
    end subroutine
    subroutine r1f2kb(ido, l1, cc, in1, ch, in2, wa1)
      import
      ! interface, source="r1f2kb.f90".
      real (rwp) :: cc(in1, ido, 2, l1), ch(in2, ido, l1, 2), wa1(ido)
    end subroutine
    subroutine r1f2kf(ido, l1, cc, in1, ch, in2, wa1)
      import
      ! interface, source="r1f2kf.f90".
      real (rwp) :: ch(in2, ido, 2, l1), cc(in1, ido, l1, 2), wa1(ido)
    end subroutine
    subroutine r1f3kb(ido, l1, cc, in1, ch, in2, wa1, wa2)
      import
      ! interface, source="r1f3kb.f90".
      real (rwp) :: cc(in1, ido, 3, l1), ch(in2, ido, l1, 3), wa1(ido), &
        wa2(ido)
    end subroutine
    subroutine r1f3kf(ido, l1, cc, in1, ch, in2, wa1, wa2)
      import
      ! interface, source="r1f3kf.f90".
      real (rwp) :: ch(in2, ido, 3, l1), cc(in1, ido, l1, 3), wa1(ido), &
        wa2(ido)
    end subroutine
    subroutine r1f4kb(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3)
      import
      ! interface, source="r1f4kb.f90".
      real (rwp) :: cc(in1, ido, 4, l1), ch(in2, ido, l1, 4), wa1(ido), &
        wa2(ido), wa3(ido)
    end subroutine
    subroutine r1f4kf(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3)
      import
      ! interface, source="r1f4kf.f90".
      real (rwp) :: cc(in1, ido, l1, 4), ch(in2, ido, 4, l1), wa1(ido), &
        wa2(ido), wa3(ido)
    end subroutine
    subroutine r1f5kb(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3, wa4)
      import
      ! interface, source="r1f5kb.f90".
      real (rwp) :: cc(in1, ido, 5, l1), ch(in2, ido, l1, 5), wa1(ido), &
        wa2(ido), wa3(ido), wa4(ido)
    end subroutine
    subroutine r1f5kf(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3, wa4)
      import
      ! interface, source="r1f5kf.f90".
      real (rwp) :: cc(in1, ido, l1, 5), ch(in2, ido, 5, l1), wa1(ido), &
        wa2(ido), wa3(ido), wa4(ido)
    end subroutine
    subroutine r1fgkb(ido, ip, l1, idl1, cc, c1, c2, in1, ch, ch2, in2, &
      wa)
      import
      ! interface, source="r1fgkb.f90".
      real (rwp) :: ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
        c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), &
        wa(ido)
    end subroutine
    subroutine r1fgkf(ido, ip, l1, idl1, cc, c1, c2, in1, ch, ch2, in2, &
      wa)
      import
      ! interface, source="r1fgkf.f90".
      real (rwp) :: ch(in2, ido, l1, ip), cc(in1, ido, ip, l1), &
        c1(in1, ido, l1, ip), c2(in1, idl1, ip), ch2(in2, idl1, ip), &
        wa(ido)
    end subroutine
    subroutine r2w(ldr, ldw, l, m, r, w)
      import
      ! interface, source="r2w.f90".
      Dimension :: r(ldr, *), w(ldw, *)
    end subroutine
    subroutine rfft1b(n, inc, r, lenr, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="rfft1b.f90".
      Implicit None
      Integer :: n, inc, lenr, lensav, lenwrk, ier
      real (rwp) :: r(lenr), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine rfft1f(n, inc, r, lenr, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="rfft1f.f90".
      Implicit None
      Integer :: n, inc, lenr, lensav, lenwrk, ier
      real (rwp) :: r(lenr), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine rfft1i(n, wsave, lensav, ier)
      import
      ! interface, source="rfft1i.f90".
      Implicit None
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine rfft2b(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="rfft2b.f90".
      Implicit None
      Integer :: ldim, l, m, lensav, lenwrk, ier
      real (rwp) :: r(ldim, m), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine rfft2f(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="rfft2f.f90".
      Implicit None
      Integer :: ldim, l, m, lensav, lenwrk, ier, idx, modl, modm, idh, &
        idw
      real (rwp) :: r(ldim, m), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine rfft2i(l, m, wsave, lensav, ier)
      import
      ! interface, source="rfft2i.f90".
      Implicit None
      Integer :: l, m, lensav, ier
      Integer :: lwsav, mwsav, mmsav, ier1
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine rfftb1(n, in, c, ch, wa, fac)
      import
      ! interface, source="rfftb1.f90".
      real (rwp) :: ch(*), c(in, *), wa(n), fac(15)
    end subroutine
    subroutine rfftf1(n, in, c, ch, wa, fac)
      import
      ! interface, source="rfftf1.f90".
      real (rwp) :: ch(*), c(in, *), wa(n), fac(15)
    end subroutine
    subroutine rffti1(n, wa, fac)
      import
      ! interface, source="rffti1.f90".
      real (rwp) :: wa(n), fac(15)
    end subroutine
    subroutine rfftmb(lot, jump, n, inc, r, lenr, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="rfftmb.f90".
      Integer :: lot, jump, n, inc, lenr, lensav, lenwrk, ier
      real (rwp) :: r(lenr), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine rfftmf(lot, jump, n, inc, r, lenr, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="rfftmf.f90".
      Integer :: lot, jump, n, inc, lenr, lensav, lenwrk, ier
      real (rwp) :: r(lenr), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine rfftmi(n, wsave, lensav, ier)
      import
      ! interface, source="rfftmi.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine sinq1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="sinq1b.f90".
      Integer :: n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine sinq1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="sinq1f.f90".
      Integer :: n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine sinq1i(n, wsave, lensav, ier)
      import
      ! interface, source="sinq1i.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine sinqmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="sinqmb.f90".
      Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine sinqmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="sinqmf.f90".
      Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine sinqmi(n, wsave, lensav, ier)
      import
      ! interface, source="sinqmi.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine sint1b(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="sint1b.f90".
      Integer :: n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine sint1f(n, inc, x, lenx, wsave, lensav, work, lenwrk, ier)
      import
      ! interface, source="sint1f.f90".
      Integer :: n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine sint1i(n, wsave, lensav, ier)
      import
      ! interface, source="sint1i.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine sintb1(n, inc, x, wsave, xh, work, ier)
      import
      ! interface, source="sintb1.f90".
      real (rwp) :: x(inc, *), wsave(*), xh(*)
    end subroutine
    subroutine sintf1(n, inc, x, wsave, xh, work, ier)
      import
      ! interface, source="sintf1.f90".
      real (rwp) :: x(inc, *), wsave(*), xh(*)
    end subroutine
    subroutine sintmb(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="sintmb.f90".
      Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine sintmf(lot, jump, n, inc, x, lenx, wsave, lensav, work, &
      lenwrk, ier)
      import
      ! interface, source="sintmf.f90".
      Integer :: lot, jump, n, inc, lenx, lensav, lenwrk, ier
      real (rwp) :: x(inc, *), wsave(lensav), work(lenwrk)
    end subroutine
    subroutine sintmi(n, wsave, lensav, ier)
      import
      ! interface, source="sintmi.f90".
      Integer :: n, lensav, ier
      real (rwp) :: wsave(lensav)
    end subroutine
    subroutine tables(ido, ip, wa)
      import
      ! interface, source="tables.f90".
      real (rwp) :: wa(ido, ip-1, 2)
    end subroutine
    subroutine w2r(ldr, ldw, l, m, r, w)
      import
      ! interface, source="w2r.f90".
      Dimension :: r(ldr, *), w(ldw, *)
    end subroutine
    Logical Function xercon(inc, jump, n, lot)
      import
      ! interface, source="xercon.f90".
      Integer :: inc, jump, n, lot
    end Function
    subroutine xerfft(srname, info)
      import
      ! interface, source="xerfft.f90".
      Character (6) :: srname
      Integer :: info
    end subroutine
  end interface
end module
