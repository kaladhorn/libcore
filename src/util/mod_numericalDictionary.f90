!******************************************************************************!
!                        mod_numericalDictionary module
!------------------------------------------------------------------------------!
!> Simple dictionary data structure to store double-precision real values.
!>
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_numericalDictionary
    use core
    use ext_character

    use mod_string,       only: FString
    use mod_exception,    only: FException, MESSAGE_LEVEL_WARNING
    use mod_dictionary,   only: FDictionary

    implicit none
    private
    save

!******************************************************************************!
!> Numerical dictionary description
!******************************************************************************!
    type, public, extends(FDictionary) :: FNumericalDictionary
        real(e), dimension(:), allocatable :: val
    contains
        procedure, private :: numericalDictionary_setCharacterScalar
        procedure, private :: numericalDictionary_setStringScalar
        procedure, private :: numericalDictionary_setCharacterArray
        procedure, private :: numericalDictionary_setStringArray
        generic, public :: set => numericalDictionary_setCharacterScalar, &
            numericalDictionary_setStringScalar, &
            numericalDictionary_setCharacterArray, &
            numericalDictionary_setStringArray

        procedure, private :: numericalDictionary_valueScalarS
        procedure, private :: numericalDictionary_valueScalarC
        generic, public :: value => numericalDictionary_valueScalarS, &
            numericalDictionary_valueScalarC

        procedure, private :: numericalDictionary_getValueScalarS
        procedure, private :: numericalDictionary_getValueScalarC
        generic, public :: getValue => numericalDictionary_getValueScalarS, &
            numericalDictionary_getValueScalarC

        procedure, public :: getValues

        procedure, private :: dictionary_equal
        generic, public :: operator(==) => dictionary_equal

        procedure, private :: dictionary_differ
        generic, public :: operator(/=) => dictionary_differ

        procedure, public :: print
        procedure, public :: expandArrays
    end type

    interface FNumericalDictionary
        module procedure numericalDictionary_init
    end interface

contains
!******************************************************************************!
!> Initialise a numerical dictionary object with a list of keys and values.
!******************************************************************************!
    function numericalDictionary_init(keys, values, stat) result(this)
        type(FNumericalDictionary) :: this
        type(FString), dimension(:),   intent(in) :: keys
        real(e),       dimension(:),   intent(in) :: values
        type(FException), intent(out), optional   :: stat
!------
        type(FException) :: stat_
!------
        if (size(values) == size(keys)) then
            this%key_ = keys
            this%val = values
            this%size_ = size(keys)
        else
            call stat_%raise("keys and values arrays do not have the same size (" // &
                size(keys) // " and " // size(values) // ").")
        end if

        if (present(stat)) call stat%transfer(stat_)
    end function
!******************************************************************************!
!> Add keys to a dictionary object (Character version).
!******************************************************************************!
    subroutine numericalDictionary_setCharacterArray(this, key, value)
        class(FNumericalDictionary), intent(inout) :: this
        character(*), dimension(:), intent(in) :: key
        real(e), dimension(size(key)), intent(in) :: value
!------
        integer :: i
!------
        ! Make sure that the number of keys and the number of values is the same
        do i=1, size(key)

            ! Set the key-value pair
            call this%set(key(i), value(i))
        end do
    end subroutine
!******************************************************************************!
!> Add keys to a dictionary object (FString version).
!******************************************************************************!
    subroutine numericalDictionary_setStringArray(this, key, value)
        class(FNumericalDictionary), intent(inout) :: this
        type(FString), dimension(:), intent(in) :: key
        real(e), dimension(size(key)), intent(in) :: value
!------
        integer :: i
!------
        do i=1, size(key)

            ! Set the key with a default value
            call this%set(key(i), value(i))
        end do
    end subroutine
!******************************************************************************!
!> Add a key to a dictionary object (Character version). Simply calls the
!> FString version.
!******************************************************************************!
    subroutine numericalDictionary_setCharacterScalar(this, key, value)
        class(FNumericalDictionary), intent(inout) :: this
        character(*), intent(in) :: key
        real(e), intent(in) :: value
!------
        integer :: i
!------
        ! Get the value to add to the dictionary

        ! Make sure the key is not already defined
        do i=1, this%size()
            if (this%key(i) == key) then
                ! If it is, set the value
                this%val(i) = value
                return
            end if
        end do

        ! Increase the size of the internal arrays if needed
        this%size_ = this%size_ + 1
        if (.not.allocated(this%key_) .or. .not. allocated(this%val)) then
            call expandArrays(this)
        else
            if (this%size() > size(this%key_) .or. this%size() > size(this%val)) &
                call expandArrays(this)
        end if

        ! Set the value
        this%key_(this%size()) = key
        this%val(this%size()) = value
    end subroutine
!******************************************************************************!
!> Add a key to a dictionary object (FString version).
!******************************************************************************!
    subroutine numericalDictionary_setStringScalar(this, key, value)
        class(FNumericalDictionary), intent(inout) :: this
        type(FString), intent(in) :: key
        real(e), intent(in) :: value
!------
        call this%set(key%string(), value)
    end subroutine
!******************************************************************************!
!> Get the value for a given key (character scalar version). No error checking
!> is done. No error checking is done, if it is important, use the subroutine
!> version `getValueScalarC` instead.
!******************************************************************************!
    pure function numericalDictionary_valueScalarC(this, key) result(value)
        real(e) :: value !< `0` if the key is not defined in `this`.
        class(FNumericalDictionary), intent(in) :: this !< Dictionary to be searched
        character(*),       intent(in) :: key
!------
        integer :: i
!------
        value = 0
        do i=1, min(this%size(), size(this%val), size(this%key_))
            if (this%key_(i) == key) then
                value = this%val(i)
                exit
            end if
        end do
    end function
!******************************************************************************!
!> Get the value for a given key (FString version). No error checking is done.
!> Just calls `valueScalarC` internally. No error checking is done, if it is
!> important, use the subroutine version `getValueScalarS` instead.
!******************************************************************************!
    pure function numericalDictionary_valueScalarS(this, key) result(value)
        real(e) :: value !< `0` if the key is not defined in `this`.
        class(FNumericalDictionary), intent(in) :: this !< Dictionary to be searched
        type(FString), intent(in) :: key
!------
        value = numericalDictionary_valueScalarC(this, key%string())
    end function
!******************************************************************************!
!>Get the value for a given key.
!******************************************************************************!
    subroutine numericalDictionary_getValueScalarC(this, key, value, err)
        class(FNumericalDictionary), intent(in) :: this
        character(*), intent(in)  :: key
        real(e),      intent(out) :: value
        type(FException), intent(out), optional :: err !< Error status
!------
        integer :: i
        type(FException) :: err_
!------
        value = 0
        do i=1, this%size()
            if (this%key_(i) == key) then
                value = this%val(i)
                return
            end if
        end do

        ! Print a warning if no value has been found for the given key
        call err_%raise("The key '" // key // "' could not be found", MESSAGE_LEVEL_WARNING)
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the value for a given key (FString version). Just calls getValueScalarC
!> internally.
!******************************************************************************!
    subroutine numericalDictionary_getValueScalarS(this, key, value, err)
        class(FNumericalDictionary), intent(in) :: this
        type(FString), intent(in)  :: key
        real(e),       intent(out) :: value
        type(FException), intent(out), optional :: err !< Error status
!------
        type(FException) :: err_
!------
        call numericalDictionary_getValueScalarC(this, key%string(), value, err_)

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Get the values defined in a dictionary object.
!******************************************************************************!
    pure subroutine getValues(this, value)
        class(FNumericalDictionary), intent(in) :: this
        real(e), dimension(:), allocatable, intent(out) :: value
!------
        if (.not.allocated(this%val) .or. this%size() == 0) then
            allocate(value(0))
            return
        end if

        value = this%val(:this%size())
    end subroutine
!******************************************************************************!
!> Reallocate the internal arrays of a dictionary object to mare space
!> for more elements. For internal use only.
!******************************************************************************!
    pure subroutine expandArrays(this)
        class(FNumericalDictionary), intent(inout) :: this
!------
        type(FString), dimension(:), allocatable :: tmps ! Temporary array for the keys
        real(e),       dimension(:), allocatable :: tmpr ! Temporary array for the values
!------
!        call this%FDictionary%expandArrays
        if (.not.allocated(this%key_)) then
            allocate(this%key_(this%initialSize))
        else
            call move_alloc(from=this%key_, to=tmps)
            allocate(this%key_(size(tmps)+this%sizeIncrement))
            this%key_(:size(tmps)) = tmps
        end if

        if (.not.allocated(this%val)) then
            allocate(this%val(size(this%key_)))
        else
            allocate(tmpr(size(this%key_)))
            tmpr(:size(this%val)) = this%val(:)
            deallocate(this%val)
            call move_alloc(from=tmpr, to=this%val)
        end if
    end subroutine
!******************************************************************************!
!> Compare two FDictionary objects.
!******************************************************************************!
    elemental function dictionary_equal(this, dict) result(identical)
        logical :: identical !< .true. if they are identical, .false. otherwise.
        class(FNumericalDictionary), intent(in) :: this
        class(FNumericalDictionary), intent(in) :: dict
!------
        integer :: i
!------
        identical = .false.

        if (this%size() /= dict%size()) return
        if (allocated(this%key_) .neqv. allocated(dict%key_)) return
        if (allocated(this%val) .neqv. allocated(dict%val)) return

        if (allocated(this%val)) then
            do i=1, min(this%size(), size(this%val), size(this%key_))
                if (this%val(i) /= numericalDictionary_valueScalarS(dict, this%key_(i))) then
                    return
                end if
            end do
        end if

        identical = .true.
    end function
!******************************************************************************!
!> Compare two FDictionary objects.
!******************************************************************************!
    elemental function dictionary_differ(this, dict) result(different)
        logical :: different !< .true. if they are different, .false. otherwise.
        class(FNumericalDictionary), intent(in) :: this
        class(FNumericalDictionary), intent(in) :: dict
!------
        integer :: i
!------
        different = .true.
        if (this%size() /= dict%size()) return

        do i=1, this%size()
            if (this%val(i) /= numericalDictionary_valueScalarS(dict, this%key_(i))) then
                return
            end if
        end do

        different = .false.
    end function
!******************************************************************************!
!> Print a human-readable representation fo a FDictionary object to the
!> standard output.
!******************************************************************************!
    subroutine print(this)
        class(FNumericalDictionary), intent(in) :: this
!------
        integer :: i
!------
        write(*,'(a)') "{"

        ! Print the first key-value pair
        write(*,'(a)',advance="no") "    "//this%key(1)//" : "//this%val(1)

        ! Print the rest of the dictionary
        do i=2, this%size()
            write(*,'(a)') ","
            write(*,'(a)',advance="no") "    "//this%key(i)//" : "//this%val(i)
        end do
        write(*,*)

        write(*,'(a)') "}"
    end subroutine
end module !LCOV_EXCL_LINE
