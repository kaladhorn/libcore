    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine factor(n, nf, fac)
        implicit none
        include 'kinds.inc'
        real(rwp) :: fac(*)
        integer :: n, nf
        
        integer :: j, nl, nq, nr, ntry
        integer ntryh(4)
        data ntryh(1), ntryh(2), ntryh(3), ntryh(4)/4, 2, 3, 5/

        nl = n
        nf = 0
        j = 0
100     j = j + 1
        if (j-4) 110, 110, 120
110     ntry = ntryh(j)
        go to 130
120     ntry = ntry + 2
130     nq = nl/ntry
        nr = nl - ntry*nq
        if (nr) 100, 140, 100
140     nf = nf + 1
        fac(nf) = ntry
        nl = nq
        if (nl/=1) go to 130
    end subroutine
