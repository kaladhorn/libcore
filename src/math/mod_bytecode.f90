!******************************************************************************!
!                               mod_bytecode
!------------------------------------------------------------------------------!
!
!
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_bytecode
    use core
    use mod_ast
    use ext_character

    use mod_exception, only: FException, FExceptionDescription, MESSAGE_LEVEL_ERROR

    implicit none
    private
    save

    type, public :: FBytecode
        integer(ik),  dimension(:),   allocatable :: instruction !< List of instructions
        real(wp), dimension(:),   allocatable :: data        !< Values of the variables and intermediate results
        integer(ik),  dimension(:,:), allocatable :: argument    !< Indices of the arguments for each instruction
        integer(ik) :: start_address = 0 !< Index of the first instruction
    contains
        ! I/O
        procedure, public :: print

        ! Run the bytecode
        procedure, private :: run_simple
        procedure, private :: run_internal
        procedure, public  :: run_vector_linear
        procedure, public  :: run_vector
        generic,   public  :: run => run_simple, run_internal, run_vector_linear, run_vector

        ! Optimisation procedures
        procedure, public :: optimise
        procedure, public :: optimise_ine
        procedure, public :: optimise_gcn
        procedure, public :: optimise_fma
        procedure, public :: optimise_compact

        ! Validate a bytecode
        procedure, public :: check
    end type

    interface FBytecode
        module procedure bytecode_init
    end interface

!******************************************************************************!
!> Bytecode error description.
!******************************************************************************!
    type, public, extends(FExceptionDescription) :: FBytecodeError
        private
        integer :: address
        character(:), allocatable :: line
    end type

contains
!******************************************************************************!
!> Allocate data structures for a bytecode object.
!******************************************************************************!
    function bytecode_init(nInstructions, nConstants, nVariables) result(this)
        integer, intent(in) :: nInstructions
        integer, intent(in) :: nConstants
        integer, intent(in) :: nVariables
        type(FBytecode) :: this
!------
        ! Allocate the instruction array
        allocate(this%instruction(nInstructions+nConstants+nVariables))
        this%instruction = -1

        ! Allocate the argument address array
        allocate(this%argument(NARGS,nInstructions+nConstants+nVariables))
        this%argument = -1

        ! Allocate the data array
        allocate(this%data(nInstructions+nConstants+nVariables))
        this%data = 0
    end function
!******************************************************************************!
!> Check a bytecode object.
!******************************************************************************!
    subroutine check(this, stat)
        class(FBytecode), intent(in) :: this
        type(FException), intent(out) :: stat
!------
        integer :: a, i

        ! Make sure the arrays are allocated
        if (.not.allocated(this%instruction) .or. &
            .not.allocated(this%data) .or. &
            .not.allocated(this%argument)) then

            call stat%raise("No instructions.")
            return
        end if

        ! Check instructions
        do i=1, size(this%instruction)
            if (this%instruction(i) < -1 .or. this%instruction(i) >= CODES_COUNT) then
                call stat%raise( &
                    bytecodeError_init(this, i, "Illegal instruction."))
                return
            end if
        end do

        ! Make sure the arrays have the right size
        if (size(this%argument, 1) /= NARGS) then
            call stat%raise("Wrong number of arguments in the arguments array: " // &
                size(this%argument, 1) // " instead of  " // NARGS // ".")
            return
        end if
        if (size(this%argument, 2) /= size(this%instruction)) then
            call stat%raise("Wrong number of lines in the arguments array: " // &
                size(this%argument, 2) // " instead of  " // size(this%instruction) // ".")
            return
        end if
        if (size(this%data) /= size(this%instruction)) then
            call stat%raise("Wrong size for the data array: " // &
                size(this%data) // " instead of  " // size(this%instruction) // ".")
            return
        end if

        !Check the first address
        if (this%start_address < 1) then
            call stat%raise("Wrong code offset.")
            return
        end if

        !Make sure no instruction is defined before the first address
        do i=1, this%start_address
            if (this%instruction(i) /= CODE_NONE .and. this%instruction(i) /= CODE_INVALID) then
                call stat%raise( &
                    bytecodeError_init(this, i, "Illegal instruction in variable definition section."))
            end if
        end do

        ! Check the arguments address
        do i=1, size(this%instruction)
            do a=1, NARGS
                if (this%argument(a,i) /= -1) then
                    if (this%argument(a,i) == 0) then
                        call stat%raise( &
                            bytecodeError_init(this, i, "Unset argument address."))
                    else if (this%argument(a,i) < -1) then
                        call stat%raise( &
                            bytecodeError_init(this, i, "Negative argument address."))
                    else if (this%argument(a,i) > size(this%instruction)) then
                        call stat%raise( &
                            bytecodeError_init(this, i, "Argument address too large."))
                    end if
                end if
            end do
        end do
    end subroutine

    function getLineString(this, i) result(string)
        class(FBytecode), intent(in) :: this
        integer, intent(in) :: i
        character(:), allocatable :: string
!------
        character(1000) :: tmp
        character(:), allocatable :: instruction_symbol
!------
        if (this%instruction(i) > CODE_INVALID .and. this%instruction(i) < CODES_COUNT) then
            instruction_symbol = trim(name(this%instruction(i)))
        else
            instruction_symbol = "@" // this%instruction(i)
        end if

        if (this%argument(1,i) == -1) then
            if (this%instruction(i) == CODE_NONE) then
                write(tmp,'(a,i0,2(a,g0))') "$", i, " := ", this%data(i), "    # ", this%data(i)
            else if (this%instruction(i) == CODE_INVALID) then
                write(tmp,'(a)') "#"
            else
                write(tmp,*) "#", i, "|", this%data(i), "|", this%instruction(i), &
                    this%argument(:,i), "    # ", this%data(i)
            end if
        else
            if (this%argument(2,i) == -1) then
                write(tmp,'(2(a,i0))') "$", i, " := "// instruction_symbol // &
                    " $", this%argument(1,i)
            else
                if (this%argument(3,i) == -1) then
                    write(tmp,'(3(a,i0))') "$", i, " := $", this%argument(2,i), " " // &
                        instruction_symbol // " $", this%argument(1,i)
                else
                    write(tmp,'(4(a,i0))') "$", i, " := "// instruction_symbol // &
                        " $", this%argument(1,i), " $", this%argument(2,i), " $", &
                        this%argument(3,i)
                end if
            end if
        end if

        string = trim(tmp)
    end function
!******************************************************************************!
!> Print a representation of a bytecode object to standard output.
!******************************************************************************!
    subroutine print(this)
        class(FBytecode), intent(in) :: this
!------
        integer :: i
!------
        write(*,'(a)') "#"
        write(*,'(a,i0)') "# Instruction count: ", size(this%instruction)
        write(*,'(a,i0)') "# First instruction address: ", this%start_address
        write(*,'(a)') "#"
        write(*,'(a)') "# Data"
        do i=1, this%start_address-1
            if (this%instruction(i) == CODE_NONE) then
                write(*,'(a,i0,2(a,g0))') "$", i, " := ", this%data(i), "    # ", this%data(i)
            else if (this%instruction(i) == CODE_INVALID) then
                write(*,'(a)') "#"
            else if (this%instruction(i) == CODE_VAR) then
                write(*,'(a,i0,2(a,g0))') "$", i, " := ", this%data(i), "    # ", this%data(i)
            else if (this%instruction(i) == CODE_CONSTANT) then
                write(*,'(a,i0,2(a,g0))') "@", i, " := ", this%data(i), "    # ", this%data(i)
            else
                write(*,*) i, "|", this%data(i), "|", this%instruction(i), &
                    this%argument(:,i), "    # ", this%data(i)
            end if
        end do
        write(*,'(a)') "# Code"
        do i=this%start_address, size(this%instruction)
            if (this%argument(1,i) == -1) then
                if (this%instruction(i) == CODE_NONE) then
                    write(*,'(a,i0,2(a,g0))') "$", i, " := ", this%data(i), "    # ", this%data(i)
                else if (this%instruction(i) == CODE_INVALID) then
                    write(*,'(a)') "#"
                else
                    write(*,*) "#", i, "|", this%data(i), "|", this%instruction(i), &
                        this%argument(:,i), "    # ", this%data(i)
                end if
            else
                if (this%argument(2,i) == -1) then
                    write(*,'(2(a,i0),a,g0)') "$", i, " := "// trim(name(this%instruction(i))) // &
                        " $", this%argument(1,i), "    # ", this%data(i)
                else
                    if (this%argument(3,i) == -1) then
                        write(*,'(3(a,i0),a,g0)') "$", i, " := $", this%argument(2,i), " " // &
                            trim(name(this%instruction(i))) // " $", this%argument(1,i), &
                            "    # ", this%data(i)
                    else
                        write(*,'(4(a,i0),a,g0)') "$", i, " := "// trim(name(this%instruction(i))) // &
                            " $", this%argument(1,i), " $", this%argument(2,i), " $", &
                            this%argument(3,i), "    # ", this%data(i)
                    end if
                end if
            end if
        end do
    end subroutine
!******************************************************************************!
!> Optimise a bytecode program.
!******************************************************************************!
    subroutine optimise(this)
        class(FBytecode), intent(inout) :: this

        call this%optimise_gcn

        call this%optimise_fma

        call this%optimise_ine

        call this%optimise_compact
    end subroutine
!******************************************************************************!
!> Remove identical instructions from an instruction list.
!******************************************************************************!
    subroutine optimise_ine(this)
        class(FBytecode), intent(inout) :: this
!------
        integer :: i, j, k, changed, maxiter, n
!------

        changed = -1

        ! Set the maximum number of iterations
        maxiter = 10

        ! Attempt to optimise several times to eliminate more complex patterns
        do n=1, maxiter
            changed = 0

            ! Test every instruction in the list
            do i=1, size(this%instruction)-1

                ! Ignore invalid instructions
                if (this%instruction(i) == CODE_INVALID) cycle

                ! Look for identical instructions in the rest of the bytecode
                do j=i+1, size(this%instruction)
                    if ((this%instruction(i) == this%instruction(j))  .and. &
                        (this%argument(1,i) == this%argument(1,j)) .and. &
                        (this%argument(2,i) == this%argument(2,j)) .and. &
                        (this%argument(3,i) == this%argument(3,j)) .and. &
                        (this%data(i) == this%data(j))) then

                        ! The instruction j can be eliminated

                        ! Make sure that any instruction depending on j now uses the result from i instead
                        do k=j+1, size(this%instruction)
                            if (this%argument(1,k) == j) this%argument(1,k) = i
                            if (this%argument(2,k) == j) this%argument(2,k) = i
                            if (this%argument(3,k) == j) this%argument(3,k) = i
                        end do

                        ! Make j invalid
                        this%argument(:,j) = -1
                        this%data(j) = 0
                        this%instruction(j) = CODE_INVALID

                        changed = changed + 1
                    end if
                end do
            end do

            ! Exit if no optimisation could be done
            if (changed == 0) exit
        end do
    end subroutine
!******************************************************************************!
!> Group constants at the begining of the instructions list and shift the
!> start_address index to avoid processing them.
!******************************************************************************!
    subroutine optimise_gcn(this)
        class(FBytecode), intent(inout) :: this
!------
        integer :: i, n
        integer(ik), dimension(:), allocatable :: tmpi, newAddress
        real(wp), dimension(:), allocatable :: tmpd
        integer(ik), dimension(:,:), allocatable :: tmpa
!------
        ! Count the number of constants
        n = 0
        do i=1, size(this%instruction)
            if (this%instruction(i) == CODE_NONE) n = n + 1
        end do

        ! Do nothing if there is no constant
        if (n==0) return

        ! Allocate temporary arrays
        allocate(tmpi(size(this%instruction)))
        tmpi = CODE_INVALID
        allocate(tmpa(NARGS,size(this%instruction)))
        tmpa = -1
        allocate(tmpd(size(this%instruction)))
        tmpd = 0
        allocate(newAddress(size(this%instruction)))
        newAddress = 0

        if (this%start_address > 1) then
            ! Pack the variables at the beginning of the instruction array
            tmpi(:this%start_address-1)   = this%instruction(:this%start_address-1)
            tmpa(:,:this%start_address-1) = this%argument(:,:this%start_address-1)
            tmpd(:this%start_address-1)   = this%data(:this%start_address-1)
            do i=1, this%start_address-1
                newAddress(i) = i
            end do
        end if

        ! Pack the constants after the variables
        n = -1
        do i=this%start_address, size(this%instruction)
            if (this%instruction(i) == CODE_NONE) then
                n = n + 1

                ! Move the instruction and value
                tmpi(n+this%start_address) = this%instruction(i)
                tmpd(n+this%start_address) = this%data(i)
                newAddress(i) = n+this%start_address
            end if
        end do

        ! Transfer the other instructions
        do i=this%start_address, size(this%instruction)
            if (this%instruction(i) /= CODE_NONE) then
                n = n + 1

                ! Move the instruction, value and arguments
                tmpi(n+this%start_address) = this%instruction(i)
                tmpa(:,n+this%start_address) = this%argument(:,i)
                tmpd(n+this%start_address) = this%data(i)
                newAddress(i) = n+this%start_address
            end if
        end do

        ! Update the references
        do i=1, size(this%instruction)
            if (tmpa(1,i) > 0) tmpa(1,i) = newAddress(tmpa(1,i))
            if (tmpa(2,i) > 0) tmpa(2,i) = newAddress(tmpa(2,i))
            if (tmpa(3,i) > 0) tmpa(3,i) = newAddress(tmpa(3,i))
        end do

        ! Transfer the arrays
        deallocate(this%data)
        call move_alloc(from=tmpd, to=this%data)
        deallocate(this%instruction)
        call move_alloc(from=tmpi, to=this%instruction)
        deallocate(this%argument)
        call move_alloc(from=tmpa, to=this%argument)

        ! Set the new start address
        do i=1, size(this%instruction)
            if (this%instruction(i) == CODE_NONE) this%start_address = i
        end do
    end subroutine
!******************************************************************************!
!> Detect a+b*c patterns in the bytecode and turns them into FMA instructions.
!******************************************************************************!
    subroutine optimise_fma(this)
        class(FBytecode), intent(inout) :: this
!------
        integer :: i, j
        integer, dimension(:), allocatable :: dep
!------
        allocate(dep(size(this%instruction)))

        do i=1, size(this%instruction)
            ! Look for multiplications
            if (this%instruction(i) /= CODE_MULTIPLY) cycle

            dep = 0

            ! Look for an addition that uses the result from i
            do j=i+1, size(this%instruction)
                if (any(this%argument(:,j) == i)) then
                    if (this%instruction(j) == CODE_ADD) then
                        dep(i) = 1
                    else
                        dep(j) = -1
                    end if
                end if
            end do

            ! Look for another instruction if no addition depend on i
            if (all(dep == 0)) cycle

            ! If there are any 1 in the dep array, then some instructions can be
            ! replaced by FMAs
            if (count(dep == 1) /= 0) then
                do j=i+1, size(this%instruction)
                    if (this%instruction(j) /= CODE_ADD) cycle

                    if (this%argument(1,j) == i) then
                        this%instruction(j) = CODE_FMA
                        this%argument(1,j) = this%argument(2,j)
                        this%argument(2,j) = this%argument(2,i)
                        this%argument(3,j) = this%argument(1,i)
                    else if (this%argument(2,j) == i) then
                        this%instruction(j) = CODE_FMA
                        this%argument(1,j) = this%argument(1,j)
                        this%argument(2,j) = this%argument(2,i)
                        this%argument(3,j) = this%argument(1,i)
                    end if
                end do
            end if

            ! If there are any -1 in the dep array, then the multiplication must
            ! still be present in the optimised bytecode. Otherwise, it can be
            ! removed
            if (count(dep == -1) == 0) then
                this%instruction(i) = CODE_INVALID
                this%argument(:,i) = -1
            end if
        end do
    end subroutine
!******************************************************************************!
!> Remove invalid instructions from an instruction list.
!******************************************************************************!
    subroutine optimise_compact(this)
        class(FBytecode), intent(inout) :: this
!------
        integer :: i, n
        integer(ik), dimension(:), allocatable :: newAddress
        integer(ik), dimension(:), allocatable :: tmpi
        real(wp), dimension(:), allocatable :: tmpd
        integer(ik), dimension(:,:), allocatable :: tmpa
!------

        if (size(this%instruction) <= this%start_address) return

        ! Count the number of actual instructions
        allocate(newAddress(size(this%instruction)))
        newAddress = -1
        do i=1, this%start_address
            newAddress(i) = i
        end do
        n = this%start_address-1
        do i=this%start_address, size(this%instruction)
            if (this%instruction(i) /= CODE_INVALID) then
                n = n + 1
                newAddress(i) = n
            end if
        end do

        ! Allocate temporary arrays
        allocate(tmpi(n))
        tmpi = CODE_INVALID
        allocate(tmpd(n))
        tmpd = 0.0_e
        allocate(tmpa(NARGS,n))
        tmpa = -1

        ! Copy the data and instructions
        do i=1, this%start_address
            tmpi(i) = this%instruction(i)
            tmpd(i) = this%data(i)
        end do
        n = this%start_address-1
        do i=this%start_address, size(this%instruction)
            if (this%instruction(i) /= CODE_INVALID) then
                n = n + 1
                tmpd(n) = this%data(i)
                tmpi(n) = this%instruction(i)
                tmpa(:,n) = this%argument(:,i)
            end if
        end do

        ! Update the arguments
        do i=1, size(tmpi)
            if (tmpa(1,i) > 0) tmpa(1,i) = newAddress(tmpa(1,i))
            if (tmpa(2,i) > 0) tmpa(2,i) = newAddress(tmpa(2,i))
            if (tmpa(3,i) > 0) tmpa(3,i) = newAddress(tmpa(3,i))
        end do

        ! Transfer the arrays
        deallocate(this%data)
        call move_alloc(from=tmpd, to=this%data)
        deallocate(this%instruction)
        call move_alloc(from=tmpi, to=this%instruction)
        deallocate(this%argument)
        call move_alloc(from=tmpa, to=this%argument)
    end subroutine
!******************************************************************************!
!> Evaluate a function (subroutine version).
!******************************************************************************!
    pure subroutine run_simple(this, result, err)
        class(FBytecode), intent(inout) :: this
        real(wp), intent(out) :: result
        type(FException), intent(out), optional :: err
!------
        type(FException) :: err_
!------
        result = 0

        ! Nothing can be done if the bytecode is not compiled
        if (.not.allocated(this%instruction)) then
            call err_%raise("The expression was not compiled")
        end if

        if (err_ == 0) then

            ! Call the internal evaluation subroutine
            call run_internal(this)
            result = this%data(size(this%data))
        end if

        ! Report any issue
        if (present(err)) call err%transfer(err_)
    end subroutine
!******************************************************************************!
!> Run a bytecode executable (function version)
!******************************************************************************!
    function runf_(this) result(result)
        class(FBytecode), intent(inout) :: this
        real(wp) :: result
!------
        result = 0

        ! Nothing can be done if the expression has not been compiled
        if (.not.allocated(this%instruction)) return

        ! Call the internal evaluation subroutine
        call run_internal(this)

        result = this%data(size(this%data))
    end function
!******************************************************************************!
!> Run a bytecode executable.
!******************************************************************************!
    pure subroutine run_internal(this)
        class(FBytecode), intent(inout) :: this
!------
        integer :: i
!------
        associate(instruction => this%instruction, &
                         data => this%data, &
                     argument => this%argument, &
                start_address => this%start_address)

            if (start_address < 1 .or. .not.allocated(this%instruction)) return

            ! Process the instruction list
            do i=start_address, size(instruction)
                select case(instruction(i))
                    case (CODE_INVALID);
                    case (CODE_NONE);
                    case (CODE_ID);        data(i) =   data(argument(1,i))
                    case (CODE_MINUS);     data(i) = - data(argument(1,i))
                    case (CODE_ADD);       data(i) = data(argument(2,i)) + data(argument(1,i))
                    case (CODE_SUBSTRACT); data(i) = data(argument(2,i)) - data(argument(1,i))
                    case (CODE_MULTIPLY);  data(i) = data(argument(2,i)) * data(argument(1,i))
                    case (CODE_DIVIDE);    data(i) = data(argument(2,i)) / data(argument(1,i))
                    case (CODE_POW);       associate(x=>data(argument(1,i)), y=>data(argument(2,i)))
                                               if (y < 0) then
                                                   if (nint(x)*10 == nint(x*10)) then
                                                       data(i) = y ** nint(x)
                                                   else
                                                       data(i) = y ** x
                                                   end if
                                               else
                                                   data(i) = y ** x
                                               end if
                                           end associate
                    case (CODE_POW_INT);   data(i) = data(argument(2,i)) ** nint(data(argument(1,i)))
                    case (CODE_ABS);       data(i) = abs(data(argument(1,i)))
                    case (CODE_EXP);       data(i) = exp(data(argument(1,i)))
                    case (CODE_LN);        data(i) = log(data(argument(1,i)))
                    case (CODE_LOG10);     data(i) = log10(data(argument(1,i)))
                    case (CODE_SQRT);      data(i) = sqrt(data(argument(1,i)))
                    case (CODE_ERF);       data(i) = erf(data(argument(1,i)))
                    case (CODE_ERFC);      data(i) = erfc(data(argument(1,i)))
                    case (CODE_SIN);       data(i) = sin(data(argument(1,i)))
                    case (CODE_COS);       data(i) = cos(data(argument(1,i)))
                    case (CODE_TAN);       data(i) = tan(data(argument(1,i)))
                    case (CODE_ASIN);      data(i) = asin(data(argument(1,i)))
                    case (CODE_ACOS);      data(i) = acos(data(argument(1,i)))
                    case (CODE_ATAN);      data(i) = atan(data(argument(1,i)))
                    case (CODE_SINH);      data(i) = sinh(data(argument(1,i)))
                    case (CODE_COSH);      data(i) = cosh(data(argument(1,i)))
                    case (CODE_TANH);      data(i) = tanh(data(argument(1,i)))
                    case (CODE_ASINH);     data(i) = asinh(data(argument(1,i)))
                    case (CODE_ACOSH);     data(i) = acosh(data(argument(1,i)))
                    case (CODE_ATANH);     data(i) = atanh(data(argument(1,i)))
                    case (CODE_FMA);       data(i) = data(argument(3,i))*data(argument(2,i))+data(argument(1,i))
                    case (CODE_EQUAL);     if (data(argument(2,i)) == data(argument(1,i))) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                   case (CODE_DIFFERENT);  if (data(argument(2,i)) /= data(argument(1,i))) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                    case (CODE_LESS);      if (data(argument(2,i)) < data(argument(1,i))) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                    case (CODE_GREATER);   if (data(argument(2,i)) > data(argument(1,i))) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                    case (CODE_LESS_EQ);   if (data(argument(2,i)) <= data(argument(1,i))) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                    case (CODE_GREAT_EQ);  if (data(argument(2,i)) >= data(argument(1,i))) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                    case (CODE_NOT);       if (data(argument(1,i)) == 0) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                    case (CODE_AND);       if (data(argument(2,i))==1 .and. data(argument(1,i))==1) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                    case (CODE_OR);        if (data(argument(2,i))==1 .or. data(argument(1,i))==1) then
                                               data(i) = 1.0_e
                                           else
                                               data(i) = 0.0_e
                                           end if
                    !@todo un-comment the error stop once all the supported compilers allow error stop in pure procedures
                    case default; !error stop
                end select
            end do
        end associate
    end subroutine
!******************************************************************************!
!> Evaluate a function. This version accepts values for one independent
!> variable.
!>
!> The expression is evaluated once for each input value, and the
!> results are stored in the output array. This subroutine is designed to have
!> better performances than the scalar versions.
!>
!> The optional parameter `blocksize` controls the number of iterations in
!> loops in the subroutine, which can affect performance.
!******************************************************************************!
    subroutine run_vector_linear(this, variable_index, x0, N, dx, result, blockSize, stat)
        class(FBytecode), intent(inout) :: this
        integer,  intent(in) :: variable_index
        real(wp), intent(in) :: x0
        integer,  intent(in) :: N
        real(wp), intent(in) :: dx
        real(wp), dimension(:), allocatable, intent(out) :: result
        integer, intent(in), optional :: blockSize
        type(FException), intent(out), optional :: stat
!------
        integer :: chunk_size, n_chunks
        real(wp), dimension(:,:), allocatable :: data, data_common
        integer :: i, a, c, d, l
        type(FException) :: stat_
!------
        body: block
            associate(instruction => this%instruction, &
                    start_address => this%start_address)

                ! There is a bug somewhere if the expression has not been compiled
                call assert(allocated(this%instruction))

                ! Allocate the result array
                allocate(result(N))
                result = 0

                ! Make sure all the other variables are defined
                ! Get the chunk size
                if (present(blockSize)) then
                    chunk_size = blockSize
                else
                    chunk_size = 48
                end if
                n_chunks = N/chunk_size

                ! Allocate the data array
                allocate(data(chunk_size,size(this%data)))
                data = 0

                allocate(data_common(chunk_size,size(this%data)))
                data_common = 0
                do i=1, this%start_address
                    data_common(:,i) = this%data(i)
                end do
                l = chunk_size

                do c=1, n_chunks

                    ! Set the data array
                    data = 0
                    do i=1, size(this%instruction)
                        data(:,i) = this%data(i)
                    end do
                    do d=1, chunk_size
                        data(d,variable_index) = x0 + dx*((c-1)*chunk_size+(d-1))
                    end do

                    ! Process the instruction list
                    do i=this%start_address, size(this%instruction)
                        associate(a1=>this%argument(1,i), &
                            a2=>this%argument(2,i), &
                            a3=>this%argument(3,i))

                            include 'math/bytecode_run_kernel.inc'
                        end associate
                    end do

                    ! Save the results
                    do d=1, chunk_size
                        a = (c-1)*chunk_size + d
                        result(a) = data(d,size(data,2))
                    end do
                end do

                ! Process the last incomplete chunk
                l = N - n_chunks*chunk_size

                ! Nothing to do if there are no more iterations
                if (l == 0) exit body

                ! Set the data array
                data = 0
                do i=1, size(this%instruction)
                    data(:,i) = this%data(i)
                end do
                do d=1, l
                    data(d,variable_index) = x0 + dx*((c-1)*chunk_size+(d-1))
                end do

                ! Process the instruction list
                do i=this%start_address, size(this%instruction)
                    associate(a1=>this%argument(1,i), &
                        a2=>this%argument(2,i), &
                        a3=>this%argument(3,i))

                        include 'math/bytecode_run_kernel.inc'
                    end associate
                end do

                ! Save the results
                do d=1, l
                    a = n_chunks*chunk_size + d
                    result(a) = data(d,size(data,2))
                end do
            end associate
        end block body

        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Evaluate a function. This version accepts values for one independent
!> variable. The expression is evaluated once for each input value, and the
!> results are stored in the output array. This subroutine is designed to have
!> better performances than the scalar versions.
!>
!> The optional parameter `blocksize` controls the number of iterations in
!> loops in the subroutine, which can affect performance.
!******************************************************************************!
    subroutine run_vector(this, variable_index, x, result, blockSize, stat)
        class(FBytecode),       intent(inout) :: this !< Expression to evaluate
        integer,                intent(in)    :: variable_index !< Name of the independent variable
        real(wp), dimension(:), intent(in)    :: x    !< Values of the independent variable
        real(wp), dimension(:), allocatable, intent(out) :: result !< Results
        integer, intent(in), optional :: blockSize
        type(FException), intent(out), optional :: stat !< Error status
!------
        integer :: chunk_size, n_chunks
        real(wp), dimension(:,:), allocatable :: data
        integer :: i, a, c, d, l
        type(FException) :: stat_
!------
        body: block
            associate(instruction => this%instruction, &
                    start_address => this%start_address)

                ! There is a bug somewhere if the expression has not been compiled
                call assert(allocated(this%instruction))

                ! Allocate the result array
                allocate(result(size(x)))
                result = 0

                ! Get the chunk size
                if (present(blockSize)) then
                    chunk_size = blockSize
                else
                    chunk_size = 48
                end if
                n_chunks = size(x)/chunk_size

                ! Allocate the data array
                allocate(data(chunk_size,size(this%data)))
                l = chunk_size

                do c=1, n_chunks

                    ! Set the data array
                    data = 0
                    do i=1, size(this%instruction)
                        data(:,i) = this%data(i)
                    end do
                    do d=1, chunk_size
                        a = (c-1)*chunk_size + d
                        data(d,variable_index) = x(a)
                    end do

                    ! Process the instruction list
                    do i=this%start_address, size(this%instruction)
                        associate(a1=>this%argument(1,i), &
                            a2=>this%argument(2,i), &
                            a3=>this%argument(3,i))

                            include 'math/bytecode_run_kernel.inc'
                        end associate
                    end do

                    ! Save the results
                    do d=1, chunk_size
                        a = (c-1)*chunk_size + d
                        result(a) = data(d,size(data,2))
                    end do
                end do

                ! Process the last incomplete chunk
                l = size(x) - n_chunks*chunk_size

                ! Nothing to do if there are no more iterations
                if (l == 0) exit body

                ! Set the data array
                data = 0
                do i=1, size(this%instruction)
                    data(:,i) = this%data(i)
                end do
                do d=1, l
                    a = n_chunks*chunk_size + d
                    data(d,variable_index) = x(a)
                end do

                ! Process the instruction list
                do i=this%start_address, size(this%instruction)
                    associate(a1=>this%argument(1,i), &
                        a2=>this%argument(2,i), &
                        a3=>this%argument(3,i))

                        include 'math/bytecode_run_kernel.inc'
                    end associate
                end do

                ! Save the results
                do d=1, l
                    a = n_chunks*chunk_size + d
                    result(a) = data(d,size(data,2))
                end do
            end associate
        end block body

        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Set up a file error object based on a file and a user-provided message.
!******************************************************************************!
    function bytecodeError_init(code, address, message, level) result(this)
        type(FBytecode), intent(in) :: code
        integer, intent(in) :: address
        character(*), intent(in) :: message
        integer, intent(in), optional :: level
        type(FBytecodeError) :: this
!------
        character(:), allocatable :: context
        integer :: ilevel
!------
        ! Get the context
        if (address > 0 .and. address < size(code%instruction)) then
            context = bold("In bytecode:" // address // ":") // new_line("") // new_line("") // &
                "    " // getLineString(code, address) // new_line("")
        else
            context = ""
        end if

        ! Set the message level
        if (present(level)) then
            ilevel = level
        else
            ilevel = MESSAGE_LEVEL_ERROR
        end if

        ! Call the super-class' constructor
        this%FExceptionDescription = &
            FExceptionDescription(message=message, &
                                  context=context, &
                                    level=ilevel)
    end function
end module
