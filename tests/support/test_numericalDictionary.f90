module test_numericalDictionary
    use core
    use mod_test
    use ext_character

    use config,         only: TEST_PATH
    use mod_string,     only: FString
    use mod_exception,  only: FException
    use mod_numericalDictionary, only: FNumericalDictionary

    implicit none
    private

    public :: testAll
contains
    subroutine testAll
        call testInit
        call testEmpty
        call testRealloc
        call testCompare
        call testAccessors
        call testArraySetters
        call testPrint
        call testsFinished
    end subroutine

    subroutine testPrint
        type(FNumericalDictionary) :: dict

        dict = FNumericalDictionary([FString("foo"), FString("bar")], [1.0_e, 2.0_e])
        call dict%print
    end subroutine

    subroutine testInit
        type(FNumericalDictionary) :: dict, dict3, dict4
        type(FString), dimension(:), allocatable :: keys, test_keys
        real(wp), dimension(:), allocatable :: values, test_values
        character(3), dimension(:), allocatable :: keys_char
        type(FException) :: stat
        type(FString) :: k2

        call startTestGroup("constructors")

        ! Set the test dictionary object
        keys = [FString("foo"), FString("bar")]
        keys_char = ["foo", "bar"]

        values = [1.0_e]
        dict = FNumericalDictionary(keys, values, stat)
        call check(stat=="keys and values arrays do not have the same size (2 and 1).", &
            "size mismatch detection", "error: " // stat%string())
        call stat%discard

        values = [1.0_e, 2.0_e]
        dict = FNumericalDictionary(keys, values)

        call check(dict%size()==2, "dictionary size", &
            "wrong dictionary size: " // dict%size() // ", should be 2")

        call check(dict%value("foo") == 1, "value getter", &
            "wrong value for key foo: " // dict%value("foo"))

        ! Test the character array setter
        call dict3%set(keys_char, values)
        call check(dict == dict3, "character array setter", &
            "wrong result for the character array setter")

        ! Test the string array setter
        call dict4%set(keys, values)
        call check(dict == dict4, "string array setter", &
            "wrong result for the string array setter")

        ! Check the keys array
        call dict%getKeys(test_keys)
        call check(all(test_keys == keys), "keys array getter", &
            "wrong keys array")

        ! Check the values array
        call dict%getValues(test_values)
        call check(all(test_values == values), "values array getter", &
            "wrong values array")

        ! Set a key with no value (character version)
        call dict%set("baz")
        call check(dict%value("baz") == 0, "character value getter", &
            "wrong value for key baz: " // dict%value("baz"))

        ! Set a key with no value (string version)
        call dict%set(FString("baz"))
        call check(dict%value("baz") == 0, "string value getter", &
            "wrong value for key baz: " // dict%value("baz"))

        call dict%getKey(1, k2)
        call check(k2=="foo", "key access by index", &
            "wrong key: """ // k2 // """ instead of ""foo"".")
        call check(dict%key(1)=="foo", "key access by index (function)", &
            "wrong key: """ // dict%key(1) // """ instead of ""foo"".")
        call check(dict%isSet("foo"), "key set accessor (character)", &
            "the key ""foo"" is not set.")
        call check(dict%isSet(FString("foo")), "key set accessor (string)", &
            "the key ""foo"" is not set.")

    end subroutine

    subroutine testEmpty
        type(FNumericalDictionary) :: dict
        type(FString), dimension(:), allocatable :: keys
        real(wp), dimension(:), allocatable :: values

        call startTestGroup("empty dictionary")

        ! Test an empty dictionary
        call dict%getValues(values)
        call check(size(values) == 0, "values array", "wrong values array")
        call dict%getKeys(keys)
        call check(size(keys) == 0, "keys array", "wrong keys array")
    end subroutine

    subroutine testCompare
        type(FNumericalDictionary) :: dict, dict2
        type(FString), dimension(:), allocatable :: keys
        real(wp), dimension(:), allocatable :: values

        call startTestGroup("comparisons")

        ! Set the test dictionary object
        keys = [FString("foo"), FString("bar")]
        values = [1.0_e, 2.0_e]
        dict = FNumericalDictionary(keys, values)

        dict2 = dict
        call check(dict2 == dict, "== comparison", &
            "wrong comparison result:" // (dict2 == dict) // ".")
        call check(.not. (dict2 /= dict), "inverse /= comparison", &
            "wrong comparison result:" // (dict2 /= dict) // ".")

        call dict2%set("foo", 2.0_e)
        call check(dict2 /= dict, "/= comparison", &
            "wrong comparison result:" // (dict2 /= dict) // ".")
        call check(.not. (dict2 == dict), "inverse == comparison", &
            "wrong comparison result:" // (dict2 == dict))
    end subroutine

    subroutine testArraySetters
        type(FNumericalDictionary) :: dict, dict2
        type(FString), dimension(:), allocatable :: keys
        real(wp), dimension(:), allocatable :: values
        character(3), dimension(:), allocatable :: keys_char

        ! Set the test dictionary object
        keys = [FString("foo"), FString("bar")]
        keys_char = ["foo", "bar"]
        values = [1.0_e, 2.0_e]

        call dict%set(keys)
        call dict2%set(keys_char)
        call assert(dict == dict2, "wrong result for array setters without values")
    end subroutine

    subroutine testRealloc
        type(FNumericalDictionary) :: dict
        integer :: i

        ! Test the automatic reallocation of the internal arrays
        do i=1, 11
            call dict%set("v"//i, 1.0_wp * i)
            call assert(dict%value("v"//i) == 1.0_wp * i, "wrong value")
        end do
    end subroutine

    subroutine testAccessors
        type(FNumericalDictionary) :: dict
        type(FException) :: stat
        real(wp) :: x, x1, x2, x3, x4

        ! Set the test dictionary object
        call random_number(x)
        call dict%set(FString("foo"), x)

        x1 = dict%value(FString("foo"))
        call assert(x == x1, "wrong result from accessor 1")

        call dict%getValue(FString("foo"), x2)
        call assert(x == x2, "wrong result from accessor 2")

        x3 = dict%value("foo")
        call assert(x == x3, "wrong result from accessor 3")

        call dict%getValue("foo", x4)
        call assert(x == x4, "wrong result from accessor 4")

        call dict%getValue("baz", x, stat)
        call assert(stat /= 0, "wrong unset value (1)")
        call stat%discard
        call dict%getValue(FString("baz"), x, stat)
        call assert(stat /= 0, "wrong unset value (2)")
        call stat%discard
    end subroutine

end module

program test
    use test_numericalDictionary

    call testAll
end program