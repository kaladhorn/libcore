module test_timer
    use core
    use mod_test
    use mod_timer, only: FTimer
    
    implicit none
    public
    save
contains
    subroutine testAll
        call testCpuTime
        call testWallTime
    end subroutine
    
    subroutine testCpuTime
        type(FTimer) :: t
        real(e) :: cpuTime
        
        call startTestGroup("CPU time measurements")
        call t%start
        call execute_command_line("sleep 0.1")
        call t%stop
        call check(t%cpuTime()<.001_e, "simple CPU time test (function)")
        call t%getCputime(cpuTime)
        call check(t%cpuTime()==cpuTime, "simple CPU time test (subroutine)")
        call t%reset
        call check(t%cpuTime()==0.0_e, "timer reset")
    end subroutine
    
    subroutine testWallTime
        type(FTimer) :: t
        real(e) :: walltime
        
        call startTestGroup("walltime measurements")
        
        call t%start
        call execute_command_line("sleep 0.1")
        call t%stop
        call check(t%wallTime()>0.1_e, "simple walltime test (function)")
        call t%getWalltime(walltime)
        call check(t%wallTime()==walltime, "simple walltime test (subroutine)")
        call t%reset
        call check(t%wallTime()==0.0_e, "timer reset")
    end subroutine
end module

program test
    use test_timer
    use mod_test
    
    call testAll
    
    call testsFinished
end program