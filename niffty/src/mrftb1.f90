    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mrftb1(m, im, n, in, c, ch, wa, fac)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) ch(m, *), c(in, *), wa(n), fac(15)

        nf = fac(2)
        na = 0
        do k1 = 1, nf
            ip = fac(k1+2)
            na = 1 - na
            if (ip<=5) go to 100
            if (k1==nf) go to 100
            na = 1 - na
100     end do
        half = .5_rwp
        halfm = -.5
        modn = mod(n, 2)
        nl = n - 2
        if (modn/=0) nl = n - 1
        if (na==0) go to 110
        m2 = 1 - im
        do i = 1, m
            m2 = m2 + im
            ch(i, 1) = c(m2, 1)
            ch(i, n) = c(m2, n)
        end do
        do j = 2, nl, 2
            m2 = 1 - im
            do i = 1, m
                m2 = m2 + im
                ch(i, j) = half*c(m2, j)
                ch(i, j+1) = halfm*c(m2, j+1)
            end do
        end do
        go to 120
110     continue
        do j = 2, nl, 2
            m2 = 1 - im
            do i = 1, m
                m2 = m2 + im
                c(m2, j) = half*c(m2, j)
                c(m2, j+1) = halfm*c(m2, j+1)
            end do
        end do
120     l1 = 1
        iw = 1
        do k1 = 1, nf
            ip = fac(k1+2)
            l2 = ip*l1
            ido = n/l2
            idl1 = ido*l1
            if (ip/=4) go to 150
            ix2 = iw + ido
            ix3 = ix2 + ido
            if (na/=0) go to 130
            call mradb4(m, ido, l1, c, im, in, ch, 1, m, wa(iw), wa(ix2), &
              wa(ix3))
            go to 140
130         call mradb4(m, ido, l1, ch, 1, m, c, im, in, wa(iw), wa(ix2), &
              wa(ix3))
140         na = 1 - na
            go to 270
150         if (ip/=2) go to 180
            if (na/=0) go to 160
            call mradb2(m, ido, l1, c, im, in, ch, 1, m, wa(iw))
            go to 170
160         call mradb2(m, ido, l1, ch, 1, m, c, im, in, wa(iw))
170         na = 1 - na
            go to 270
180         if (ip/=3) go to 210
            ix2 = iw + ido
            if (na/=0) go to 190
            call mradb3(m, ido, l1, c, im, in, ch, 1, m, wa(iw), wa(ix2))
            go to 200
190         call mradb3(m, ido, l1, ch, 1, m, c, im, in, wa(iw), wa(ix2))
200         na = 1 - na
            go to 270
210         if (ip/=5) go to 240
            ix2 = iw + ido
            ix3 = ix2 + ido
            ix4 = ix3 + ido
            if (na/=0) go to 220
            call mradb5(m, ido, l1, c, im, in, ch, 1, m, wa(iw), wa(ix2), &
              wa(ix3), wa(ix4))
            go to 230
220         call mradb5(m, ido, l1, ch, 1, m, c, im, in, wa(iw), wa(ix2), &
              wa(ix3), wa(ix4))
230         na = 1 - na
            go to 270
240         if (na/=0) go to 250
            call mradbg(m, ido, ip, l1, idl1, c, c, c, im, in, ch, ch, 1, m, &
              wa(iw))
            go to 260
250         call mradbg(m, ido, ip, l1, idl1, ch, ch, ch, 1, m, c, c, im, in, &
              wa(iw))
260         if (ido==1) na = 1 - na
270         l1 = l2
            iw = iw + (ip-1)*ido
        end do
    end subroutine
