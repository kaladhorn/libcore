!******************************************************************************!
!                           com_fft module
!------------------------------------------------------------------------------!
!> Wrappers around FFTW-compatible Fourier transform procedures
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module com_fft
    use core
    use iso_c_binding
    implicit none
    private
    save
    include 'fftw3.f03'

    public :: fft_1d_rc
    public :: fft_1d_cc
    public :: ifft_1d_cc
contains
!******************************************************************************!
!> 1-dimensional real to complex FFT
!******************************************************************************!
    subroutine fft_1d_rc(f, g)
        real(e), dimension(:), intent(in) :: f
        complex(c_double_complex), dimension(:), allocatable, intent(out) :: g
!------
        type(c_ptr) :: plan
        real(e), dimension(:), allocatable :: tmp
        complex(c_double_complex), dimension(:), allocatable :: out
        integer :: i, l, n, unit, s
!------

        tmp = f
        allocate(out(size(tmp)/2+1))
        out = 0

        plan = fftw_plan_dft_r2c_1d(size(tmp), tmp, out, FFTW_ESTIMATE)
        call fftw_execute_dft_r2c(plan, tmp, out)
        call fftw_destroy_plan(plan)

!        open(newunit=unit, file="raw_fft.dat")
!        do i=1, size(out)
!            write(unit,*) i, abs(out(i)), real(out(i), e), aimag(out(i))
!        end do
!        close(unit)

        n = size(f)

        if (nint(n*1.0_e/2) == n*1.0_e/2) then
            ! n is even
            l = n / 2 - 1
            s = n / 2
        else
            ! n is odd
            l = (n-1)/2
            s = (n-1)/2
        end if

        allocate(g(n))
        g = 5

        do i=1, s
            g(l+i) = conjg(out(i))
        end do
        g(n) = conjg(out(s+1))
        do i=1, l
            g(i) = out(s-i+1)
        end do

        g = g / sqrt(TWOPI)
    end subroutine
!******************************************************************************!
!> 1-dimensional complex to complex FFT
!******************************************************************************!
    subroutine fft_1d_cc(f, g)
        complex(c_double_complex), dimension(:), intent(in) :: f
        complex(c_double_complex), dimension(:), allocatable, intent(out) :: g
!------
        type(c_ptr) :: plan
        complex(c_double_complex), dimension(:), allocatable :: tmp, out
        integer :: i, l, n, unit, s, j
!------

        n = size(f)

        tmp = f
        allocate(out(n))
        out = 0

        plan = fftw_plan_dft_1d(size(tmp), tmp, out, FFTW_FORWARD, FFTW_ESTIMATE)
        call fftw_execute_dft(plan, tmp, out)
        call fftw_destroy_plan(plan)

!        open(newunit=unit, file="raw_cfft.dat")
!        do i=1, size(out)
!            write(unit,*) i, abs(out(i)), real(out(i), e), aimag(out(i))
!        end do
!        close(unit)

        if (nint(n*1.0_e/2) == n*1.0_e/2) then
            ! n is even
            l = n / 2 - 1
            s = n / 2
        else
            ! n is odd
            l = (n-1)/2
            s = (n-1)/2
        end if

        allocate(g(n))
        g = 5

        do i=1, s
            g(l+i) = conjg(out(i))
        end do
        g(n) = conjg(out(s+1))
        do i=1, l
            g(i) = conjg(out(s+i+1))
        end do

        g = g / sqrt(TWOPI)
    end subroutine
!******************************************************************************!
!> 1-dimensional complex to complex inverse FFT
!******************************************************************************!
    subroutine ifft_1d_cc(g, f)
        complex(c_double_complex), dimension(:), intent(in) :: g
        complex(c_double_complex), dimension(:), allocatable, intent(out) :: f
!------
        type(c_ptr) :: plan
        complex(c_double_complex), dimension(:), allocatable :: tmp
        integer :: n, i, l, unit, s
!------

        n = size(g)
        if (nint(n*1.0_e/2) == n*1.0_e/2) then
            ! n is even
            l = n / 2 - 1
            s = n / 2
        else
            ! n is odd
            l = (n-1)/2
            s = (n-1)/2
        end if

        allocate(tmp(n))
        tmp = 2
        do i=1, (n-s)
            tmp(i) = conjg(g(s+i))
        end do
        do i=1, s
            tmp(n-s+i) = conjg(g(i))
        end do
        tmp = tmp * sqrt(TWOPI)

!        open(newunit=unit, file="raw_cifft.dat")
!        do i=1, size(tmp)
!            write(unit,*) i, abs(tmp(i)), real(tmp(i), e), aimag(tmp(i))
!        end do
!        close(unit)

        allocate(f(n))
        f = 0
        plan = fftw_plan_dft_1d(size(tmp), tmp, f, FFTW_BACKWARD, FFTW_ESTIMATE)
        call fftw_execute_dft(plan, tmp, f)
        call fftw_destroy_plan(plan)
        f = f / n
    end subroutine

    subroutine com_fft_is_fftw() bind(C)
    end subroutine
end module
