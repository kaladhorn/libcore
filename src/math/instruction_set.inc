!******************************************************************************!
!                         instruction_set include file
!______________________________________________________________________________!
!> Basic definitions of the instruction set used in mode_bytecode.
!>
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!

    integer, parameter, public :: NARGS          = 3   !< Number of arguments per instruction

    integer, parameter, public :: INSTRUCTION_KIND = kind(1)
    integer, parameter, public :: ik = INSTRUCTION_KIND

!------ Instructions
    integer(ik), parameter, public :: CODE_INVALID   = -1  !< Invalid instruction
    integer(ik), parameter, public :: CODE_NONE      = 0   !< No-op (also used for constants)
!------ Data definitions
    integer(ik), parameter, public :: CODE_VAR       = 1   ! Variable definition
    integer(ik), parameter, public :: CODE_CONSTANT  = 2   ! Constant definition
!------ Mathematical standard operators
    integer(ik), parameter, public :: CODE_ID        = 3   !< Unary plus
    integer(ik), parameter, public :: CODE_MINUS     = 4   !< Unary minus
    integer(ik), parameter, public :: CODE_ADD       = 5   !< Addition
    integer(ik), parameter, public :: CODE_SUBSTRACT = 6   !< Substraction
    integer(ik), parameter, public :: CODE_MULTIPLY  = 7   !< Multiplication
    integer(ik), parameter, public :: CODE_DIVIDE    = 8   !< Division
    integer(ik), parameter, public :: CODE_POW       = 9   !< Power
    integer(ik), parameter, public :: CODE_POW_INT   = 10  !< Integer power (allows to calculate x**y when x<0 and y is integer)
!------ Mathematical standard functions
    integer(ik), parameter, public :: CODE_ABS       = 11  !< Absolute value
    integer(ik), parameter, public :: CODE_EXP       = 12  !< Exponential
    integer(ik), parameter, public :: CODE_LN        = 13  !< Natural logarithm
    integer(ik), parameter, public :: CODE_LOG10     = 14  !< Decimal logarithm
    integer(ik), parameter, public :: CODE_SQRT      = 15  !< Square root
    integer(ik), parameter, public :: CODE_ERF       = 16  !< Error function
    integer(ik), parameter, public :: CODE_ERFC      = 17  !< Complementary error function
    integer(ik), parameter, public :: CODE_SIN       = 18  !< Sine
    integer(ik), parameter, public :: CODE_COS       = 19  !< Cosine
    integer(ik), parameter, public :: CODE_TAN       = 20  !< Tangent
    integer(ik), parameter, public :: CODE_ASIN      = 21  !< Arc sine
    integer(ik), parameter, public :: CODE_ACOS      = 22  !< Arc cosine
    integer(ik), parameter, public :: CODE_ATAN      = 23  !< Arc tangent
    integer(ik), parameter, public :: CODE_SINH      = 24  !< Hyperbolic sine
    integer(ik), parameter, public :: CODE_COSH      = 25  !< Hyperbolic cosine
    integer(ik), parameter, public :: CODE_TANH      = 26  !< Hyperbolic tangent
    integer(ik), parameter, public :: CODE_ASINH     = 27  !< Inverse hyperbolic sine
    integer(ik), parameter, public :: CODE_ACOSH     = 28  !< Inverse hyperbolic cosine
    integer(ik), parameter, public :: CODE_ATANH     = 29  !< Inverse hyperbolic tangent
    integer(ik), parameter, public :: CODE_FMA       = 30  !< a*b+c
    integer(ik), parameter, public :: CODE_FMA_END   = 31  !<
!------ Flow control instructions
    integer(ik), parameter, public :: CODE_JUMP      = 32  !< Unconditional jump
    integer(ik), parameter, public :: CODE_IFJUMP    = 33  !< Conditional jump
!------ Comparison operators
    integer(ik), parameter, public :: CODE_EQUAL     = 34  !< == comparison
    integer(ik), parameter, public :: CODE_DIFFERENT = 35  !< /= comparison
    integer(ik), parameter, public :: CODE_LESS      = 36  !< < comparison
    integer(ik), parameter, public :: CODE_GREATER   = 37  !< > comparison
    integer(ik), parameter, public :: CODE_LESS_EQ   = 38  !< <= comparison
    integer(ik), parameter, public :: CODE_GREAT_EQ  = 39  !< >= comparison
!------ Logical operators
    integer(ik), parameter, public :: CODE_NOT       = 40  !< logical not
    integer(ik), parameter, public :: CODE_AND       = 41  !< logical and
    integer(ik), parameter, public :: CODE_OR        = 42  !< logical or
!------ End of instruction codes
    integer(ik), parameter, public :: CODES_COUNT    = 43  !< Number of instructions

    !< Instructions names for human-friendly display
    character(*), dimension(-1:CODES_COUNT-1), parameter, public :: name = [ &
        "INV  ", "NOP  ", "VAR  ", "CONST", &
		"+    ", "-    ", "+    ", "-    ", &
		"*    ", "/    ", "^    ", "^    ", &
		"abs  ", "exp  ", "log  ", "log10", &
		"sqrt ", "erf  ", "erfc ", "sin  ", &
		"cos  ", "tan  ", "asin ", "acos ", &
		"atan ", "sinh ", "cosh ", "tanh ", &
		"asinh", "acosh", "atanh", "fma  ", &
		"fma_ ", "jump ", "ifjmp", "==   ", &
		"/=   ", "<    ", ">    ", "<=   ", &
		">=   ", "!    ", "&    ", "|    " &
    ]

    !< Operator symbols for human-friendly display
    character(*), dimension(-1:CODES_COUNT-1), parameter, public :: symbol = [ &
        "  ", "  ", "  ", "  ", &
		"  ", "- ", "+ ", "- ", &
		"* ", "/ ", "**", "**", &
		"  ", "  ", "  ", "  ", &
		"  ", "  ", "  ", "  ", &
		"  ", "  ", "  ", "  ", &
		"  ", "  ", "  ", "  ", &
		"  ", "  ", "  ", "  ", &
		"  ", "  ", "  ", "==", &
		"/=", "< ", "> ", "<=", &
		">=", "  ", "& ", "| " &
    ]

    !< Left-hand side symbols for human-friendly display
    character(*), dimension(-1:CODES_COUNT-1), parameter, public :: leftSymbol = [ &
        "     ", "     ", "     ", "     ", &
		"     ", "     ", "     ", "     ", &
		"     ", "     ", "     ", "     ", &
		"|    ", "exp  ", "ln   ", "log10", &
		"sqrt ", "erf  ", "erfc ", "sin  ", &
		"cos  ", "tan  ", "asin ", "acos ", &
		"atan ", "sinh ", "cosh ", "tanh ", &
		"asinh", "acosh", "atanh", "     ", &
		"     ", "     ", "     ", "     ", &
		"     ", "     ", "     ", "     ", &
		"     ", "!    ", "     ", "     " &
    ]

    !< Right-hand side symbols for human-friendly display
    character(*), dimension(-1:CODES_COUNT-1), parameter, public :: rightSymbol = [ &
        " ", " ", " ", " ", &
		" ", " ", " ", " ", &
		" ", " ", " ", " ", &
		"|", " ", " ", " ", &
		" ", " ", " ", " ", &
		" ", " ", " ", " ", &
		" ", " ", " ", " ", &
		" ", " ", " ", " ", &
		" ", " ", " ", " ", &
		" ", " ", " ", " ", &
		" ", " ", " ", " " &
    ]
