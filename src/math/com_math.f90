!******************************************************************************!
!                           com_math module
!______________________________________________________________________________!
!> Misc math-related procedures.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module com_math
    use iso_c_binding
    use core

    implicit none
    private
    save

    !> Identity matrix
    real(e), dimension(3,3), parameter :: id3 = reshape([1,0,0,0,1,0,0,0,1],[3,3])
    public :: id3

!******************************************************************************!
!> Matrix inversion
!******************************************************************************!
    interface operator(.i.)
        module procedure matinv
    end interface
    public :: operator(.i.)
!******************************************************************************!
!> Moore-Penrose pseudoinvert matrix
!******************************************************************************!
    interface operator(.mpi.)
        module procedure moorePenrose
    end interface
    public :: operator(.mpi.)

    public :: tens_product
    public :: normalize
    public :: rotationMatrix
    public :: eigenvalues
    public :: cross_product
    public :: det

    public :: reduce
    public :: inverse

contains
!******************************************************************************!
!> Simplify a matrix using the Voigt convention.
!******************************************************************************!
    pure function reduce(A) result(B)
        real(e), dimension(3,3,3,3), intent(in) :: A
        real(e), dimension(6,6) :: B
!------
        B(1,1) = A(1,1,1,1)
        B(1,2) = A(1,1,2,2)
        B(1,3) = A(1,1,3,3)
        B(1,4) = A(1,1,2,3)
        B(1,5) = A(1,1,3,1)
        B(1,6) = A(1,1,1,2)

        B(2,1) = A(2,2,1,1)
        B(2,2) = A(2,2,2,2)
        B(2,3) = A(2,2,3,3)
        B(2,4) = A(2,2,2,3)
        B(2,5) = A(2,2,3,1)
        B(2,6) = A(2,2,1,2)

        B(3,1) = A(3,3,1,1)
        B(3,2) = A(3,3,2,2)
        B(3,3) = A(3,3,3,3)
        B(3,4) = A(3,3,2,3)
        B(3,5) = A(3,3,3,1)
        B(3,6) = A(3,3,1,2)

        B(4,1) = A(2,3,1,1)
        B(4,2) = A(2,3,2,2)
        B(4,3) = A(2,3,3,3)
        B(4,4) = A(2,3,2,3)
        B(4,5) = A(2,3,3,1)
        B(4,6) = A(2,3,1,2)

        B(5,1) = A(3,1,1,1)
        B(5,2) = A(3,1,2,2)
        B(5,3) = A(3,1,3,3)
        B(5,4) = A(3,1,2,3)
        B(5,5) = A(3,1,3,1)
        B(5,6) = A(3,1,1,2)

        B(6,1) = A(1,2,1,1)
        B(6,2) = A(1,2,2,2)
        B(6,3) = A(1,2,3,3)
        B(6,4) = A(1,2,2,3)
        B(6,5) = A(1,2,3,1)
        B(6,6) = A(1,2,1,2)
    end function
!******************************************************************************!
!Subroutine to find the inverse of a square matrix
!Author : Louisda16th a.k.a Ashwith J. Rego
!Reference : Algorithm has been well explained in:
!http://math.uww.edu/~mcfarlat/inverse.htm
!http://www.tutor.ms.unimelb.edu.au/matrix/matrix_inverse.html
!******************************************************************************!
    function findInv(matrix) result(inverse)
        real(e), intent(in), dimension(:,:) :: matrix                !<Input matrix
        real(e), dimension(size(matrix,1),size(matrix,2)) :: inverse !<Inverted matrix
!------
        integer :: n
        logical :: FLAG = .TRUE.
        integer :: i, j, k
        real(e) :: m
        real(e), dimension(:,:), allocatable :: augmatrix !augmented matrix

        n = size(matrix,1)
        if (size(matrix,2) /= n) then
            write(*,*) "Cannot invert non-square matrices"
            stop
        end if
        allocate(augmatrix(n,2*n))
        !Augment input matrix with an identity matrix
        do i = 1, n
            do j = 1, 2*n
                if (j <= n ) then
                    augmatrix(i,j) = matrix(i,j)
                else if ((i+n) == j) then
                    augmatrix(i,j) = 1
                else
                    augmatrix(i,j) = 0
                end if
            end do
        end do

        !Reduce augmented matrix to upper traingular form
        do k =1, n-1
            if (augmatrix(k,k) == 0) then
                FLAG = .FALSE.
                do i = k+1, n
                    if (augmatrix(i,k) /= 0) then
                        do j = 1,2*n
                            augmatrix(k,j) = augmatrix(k,j)+augmatrix(i,j)
                        end do
                        FLAG = .TRUE.
                        exit
                    end if
                    if (FLAG .EQV. .FALSE.) then
                        PRINT*, "Matrix is non - invertible"
                        inverse = 0
                        return
                    end if
                end do
            end if
            do j = k+1, n
                m = augmatrix(j,k)/augmatrix(k,k)
                do i = k, 2*n
                    augmatrix(j,i) = augmatrix(j,i) - m*augmatrix(k,i)
                end do
            end do
        end do

        !Test for invertibility
        do i = 1, n
            if (augmatrix(i,i) == 0) then
                write(*,*) "Matrix is non - invertible"
                inverse = 0
                return
            end if
        end do

        !Make diagonal elements as 1
        do i = 1 , n
            m = augmatrix(i,i)
            do j = i , (2 * n)
                   augmatrix(i,j) = (augmatrix(i,j) / m)
            end do
        end do

        !Reduced right side half of augmented matrix to identity matrix
        do k = n-1, 1, -1
            do i =1, k
            m = augmatrix(i,k+1)
                do j = k, (2*n)
                    augmatrix(i,j) = augmatrix(i,j) -augmatrix(k+1,j) * m
                end do
            end do
        end do

        !store answer
        do i =1, n
            do j = 1, n
                inverse(i,j) = augmatrix(i,j+n)
            end do
        end do
    end function
!******************************************************************************!
!> Calculate the Moore-Penrose pseudoinverse of a matrix.
!******************************************************************************!
    function moorePenrose(Q) result(Qp)
        real(e), dimension(:,:), intent(in) :: Q
        real(e), dimension(size(Q,2), size(Q,1)) :: Qt
        real(e), dimension(size(Q,2), size(Q,1)) :: Qp
        real(e), dimension(3,3) :: Qm
        real(e), dimension(3,3) :: Qi

        Qt = transpose(Q)
        Qm = matmul(Qt, Q)
        Qi = .i.Qm
        Qp = matmul(Qi,Qt)
    end function
!******************************************************************************!
! Returns A as its pxp symetric matrix square-root.
! Calls Fortran subroutine jacobid.
!******************************************************************************!
    subroutine rtmat(A, eig, G, GD)
        real(e), dimension(:,:) :: A
        real(e), dimension(:) :: eig
        real(e), dimension(:,:) :: G
        real(e), dimension(:,:) :: GD

        integer :: p
        integer :: i,j,nrot
        real(e) :: dum

        p = size(A,1)
        call jacobid(A,p,p,eig,G,nrot)
! eig now contains the eigenvalues of A, and the corresponding
! eigenvectors are now the columns of G.
! Set A as the diagonal matrix of eigenvalue square roots:
        do i=1,p
            do j=1,p
                A(i,j)=0.
            end do
            A(i,i)=sqrt(eig(i))
        end do
        GD = matmul(G, A)
! Replace G by its transpose:
        do i=1,p
            do j=1,i
                dum=G(i,j)
                G(i,j)=G(j,i)
                G(j,i)=dum
            end do
        end do
        A = matmul(GD, G)
    end subroutine
!******************************************************************************!
! ***revised by PE for double precision for use with Splus***
!
! Computes all eigenvalues and eigenvectors of a real symmetric
! matrix a, which is of size n, stored in a physical np x np array.
! On output, elements of a above the diagonal are destroyed.
! d returns the eigenvalues of a in its first n elements.
! v is a matrix with the same logical and physical dimensions as a,
! whose columns contain, on output, the normalized eigenvectors of a.
! nrot returns the number of jacobi rotations that were required.
!******************************************************************************!
    subroutine jacobid(a,n,np,d,v,nrot)

        integer :: n,np,nrot
        real(e) :: a(np,np),d(np),v(np,np)
        integer, parameter :: NMAX=500
        integer :: i,ip,iq,j
        real(e) :: c,g,h,s,sm,t,tau,theta,tresh,b(NMAX),z(NMAX)

        do ip=1,n
            do iq=1,n
                v(ip,iq)=0.
            end do
            v(ip,ip)=1.
        end do

        do ip=1,n
            b(ip)=a(ip,ip)
            d(ip)=b(ip)
            z(ip)=0.
        end do
        nrot=0
        do i=1,50
            sm=0.
            do ip=1,n-1
                do iq=ip+1,n
                    sm=sm+abs(a(ip,iq))
                end do
            end do
            if(sm.eq.0.)return
            if(i.lt.4)then
                tresh=0.2*sm/n**2
            else
                tresh=0.
            endif
            do ip=1,n-1
                do iq=ip+1,n
                    g=100.*abs(a(ip,iq))
                    if((i.gt.4).and.(abs(d(ip))+ &
                        g.eq.abs(d(ip))).and.(abs(d(iq))+g.eq.abs(d(iq))))then
                        a(ip,iq)=0.
                    else if(abs(a(ip,iq)).gt.tresh)then
                        h=d(iq)-d(ip)
                        if(abs(h)+g.eq.abs(h))then
                            t=a(ip,iq)/h
                        else
                            theta=0.5*h/a(ip,iq)
                            t=1./(abs(theta)+sqrt(1.+theta**2))
                            if(theta.lt.0.)t=-t
                        endif
                        c=1./sqrt(1+t**2)
                        s=t*c
                        tau=s/(1.+c)
                        h=t*a(ip,iq)
                        z(ip)=z(ip)-h
                        z(iq)=z(iq)+h
                        d(ip)=d(ip)-h
                        d(iq)=d(iq)+h
                        a(ip,iq)=0.
                        do j=1,ip-1
                            g=a(j,ip)
                            h=a(j,iq)
                            a(j,ip)=g-s*(h+g*tau)
                            a(j,iq)=h+s*(g-h*tau)
                        end do
                        do j=ip+1,iq-1
                            g=a(ip,j)
                            h=a(j,iq)
                            a(ip,j)=g-s*(h+g*tau)
                            a(j,iq)=h+s*(g-h*tau)
                        end do
                        do j=iq+1,n
                            g=a(ip,j)
                            h=a(iq,j)
                            a(ip,j)=g-s*(h+g*tau)
                            a(iq,j)=h+s*(g-h*tau)
                        end do
                        do j=1,n
                            g=v(j,ip)
                            h=v(j,iq)
                            v(j,ip)=g-s*(h+g*tau)
                            v(j,iq)=h+s*(g-h*tau)
                        end do
                        nrot=nrot+1
                    endif
                end do
            end do
            do ip=1,n
                b(ip)=b(ip)+z(ip)
                d(ip)=b(ip)
                z(ip)=0.
            end do
        end do
    end subroutine
!******************************************************************************!
!> Calculate the determinant of a 3x3 matrix.
!******************************************************************************!
     pure function det(M)
        real(e), dimension(3,3), intent(in) :: M    !! 3x3 matrix
        real(e) :: det
!------
        det = M(1,1) * ((M(2,2)*M(3,3)) - (M(2,3)*M(3,2)))
        det = det + M(2,1) * (M(3,2)*M(1,3) - M(3,3)*M(1,2))
        det = det + M(3,1) * ((M(1,2)*M(2,3)) - (M(1,3)*M(2,2)))
        return
    end function
!******************************************************************************!
!> Calculate the invert of a 3x3 matrix.
!******************************************************************************!
  function matinv(a)
      real(e) , dimension(3,3), intent(in) :: a
      real(e) , dimension(3,3) :: matinv
      integer :: i, im, ip, j, jm, jp
      real(e) :: deta, detai
! --- Transposed cofactor matrix
      do j=1,3
        jm=mod(j+1,3)+1
        jp=mod(j,  3)+1
        do i=1,3
          im=mod(i+1,3)+1
          ip=mod(i,  3)+1
! ---     Note this is a transposed cofactor matrix
          matinv(i,j)=a(jp,ip)*a(jm,im)-a(jp,im)*a(jm,ip)
        enddo
      enddo
! --- Determinant
      deta=0.0_e
      do i=1,3
        deta=deta+a(i,1)*matinv(1,i)
      enddo
      call assert(deta /= 0, "Error during inversion: singular matrix")
      detai=1.0_e/deta
! --- Inverse marix
      do j=1,3
        do i=1,3
           matinv(i,j)=matinv(i,j)*detai
        enddo
      enddo
      end function
!******************************************************************************!
!> Compute the tensor product of 2 vectors (double precision version)
!******************************************************************************!
    function tens_product(u, v) result(M)
        real(e), dimension(:), intent(in) :: u
        real(e), dimension(size(u)), intent(in) :: v
        real(e), dimension(size(u),size(u)) :: m
        integer :: i, j

        do i=1, size(u)
            do j=1, size(u)
                m(i,j) = u(i)*v(j)
            end do
        end do
    end function
!******************************************************************************!
!> Compute a transformation matrix corresponding to a rotation of angle theta
!> around the axis u
!******************************************************************************!
    pure function rotationMatrix(u, theta) result(R)
        real(e), dimension(3), intent(in) :: u
        real(e), intent(in) :: theta
        real(e), dimension(3,3) :: R
!------
        real(e), dimension(3) :: v
        real(e) :: ux, uy, uz, ux2, uy2, uz2
        real(e) :: cost, sint
!------
        v = u
        call normalize(v)
        cost = cos(theta)
        sint = sin(theta)
        ux = v(1)
        uy = v(2)
        uz = v(3)
        ux2 = ux*ux
        uy2 = uy*uy
        uz2 = uz*uz

        R(1,1) = ux2 + (1-ux2)*cost
        R(2,1) = ux*uy*(1-cost)+uz*sint
        R(3,1) = ux*uz*(1-cost)-uy*sint

        R(1,2) = ux*uy*(1-cost)-uz*sint
        R(2,2) = uy2 + (1-uy2)*cost
        R(3,2) = uy*uz*(1-cost)+ux*sint

        R(1,3) = ux*uz*(1-cost)+uy*sint
        R(2,3) = uy*uz*(1-cost)-ux*sint
        R(3,3) = uz2 + (1-uz2)*cost
    end function
!******************************************************************************!
!> Normalize a double precision vector
!******************************************************************************!
    pure subroutine normalize(u)
        real(e), dimension(:), intent(inout) :: u
        real(e) :: n
        integer :: i

        n = 0.0_e
        do i=1, size(u)
            n = n + u(i)*u(i)
        end do
        n = sqrt(n)
        if (n /= 1.0) then
            u = u / n
        end if
    end subroutine
!******************************************************************************!
!> Vector cross-product.
!******************************************************************************!
    pure function cross_product(u, v) result(w)
        real(e), dimension(3), intent(in) :: u
        real(e), dimension(3), intent(in) :: v
        real(e), dimension(3) :: w
!------ 
        w(1) = u(2)*v(3) - u(3)*v(2)
        w(2) = u(3)*v(1) - u(1)*v(3)
        w(3) = u(1)*v(2) - u(2)*v(1)
    end function
!******************************************************************************!
!> Calculate the eigenvalues and eigenvectors of a 3x3 matrix.
!******************************************************************************!
    SUBROUTINE eigenvalues(A, Q, W)
! Calculates the eigenvalues and normalized eigenvectors of a symmetric 3x3
! matrix A using the Jacobi algorithm.
! The upper triangular part of A is destroyed during the calculation,
! the diagonal elements are read but not destroyed, and the lower
! triangular elements are not referenced at all.
! ----------------------------------------------------------------------------
! Parameters:
!   A: The symmetric input matrix
!   Q: Storage buffer for eigenvectors
!   W: Storage buffer for eigenvalues
! ----------------------------------------------------------------------------
        real(e), dimension(3,3), intent(inout) :: A
        real(e), dimension(3,3), intent(out) ::Q
        real(e), dimension(3), intent(out) ::W
!------
        integer, parameter :: n = 3
        real(e) :: SD, SO, S, C, T, G, H, Z, THETA, THRESH
        integer :: i, x, y, r

!     Initialize Q to the identitity matrix
        Q = 0
        Q(1,1) = 1
        Q(2,2) = 1
        Q(3,3) = 1

!     Initialize W to diag(A)
        W(1) = A(1,1)
        W(2) = A(2,2)
        W(3) = A(3,3)

!     Calculate SQR(tr(A))
        SD = sum(abs(W))
        SD = SD**2

!     Main iteration loop
      DO 40 I = 1, 50
!       Test for convergence
        SO = 0.0D0
        DO 50 X = 1, N
          DO 51 Y = X+1, N
            SO = SO + ABS(A(X, Y))
   51     CONTINUE
   50   CONTINUE
        IF (SO .EQ. 0.0D0) THEN
          RETURN
        END IF

        IF (I .LT. 4) THEN
          THRESH = 0.2D0 * SO / N**2
        ELSE
          THRESH = 0.0D0
        END IF

!       Do sweep
        DO 60 X = 1, N
          DO 61 Y = X+1, N
            G = 100.0D0 * ( ABS(A(X, Y)) )
            IF ( I .GT. 4 .AND. ABS(W(X)) + G .EQ. ABS(W(X)) &
                          .AND. ABS(W(Y)) + G .EQ. ABS(W(Y)) ) THEN
              A(X, Y) = 0.0D0
            ELSE IF (ABS(A(X, Y)) .GT. THRESH) THEN
!             Calculate Jacobi transformation
              H = W(Y) - W(X)
              IF ( ABS(H) + G .EQ. ABS(H) ) THEN
                T = A(X, Y) / H
              ELSE
                THETA = 0.5D0 * H / A(X, Y)
                IF (THETA .LT. 0.0D0) THEN
                  T = -1.0D0 / (SQRT(1.0D0 + THETA**2) - THETA)
                ELSE
                  T = 1.0D0 / (SQRT(1.0D0 + THETA**2) + THETA)
                END IF
              END IF

              C = 1.0D0 / SQRT( 1.0D0 + T**2 )
              S = T * C
              Z = T * A(X, Y)

!             Apply Jacobi transformation
              A(X, Y) = 0.0D0
              W(X)    = W(X) - Z
              W(Y)    = W(Y) + Z
              DO 70 R = 1, X-1
                T       = A(R, X)
                A(R, X) = C * T - S * A(R, Y)
                A(R, Y) = S * T + C * A(R, Y)
   70         CONTINUE
              DO 80, R = X+1, Y-1
                T       = A(X, R)
                A(X, R) = C * T - S * A(R, Y)
                A(R, Y) = S * T + C * A(R, Y)
   80         CONTINUE
              DO 90, R = Y+1, N
                T       = A(X, R)
                A(X, R) = C * T - S * A(Y, R)
                A(Y, R) = S * T + C * A(Y, R)
   90         CONTINUE

!             Update eigenvectors
!             --- This loop can be omitted if only the eigenvalues are desired ---
              DO 100, R = 1, N
                T       = Q(R, X)
                Q(R, X) = C * T - S * Q(R, Y)
                Q(R, Y) = S * T + C * Q(R, Y)
  100         CONTINUE
            END IF
   61     CONTINUE
   60   CONTINUE
   40 CONTINUE

      PRINT *, "DSYEVJ3: No convergence."

      END SUBROUTINE

      function inverse(mat) result(C)
    !============================================================
    ! Inverse matrix
    ! Method: Based on Doolittle LU factorization for Ax=b
    ! Alex G. December 2009
    !-----------------------------------------------------------
    ! input ...
    ! a(n,n) - array of coefficients for matrix A
    ! n      - dimension
    ! output ...
    ! c(n,n) - inverse matrix of A
    ! comments ...
    ! the original matrix a(n,n) will be destroyed
    ! during the calculation
    !===========================================================
    implicit none
    integer n
    real(e), dimension(:,:), intent(in) :: mat
    real(e), dimension(size(mat,1),size(mat,1)) :: c
    real(e), dimension(:,:), allocatable :: L, U, A
    real(e), dimension(:), allocatable :: b, d, x
    double precision coeff
    integer i, j, k

    A = mat
    n = size(a,1)
    allocate(L(n,n))
    allocate(U(n,n))
    allocate(b(n))
    allocate(d(n))
    allocate(x(n))

    ! step 0: initialization for matrices L and U and b
    ! Fortran 90/95 aloows such operations on matrices
    L=0.0
    U=0.0
    b=0.0

    ! step 1: forward elimination
    do k=1, n-1
       do i=k+1,n
          coeff=a(i,k)/a(k,k)
          L(i,k) = coeff
          do j=k+1,n
             a(i,j) = a(i,j)-coeff*a(k,j)
          end do
       end do
    end do

    ! Step 2: prepare L and U matrices
    ! L matrix is a matrix of the elimination coefficient
    ! + the diagonal elements are 1.0
    do i=1,n
      L(i,i) = 1.0
    end do
    ! U matrix is the upper triangular part of A
    do j=1,n
      do i=1,j
        U(i,j) = a(i,j)
      end do
    end do

    ! Step 3: compute columns of the inverse matrix C
    do k=1,n
      b(k)=1.0
      d(1) = b(1)
    ! Step 3a: Solve Ld=b using the forward substitution
      do i=2,n
        d(i)=b(i)
        do j=1,i-1
          d(i) = d(i) - L(i,j)*d(j)
        end do
      end do
    ! Step 3b: Solve Ux=d using the back substitution
      x(n)=d(n)/U(n,n)
      do i = n-1,1,-1
        x(i) = d(i)
        do j=n,i+1,-1
          x(i)=x(i)-U(i,j)*x(j)
        end do
        x(i) = x(i)/u(i,i)
      end do
    ! Step 3c: fill the solutions x(n) into column k of C
      do i=1,n
        c(i,k) = x(i)
      end do
      b(k)=0.0
    end do
    end function inverse
end module
