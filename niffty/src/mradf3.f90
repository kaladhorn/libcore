    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mradf3(m, ido, l1, cc, im1, in1, ch, im2, in2, wa1, wa2)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) ch(in2, ido, 3, l1), cc(in1, ido, l1, 3), wa1(ido), &
          wa2(ido)

        m1d = (m-1)*im1 + 1
        m2s = 1 - im2
        arg = 2.*4.*atan(1.0_rwp)/3.
        taur = cos(arg)
        taui = sin(arg)
        do k = 1, l1
            m2 = m2s
            do m1 = 1, m1d, im1
                m2 = m2 + im2
                ch(m2, 1, 1, k) = cc(m1, 1, k, 1) + (cc(m1,1,k,2)+cc(m1,1,k,3) &
                  )
                ch(m2, 1, 3, k) = taui*(cc(m1,1,k,3)-cc(m1,1,k,2))
                ch(m2, ido, 2, k) = cc(m1, 1, k, 1) + &
                  taur*(cc(m1,1,k,2)+cc(m1,1,k,3))
            end do
        end do
        if (ido==1) return
        idp2 = ido + 2
        do k = 1, l1
            do i = 3, ido, 2
                ic = idp2 - i
                m2 = m2s
                do m1 = 1, m1d, im1
                    m2 = m2 + im2
                    ch(m2, i-1, 1, k) = cc(m1, i-1, k, 1) + ((wa1(i-2)*cc(m1, &
                      i-1,k,2)+wa1(i-1)*cc(m1,i,k,2))+(wa2(i-2)*cc(m1,i-1,k, &
                      3)+wa2(i-1)*cc(m1,i,k,3)))
                    ch(m2, i, 1, k) = cc(m1, i, k, 1) + ((wa1(i-2)*cc(m1,i,k, &
                      2)-wa1(i-1)*cc(m1,i-1,k,2))+(wa2(i-2)*cc(m1,i,k, &
                      3)-wa2(i-1)*cc(m1,i-1,k,3)))
                    ch(m2, i-1, 3, k) = (cc(m1,i-1,k,1)+taur*((wa1(i-2)* &
                      cc(m1,i-1,k,2)+wa1(i-1)*cc(m1,i,k,2))+(wa2(i-2)*cc(m1,i- &
                      1,k,3)+wa2(i-1)*cc(m1,i,k,3)))) + &
                      (taui*((wa1(i-2)*cc(m1,i,k,2)- &
                      wa1(i-1)*cc(m1,i-1,k,2))-(wa2(i-2)*cc(m1,i,k,3)- &
                      wa2(i-1)*cc(m1,i-1,k,3))))
                    ch(m2, ic-1, 2, k) = (cc(m1,i-1,k,1)+taur*((wa1(i-2)* &
                      cc(m1,i-1,k,2)+wa1(i-1)*cc(m1,i,k,2))+(wa2(i-2)*cc(m1,i- &
                      1,k,3)+wa2(i-1)*cc(m1,i,k,3)))) - &
                      (taui*((wa1(i-2)*cc(m1,i,k,2)- &
                      wa1(i-1)*cc(m1,i-1,k,2))-(wa2(i-2)*cc(m1,i,k,3)- &
                      wa2(i-1)*cc(m1,i-1,k,3))))
                    ch(m2, i, 3, k) = (cc(m1,i,k,1)+taur*((wa1(i-2)* &
                      cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k,2))+(wa2(i-2)*cc(m1,i, &
                      k,3)-wa2(i-1)*cc(m1,i-1,k,3)))) + &
                      (taui*((wa2(i-2)*cc(m1,i-1,k,3)+ &
                      wa2(i-1)*cc(m1,i,k,3))-(wa1(i-2)*cc(m1,i-1,k,2)+ &
                      wa1(i-1)*cc(m1,i,k,2))))
                    ch(m2, ic, 2, k) = (taui*((wa2(i-2)*cc(m1,i-1,k,3)+ &
                      wa2(i-1)*cc(m1,i,k,3))-(wa1(i-2)*cc(m1,i-1,k,2)+ &
                      wa1(i-1)*cc(m1,i,k,2)))) - (cc(m1,i,k,1)+taur*((wa1(i-2) &
                      *cc(m1,i,k,2)-wa1(i-1)*cc(m1,i-1,k,2))+(wa2(i-2)*cc(m1,i &
                      ,k,3)-wa2(i-1)*cc(m1,i-1,k,3))))
                end do
            end do
        end do
    end subroutine
