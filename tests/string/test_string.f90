module test_string
    use mod_test
    
    use core
    use ext_character
    use mod_string
    use ext_character
    implicit none
    private
    
    public :: testAll
contains
    subroutine testAll()
        call testinit
        call testNaturalComparison
        call testMatching
        call testConversion
        call testComparison
        call comparisonTorture
        call testConcatenation
    end subroutine
    
    subroutine testInit
        type(FString) :: string1, string, sub, string2, string3
        
        call startTestGroup("basic string properties")
        
        call check(string1%string() == "", "unset string value", &
            "wrong value for unset string: '" // string1%string() // "'.")
        
        call check(string1%isBlank(), "empty unset string", &
            "wrong value for unset string: '" // string1%isBlank() // "'.")
        string1 = ""
        call check(string1%isBlank(), "empty string", &
            "wrong value for empty string test: '" // string1%isBlank() // "'.")
        
        string1 = "sdlfiuiu"
                
        call check(string1%length()==8, "simple string length", "wrong length for string "//string1//": "//string1%length())
        string = FString("abcdαβγ")
        string2 = "abcdαβγ"
        call check(string == string2, "overloaded assignment", "wrong assigned string: "//string2//" instead of "//string)
        call check(string%length()==7, "unicode string length", "wrong length for string "//string//": "//string%length())
        
        string3 = FString(string)
        call check(string3 == string2, "string constructor",&
            "wrong assigned string: "//string2//" instead of "//string)
        
        sub = string%substring(from=5, to=5)
        call check(sub=="α", "unicode substring α", "wrong substring: "//sub)
        sub = string%substring(from=5)
        call check(sub=="αβγ", "unicode substring αβγ", "wrong substring: "//sub)
        sub = string%substring(to=4)
        call check(sub=="abcd", "unicode substring abcd", "wrong substring: "//sub)
        sub = string%substring(to=4, from=5)
        call check(sub=="", "empty substring", "wrong substring: "//sub)
        
        string2 = bold(string%string())
        call check(string2%length() == string%length(), &
            "ANSI apparent string length", &
            "wrong length: "//string2%length()//" (should be "//string%length()//")")
        call check(string2%internalLength() == string%internalLength()+8, &
            "ANSI actual string length", &
            "wrong length: "//string2%internalLength()//" (should be "//string%internalLength()+8//")")
    end subroutine
    
    subroutine testMatching
        type(FString) :: string1
        
        string1 = "azerty123456"
        call check(string1%startsWith("azert"), "valid beginning matching", "")
        call check(.not.string1%startsWith("456"), "invalid beginning matching", "")
        call check(string1%endsWith("456"), "valid ending matching", "")
        call check(.not.string1%endsWith("azert"), "invalid ending matching", "")
    end subroutine
    
    subroutine testNaturalComparison
        type(FString) :: string1, string2, string3, string4
        type(FString) :: string5, string6, string7, string8, string9
        
        call startTestGroup("natural comparisons")
        
        string1 = FString("foo", "natural")
        string2 = FString("foo2bar", "natural")
        string3 = FString("foo24bar", "natural")
        string4 = FString("foo42bar", "natural")
        string5 = FString("foo0042bar", "natural")
        string6 = FString("foo272bar", "natural")        
        string7 = FString("foobar", "natural")
        string8 = FString("foofred", "natural")
        string9 = FString("foo42fred", "natural")
        
        call check(string1<string6, "substring", &
            "wrong result: "//string1//" is not < to "//string5//".")
        call check(string1==string1, "indentical strings", &
            "wrong result: "//string1//" is not < to "//string5//".")
        call check(string7<string8, "alphabetical difference", &
            "wrong result: "//string4//" is not < to "//string5//".")
        call check(string1<string2, "< comparison 1", "")
        call check(string2<string7, "< comparison 2", "")
        call check(string2<string3, "< comparison 3", "")
        call check(string5<string6, "leading zeroes 1", "")
        call check(string3<string5, "leading zeroes 2", "")
        call check(string4==string5, "leading zeroes 3", "")
        call check(string4<string9, "leading zeroes 3", "")
        call check(string5<string9, "leading zeroes 3", "")
        
        call check(string5==string5%string(), "string/character == comparison", "")
        call check(string5%string()==string5, "character/string == comparison", "")
        call check(string5/=string2%string(), "string/character /= comparison", "")
        call check(string5%string()/=string2, "character/string /= comparison", "")
        
    end subroutine
    
    subroutine testComparison
        type(FString) :: string1, string2, string3, string4, string5
        
!------ Comparisons with uninitialised strings
        call startTestGroup("empty strings comparisons")
        string1 = "test250"
        
        call check(.not.(string2 == string1), "== comparison with unset string", "")
        call check(.not.(string1 == string2), "== comparison with unset string", "")
        call check(     (string2 /= string1), "/= comparison with unset string", "")
        call check(.not.(string2 == string1%string()), "/= comparison with unset string", "")
        call check(     (string2 /= string1%string()), "/= comparison with unset string", "")
        call check(.not.(string1 < string2), "< comparison with unset string", "")
        call check(string1 > string2, "> comparison with unset string", "")
        call check(string1 /= string2, "/= comparison with unset string", "")
        
        string2 = "test2000"
        call check(string2 > string1, "default comparison")
        
        string1 = FString("sdlfiuiu")
        
        string2 = FString("sdlfiuiu")
        
!------ 0-length comparisons
        string1 = "foo1"
        string2 = ""
        string3 = ""
        string4 = FString("abcdαβγ", "fortran")
        string5 = FString("abcdαβγ", "fortran")
        
        ! == operator
        call check(string4==string5, "alphabetical == comparison")
        call check(.not.(string1 == string2), "== comparison")
        call check(.not.(string2 == string1), "== comparison (reversed)")
        call check(.not.(string1 == string2%string()), "== string-to-character comparison")
        call check(.not.(string2 == string1%string()), "== string-to-character comparison (reversed)")
        call check(     (string2 == string3), "== comparison (2)")
        call check(     (string2 == string2%string()), "== string-to-character comparison (2)")
        
        ! /= operator
        call check(     (string1 /= string2), "/= comparison")
        call check(     (string2 /= string1), "/= comparison (reversed)")
        call check(     (string1 /= string2%string()), "/= string-to-character comparison")
        call check(     (string2 /= string1%string()), "/= string-to-character comparison (reversed)")
        call check(.not.(string2 /= string3), "/= comparison (2)")
        call check(.not.(string2 /= string2%string()), "/= string-to-character comparison (2)")
        
        ! > operator
        call check(     (string1 > string2), "> comparison")
        call check(.not.(string2 > string1), "> comparison (reversed)")
        call check(.not.(string2 > string3), "> comparison (identical)")
        call check(     (string1 > string2%string()), "> string-to-character comparison")
        call check(.not.(string2 > string1%string()), "> string-to-character comparison (reversed)")
        call check(.not.(string2 > string3%string()), "> string-to-character comparison (identical)")
                
        ! < operator
        call check(.not.(string1 < string2), "< comparison")
        call check(     (string2 < string1), "< comparison (reversed)")
        call check(.not.(string2 < string3), "< comparison (identical)")
        call check(.not.(string1 < string2%string()), "< string-to-character comparison")
        call check(     (string2 < string1%string()), "< string-to-character comparison (reversed)")
        call check(.not.(string2 < string3%string()), "< string-to-character comparison (identical)")
        
!------ Natural comparison function
        call startTestGroup("natural order comparisons")
        string1 = FString("foo1", "natural")
        string2 = FString("foo09", "natural")
        string4 = FString("abcdαβγ", "natural")
        string5 = FString("abcdαβγ", "natural")
        
        ! == operator
        call check(string4==string5, "natural == comparison", "error in natural string comparison")
        call check(string2 > string1, "natural > comparison", "error in natural string comparison 1")
        call check(.not.(string1 > string2), "natural > comparison (2)", "error in natural string comparison 2")
        call check(string1 < string2, "error in natural string comparison 3")
        call check(.not.(string1 > string2%string()), "error in natural string comparison 4")
        call check(string1 /= string2, "error in natural string comparison 5")
        call check(string2 > string1%string(), "error in natural string comparison 6")
        call check(string1 < string2%string(), "error in natural string comparison 7")
        
        string1 = FString("fooa1", "natural")
        string2 = FString("foob09", "natural")
        call assert(string2 > string1, "error in natural string comparison 8")
        call assert(string1 < string2, "error in natural string comparison 9")
        
        string1 = FString("fooa1", "natural")
        string2 = FString("fooa1", "natural")
        call assert(.not.(string2 > string1), "error in natural string comparison 8")
        call assert(.not.(string1 < string2), "error in natural string comparison 9")
        
        string1 = FString("fooa", "natural")
        string2 = FString("foob", "natural")
        call assert(string2 > string1, "error in natural string comparison 8")
        call assert(string1 < string2, "error in natural string comparison 9")
        
        string1 = FString("fooa", "natural")
        string2 = FString(string1, "natural")
        call assert(.not.(string2 > string1), "error in natural string comparison 8")
        call assert(.not.(string1 < string2), "error in natural string comparison 9")
        
!------ Default comparison function
        call startTestGroup("default order comparisons")
        string1 = FString("foo1", "fortran")
        string2 = FString("foo09", "fortran")
        call assert(string2 < string1, "error in Fortran string comparison 1")
        call assert(.not.(string1 < string2), "error in Fortran string comparison 2")
        call assert(string1 > string2, "error in Fortran string comparison 3")
        call assert(.not.(string1 < string2%string()), "error in Fortran string comparison 4")
        call assert(string1 /= string2, "error in Fortran string comparison 5")
        call assert(string2 < string1%string(), "error in Fortran string comparison 6")
        call assert(string1 > string2%string(), "error in Fortran string comparison 7")
        
    end subroutine
    
    subroutine comparisonTorture
        type(FString) :: string1, string2
        
        string1 = " .76149999999999995"
        string2 = " .76149999999999995"
        call check(string1==string2, "First comparison")
   
        string2 = " .76149999999999996"
        call check(string1<string2, "Second comparison")
    end subroutine
    
    subroutine testConversion
        type(FString) :: string, string2
        character(:), allocatable :: char, char2
        
        call startTestGroup("character-string conversions")
        char = "sdlfiuiu"
        string = FString(char)
        call check(string == char, "character to string (constructor)")
        
        string2 = char
        call check(string2 == char, "character to string (assignment)")
        
        char2 = string2
        call check(char2 == char, "string to character")
    end subroutine
    
    subroutine testConcatenation
        type(FString) :: string1, string2, string3
        
        call startTestGroup("string concatenation")
        
        string1 = "sdlfiuiu"
        string2 = "sfdargrdg " // string1
        call check(string2 == "sfdargrdg sdlfiuiu", "character//string")
        
        string2 = string1 // " sfdargrdg"
        call check(string2 == "sdlfiuiu sfdargrdg", "string//character")
        
        string2 = "sfdargrdg"
        string3 = string1 // string2
        call check(string3 == "sdlfiuiusfdargrdg", "string//string")
    end subroutine
end module

program test
    use mod_test
    use test_string
    
    call testAll
    
    call testsFinished
end program