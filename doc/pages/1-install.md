title: Install

# Installation instructions

This package uses the autotools as a build system. In most cases, installation is 
done simply by running the following commands from the root directory of the package:

```bash
	./configure
	make
	make install
```

## Configuration

### Installation directories

By default, the `$HOME` directory is used for the installation. This can be changed by using the 
`--prefix` option of the configure script. The library is installed in `$HOME/lib`.

### Required libraries

The following libraries are required:
  - `PCRE`;
  - `zlib`;
  - `libbz2`.

If they are not in a standard location, the `configure` script needs to know where to find them.
This is usually done by setting the `LDFLAGS` environment variable. Additionally, if `PCRE` is 
installed in a non-standard location, the `CFLAGS` needs to be set so that the `pcre.h` header 
can be found. For example, if these libraries are installed in `/opt/local` (as is the case for 
example if they have been installed by `macports`), the configure command would be

```bash
	./configure CFLAGS=-I/opt/local/include LDFLAGS=-L/opt/local/include
```

Alternatively, the configure script supports the `--with-pcre`, `--with-zlib` and `--with-libbz2` 
options to set the paths separately for each library.

### Optional tools

Some features are available only if some programs are installed.


## Tests

### Test suite

The test suite for the Core library is in the `tests`directory.
It is run by using

```bash
	make check
```

in the build directory.


### Code coverage

The coverage of the test suite can be determined by running

```bash
	make check-code-coverage
```

from the build directory. This must have been enabled by running `./configure` with the 
`--enable-code-coverage` option. This requires both `gcov` and `lcov` to be in the `$PATH`, as well 
as a compiler that generates data that `gcov` can interpret.
This produces HTML files in the `libcore-1.0` directory, showing the coverage for each source file.
