module test_property
    use config
    use core
    use mod_test

    use ext_character
    use mod_file,      only: FFile
    use mod_string,    only: FString
    use mod_property,  only: FProperty
    use mod_exception, only: FException
    implicit none
    private

    public :: testAll
contains
    subroutine testAll()
        call testBasic
        call testTypes
        call testComparison
        call testInterpolation
        call testExpressions
        call testJson
        call testPFile
        call testCommandLine
        call testAccess
        call testErrors
    end subroutine

    subroutine testErrors
        type(FFile) :: file
        type(FException) :: stat
        type(FProperty) :: p

        call startTestGroup("malformed property files")

        ! Read a property file
        file = FFile(TEST_PATH//"/property/wrong1.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat /= 0, "case 1", "no error detected")
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/wrong2.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Expected a property name", "case 2", "no error detected")
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/wrong3.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Expected a property value", "case 2", "no error detected")
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/wrong4.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Bad symbol in property name", "case 4", "no error detected")
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/quote1.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Unterminated character string", "wrong quotes 1", "error message: " // stat%string())
        call stat%discard

        block
            type(FProperty) :: p
            ! Read a property file
            file = FFile(TEST_PATH//"/property/quote2.param", "read")

            call p%readFile(file, stat=stat)
            call check(stat == "Unexpected quotes", "wrong quotes 2", "error message: " // stat%string())
            call stat%discard
        end block

        block
            type(FProperty) :: p
            ! Read a property file
            file = FFile(TEST_PATH//"/property/value1.param", "read")

            call p%readFile(file, stat=stat)
            call check(stat == "Invalid character in property value", "wrong value 1", "error message: " // stat%string())
            call stat%discard
        end block

        block
            type(FProperty) :: p
            ! Read a property file
            file = FFile(TEST_PATH//"/property/value2.param", "read")

            call p%readFile(file, stat=stat)
            call check(stat == "Expected a property value", "wrong value 2", "error message: " // stat%string())
            call stat%discard
        end block

        block
            type(FProperty) :: p
            ! Read a property file
            file = FFile(TEST_PATH//"/property/value3.param", "read")

            call p%readFile(file, stat=stat)
            call check(stat == "Invalid character in property value", "wrong value 3", "error message: " // stat%string())
            call stat%discard
        end block

        ! Read a property file
        file = FFile(TEST_PATH//"/property/value4.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Invalid character in property value", "wrong value 4", "error message: " // stat%string())
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/structure1.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Expected a property name", "wrong structure 1", "error message: " // stat%string())
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/structure2.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Invalid symbol in structure definition", "wrong structure 2", "error message: " // stat%string())
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/structure3.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Empty structure definition", "wrong structure 3", "error message: " // stat%string())
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/structure4.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Incomplete structure definition.", "incomplete structure", "error message: " // stat%string())
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/vector1.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Too many vector components", "wrong vector 1", "error message: " // stat%string())
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/vector2.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Empty vector definition", "wrong vector 2", "error message: " // stat%string())
        call stat%discard

        ! Read a property file
        file = FFile(TEST_PATH//"/property/vector3.param", "read")
        call p%readFile(file, stat=stat)
        call check(stat == "Wrong symbol in vector definition", "wrong vector 3", "error message: " // stat%string())
        call stat%discard

    end subroutine

    subroutine testBasic
        type(FProperty) :: p
        character(:), allocatable :: x0
        integer :: x1
        real(e) :: x2
        type(FString) :: x3
        logical :: x4
        logical :: test

        call startTestGroup("basic properties")
        ! Set values
        call p%set("foo", "baz")
        call p%set("the-answer", "42")

        ! Re-set one value
        call p%set("foo", "bar")

        ! Checks
        test = p%isSet("foo")
        call check(test, "testing set property", "property 'foo' is not set")
        call p%set("adadw", "!@$£")

        ! Test character value
        call check(p%value("the-answer") == "42", "testing nested property value", "wrong value: "//p%value("the-answer"))
        call p%get("the-answer", x0)
        call check(x0 == "42", "getting value as an character", "wrong value: "//x0)

        ! Test integer value
        call check(p%isInteger("the-answer"), "integer test")
        call p%get("the-answer", x1)
        call check(x1 == 42, "getting value as an integer", "wrong value: "//x1)

        ! Test real value
        call check(p%isReal("the-answer"), "checking real property", &
            "property value 42 was not seen as a real number")

        call p%get("the-answer", x2)
        call check(x2 == 42.0_e, "getting value as a real", "wrong value: "//x2)
        call p%set("num", "0.1")
        call p%get("num", x2)
        call check(x2 .approx. 0.1_dp, "getting 0.1 as a double-precision real", "wrong value: "//x2)

        ! Testing FString object value
        call p%get("the-answer", x3)
        call check(x3 == "42", "getting value as an string object", "wrong value: "//x3)

        ! Test logical value
        call p%set("switch", "true")
        call check(p%isLogical("switch"), "checking logical property", "property value true was not seen as a logical")
        call p%get("switch", x4)
        call check(x4, "getting logical value", "wrong value: "//x4)
        call p%set("switch", "lubizea")
        call check(.not.p%isLogical("switch"), "checking wrong logical property", "property value true was seen as a logical")

        call p%set("flag", "YES")
        call check(p%isLogical("flag"), "checking logical property", "property value YES was not seen as a logical")
        call p%get("flag", x4)
        call check(x4, "getting logical value", "wrong value: "//x4)
        call p%set("flag", "NO")
        call check(p%isLogical("flag"), "checking logical property", "property value NO was not seen as a logical")
        call p%get("flag", x4)
        call check(.not.x4, "getting logical value", "wrong value: "//x4)
    end subroutine

    subroutine testAccess
        type(FProperty) :: p
        type(FFile) :: file
        type(FString) :: val

        call startTestGroup("access metadata")

        ! Read a property file
        file = FFile(TEST_PATH//"/property/example.param", "read")

        call p%readFile(file, default=.true.)

        call p%get("glossary-title", val)
        call p%set("glossary-id", 42)
        call p%set("glossary-defstring", "foo")
        call p%prettyPrint

    end subroutine

    subroutine testTypes
        type(FProperty) :: p
        type(FException) :: err
        real(wp) :: x2
        integer :: x4
        logical :: x5

        call startTestGroup("types and conversions")

        call p%set("foo-dp", real(pi, dp))
        call p%set("foo-i", nint(pi))
        call p%set("foo-l", .true.)
        call p%set("foo-c", "π")
        call p%set("foo-s", FString("π"))

        call p%get("undefined", x2, err)
        call check(err /= 0, "reading undefined double-precision real")
        call err%discard
        call p%get("foo-l", x2, err)
        call check(err /= 0, "reading wrong double-precision real")
        call err%discard

        call p%get("undefined", x4, err)
        call check(err /= 0, "reading undefined integer")
        call err%discard
        call p%get("foo-l", x4, err)
        call check(err /= 0, "reading wrong integer")
        call err%discard

        call p%get("undefined", x5, err)
        call check(err /= 0, "reading undefined logical")
        call err%discard
        call p%get("foo-c", x5, err)
        call check(err /= 0, "reading wrong logical")
        call err%discard
    end subroutine

    subroutine testComparison
        type(FProperty) :: p, q

        call startTestGroup("comparisons")

        call p%set("foo", "baz")
        call p%set("the-answer", "42")
        q = p
        call check(p == q, "comparing similar property trees")
        call check(.not.(p /= q), "comparing similar property trees")

        ! Re-set one value
        call p%set("the-question", "72")
        call check(p /= q, "comparing different property trees")
        call check(.not.(p == q), "comparing different property trees")

    end subroutine

    subroutine testInterpolation
        type(FProperty) :: p

        call startTestGroup("interpolations")
        call p%set("foo", "bar")
        call p%set("bar", "$foo")
        call p%set("baz", "${foo}")
        call check(p%value("bar") == p%value("foo"), "testing $ interpolation", "wrong result: "//p%value("bar"))
        call check(p%value("baz") == p%value("foo"), "testing ${} interpolation", "wrong result: "//p%value("bar"))
        
        call p%set("str", ".$nil.")
        call check(p%value("str") == "..", "testing $ interpolation of non-existing variable", "wrong result: "//p%value("str"))
        call p%set("str", ".${nil}.")
        call check(p%value("str") == "..", "testing ${} interpolation of non-existing variable", "wrong result: "//p%value("str"))
    end subroutine

    subroutine testExpressions
        type(FProperty) :: p
        character(:), allocatable :: v

        call startTestGroup("mathematical expressions")
        call p%set("x", "3")
        call p%set("n", "10")
        call p%set("f", "((3*($x+$n)))")
        call p%set("g", "$(3*($x+$n))")
        call p%set("h", "((3*$x+$n))")
        call p%set("i", "sz_((3*$x+$n))_trw")
        call p%set("j", "sz_$(3*$x+$n)_trw")

        call p%get("f", v)
        call check(v=="39", "substitution 1", "wrong result: "//v//" instead of 39")

        call p%get("g", v)
        call check(v=="39", "substitution 2", "wrong result: "//v//" instead of 39")

        call p%get("h", v)
        call check(v=="19", "substitution 3", "wrong result: "//v//" instead of 19")

        call p%get("i", v)
        call check(v=="sz_19_trw", "substitution 4", "wrong result: "//v//" instead of sz_19_trw")

        call p%get("j", v)
        call check(v=="sz_19_trw", "substitution 5", "wrong result: "//v//" instead of sz_19_trw")
    end subroutine

    subroutine testJson
        type(FProperty) :: p, p_check
        type(FFile) :: file, outFile, checkFile

        call startTestGroup("JSON file I/O")

        ! Read a JSON file
        file = FFile(TEST_PATH//"/property/example.json", "read")
        p = FProperty(file)

        ! Write a JSON file
        outFile = FFile(TEST_PATH//"/property/output.json", "write")
        call p%saveToJson(outFile)
        call outFile%close

        outFile = FFile(TEST_PATH//"/property/example.param", "write")
        call p%saveToFile(outFile)
        call outFile%close

        ! Load the output file for verification
        checkFile = FFile(TEST_PATH//"/property/output.json", "read")
        p_check = FProperty(checkFile)

        call check(p == p_check, "JSON files comparison")
        call p%print
        call p%prettyPrint
    end subroutine

    subroutine testPFile
        type(FProperty) :: p, p_check
        type(FFile) :: file, outFile, checkFile

        call startTestGroup("property file I/O")

        ! Read a property file
        file = FFile(TEST_PATH//"/property/example.param", "read")
        call p%readFile(file)
        call file%close

        ! Write a property file
        outFile = FFile(TEST_PATH//"/property/output.param", "write")
        call p%saveToFile(outFile)
        call outFile%close

        checkFile = FFile(TEST_PATH//"/property/output.param", "read")
        call p_check%readFile(checkFile)
        call checkFile%close

        call check(p==p_check, "property file comparison")

        checkFile = FFile(TEST_PATH//"/property/boolean.param", "read")
        p = FProperty()
        call p%set("test", YES)
        call p_check%readFile(checkFile)
        call check(p==p_check, "property file comparison")
        
        ! Read the second test property file
        file = FFile(TEST_PATH//"/property/example2.param", "read")
        call p%readFile(file)
        call file%close
    end subroutine

    subroutine testCommandLine
        type(FProperty) :: p

        call startTestGroup("reading from command line")
        call p%readCommandLine
        call p%print
    end subroutine
end module

program test
    use mod_test
    use test_property

    call testAll

    call testsFinished
end program
