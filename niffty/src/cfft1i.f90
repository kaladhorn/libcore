    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
!******************************************************************************!    
!> FFTPACK 5.1 subroutine CFFT1I initializes array WSAVE for use in
!> its companion routines CFFT1B and CFFT1F.  Routine CFFT1I must
!> be called before the first call to  CFFT1B or CFFT1F, and after
!> whenever the value of integer N changes.
!>
!> The transform is most efficient when N is a product of small primes.
!>
!> LENSAV must be at least 2*N + INT(LOG(REAL(n))/log(2.0_rwp)) + 4.
!>
!> WSAVE is a real work array with dimension LENSAV, containing the prime
!> factors of N and also containing certain trigonometric values which 
!> will be used in routines CFFT1B or CFFT1F.
!******************************************************************************!    
    subroutine cfft1i(n, wsave, lensav, ier)
        implicit none
        include 'kinds.inc'
        integer,   intent(in)  :: n      !< Integer length of the sequence to be transformed
        integer,   intent(in)  :: lensav !< Integer dimension of WSAVE array
        integer,   intent(out) :: ier    !< Status code
        real(rwp), intent(out) :: wsave(lensav) !< Work array containing thedata for subsequent calls to CFFT1B or CFFT1F
        
        integer :: iw1
        
        ier = 0

        if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('CFFTMI ', 3)
        end if

        if (n==1) return

        iw1 = n + n + 1
        call mcfti1(n, wsave, wsave(iw1), wsave(iw1+1))

    end subroutine
