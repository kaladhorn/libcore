!******************************************************************************!
!                            regex_replace module
!______________________________________________________________________________!
!> Procedures used when using a regex object to replace substrings.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2016 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!


!******************************************************************************!
!> Match a regular expression to a character variable and apply any
!> substitution. The returned index is 0 if no match is found, < 0 in case of
!> an error, and > 0 if no match is found. If a substitution pattern is set in
!> the regex, it will be applied to the subject string.
!>
!>see: match
!******************************************************************************!
    subroutine replace(this, subject, position, stat)
        class(FRegex), intent(in) :: this
        character(:), allocatable, intent(inout) :: subject   !< input character variable
        integer, intent(out), optional :: position            !< index of the first match substring
        type(FException), intent(out), optional :: stat       !< error code
!------
        integer :: sLength, c, n, start, last, ind, length, rc
        character(:), allocatable :: buffer, tmp, substring
        integer(c_int), dimension(:), allocatable :: ovector
        type(FException) :: istat
!------
        body: block
            ! Make sure the pattern matches the subject

            ind = 0

            ! Make sure the regex is useable
            if (.not.c_associated(this%re) .or. &
                .not.c_associated(this%study)) then

                call istat%raise("trying to use an uninitialised regex object.")
                exit body
            end if

            ! Try to execute the regex
            allocate(ovector(this%ovecsize))
            ovector = 0
            rc = pcre_exec(        &
                this%re,           &
                this%study,        &
                subject,           &
                len(subject),      &
                this%offset,       &
                this%execOptions,  &
                ovector,           &
                size(ovector))

            ! Error check
            if (rc < 0) then
                call istat%raise(fcre_error_message(rc))
                exit body
            end if

            ! Position of the matched substring
            ind = ovector(1)+1
            length = ovector(2)-ovector(1)-1

            ! Get the matched substring
            allocate(character(length) :: substring)
            substring(:) = subject(ind:ind+length)

            ! Do not go any further if it does not
            if (istat /= 0) exit body

            ! Length of the substituted string
            sLength = len(substring)

            ! Do the substitution
            if (allocated(this%substitute)) then
                if (len(this%substitute) > 0) then

                    ! Get a buffer with the correct length
                    sLength = 0 ! Length of the back reference
                    last = 0    ! No-op, removes a warning about uninitialised last with some compilers

                    do c=1, this%backReferenceCount
                        n = this%backReferenceIndex(c)

                        if (n > 0) then
                            sLength = sLength + ovector(n*2+2) - ovector(n*2+1)
                        else if (n == FCRE_REF_BEFORE) then
                            sLength = sLength + ovector(1)
                        else if (n == FCRE_REF_AFTER) then
                            sLength = sLength + len(subject) - ovector(2)
                        else if (n == FCRE_REF_LAST) then
                            ! Get the last matched capture
                            last = size(ovector) * 2 / 3
                            do while(ovector(last) == 0)
                                last = last - 2
                            end do
                            sLength = sLength + ovector(last) - ovector(last-1)
                        else if (n == FCRE_REF_MATCH) then
                            sLength = sLength + ovector(2) - ovector(1)
                        end if
                    end do
                    sLength = sLength + len(this%substitute)
                    allocate(character(sLength) :: buffer)

                    ! Add the back references content in the substitution string
                    buffer(:len(this%substitute)) = this%substitute
                    buffer(len(this%substitute)+1:) = ""
                    do c=this%backReferenceCount, 1, -1
                        start = this%backReferencePosition(c)
                        n = this%backReferenceIndex(c)
                        if (n > 0) then ! Simple back reference
                            buffer(start:) = subject(ovector(n*2+1)+1:ovector(n*2+2)) // buffer(start:)
                        else if (n == FCRE_REF_BEFORE) then ! Special variable $``
                            buffer(start:) = subject(:ovector(1)) // buffer(start:)
                        else if (n == FCRE_REF_AFTER) then ! Special variable $'
                            buffer(start:) = subject(ovector(2)+1:) // buffer(start:)
                        else if (n == FCRE_REF_LAST) then ! Special variable $+
                            buffer(start:) = subject(ovector(last-1)+1:ovector(last)) // buffer(start:)
                        else if (n == FCRE_REF_MATCH) then ! Special variable $&
                            buffer(start:) = subject(ovector(1)+1:ovector(2)) // buffer(start:)
                        end if
                    end do
                else
                    allocate(character(0) :: buffer)
                end if
            else
                allocate(character(0) :: buffer)
            end if

            ! Replace the whole matched part of the string if a replacement character variable is provided
            tmp = subject(:ind-1) // buffer // subject(ind+length+1:)
            subject = tmp

        end block body

        ! Set the optional outputs
        if (present(position)) position = ind

        ! Report any issue if required
        if (present(stat)) call stat%transfer(istat)
    end subroutine
!******************************************************************************!
!> Match a regular expression to a character variable. The returned
!> index is 0 if no match is found, < 0 in case of an error, and > 0 if no match
!> is found.
!>
!> The difference with the match subroutine is that if a match is found, it will
!> be substituted with the replaceWith string.
!>
!>see: match
!******************************************************************************!
    subroutine replace_all(this, subject, index, stat)
        class(FRegex), intent(inout) :: this
        character(:), allocatable, intent(inout) :: subject
        integer, dimension(:), allocatable, optional, intent(out) :: index
        type(FException), intent(out), optional :: stat
!------
        integer, dimension(:), allocatable :: index_
        integer :: i, c
        type(FException) :: istat
!------
        ! Setup temporary arrays
        allocate(index_(len(subject)))
        index_ = 0

        ! Get the first match
        this%offset = 0
        call this%replace(subject, i, stat=istat)

        c = 0
        do while(i /= 0 .and. istat == 0)

            ! Keep the index and length values
            c = c + 1
            index_(c) = i

            ! Try another match
            call this%replace(subject, i, stat=istat)
        end do
        if (istat == "No match found.") call istat%discard

        ! Output array
        if (present(index)) index = index_(:c)

        ! Report any issue
        if (present(stat)) call stat%transfer(istat)
    end subroutine
