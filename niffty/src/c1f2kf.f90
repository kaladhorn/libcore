    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine c1f2kf(ido, l1, na, cc, in1, ch, in2, wa)
        implicit none
        include 'kinds.inc'
        integer :: ido, l1, na, in1, in2
        real(rwp) :: cc(in1, l1, ido, 2), ch(in2, l1, 2, ido), wa(ido, 1, 2)
        
        integer :: i, k
        real(rwp) :: chold1, chold2, sn, ti2, tr2
        
        if (ido>1) go to 110
        sn = 1.0_rwp/real(2*l1, rwp)
        if (na==1) go to 100
        do k = 1, l1
            chold1 = sn*(cc(1,k,1,1)+cc(1,k,1,2))
            cc(1, k, 1, 2) = sn*(cc(1,k,1,1)-cc(1,k,1,2))
            cc(1, k, 1, 1) = chold1
            chold2 = sn*(cc(2,k,1,1)+cc(2,k,1,2))
            cc(2, k, 1, 2) = sn*(cc(2,k,1,1)-cc(2,k,1,2))
            cc(2, k, 1, 1) = chold2
        end do
        return
100     do k = 1, l1
            ch(1, k, 1, 1) = sn*(cc(1,k,1,1)+cc(1,k,1,2))
            ch(1, k, 2, 1) = sn*(cc(1,k,1,1)-cc(1,k,1,2))
            ch(2, k, 1, 1) = sn*(cc(2,k,1,1)+cc(2,k,1,2))
            ch(2, k, 2, 1) = sn*(cc(2,k,1,1)-cc(2,k,1,2))
        end do
        return
110     do k = 1, l1
            ch(1, k, 1, 1) = cc(1, k, 1, 1) + cc(1, k, 1, 2)
            ch(1, k, 2, 1) = cc(1, k, 1, 1) - cc(1, k, 1, 2)
            ch(2, k, 1, 1) = cc(2, k, 1, 1) + cc(2, k, 1, 2)
            ch(2, k, 2, 1) = cc(2, k, 1, 1) - cc(2, k, 1, 2)
        end do
        do i = 2, ido
            do k = 1, l1
                ch(1, k, 1, i) = cc(1, k, i, 1) + cc(1, k, i, 2)
                tr2 = cc(1, k, i, 1) - cc(1, k, i, 2)
                ch(2, k, 1, i) = cc(2, k, i, 1) + cc(2, k, i, 2)
                ti2 = cc(2, k, i, 1) - cc(2, k, i, 2)
                ch(2, k, 2, i) = wa(i, 1, 1)*ti2 - wa(i, 1, 2)*tr2
                ch(1, k, 2, i) = wa(i, 1, 1)*tr2 + wa(i, 1, 2)*ti2
            end do
        end do
    end subroutine
