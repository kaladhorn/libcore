    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    program trfft2
        implicit none
        include 'kinds.inc'

        integer i, j, l, ldim, m, lensav, ier, lenwrk
        parameter (l=100, m=100, ldim=l)
        ! PARAMETER(LENSAV= L + 3*M +   INT(LOG(REAL(l))/LOG(2))
        ! .                          + 2*INT(LOG(REAL(m))/LOG(2)) + 12)
        parameter (lensav=430)
        parameter (lenwrk=(l+1)*m)
        real (rwp) r(l, m), rcopy(l, m)
        real (rwp) wsave(lensav), work(lenwrk), diff

        ! --- IDENTIFY TEST AND INITIALIZE FFT

        write (6, *) 'PROGRAM TRFFT2 AND RELATED MESSAGES:'
        call rfft2i(l, m, wsave, lensav, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE RFFT2I'
            stop
        end if

        ! --- GENERATE TEST MATRIX FOR FORWARD-BACKWARD TEST

        call random_seed()
        call random_number(r)
        rcopy = r

        ! --- PERFORM FORWARD TRANSFORM

        call rfft2f(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE RFFT2F !'
            stop
        end if

        ! --- PERFORM BACKWARD TRANSFORM

        call rfft2b(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE RFFT2B !'
            stop
        end if


        ! --- PRINT TEST RESULTS
        ier = 0
        diff = 0.
        do i = 1, l
            do j = 1, m
                diff = max(diff, abs(r(i,j)-rcopy(i,j)))
            end do
        end do
        write (6, *) 'RFFT2 FORWARD-BACKWARD MAX ERROR =', diff

        ! --- GENERATE TEST MATRIX FOR BACKWARD-FORWARD TEST

        call random_seed()
        call random_number(r)
        rcopy = r

        ! --- PERFORM BACKWARD TRANSFORM

        call rfft2b(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE RFFT2B !'
            stop
        end if

        ! --- PERFORM FORWARD TRANSFORM

        call rfft2f(ldim, l, m, r, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE RFFT2F !'
            stop
        end if


        ! --- PRINT TEST RESULTS
        ier = 0
        diff = 0.
        do i = 1, l
            do j = 1, m
                diff = max(diff, abs(r(i,j)-rcopy(i,j)))
            end do
        end do
        write (6, *) 'RFFT2 BACKWARD-FORWARD MAX ERROR =', diff

        write (6, '(A,/)') ' END PROGRAM TRFFT2 AND RELATED MESSAGES'
        stop
    end program
