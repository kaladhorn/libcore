    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine tables(ido, ip, wa)
        implicit none
        include 'kinds.inc'
        integer :: ido, ip
        real(rwp) :: wa(ido, ip-1, 2)

        real(rwp) :: tpi, argz, arg1, arg2, arg3, arg4
        integer :: i, j
        
        tpi = 8.*atan(1.0_rwp)
        argz = tpi/real(ip, rwp)
        arg1 = tpi/real(ido*ip, rwp)
        do j = 2, ip
            arg2 = real(j-1, rwp)*arg1
            do i = 1, ido
                arg3 = real(i-1, rwp)*arg2
                wa(i, j-1, 1) = cos(arg3)
                wa(i, j-1, 2) = sin(arg3)
            end do
            if (ip<=5) go to 100
            arg4 = real(j-1, rwp)*argz
            wa(1, j-1, 1) = cos(arg4)
            wa(1, j-1, 2) = sin(arg4)
100     end do
    end subroutine
