!******************************************************************************!
!                            mod_fileReader module
!------------------------------------------------------------------------------!
!> `[[FFileReader(type)]]` abstract derived type definition.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_fileReader

    use mod_string,    only: FString
    use mod_exception, only: FException

    implicit none
    private
    save

!******************************************************************************!
!> Abstract file reader
!******************************************************************************!
    type, abstract, public :: FFileReader
    contains
        procedure(reader_canRead), deferred, pass(this), public :: canRead
        procedure(reader_canWrite), deferred, pass(this), public :: canWrite

        procedure(reader_openFile), deferred, pass(this), public :: openFile
        procedure(reader_closeFile), deferred, pass(this), public :: closeFile

        procedure(reader_getNextLine_character), deferred, pass(this), private :: getNextLine_character
        procedure(reader_getNextLine_string), deferred, pass(this), private :: getNextLine_string
        generic, public :: getNextLine => getNextLine_character, getNextLine_string
        procedure(reader_getLines), deferred, pass(this), public :: getLines

        procedure(reader_writeLine), deferred, pass(this), public :: writeLine
        procedure(reader_writeLines), deferred, pass(this), public :: writeLines
    end type

    abstract interface
        function reader_canRead(this)
            import FFileReader
            class(FFileReader), intent(in) :: this
            logical :: reader_canRead
        end function

        function reader_canWrite(this)
            import FFileReader
            class(FFileReader), intent(in) :: this
            logical :: reader_canWrite
        end function

        subroutine reader_openFile(this, filename, mode, stat)
            import FFileReader, FException
            class(FFileReader), intent(inout), target :: this
            character(*), intent(in) :: filename
            character(*), intent(in) :: mode
            type(FException), intent(out), optional :: stat
        end subroutine

        subroutine reader_closeFile(this, stat)
            import FFileReader, FException
            class(FFileReader), intent(inout) :: this
            type(FException), intent(out), optional :: stat
        end subroutine

        subroutine reader_getNextLine_character(this, line, stat)
            import FFileReader, FException
            class(FFileReader), intent(inout) :: this
            character(*), intent(out) :: line
            type(FException), intent(out), optional :: stat
        end subroutine

        subroutine reader_getNextLine_string(this, line, stat)
            import FFileReader, FException, FString
            class(FFileReader), intent(inout) :: this
            type(FString), intent(out) :: line
            type(FException), intent(out), optional :: stat
        end subroutine

        subroutine reader_getLines(this, lines, stat)
            import FFileReader, FException, FString
            class(FFileReader), intent(inout) :: this
            type(FString), dimension(:), allocatable, intent(out) :: lines
            type(FException), intent(out), optional :: stat
        end subroutine

        subroutine reader_writeLine(this, line, stat)
            import FFileReader, FException
            class(FFileReader), intent(inout) :: this
            character(*), intent(in) :: line
            type(FException), intent(out), optional :: stat
        end subroutine

        subroutine reader_writeLines(this, lines, stat)
            import FFileReader, FException, FString
            class(FFileReader), intent(inout) :: this
            type(FString), dimension(:), intent(in) :: lines
            type(FException), intent(out), optional :: stat
        end subroutine
    end interface
    character(*), parameter, public :: MODE_READ   = "read"
    character(*), parameter, public :: MODE_WRITE  = "write"
    character(*), parameter, public :: MODE_APPEND = "append"
end module
