    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cost1i(n, wsave, lensav, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        integer n, lensav, ier
        real (rwp) wsave(lensav)

        ier = 0

        if (lensav<2*n+int(log(real(n, rwp))/log(2.0_rwp))+4) then
            ier = 2
            call xerfft('COST1I', 3)
            go to 100
        end if

        if (n<=3) return
        nm1 = n - 1
        np1 = n + 1
        ns2 = n/2
        pi = 4.*atan(1.0_rwp)
        dt = pi/float(nm1)
        fk = 0.
        do k = 2, ns2
            kc = np1 - k
            fk = fk + 1.
            wsave(k) = 2.*sin(fk*dt)
            wsave(kc) = 2.*cos(fk*dt)
        end do
        lnsv = nm1 + int(log(real(nm1, rwp))/log(2.0_rwp)) + 4
        call rfft1i(nm1, wsave(n+1), lnsv, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('COST1I', -5)
        end if
100     continue
    end subroutine
