    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mcfti1(n, wa, fnf, fac)
        implicit none
        include 'kinds.inc'
        real(rwp), intent(out) :: wa(*)
        real(rwp), intent(out) :: fac(*)
        integer, intent(in) :: n
        real(rwp), intent(out) :: fnf
        
        integer :: ido, ip, iw, k1, l1, l2, nf
        
        call factor(n, nf, fac)
        fnf = nf
        iw = 1
        l1 = 1
        do k1 = 1, nf
            ip = fac(k1)
            l2 = l1*ip
            ido = n/l2
            call tables(ido, ip, wa(iw))
            iw = iw + (ip-1)*(ido+ido)
            l1 = l2
        end do
    end subroutine
