!******************************************************************************!
!                          mod_formattedString module
!______________________________________________________________________________!
!> `[[FFormattedString(type)]]` derived type and TBPs, to represent strings with
!> formatting. Formatting can be defined using Markdown syntax. Format elements
!> are interpreted and turned into ANSI codes to display in a terminal. On top
!> of the standard Markdown tags, colours are supported using the syntax
!> `{string}{colour}`, where `colour` can be `black`, `red`, `green`, `yellow`,
!> `blue`, `magenta`, `cyan` or `white`.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_formattedString
    use iso_c_binding

    use mod_regex,     only: FRegex
    use mod_string,    only: FString
    use ext_character, only: c_f_string
    use mod_exception, only: FException

    implicit none
    private
    save

!******************************************************************************!
!> Derived type containing a formatted variable-length character scalar.
!******************************************************************************!
    type, public, extends(FString) :: FFormattedString
    end type

!******************************************************************************!
!> Constructor interface
!******************************************************************************!
    interface FFormattedString
        module procedure formattedString_init
    end interface

!******************************************************************************!
!> Interface to the wrapper around the libsoldout library.
!******************************************************************************!
    interface
        function md2ansi(input) bind(C, name="md2ansi")
            use iso_c_binding
            type(c_ptr) :: md2ansi
            character(1,c_char), dimension(*), intent(in) :: input
        end function
    end interface
contains
!******************************************************************************!
!> Initialise a formatted string.
!> The current format markers are:
!>
!>  - `*` for bold text;
!>  - `_` for underlined text;
!>  - `{text}{colour}` for coloured text.
!>
!> For now, a character in the string can only have one format. If this is not
!> the case, the result might be strange. The output string ends with a reset
!> sequence, so that the consequences of a bad format is limited to the current
!> string.
!******************************************************************************!
    function formattedString_init(input) result(this)
        character(*), intent(in) :: input
        type(FFormattedString) :: this
!------
        character(:,c_char), allocatable :: tmp, formattedString
        type(c_ptr) :: ptr
        type(FException) :: istat
        type(FRegex) :: blackRegex, redRegex, greenRegex, yellowRegex, &
            blueRegex, magentaRegex, cyanRegex, whiteRegex
!------

        tmp = input // C_NULL_CHAR

        ! Look for colours
        blackRegex   = FRegex("\{(.*)\}\s*\{black\}",   char(27) // "[30m$1" // char(27) // "[39m", "r")
        redRegex     = FRegex("\{(.*)\}\s*\{red\}",     char(27) // "[31m$1" // char(27) // "[39m", "r")
        greenRegex   = FRegex("\{(.*)\}\s*\{green\}",   char(27) // "[32m$1" // char(27) // "[39m", "r")
        yellowRegex  = FRegex("\{(.*)\}\s*\{yellow\}",  char(27) // "[33m$1" // char(27) // "[39m", "r")
        blueRegex    = FRegex("\{(.*)\}\s*\{blue\}",    char(27) // "[34m$1" // char(27) // "[39m", "r")
        magentaRegex = FRegex("\{(.*)\}\s*\{magenta\}", char(27) // "[35m$1" // char(27) // "[39m", "r")
        cyanRegex    = FRegex("\{(.*)\}\s*\{cyan\}",    char(27) // "[36m$1" // char(27) // "[39m", "r")
        whiteRegex   = FRegex("\{(.*)\}\s*\{white\}",   char(27) // "[37m$1" // char(27) // "[39m", "r")

        call blackRegex%replace_all(tmp, stat=istat)
        call redRegex%replace_all(tmp, stat=istat)
        call greenRegex%replace_all(tmp, stat=istat)
        call yellowRegex%replace_all(tmp, stat=istat)
        call blueRegex%replace_all(tmp, stat=istat)
        call magentaRegex%replace_all(tmp, stat=istat)
        call cyanRegex%replace_all(tmp, stat=istat)
        call whiteRegex%replace_all(tmp, stat=istat)

        ! Apply the format
        ptr = md2ansi(tmp)
        call c_f_string(ptr, formattedString)

        this%FString = FString(formattedString(:len(formattedString)-1))
    end function
end module
