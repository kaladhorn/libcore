    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine rfft2i(l, m, wsave, lensav, ier)
        implicit none
        include 'kinds.inc'
        integer l, m, lensav, ier
        integer lwsav, mwsav, mmsav, ier1
        real (rwp) wsave(lensav)

        ! INITIALIZE IER

        ier = 0

        ! VERIFY LENSAV

        lwsav = l + int(log(real(l, rwp))/log(2.0_rwp)) + 4
        mwsav = 2*m + int(log(real(m, rwp))/log(2.0_rwp)) + 4
        mmsav = m + int(log(real(m, rwp))/log(2.0_rwp)) + 4
        if (lensav<lwsav+mwsav+mmsav) then
            ier = 2
            call xerfft('RFFT2I', 4)
            go to 100
        end if

        call rfftmi(l, wsave(1), lwsav, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('RFFT2I', -5)
            go to 100
        end if
        call cfftmi(m, wsave(lwsav+1), mwsav, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('RFFT2I', -5)
        end if

        call rfftmi(m, wsave(lwsav+mwsav+1), mmsav, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('RFFT2I', -5)
            go to 100
        end if

100     continue
    end subroutine
