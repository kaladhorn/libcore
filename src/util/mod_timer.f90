!******************************************************************************!
!                            mod_timer module
!------------------------------------------------------------------------------!
!> `[[FTimer(type)]]` derived type and type-bound procedures, for CPU time and
!> wall time measurments.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_timer
    use iso_fortran_env

    use core, only: long, wp !LCOV_EXCL_LINE

    implicit none
    private
    save

!******************************************************************************!
!> Derived type for timer objects.
!******************************************************************************!
    type, public :: FTimer
        private
        integer(long)  :: wallTimeCount = 0     !<
        integer(long)  :: cumulatedWallTime = 0 !<
        real(wp)       :: wallTimeClockRate = 0 !<

        real(wp) :: cpuTimerStart = 0     !<
        real(wp) :: cumulatedCpuTime = 0  !<
    contains
        procedure, public :: start    !< Start the timer
        procedure, public :: stop     !< Pause the timer
        procedure, public :: reset    !< Reset the timer
        procedure, public :: walltime !< Get the elapsed wall time
        procedure, public :: getWalltime !< Get the elapsed wall time
        procedure, public :: cpuTime     !< Get the elapsed CPU time
        procedure, public :: getCpuTime  !< Get the elapsed CPU time
    end type

contains
!******************************************************************************!
!> Start a timer.
!******************************************************************************!
    subroutine start(this)
        class(FTimer), intent(inout) :: this
!------
        if (this%wallTimeCount == 0) then
            call system_clock(count=this%wallTimeCount, count_rate=this%wallTimeClockRate)
        end if
        if (this%cpuTimerStart == 0) then
            call cpu_time(this%cpuTimerStart)
        end if
    end subroutine
!******************************************************************************!
!> Stop a timer.
!******************************************************************************!
    subroutine stop(this)
        class(FTimer), intent(inout) :: this
!------
        integer(long) :: count
        real(wp) :: time
!------
        ! Stop the wall time counter
        if (this%wallTimeCount /= 0) then
            call system_clock(count=count)
            this%cumulatedWallTime = this%cumulatedWallTime + (count-this%wallTimeCount)
            this%wallTimeCount = 0
        end if

        ! Stop the CPU timer
        if (this%cpuTimerStart /= 0) then
            call cpu_time(time)
            this%cumulatedCpuTime = time - this%cpuTimerStart
            this%cpuTimerStart = 0
        end if
    end subroutine
!******************************************************************************!
!> Reset a timer.
!******************************************************************************!
    subroutine reset(this)
        class(FTimer), intent(inout) :: this
!------
        this%wallTimeCount = 0
        this%cumulatedWallTime = 0
        this%cpuTimerStart = 0
        this%cumulatedCpuTime = 0
    end subroutine
!******************************************************************************!
!> Read the wall time from a timer.
!******************************************************************************!
    function walltime(this) result(t)
        class(FTimer), intent(in) :: this
        real(wp) :: t
!------
        t = 0
        if (this%cumulatedWallTime /= 0) then
            t = this%cumulatedWallTime / this%wallTimeClockRate
        end if
    end function
!******************************************************************************!
!> Read the wall time from a timer.
!******************************************************************************!
    subroutine getWalltime(this, t)
        class(FTimer), intent(in) :: this
        real(wp), intent(out) :: t
!------
        t = 0
        if (this%cumulatedWallTime /= 0) then
            t = this%cumulatedWallTime / this%wallTimeClockRate
        end if
    end subroutine
!******************************************************************************!
!> Read the CPU time from a timer.
!******************************************************************************!
    function cpuTime(this) result(t)
        class(FTimer), intent(in) :: this
        real(wp) :: t
!------
        t = 0
        if (this%cumulatedCpuTime /= 0) then
            t = this%cumulatedCpuTime
        end if
    end function
!******************************************************************************!
!> Read the CPU time from a timer.
!******************************************************************************!
    subroutine getCpuTime(this, t)
        class(FTimer), intent(in) :: this
        real(wp), intent(out) :: t
!------
        t = 0
        if (this%cumulatedCpuTime /= 0) then
            t = this%cumulatedCpuTime
        end if
    end subroutine
end module !LCOV_EXCL_LINE
