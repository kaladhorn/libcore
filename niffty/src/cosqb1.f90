    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine cosqb1(n, inc, x, wsave, work, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) x, wsave, work
        dimension x(inc, *), wsave(*), work(*)

        ier = 0
        ns2 = (n+1)/2
        np2 = n + 2
        do i = 3, n, 2
            xim1 = x(1, i-1) + x(1, i)
            x(1, i) = .5_rwp*(x(1,i-1)-x(1,i))
            x(1, i-1) = .5_rwp*xim1
        end do
        x(1, 1) = .5_rwp*x(1, 1)
        modn = mod(n, 2)
        if (modn/=0) go to 100
        x(1, n) = .5_rwp*x(1, n)
100     lenx = inc*(n-1) + 1
        lnsv = n + int(log(real(n, rwp))/log(2.0_rwp)) + 4
        lnwk = n

        call rfft1b(n, inc, x, lenx, wsave(n+1), lnsv, work, lnwk, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('COSQB1', -5)
            go to 120
        end if

        do k = 2, ns2
            kc = np2 - k
            work(k) = wsave(k-1)*x(1, kc) + wsave(kc-1)*x(1, k)
            work(kc) = wsave(k-1)*x(1, k) - wsave(kc-1)*x(1, kc)
        end do
        if (modn/=0) go to 110
        x(1, ns2+1) = wsave(ns2)*(x(1,ns2+1)+x(1,ns2+1))
110     do k = 2, ns2
            kc = np2 - k
            x(1, k) = work(k) + work(kc)
            x(1, kc) = work(k) - work(kc)
        end do
        x(1, 1) = x(1, 1) + x(1, 1)
120     return
    end subroutine
