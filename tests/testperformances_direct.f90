program testPerformanceDirect
    use core
    use ext_character
    
    use mod_expression, only: FExpression
    use mod_timer, only: FTimer
    
    implicit none
    
    real(e) :: x, y, z
    integer :: i, Niter
    type(FTimer) :: timer
    real(e), dimension(:), allocatable :: res
    
    Niter = 5000000
    allocate(res(Niter))
    res = 0
    
    x = 0.175_e
    y = 0.110_e
    z = 0.900_e
    
    call timer%start
    do i = 1, Niter
        res(i) = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+ &
            x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))
    end do
    call timer%stop
end program