/*******************************************************************************
*                           mod_string_helper.c                                *
*------------------------------------------------------------------------------*
* Interface with the soldout library for Markdown parsing.                     *
*                                                                              *
*  Written by Paul Fossati, <paul.fossati@gmail.com>                           *
*  Copyright (c)  2016 Paul Fossati                                            *
*                                                                              *
*------------------------------------------------------------------------------*
* Redistribution and use in source and binary forms, with or without           *
* modification, are permitted provided that the following conditions are met:  *
*                                                                              *
*     * Redistributions of source code must retain the above copyright notice, *
*       this list of conditions and the following disclaimer.                  *
*                                                                              *
*     * Redistributions in binary form must reproduce the above copyright      *
*       notice, this list of conditions and the following disclaimer in the    *
*       documentation and/or other materials provided with the distribution.   *
*                                                                              *
*     * The name of the author may not be used to endorse or promote products  *
*      derived from this software without specific prior written permission    *
*      from the author.                                                        *
*                                                                              *
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  *
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    *
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   *
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     *
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          *
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         *
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     *
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      *
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      *
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   *
* POSSIBILITY OF SUCH DAMAGE.                                                  *
*******************************************************************************/
#include "markdown.h"
#include "buffer.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


/*******************************************************************************
* Markdow to ANSI renderer                                                     *
*******************************************************************************/

// static void
// latex_text_escape(struct buf *ob, char *src, size_t size) {
// 	size_t  i = 0, org;
// 	while (i < size) {
// 		/* copying directly unescaped characters */
// 		org = i;
// 		while (i < size && src[i] != '&' && src[i] != '%'
// 		&& src[i] != '$' && src[i] != '#' && src[i] != '_'
// 		&& src[i] != '{' && src[i] != '}' && src[i] != '~'
// 		&& src[i] != '^' && src[i] != '\\' && src[i] != '<'
// 		&& src[i] != '>')
// 			i += 1;
// 		if (i > org) bufput(ob, src + org, i - org);
//
// 		/* escaping */
// 		if (i >= size) break;
// 		else if (src[i] == '&') BUFPUTSL(ob, "\\&");
// 		else if (src[i] == '%') BUFPUTSL(ob, "\\%");
// 		else if (src[i] == '$') BUFPUTSL(ob, "\\$");
// 		else if (src[i] == '#') BUFPUTSL(ob, "\\#");
// 		else if (src[i] == '_') BUFPUTSL(ob, "\\_");
// 		else if (src[i] == '{') BUFPUTSL(ob, "\\{");
// 		else if (src[i] == '}') BUFPUTSL(ob, "\\}");
// 		else if (src[i] == '<') BUFPUTSL(ob, "$<$");
// 		else if (src[i] == '>') BUFPUTSL(ob, "$<$");
// 		else if (src[i] == '~') BUFPUTSL(ob, "\\textasciitilde{}");
// 		else if (src[i] == '^') BUFPUTSL(ob, "\\textasciicircum{}");
// 		else if (src[i] == '\\') BUFPUTSL(ob, "\\textbackslash{}");
// 		i += 1; } }

static void
ansi_prolog(struct buf *ob, void *opaque) {
}

static void
ansi_epilog(struct buf *ob, void *opaque) {
}

// static int
// latex_autolink(struct buf *ob, struct buf *link, enum mkd_autolink type,
// 						void *opaque) {
// 	if (!link || !link->size) return 0;
// 	BUFPUTSL(ob, "\\href{");
// 	if (type == MKDA_IMPLICIT_EMAIL) BUFPUTSL(ob, "mailto:");
// 		bufput(ob, link->data, link->size);
// 	BUFPUTSL(ob, "}{");
// 	if (type == MKDA_EXPLICIT_EMAIL && link->size > 7)
// 		latex_text_escape(ob, link->data + 7, link->size - 7);
// 	else	latex_text_escape(ob, link->data, link->size);
// 	BUFPUTSL(ob, "}");
// 	return 1; }

// static int
// latex_link(struct buf *ob, struct buf *link, struct buf *title,
// 			struct buf *content, void *opaque) {
// 	BUFPUTSL(ob, "\\href{");
// 	if (link && link->size) bufput(ob, link->data, link->size);
// 	BUFPUTSL(ob, "}{");
// 	if (content && content->size)
// 		bufput(ob, content->data, content->size);
// 	BUFPUTSL(ob, "}");
// 	return 1; }

// static int
// latex_image(struct buf *ob, struct buf *link, struct buf *title,
// 			struct buf *alt, void *opaque) {
// 	if (!link || !link->size) return 0;
// 	BUFPUTSL(ob, "\\includegraphics{");
// 	bufput(ob, link->data, link->size);
// 	BUFPUTSL(ob, "}");
// 	return 1; }

static void
ansi_blockcode(struct buf *ob, struct buf *text, void *opaque) {
	if (ob->size) bufputc(ob, '\n');
	if (text) bufput(ob, text->data, text->size);
}

// static void
// latex_blockquote(struct buf *ob, struct buf *text, void *opaque) {
// 	if (ob->size) bufputc(ob, '\n');
// 	BUFPUTSL(ob, "\\begin{quote}\n");
// 	if (text) bufput(ob, text->data, text->size);
// 	BUFPUTSL(ob, "\\end{quote}\n"); }

// static int
// latex_codespan(struct buf *ob, struct buf *text, void *opaque) {
// 	BUFPUTSL(ob, "\\texttt{");
// 	if (text) latex_text_escape(ob, text->data, text->size);
// 	BUFPUTSL(ob, "}");
// 	return 1; }

// static void
// latex_header(struct buf *ob, struct buf *text, int level, void *opaque) {
// 	if (ob->size) bufputc(ob, '\n');
// 	switch(level) {
// 		case 1:
// 			BUFPUTSL(ob,"\\section{");
// 			break;
// 		case 2:
// 			BUFPUTSL(ob, "\\subsection{");
// 			break;
// 		case 3:
// 			BUFPUTSL(ob, "\\subsubsection{");
// 			break;
// 		default:
// 			fprintf(stderr, "Warning: ignoring header level %d\n",
//                                         level);
// 	}
// 	if (text) bufput(ob, text->data, text->size);
// 	if (level >= 1 && level <= 3) BUFPUTSL(ob, "}\n");
// }

static int
ansi_double_emphasis(struct buf *ob, struct buf *text, char c, void *opaque) {
	if (!text || !text->size) return 0;
	BUFPUTSL(ob, "\033[1m");
	bufput(ob, text->data, text->size);
	BUFPUTSL(ob, "\033[22m");
	return 1; }

static int
ansi_emphasis(struct buf *ob, struct buf *text, char c, void *opaque) {
	if (!text || !text->size) return 0;
	BUFPUTSL(ob, "\033[4m");
	if (text) bufput(ob, text->data, text->size);
	BUFPUTSL(ob, "\033[24m");
	return 1;
}

static int
ansi_linebreak(struct buf *ob, void *opaque) {
	BUFPUTSL(ob, "\n");
	return 1;
}

static void
ansi_paragraph(struct buf *ob, struct buf *text, void *opaque) {
	if (ob->size) bufputc(ob, '\n');
	if (text) bufput(ob, text->data, text->size);
	BUFPUTSL(ob, "\n"); }

// static void
// ansi_list(struct buf *ob, struct buf *text, int flags, void *opaque) {
// 	if (ob->size) bufputc(ob, '\n');
// 	if (text) bufput(ob, text->data, text->size);

// static void
// ansi_listitem(struct buf *ob, struct buf *text, int flags, void *opaque) {
// 	BUFPUTSL(ob, " * ");
// 	if (text) {
// 		while (text->size && text->data[text->size - 1] == '\n')
// 			text->size -= 1;
// 		bufput(ob, text->data, text->size); }
// 	BUFPUTSL(ob, "\n"); }

// static void
// latex_hrule(struct buf *ob, void *opaque) {
// 	if (ob->size) bufputc(ob, '\n');
// 	BUFPUTSL(ob, "\\hrule"); }

// static int
// latex_triple_emphasis(struct buf *ob, struct buf *text, char c, void *opaque) {
// 	if (!text || !text->size) return 0;
// 	BUFPUTSL(ob, "\\textbf{\\emph{");
// 	bufput(ob, text->data, text->size);
// 	BUFPUTSL(ob, "}}");
// 	return 1; }

static void
ansi_entity(struct buf *ob, struct buf *entity, void *opaque) {
		bufput(ob, entity->data, entity->size);
}

static void
ansi_normal_text(struct buf *ob, struct buf *text, void *opaque) {
	if (text) bufput(ob, text->data, text->size);
}


/* renderer structure */
static struct mkd_renderer to_ansi = {
	/* document-level callbacks */
	ansi_prolog,
	ansi_epilog,

	/* block-level callbacks */
	ansi_blockcode,
	NULL,
	ansi_blockcode,
	NULL,
	NULL,
	NULL,
	NULL,
	ansi_paragraph,
	NULL,
	NULL,
	NULL,

	/* span-level callbacks */
	NULL,
	NULL,
	ansi_double_emphasis,
	ansi_emphasis,
	NULL,
	ansi_linebreak,
	NULL,
	NULL,
	NULL,

	/* low-level callbacks */
	ansi_entity,
	ansi_normal_text,

	/* renderer data */
	64,
	"*_",
	NULL };

#define DEFAULT_OUTPUT_SIZE 100

char *md2ansi(char *input) {

	char *output;

	// Create a buffer object with the input
	struct buf *ib;
	ib = bufnew(strlen(input));
	ib->data = input;
	ib->size = strlen(input);
	ib->unit = 0;
	ib->ref = 0;

	// Create a buffer object for the output
	struct buf *ob;
	ob = bufnew(DEFAULT_OUTPUT_SIZE);

	markdown(ob, ib, &to_ansi);
	output = ob->data;
	output[ob->size] = '\0';
	ob->data = NULL;
	ib->data = NULL;

	// Cleanup
	bufrelease(ib);
	bufrelease(ob);
	return output;
}
