    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    program tcfft2
        implicit none
        include 'kinds.inc'

        integer i, j, l, ldim, m, lensav, ier, lenwrk
        parameter (l=100, m=100, ldim=l)
        ! PARAMETER(LENSAV=2*(L+M) + INT(LOG(REAL(L))/LOG(2))
        ! .                         + INT(LOG(REAL(M))/LOG(2)) + 8)
        parameter (lensav=420)
        parameter (lenwrk=2*l*m)
        complex (cwp) c(l, m)
        real (rwp) rr(l, m), ri(l, m)
        real (rwp) wsave(lensav), work(lenwrk), diff

        ! --- IDENTIFY TEST AND INITIALIZE FFT

        write (6, *) 'PROGRAM TCFFT2 AND RELATED MESSAGES:'
        call cfft2i(l, m, wsave, lensav, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT2I'
            stop
        end if

        ! --- GENERATE TEST MATRIX FOR FORWARD-BACKWARD TEST

        call random_seed()
        call random_number(rr)
        call random_number(ri)
        c = cmplx(rr, ri)

        ! --- PERFORM FORWARD TRANSFORM

        call cfft2f(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT2F !'
            stop
        end if

        ! --- PERFORM BACKWARD TRANSFORM

        call cfft2b(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT2B !'
            stop
        end if


        ! --- PRINT TEST RESULTS
        ier = 0
        diff = 0.
        do i = 1, l
            do j = 1, m
                diff = max(diff, abs(c(i,j)-cmplx(rr(i,j),ri(i,j))))
            end do
        end do
        write (6, *) 'CFFT2 FORWARD-BACKWARD MAX ERROR =', diff

        ! --- GENERATE TEST MATRIX FOR BACKWARD-FORWARD TEST

        call random_seed()
        call random_number(rr)
        call random_number(ri)
        c = cmplx(rr, ri)

        ! --- PERFORM BACKWARD TRANSFORM

        call cfft2b(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT2B !'
            stop
        end if

        ! --- PERFORM FORWARD TRANSFORM

        call cfft2f(ldim, l, m, c, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT2F !'
            stop
        end if


        ! --- PRINT TEST RESULTS
        ier = 0
        diff = 0.
        do i = 1, l
            do j = 1, m
                diff = max(diff, abs(c(i,j)-cmplx(rr(i,j),ri(i,j))))
            end do
        end do
        write (6, *) 'CFFT2 BACKWARD-FORWARD MAX ERROR =', diff

        write (6, '(A,/)') ' END PROGRAM TCFFT2 AND RELATED MESSAGES'
        stop
    end program
