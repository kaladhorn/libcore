!******************************************************************************!
!                           com_geometry module
!______________________________________________________________________________!
!> Misc geometry-related procedures.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module com_geometry
    use core
    use com_math, only: cross_product

    implicit none
    private
    save

    integer, parameter, public :: CARTESIAN_COORDINATES = 1
    integer, parameter, public :: CYLINDRIC_COORDINATES = 2
    integer, parameter, public :: REDUCED_CARTESIAN     = 3

    public :: testIntersection
contains
!******************************************************************************!
!> Determine whether a ray intersects a triangle
!>
!> Determine whether a ray intersects a triangle using a Möller–Trumbore
!> algorithm.
!> The code is mostly from
!> http://en.wikipedia.org/wiki/Möller–Trumbore_intersection_algorithm
!******************************************************************************!
    function intersects(a, b, c, origin, direction)
        real(e), dimension(3), intent(in) ::a
        real(e), dimension(3), intent(in) ::b
        real(e), dimension(3), intent(in) ::c
        real(e), dimension(3), intent(in) ::origin
        real(e), dimension(3), intent(in) ::direction
        logical :: intersects
!------
        real(e), dimension(3) :: e1, e2, P, Q, T
        real(e) :: d, id, u, v, tolerance, test
!------
        intersects = .false.
        tolerance = 1.0e-6_e

        ! Find vectors for two edges sharing V1
        e1 = b - a
        e2 = c - a

        ! Begin calculating determinant - also used to calculate u parameter
        P = cross_product(direction, e2)

        ! If determinant is near zero, ray lies in plane of triangle
        d = dot_product(e1, P)
        if (d > -tolerance .and. d < tolerance) then
            return
        end if
        id = 1.0_e/d

        ! Calculate distance from a to ray origin
        T = origin - a

        ! Calculate u parameter and test bound
        u = dot_product(t, p) * id

        ! The intersection lies outside of the triangle
        if (u < 0 .or. u > 1) return

        ! Prepare to test v parameter
        Q = cross_product(T, e1)

        ! Calculate V parameter and test bound
        v = dot_product(direction, Q) * id;

        ! The intersection lies outside of the triangle
        if (v < 0 .or. u + v  > 1) return

        test = dot_product(e2, Q) * id

        if(test > tolerance) intersects = .true. !ray intersection
    end function


    subroutine testIntersection(a, b, c, origin, direction, intersects, warning)
        real(e), dimension(3), intent(in) ::a
        real(e), dimension(3), intent(in) ::b
        real(e), dimension(3), intent(in) ::c
        real(e), dimension(3), intent(in) ::origin
        real(e), dimension(3), intent(in) ::direction
        logical, intent(out) :: intersects
        logical, intent(out) :: warning
!------
        real(e), dimension(3) :: e1, e2, P, Q, T
        real(e) :: d, id, u, v, tolerance, test
!------
        intersects = .false.
        warning = .false.
        tolerance = 1.0e-6_e

        ! Find vectors for two edges sharing V1
        e1 = b - a
        e2 = c - a

        ! Begin calculating determinant - also used to calculate u parameter
        P = cross_product(direction, e2)

        ! If determinant is near zero, ray lies in plane of triangle
        d = dot_product(e1, P)
        if (d > -tolerance .and. d < tolerance) then
            warning = .true.
            return
        end if
        id = 1.0_e/d

        ! Calculate distance from a to ray origin
        T = origin - a

        ! Calculate u parameter and test bound
        u = dot_product(t, p) * id

        ! The intersection lies outside of the triangle
        if (u < 0 .or. u > 1) return

        ! Prepare to test v parameter
        Q = cross_product(T, e1)

        ! Calculate V parameter and test bound
        v = dot_product(direction, Q) * id;

        ! The intersection lies outside of the triangle
        if (v < 0 .or. u + v  > 1) return

        test = dot_product(e2, Q) * id

        if(test > tolerance) then
            intersects = .true. !ray intersection
            warning = .true.
        end if
    end subroutine

    subroutine getEulerAngles(M, theta, phi, psi)
        real(e), dimension(3,3), intent(in) :: M
        real(e), intent(out) :: theta, phi, psi

        real(e) :: cost

        if (M(3,1) /= -1 .and. M(3,1) /= 1) then
            theta = -asin(M(3,1))
            cost = cos(theta)
            psi = atan2(M(3,2)/cost, M(3,3)/cost)
            phi = atan2(M(2,1)/cost, M(1,1)/cost)
        else
            phi = 0
            if (M(3,1) == -1) then
                theta = HALFPI
                psi = phi + atan2(M(1,2), M(1,3))
            else
                theta = -HALFPI
                psi = -phi + atan2(-M(1,2), -M(1,3))
            end if
        end if
    end subroutine
end module
