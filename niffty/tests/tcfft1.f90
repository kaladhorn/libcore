    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    program tcfft1
        implicit none
        include 'kinds.inc'

        integer i, n, lensav, ier, lenwrk
        parameter (n=1000)
        ! PARAMETER(LENSAV=2*N + INT(LOG(REAL(N))/LOG(2)) + 4)
        parameter (lensav=2013)
        parameter (lenwrk=2*n)
        complex (cwp) c(n)
        real (rwp) rr(n), ri(n)
        real (rwp) wsave(lensav), work(lenwrk), diff

        ! --- IDENTIFY TEST AND INITIALIZE FFT

        write (6, *) 'PROGRAM TCFFT1 AND RELATED MESSAGES:'
        call cfft1i(n, wsave, lensav, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT1I'
            stop
        end if

        ! --- GENERATE TEST VECTOR FOR BACKWARD-FORWARD TEST

        call random_seed()
        call random_number(rr)
        call random_number(ri)
        c = cmplx(rr, ri)

        ! --- PERFORM BACKWARD TRANSFORM

        call cfft1b(n, 1, c, n, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT1B !'
            stop
        end if

        ! --- PERFORM FORWARD TRANSFORM

        call cfft1f(n, 1, c, n, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT1F !'
            stop
        end if

        ! --- PRINT TEST RESULTS

        ier = 0
        diff = 0.
        do i = 1, n
            diff = max(diff, abs(c(i)-cmplx(rr(i),ri(i))))
        end do
        write (6, *) 'CFFT1 BACKWARD-FORWARD MAX ERROR =', diff

        ! --- IDENTIFY TEST AND INITIALIZE FFT

        write (6, *) 'PROGRAM TCFFT1 AND RELATED MESSAGES:'
        call cfft1i(n, wsave, lensav, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT1I'
            stop
        end if

        ! --- GENERATE TEST VECTOR FOR FORWARD-BACKWARD TEST

        call random_seed()
        call random_number(rr)
        call random_number(ri)
        c = cmplx(rr, ri)

        ! --- PERFORM FORWARD TRANSFORM

        call cfft1f(n, 1, c, n, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT1F !'
            stop
        end if

        ! --- PERFORM BACKWARD TRANSFORM

        call cfft1b(n, 1, c, n, wsave, lensav, work, lenwrk, ier)
        if (ier/=0) then
            write (6, *) 'ERROR ', ier, ' IN ROUTINE CFFT1B !'
            stop
        end if


        ! --- PRINT TEST RESULTS
        ier = 0
        diff = 0.
        do i = 1, n
            diff = max(diff, abs(c(i)-cmplx(rr(i),ri(i))))
        end do
        write (6, *) 'CFFT1 FORWARD-BACKWARD MAX ERROR =', diff

        write (6, '(A,/)') ' END PROGRAM TCFFT1 AND RELATED MESSAGES'
        stop
    end program
