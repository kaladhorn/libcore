!******************************************************************************!
!>                             test_regex module
!------------------------------------------------------------------------------!
!>
!> Unit tests for the mod_regex module of the FCRE library. Uses FUnit from the
!> FLIBS project, available at http://flibs.sourceforge.net
!>
!> Written by Paul Fossati, <paul.fossati@gmail.com>
!> Copyright (c) 2014 Paul Fossati
!>
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module test_regex
    use core
    use ext_character

    use mod_test

    use mod_regex,            only: FRegex
    use mod_string,           only: FString
    use mod_exception,        only: FException
    use mod_stringDictionary, only: FStringDictionary

    use fcre
    implicit none
    private
    save

    public :: testAll
contains
    subroutine testAll

        call testFcre
        call test_match_simple_character
        call test_match_simple_string
        call test_match_caseless
        call test_match_reluctant
        call test_match_all
        call test_capture_array
        call test_capture_dictionary
        call test_capture_names
        call test_substitution_simple
        call test_substitution_backref
        call test_substitution_variable_1
        call test_substitution_variable_2
        call test_substitution_variable_3
        call test_substitution_variable_4
        call test_substitution_caseless
        call test_split
        call testsFinished
    end subroutine
!******************************************************************************!
!> Test matching procedure using character variables.
!******************************************************************************!
    subroutine test_match_simple_character()
        type(FRegex) :: re
        integer :: index, index2
        type(FException) :: stat
        character(:), allocatable :: substring, substring2
        type(FString), dimension(:), allocatable :: captures
!------
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b')

        call startTestGroup("simple pattern matching (character version)")

        call check(re%doesMatch('sd dmoir DZ54 po32 54 era312'), &
            "simple pattern matching (character)", &
            "matching did not work")
        call re%match('sd dmoir DZ54 po32 54 era312', index, substring, stat)
        call check(stat == 0, "simple match", stat%string())
        call check(index == 15, "matched substring index", "wrong index: " // index)
        call check(substring == "po32", "matched substring", &
            "wrong matched substring: """ // substring // """.")

        call re%match('sd dmoir DZ54 po32 54 era312', index2, stat)
        call check(index2 == index, "match_single_position_character", &
            "inconsistent index: " // index2 // " instead of " // index // ".")
        call re%match('sd dmoir DZ54 po32 54 era312', substring2, stat)
        call check(substring2 == substring, "match_single_substring_character", &
            "inconsistent substring: """ // substring2 // """ instead of """ // substring // """.")
    end subroutine
!******************************************************************************!
!> Test matching procedure using string objects.
!******************************************************************************!
    subroutine test_match_simple_string()
        type(FRegex) :: re
        integer :: index, index2
        type(FException) :: stat
        type(FString), allocatable :: substring, substring2, subject
        type(FString), dimension(:), allocatable :: captures
!------
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b')

        call startTestGroup("simple pattern matching (string version)")

        subject = FString("sd dmoir DZ54 po32 54 era312")

        call check(re%doesMatch(subject), &
            "simple pattern matching (string)", &
            "matching did not work")
        call re%match(subject, index, substring, stat)
        call check(stat == 0, "simple match", stat%string())
        call check(index == 15, "matched substring index", "wrong index: " // index)
        call check(substring == "po32", "matched substring", &
            "wrong matched substring: """ // substring // """.")

        call re%match(subject, index2, stat)
        call check(index2 == index, "match_single_position_string", &
            "inconsistent index: " // index2 // " instead of " // index // ".")
        call re%match(subject, substring2, stat)
        call check(substring2 == substring, "match_single_substring_string", &
            "inconsistent substring: """ // substring2 // """ instead of """ // substring // """.")
    end subroutine
!******************************************************************************!
!> Test case-insensitive matching.
!******************************************************************************!
    subroutine test_match_caseless()
        type(FRegex) :: re
        integer :: index
        character(:), allocatable :: substring
!------
        call startTestGroup("case sensitivity")

        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', options="i")
        call re%match('sd dmoir DZ54 po32 54 era312', index, substring)
        call check(index == 10, "case-insensitive matching", &
            "wrong index for the matched substring: " // index // " instead of 10.")
        call check(substring == "DZ54", "case-insensitive matched substring", &
            "wrong matched substring: " // substring // " instead of ""DZ54"".")

        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b')
        call re%match('sd dmoir DZ54 po32 54 era312', index, substring)
        call check(index == 15, "case-sensitive matching", &
            "wrong index for the matched substring: " // index // " instead of 15.")
        call check(substring == "po32", "case-sensitive matched substring", &
            "wrong matched substring: " // substring // " instead of ""po32"".")
    end subroutine
!******************************************************************************!
!> Test reluctant matching.
!******************************************************************************!
    subroutine test_match_reluctant()
        type(FRegex) :: re
        integer :: index
        character(:), allocatable :: substring
!------
        call startTestGroup("greed")

        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b\s*', options="r")
        call re%match('sd dmoir DZ54 po32 54 era312', index, substring)
        call check(index == 15, "reluctant matching", &
            "wrong index for the matched substring: " // index // " instead of 15.")
        call check(substring == "po32", "reluctant matched substring", &
            "wrong matched substring: " // substring // " instead of ""po32"".")
        call check(len(substring) == 4, "reluctant matched substring length", &
            "wrong length for the matched substring: " // len(substring) // " instead of 4.")

        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b\s*')
        call re%match('sd dmoir DZ54 po32 54 era312', index, substring)
        call check(index == 15, "greedy matching", &
            "wrong index for the matched substring: " // index // " instead of 15.")
        call check(substring == "po32 ", "greedy matched substring", &
            "wrong matched substring: " // substring // " instead of ""po32 "".")
        call check(len(substring) == 5, "greedy matched substring length", &
            "wrong length for the matched substring: " // len(substring) // " instead of 5.")
    end subroutine
!******************************************************************************!
!> Test multiple matching.
!******************************************************************************!
    subroutine test_match_all()
        type(FRegex) :: re
        integer, dimension(:), allocatable :: index
        integer, dimension(2), parameter :: vi = [15, 20]
        integer, dimension(2), parameter :: vl = [4, 2]
        type(FString), dimension(:), allocatable :: substrings
        character(:), allocatable :: subject
!------
        call startTestGroup("multiple matching")

        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b')
        call re%match('sd dmoir DZ54 po32 54 era312', index, substrings)
        call check(size(index) == size(vi), "number of substrings", &
            "found " // size(index) // " substrings instead of 2.")
        call check(all(index == vi), "substrings positions", &
            "wrong indices found: " // index // " instead of " // vi // ".")
        call check(all(substrings%length() == vl), "substrings sizes", &
            "wrong sizes found: " // substrings%length() // " instead of " // vl // ".")

        subject = "/Users/paul/Codes/libcore/tests/directory/.{3}"
        re = FRegex("(?<!\/)\/(?!\/)")
        call re%match(subject, index, substrings)
        call check(size(index) == 7, "number of substrings", &
            "found " // size(index) // " substrings instead of 7.")
    end subroutine
!******************************************************************************!
!> Test anonymous capture.
!******************************************************************************!
    subroutine test_capture_array
        type(FRegex) :: re
        type(FString), dimension(:), allocatable :: capture
        character(:), allocatable :: subject
        integer :: i
        type(FString), dimension(:), allocatable :: ref
!------
        call startTestGroup("anonymous captures")

        subject = "file/that/does/not.exist"
        allocate(ref(3))
        ref(1) = "file/that/does/"
        ref(2) = "not"
        ref(3) = "exist"

        re = FRegex("^(?<path>.*/)?(?:$|(?<basename>.+?)(?:\.(?<extension>[^.]*$)|$))", options="i")
        call re%capture(subject, capture)
        call check(size(capture) == 3, "number of captured strings", &
            "wrong size for the capture array: "//size(capture)//".")
        call check(all(capture == ref), "captured strings", &
            "wrong captured string.")
    end subroutine
!******************************************************************************!
!> Test anonymous capture.
!******************************************************************************!
    subroutine test_capture_dictionary
        type(FRegex) :: re
        type(FStringDictionary) :: capture
        character(:), allocatable :: subject
        integer :: i
        type(FStringDictionary) :: ref
!------
        call startTestGroup("named captures")

        subject = "file/that/does/not.exist"
        call ref%set("path", "file/that/does/")
        call ref%set("basename", "not")
        call ref%set("extension", "exist")

        re = FRegex("^(?<path>.*/)?(?:$|(?<basename>.+?)(?:\.(?<extension>[^.]*$)|$))", options="i")
        call re%capture(subject, capture)
        call check(capture%size() == 3, "number of captured strings", &
            "wrong size for the capture array: " // capture%size() // ".")
        call check(capture == ref, "captured strings", &
            "wrong captured string.")
    end subroutine

    subroutine test_capture_names
        type(FRegex) :: re
        type(FString), dimension(:), allocatable :: name

        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b')
        call re%getCaptureNames(name)
        call assert(size(name) == 3, "wrong number of named captures")
        call assert(name(1) == "a", "wrong name: 1")
        call assert(name(2) == "b", "wrong name: 2")
        call assert(name(3) == "c", "wrong name: 3")
    end subroutine







    subroutine test_substitution_simple
        type(FRegex) :: re
        integer :: index
        character(:), allocatable :: subject
!------
        call startTestGroup("substitutions")

        subject = 'sd dmoir DZ54 po32 54 era312'
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', '____')
        call re%replace(subject, index)

        call check(index == 15, "matched substring index", &
            "wrong index: " // index // " instead of 15.")
        call check(subject == "sd dmoir DZ54 ____ 54 era312", "substituted string", &
            "wrong result: """ // subject // """ instead of ""sd dmoir DZ54 ____ 54 era312"".")
    end subroutine

    subroutine test_substitution_backref()
        type(FRegex) :: re
        integer :: index
        character(:), allocatable :: subject

        subject = 'sd dmoir DZ54 po32 54 era312'
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', '$2 $3 $1')
        call re%replace(subject, index)

        call check(subject=="sd dmoir DZ54 3 2 po 54 era312", &
            "back references in substituted string", &
            "wrong result: """ // subject // """ instead of ""sd dmoir DZ54 3 2 po 54 era312"".")
    end subroutine

    subroutine test_substitution_variable_1()
        type(FRegex) :: re
        integer :: index
        character(:), allocatable :: subject

        subject = 'sd dmoir DZ54 po32 54 era312'
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', '/$`/')
        call re%replace(subject, index)

        call check(subject=="sd dmoir DZ54 /sd dmoir DZ54 / 54 era312", &
            "$` in substituted string", &
            "wrong result: """ // subject // """ instead of ""sd dmoir DZ54 /sd dmoir DZ54 / 54 era312"".")
    end subroutine

    subroutine test_substitution_variable_2()
        type(FRegex) :: re
        integer :: index
        character(:), allocatable :: subject

        subject = 'sd dmoir DZ54 po32 54 era312'
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', '/$''/')
        call re%replace(subject, index)

        call check(subject=="sd dmoir DZ54 / 54 era312/ 54 era312", &
            "$' in substituted string", &
            "wrong result: """ // subject // """ instead of ""sd dmoir DZ54 / 54 era312/ 54 era312"".")
    end subroutine

    subroutine test_substitution_variable_3()
        type(FRegex) :: re
        integer :: index
        character(:), allocatable :: subject

        subject = 'sd dmoir DZ54 po32 54 era312'
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', '/$+/')
        call re%replace(subject, index)

        call check(subject=="sd dmoir DZ54 /2/ 54 era312", &
            "$+ in substituted string", &
            "wrong result: """ // subject // """ instead of ""sd dmoir DZ54 /2/ 54 era312"".")
    end subroutine

    subroutine test_substitution_variable_4()
        type(FRegex) :: re
        integer :: index
        character(:), allocatable :: subject

        subject = 'sd dmoir DZ54 po32 54 era312'
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', '/$&/')
        call re%replace(subject, index)

        call check(subject=="sd dmoir DZ54 /po32/ 54 era312", &
            "$& in substituted string", &
            "wrong result: """ // subject // """ instead of ""sd dmoir DZ54 /po32/ 54 era312"".")
    end subroutine

    subroutine test_substitution_caseless()
        type(FRegex) :: re
        integer :: index, i
        character(:), allocatable :: subject

        subject = 'sd dmoir DZ54 po32 54 era312'
        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', '$2 $3 $1', options="i")
        call re%replace(subject, index)

        call check(subject=="sd dmoir 5 4 DZ po32 54 era312", &
            "case-iunsensitive substitution", &
            "wrong result: """ // subject // """ instead of ""sd dmoir 5 4 DZ po32 54 era312"".")
    end subroutine

    subroutine test_replace_all
        type(FRegex) :: re
        integer, dimension(:), allocatable :: index
        integer, dimension(2), parameter :: vi = [15, 19]
        character(:), allocatable :: subject
        type(FException) :: stat

        re = FRegex('\b(?<c>[a-z]*)(?<a>[0-9])(?<b>[0-9])\b', '###')
        subject = 'sd dmoir DZ54 po32 54 era312'
        call re%replace_all(subject, index)
        call assert(subject == 'sd dmoir DZ54 ### ### era312', "wrong substituted string")
        call assert(size(index) == size(vi), "wrong match count in replace_all")
        call assert(all(index == vi), "wrong index found in replace_all")

        subject = 'not working'
        call re%replace_all(subject, index, stat)
        call assert(stat == "No match found.", "error")
    end subroutine

    subroutine testFcre
        integer :: code, pos, status

        code = 99
        pos = fcre_option_position(code, status)
        call check(status==FCRE_ERROR_BADOPTION, "error code", "got wrong error message")
    end subroutine

    subroutine test_split
        type(FRegex) :: re
        character(:), allocatable :: subject
        type(FString), dimension(:), allocatable :: split

        call startTestGroup("string splitting")

        subject = "sufblkjn"
        re = FRegex("/")
        call re%split(subject, split)
        call check(size(split)==1, "simple split 1: number of substrings", &
            "wrong number of substrings: " // size(split) // " instead of 1.")
        call check(split(1)==subject, "simple split 1: substring", &
            "wrong substring: " // split(1) // " instead of ""sufblkjn"".")
    end subroutine
end module


program test
    use test_regex

    call testAll
end program