    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine r1f3kf(ido, l1, cc, in1, ch, in2, wa1, wa2)
        implicit none
        include 'kinds.inc'
        integer :: ido, l1, in1, in2
        real(rwp) :: ch(in2, ido, 3, l1)
        real(rwp) :: cc(in1, ido, l1, 3)
        real(rwp) :: wa1(ido)
        real(rwp) :: wa2(ido)
        
        real(rwp) :: arg, taur, taui
        integer :: i, k, ic, idp2

        arg = 2.*4.*atan(1.0_rwp)/3.
        taur = cos(arg)
        taui = sin(arg)
        do k = 1, l1
            ch(1, 1, 1, k) = cc(1, 1, k, 1) + (cc(1,1,k,2)+cc(1,1,k,3))
            ch(1, 1, 3, k) = taui*(cc(1,1,k,3)-cc(1,1,k,2))
            ch(1, ido, 2, k) = cc(1, 1, k, 1) + taur*(cc(1,1,k,2)+cc(1,1,k,3))
        end do
        if (ido==1) return
        idp2 = ido + 2
        do k = 1, l1
            do i = 3, ido, 2
                ic = idp2 - i
                ch(1, i-1, 1, k) = cc(1, i-1, k, 1) + ((wa1(i-2)*cc(1,i-1,k, &
                  2)+wa1(i-1)*cc(1,i,k,2))+(wa2(i-2)*cc(1,i-1,k, &
                  3)+wa2(i-1)*cc(1,i,k,3)))
                ch(1, i, 1, k) = cc(1, i, k, 1) + ((wa1(i-2)*cc(1,i,k, &
                  2)-wa1(i-1)*cc(1,i-1,k,2))+(wa2(i-2)*cc(1,i,k, &
                  3)-wa2(i-1)*cc(1,i-1,k,3)))
                ch(1, i-1, 3, k) = (cc(1,i-1,k,1)+taur*((wa1(i-2)* &
                  cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k,2))+(wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3)))) + (taui*((wa1(i-2)*cc(1,i,k,2)-wa1(i &
                  -1)*cc(1,i-1,k,2))-(wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))))
                ch(1, ic-1, 2, k) = (cc(1,i-1,k,1)+taur*((wa1(i-2)* &
                  cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k,2))+(wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3)))) - (taui*((wa1(i-2)*cc(1,i,k,2)-wa1(i &
                  -1)*cc(1,i-1,k,2))-(wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))))
                ch(1, i, 3, k) = (cc(1,i,k,1)+taur*((wa1(i-2)*cc(1,i,k,2)- &
                  wa1(i-1)*cc(1,i-1,k,2))+(wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3)))) + (taui*((wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3))-(wa1(i-2)*cc(1,i-1,k,2)+ &
                  wa1(i-1)*cc(1,i,k,2))))
                ch(1, ic, 2, k) = (taui*((wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3))-(wa1(i-2)*cc(1,i-1,k,2)+ &
                  wa1(i-1)*cc(1,i,k,2)))) - (cc(1,i,k,1)+taur*((wa1(i-2)*cc(1, &
                  i,k,2)-wa1(i-1)*cc(1,i-1,k,2))+(wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))))
            end do
        end do
    end subroutine
