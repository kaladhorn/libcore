    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine mrftf1(m, im, n, in, c, ch, wa, fac)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) ch(m, *), c(in, *), wa(n), fac(15)

        nf = fac(2)
        na = 1
        l2 = n
        iw = n
        do k1 = 1, nf
            kh = nf - k1
            ip = fac(kh+3)
            l1 = l2/ip
            ido = n/l2
            idl1 = ido*l1
            iw = iw - (ip-1)*ido
            na = 1 - na
            if (ip/=4) go to 110
            ix2 = iw + ido
            ix3 = ix2 + ido
            if (na/=0) go to 100
            call mradf4(m, ido, l1, c, im, in, ch, 1, m, wa(iw), wa(ix2), &
              wa(ix3))
            go to 190
100         call mradf4(m, ido, l1, ch, 1, m, c, im, in, wa(iw), wa(ix2), &
              wa(ix3))
            go to 190
110         if (ip/=2) go to 130
            if (na/=0) go to 120
            call mradf2(m, ido, l1, c, im, in, ch, 1, m, wa(iw))
            go to 190
120         call mradf2(m, ido, l1, ch, 1, m, c, im, in, wa(iw))
            go to 190
130         if (ip/=3) go to 150
            ix2 = iw + ido
            if (na/=0) go to 140
            call mradf3(m, ido, l1, c, im, in, ch, 1, m, wa(iw), wa(ix2))
            go to 190
140         call mradf3(m, ido, l1, ch, 1, m, c, im, in, wa(iw), wa(ix2))
            go to 190
150         if (ip/=5) go to 170
            ix2 = iw + ido
            ix3 = ix2 + ido
            ix4 = ix3 + ido
            if (na/=0) go to 160
            call mradf5(m, ido, l1, c, im, in, ch, 1, m, wa(iw), wa(ix2), &
              wa(ix3), wa(ix4))
            go to 190
160         call mradf5(m, ido, l1, ch, 1, m, c, im, in, wa(iw), wa(ix2), &
              wa(ix3), wa(ix4))
            go to 190
170         if (ido==1) na = 1 - na
            if (na/=0) go to 180
            call mradfg(m, ido, ip, l1, idl1, c, c, c, im, in, ch, ch, 1, m, &
              wa(iw))
            na = 1
            go to 190
180         call mradfg(m, ido, ip, l1, idl1, ch, ch, ch, 1, m, c, c, im, in, &
              wa(iw))
            na = 0
190         l2 = l1
        end do
        sn = 1.0_rwp/n
        tsn = 2.0_rwp/n
        tsnm = -tsn
        modn = mod(n, 2)
        nl = n - 2
        if (modn/=0) nl = n - 1
        if (na/=0) go to 200
        m2 = 1 - im
        do i = 1, m
            m2 = m2 + im
            c(m2, 1) = sn*ch(i, 1)
        end do
        do j = 2, nl, 2
            m2 = 1 - im
            do i = 1, m
                m2 = m2 + im
                c(m2, j) = tsn*ch(i, j)
                c(m2, j+1) = tsnm*ch(i, j+1)
            end do
        end do
        if (modn/=0) return
        m2 = 1 - im
        do i = 1, m
            m2 = m2 + im
            c(m2, n) = sn*ch(i, n)
        end do
        return
200     m2 = 1 - im
        do i = 1, m
            m2 = m2 + im
            c(m2, 1) = sn*c(m2, 1)
        end do
        do j = 2, nl, 2
            m2 = 1 - im
            do i = 1, m
                m2 = m2 + im
                c(m2, j) = tsn*c(m2, j)
                c(m2, j+1) = tsnm*c(m2, j+1)
            end do
        end do
        if (modn/=0) return
        m2 = 1 - im
        do i = 1, m
            m2 = m2 + im
            c(m2, n) = sn*c(m2, n)
        end do
    end subroutine
