module test_string
    use mod_test

    use core
    use ext_character
!    use mod_string
    use ext_character

    use mod_formattedString, only: FFormattedString

    implicit none
    private

    public :: testAll
contains
    subroutine testAll()
        call testinit
        call testColor
    end subroutine

    subroutine testInit
        type(FFormattedString) :: string, string2
        character(:), allocatable :: c
        call startTestGroup("basic string properties")

        string = FFormattedString("This *is* a _test_ string")
        string2 = FFormattedString("This "//char(27)//"[4mis"//char(27)//"[24m a "// &
            char(27)//"[4mtest"//char(27)//"[24m string")
        call check(string == string2, "formatted string", &
            "wrong string: """// string // """ instead of """ // string2 // """.")
        
        string = FFormattedString("This **is** a \_test\_ string")
        c = "This "//char(27)//"[1mis"//char(27)//"[22m a _test_ string"
        call check(string==c, "escaped format markers")
    end subroutine

    subroutine testColor
        type(FFormattedString) :: string
        character(:), allocatable :: comp

        comp = "This is "//char(27)//"[31mred"//char(27)//"[39m and this is "//char(27)//"[33mlighter"//char(27)//"[39m."
        string = FFormattedString("This is {red}{red} and this is {lighter}{yellow}.")
        call check(string==comp, "colour formatting", &
            "wrong formatting: """ // string // """ instead of """ // comp // """.")
    end subroutine
end module

program test
    use mod_test
    use test_string

    call testAll

    call testsFinished
end program