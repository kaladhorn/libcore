    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine rfftf1(n, in, c, ch, wa, fac)
        implicit none
        include 'kinds.inc'
        integer :: n, in
        real(rwp) :: ch(*)
        real(rwp) :: c(in, *)
        real(rwp) :: wa(n)
        real(rwp) :: fac(15)
        
        real(rwp) :: sn, tsn, tsnm
        integer :: ido, idl1, ip, iw, ix2, ix3, ix4, j, k1, kh, l1, l2, modn, na, nf, nl

        nf = fac(2)
        na = 1
        l2 = n
        iw = n
        do k1 = 1, nf
            kh = nf - k1
            ip = fac(kh+3)
            l1 = l2/ip
            ido = n/l2
            idl1 = ido*l1
            iw = iw - (ip-1)*ido
            na = 1 - na
            if (ip/=4) go to 110
            ix2 = iw + ido
            ix3 = ix2 + ido
            if (na/=0) go to 100
            call r1f4kf(ido, l1, c, in, ch, 1, wa(iw), wa(ix2), wa(ix3))
            go to 190
100         call r1f4kf(ido, l1, ch, 1, c, in, wa(iw), wa(ix2), wa(ix3))
            go to 190
110         if (ip/=2) go to 130
            if (na/=0) go to 120
            call r1f2kf(ido, l1, c, in, ch, 1, wa(iw))
            go to 190
120         call r1f2kf(ido, l1, ch, 1, c, in, wa(iw))
            go to 190
130         if (ip/=3) go to 150
            ix2 = iw + ido
            if (na/=0) go to 140
            call r1f3kf(ido, l1, c, in, ch, 1, wa(iw), wa(ix2))
            go to 190
140         call r1f3kf(ido, l1, ch, 1, c, in, wa(iw), wa(ix2))
            go to 190
150         if (ip/=5) go to 170
            ix2 = iw + ido
            ix3 = ix2 + ido
            ix4 = ix3 + ido
            if (na/=0) go to 160
            call r1f5kf(ido, l1, c, in, ch, 1, wa(iw), wa(ix2), wa(ix3), &
              wa(ix4))
            go to 190
160         call r1f5kf(ido, l1, ch, 1, c, in, wa(iw), wa(ix2), wa(ix3), &
              wa(ix4))
            go to 190
170         if (ido==1) na = 1 - na
            if (na/=0) go to 180
            call r1fgkf(ido, ip, l1, idl1, c, c, c, in, ch, ch, 1, wa(iw))
            na = 1
            go to 190
180         call r1fgkf(ido, ip, l1, idl1, ch, ch, ch, 1, c, c, in, wa(iw))
            na = 0
190         l2 = l1
        end do
        sn = 1.0_rwp/n
        tsn = 2.0_rwp/n
        tsnm = -tsn
        modn = mod(n, 2)
        nl = n - 2
        if (modn/=0) nl = n - 1
        if (na/=0) go to 200
        c(1, 1) = sn*ch(1)
        do j = 2, nl, 2
            c(1, j) = tsn*ch(j)
            c(1, j+1) = tsnm*ch(j+1)
        end do
        if (modn/=0) return
        c(1, n) = sn*ch(n)
        return
200     c(1, 1) = sn*c(1, 1)
        do j = 2, nl, 2
            c(1, j) = tsn*c(1, j)
            c(1, j+1) = tsnm*c(1, j+1)
        end do
        if (modn/=0) return
        c(1, n) = sn*c(1, n)
    end subroutine
