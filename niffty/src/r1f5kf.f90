    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine r1f5kf(ido, l1, cc, in1, ch, in2, wa1, wa2, wa3, wa4)
        implicit none
        include 'kinds.inc'
        integer :: ido, l1, in1, in2
        real(rwp) :: cc(in1, ido, l1, 5)
        real(rwp) :: ch(in2, ido, 5, l1)
        real(rwp) :: wa1(ido)
        real(rwp) :: wa2(ido)
        real(rwp) :: wa3(ido)
        real(rwp) :: wa4(ido)
        
        real(rwp) :: arg, ti11, ti12, tr11, tr12
        integer :: i, k, ic, idp2

        arg = 2.*4.*atan(1.0_rwp)/5.
        tr11 = cos(arg)
        ti11 = sin(arg)
        tr12 = cos(2.*arg)
        ti12 = sin(2.*arg)
        do k = 1, l1
            ch(1, 1, 1, k) = cc(1, 1, k, 1) + (cc(1,1,k,5)+cc(1,1,k,2)) + &
              (cc(1,1,k,4)+cc(1,1,k,3))
            ch(1, ido, 2, k) = cc(1, 1, k, 1) + tr11*(cc(1,1,k,5)+cc(1,1,k,2)) &
              + tr12*(cc(1,1,k,4)+cc(1,1,k,3))
            ch(1, 1, 3, k) = ti11*(cc(1,1,k,5)-cc(1,1,k,2)) + &
              ti12*(cc(1,1,k,4)-cc(1,1,k,3))
            ch(1, ido, 4, k) = cc(1, 1, k, 1) + tr12*(cc(1,1,k,5)+cc(1,1,k,2)) &
              + tr11*(cc(1,1,k,4)+cc(1,1,k,3))
            ch(1, 1, 5, k) = ti12*(cc(1,1,k,5)-cc(1,1,k,2)) - &
              ti11*(cc(1,1,k,4)-cc(1,1,k,3))
        end do
        if (ido==1) return
        idp2 = ido + 2
        do k = 1, l1
            do i = 3, ido, 2
                ic = idp2 - i
                ch(1, i-1, 1, k) = cc(1, i-1, k, 1) + ((wa1(i-2)*cc(1,i-1,k, &
                  2)+wa1(i-1)*cc(1,i,k,2))+(wa4(i-2)*cc(1,i-1,k, &
                  5)+wa4(i-1)*cc(1,i,k,5))) + ((wa2(i-2)*cc(1,i-1,k, &
                  3)+wa2(i-1)*cc(1,i,k,3))+(wa3(i-2)*cc(1,i-1,k, &
                  4)+wa3(i-1)*cc(1,i,k,4)))
                ch(1, i, 1, k) = cc(1, i, k, 1) + ((wa1(i-2)*cc(1,i,k, &
                  2)-wa1(i-1)*cc(1,i-1,k,2))+(wa4(i-2)*cc(1,i,k, &
                  5)-wa4(i-1)*cc(1,i-1,k,5))) + ((wa2(i-2)*cc(1,i,k, &
                  3)-wa2(i-1)*cc(1,i-1,k,3))+(wa3(i-2)*cc(1,i,k, &
                  4)-wa3(i-1)*cc(1,i-1,k,4)))
                ch(1, i-1, 3, k) = cc(1, i-1, k, 1) + &
                  tr11*(wa1(i-2)*cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k,2)+ &
                  wa4(i-2)*cc(1,i-1,k,5)+wa4(i-1)*cc(1,i,k,5)) + &
                  tr12*(wa2(i-2)*cc(1,i-1,k,3)+wa2(i-1)*cc(1,i,k,3)+ &
                  wa3(i-2)*cc(1,i-1,k,4)+wa3(i-1)*cc(1,i,k,4)) + &
                  ti11*(wa1(i-2)*cc(1,i,k,2)-wa1(i-1)*cc(1,i-1,k,2)-(wa4(i- &
                  2)*cc(1,i,k,5)-wa4(i-1)*cc(1,i-1,k, &
                  5))) + ti12*(wa2(i-2)*cc(1,i,k,3)-wa2(i-1)*cc(1,i-1,k,3)- &
                  (wa3(i-2)*cc(1,i,k,4)-wa3(i-1)*cc(1,i-1,k,4)))
                ch(1, ic-1, 2, k) = cc(1, i-1, k, 1) + &
                  tr11*(wa1(i-2)*cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k,2)+ &
                  wa4(i-2)*cc(1,i-1,k,5)+wa4(i-1)*cc(1,i,k,5)) + &
                  tr12*(wa2(i-2)*cc(1,i-1,k,3)+wa2(i-1)*cc(1,i,k,3)+ &
                  wa3(i-2)*cc(1,i-1,k,4)+wa3(i-1)*cc(1,i,k,4)) - &
                  (ti11*(wa1(i-2)*cc(1,i,k,2)-wa1(i-1)*cc(1,i-1,k, &
                  2)-(wa4(i-2)*cc(1,i,k,5)-wa4(i-1)*cc(1,i-1,k,5)))+ti12*(wa2( &
                  i-2)*cc(1,i,k,3)-wa2(i-1)*cc(1,i-1,k, &
                  3)-(wa3(i-2)*cc(1,i,k,4)-wa3(i-1)*cc(1,i-1,k,4))))
                ch(1, i, 3, k) = (cc(1,i,k,1)+tr11*((wa1(i-2)*cc(1,i,k,2)- &
                  wa1(i-1)*cc(1,i-1,k,2))+(wa4(i-2)*cc(1,i,k,5)- &
                  wa4(i-1)*cc(1,i-1,k,5)))+tr12*((wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))+(wa3(i-2)*cc(1,i,k,4)- &
                  wa3(i-1)*cc(1,i-1,k,4)))) + (ti11*((wa4(i-2)*cc(1,i-1,k,5)+ &
                  wa4(i-1)*cc(1,i,k,5))-(wa1(i-2)*cc(1,i-1,k,2)+ &
                  wa1(i-1)*cc(1,i,k,2)))+ti12*((wa3(i-2)*cc(1,i-1,k,4)+ &
                  wa3(i-1)*cc(1,i,k,4))-(wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3))))
                ch(1, ic, 2, k) = (ti11*((wa4(i-2)*cc(1,i-1,k,5)+ &
                  wa4(i-1)*cc(1,i,k,5))-(wa1(i-2)*cc(1,i-1,k,2)+ &
                  wa1(i-1)*cc(1,i,k,2)))+ti12*((wa3(i-2)*cc(1,i-1,k,4)+ &
                  wa3(i-1)*cc(1,i,k,4))-(wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3)))) - (cc(1,i,k,1)+tr11*((wa1(i-2)*cc(1, &
                  i,k,2)-wa1(i-1)*cc(1,i-1,k,2))+(wa4(i-2)*cc(1,i,k,5)- &
                  wa4(i-1)*cc(1,i-1,k,5)))+tr12*((wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))+(wa3(i-2)*cc(1,i,k,4)- &
                  wa3(i-1)*cc(1,i-1,k,4))))
                ch(1, i-1, 5, k) = (cc(1,i-1,k,1)+tr12*((wa1(i-2)* &
                  cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k,2))+(wa4(i-2)*cc(1,i-1,k,5)+ &
                  wa4(i-1)*cc(1,i,k,5)))+tr11*((wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3))+(wa3(i-2)*cc(1,i-1,k,4)+ &
                  wa3(i-1)*cc(1,i,k,4)))) + (ti12*((wa1(i-2)*cc(1,i,k,2)-wa1(i &
                  -1)*cc(1,i-1,k,2))-(wa4(i-2)*cc(1,i,k,5)- &
                  wa4(i-1)*cc(1,i-1,k,5)))-ti11*((wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))-(wa3(i-2)*cc(1,i,k,4)- &
                  wa3(i-1)*cc(1,i-1,k,4))))
                ch(1, ic-1, 4, k) = (cc(1,i-1,k,1)+tr12*((wa1(i-2)* &
                  cc(1,i-1,k,2)+wa1(i-1)*cc(1,i,k,2))+(wa4(i-2)*cc(1,i-1,k,5)+ &
                  wa4(i-1)*cc(1,i,k,5)))+tr11*((wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3))+(wa3(i-2)*cc(1,i-1,k,4)+ &
                  wa3(i-1)*cc(1,i,k,4)))) - (ti12*((wa1(i-2)*cc(1,i,k,2)-wa1(i &
                  -1)*cc(1,i-1,k,2))-(wa4(i-2)*cc(1,i,k,5)- &
                  wa4(i-1)*cc(1,i-1,k,5)))-ti11*((wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))-(wa3(i-2)*cc(1,i,k,4)- &
                  wa3(i-1)*cc(1,i-1,k,4))))
                ch(1, i, 5, k) = (cc(1,i,k,1)+tr12*((wa1(i-2)*cc(1,i,k,2)- &
                  wa1(i-1)*cc(1,i-1,k,2))+(wa4(i-2)*cc(1,i,k,5)- &
                  wa4(i-1)*cc(1,i-1,k,5)))+tr11*((wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))+(wa3(i-2)*cc(1,i,k,4)- &
                  wa3(i-1)*cc(1,i-1,k,4)))) + (ti12*((wa4(i-2)*cc(1,i-1,k,5)+ &
                  wa4(i-1)*cc(1,i,k,5))-(wa1(i-2)*cc(1,i-1,k,2)+ &
                  wa1(i-1)*cc(1,i,k,2)))-ti11*((wa3(i-2)*cc(1,i-1,k,4)+ &
                  wa3(i-1)*cc(1,i,k,4))-(wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3))))
                ch(1, ic, 4, k) = (ti12*((wa4(i-2)*cc(1,i-1,k,5)+ &
                  wa4(i-1)*cc(1,i,k,5))-(wa1(i-2)*cc(1,i-1,k,2)+ &
                  wa1(i-1)*cc(1,i,k,2)))-ti11*((wa3(i-2)*cc(1,i-1,k,4)+ &
                  wa3(i-1)*cc(1,i,k,4))-(wa2(i-2)*cc(1,i-1,k,3)+ &
                  wa2(i-1)*cc(1,i,k,3)))) - (cc(1,i,k,1)+tr12*((wa1(i-2)*cc(1, &
                  i,k,2)-wa1(i-1)*cc(1,i-1,k,2))+(wa4(i-2)*cc(1,i,k,5)- &
                  wa4(i-1)*cc(1,i-1,k,5)))+tr11*((wa2(i-2)*cc(1,i,k,3)- &
                  wa2(i-1)*cc(1,i-1,k,3))+(wa3(i-2)*cc(1,i,k,4)- &
                  wa3(i-1)*cc(1,i-1,k,4))))
            end do
        end do
    end subroutine
