module test_bytecode
    use core
    use mod_test
    use ext_character
    
    use mod_timer,     only: FTimer
    use mod_bytecode,  only: FBytecode
    use mod_exception, only: FException
    
    use mod_ast
    
    implicit none
    public
    save
    
contains
    
    subroutine test_all
        call test_errors
        call test_optimisation
        call test_vectors
    end subroutine
    
    subroutine test_errors
        type(FBytecode) :: code
        real(e) :: result
        type(FException) :: stat
        
        call startTestGroup("Error conditions")
        
        call code%check(stat)
        call check(stat /= 0, &
            "Unset bytecode", &
            "Unset bytecode was not detected")
        call stat%discard
        call code%run(result, stat)
        call check(stat /= 0, &
            "Executing unset bytecode", &
            "Unset bytecode was not detected during execution")
        call stat%discard
        
        call getBytecode(code)
        code%instruction(4) = int(z'baad')
        call code%check(stat)
        call check(stat == "Illegal instruction.", "Illegal instruction", &
            "Illegal instruction was not caught")
        call stat%discard
        call code%run(result)
        
        call getBytecode(code)
        code%argument(1,2) = int(z'baad')
        call code%check(stat)
        call check(stat == "Argument address too large.", &
            "Illegal argument address", &
            "Illegal address was not caught. Error message: " // stat%string())
        call stat%discard
        call code%run(result)
        code%argument(1,2) = -42
        call code%check(stat)
        call check(stat == "Negative argument address.", &
            "Negative argument address", &
            "Negative address was not caught. Error message: " // stat%string())
        call stat%discard
        call code%run(result)
        code%argument(1,2) = 0
        call code%check(stat)
        call check(stat == "Unset argument address.", &
            "Unset argument address", &
            "Negative address was not caught. Error message: " // stat%string())
        call stat%discard
        call code%run(result)
        
        call getBytecode(code)
        code%start_address = -1
        call code%check(stat)
        call check(stat == "Wrong code offset.", &
            "Wrong code offset", &
            "Wrong code offset was not caught. Error message: " // stat%string())
        call stat%discard
        call code%run(result)
        
        call getBytecode(code)
        code%start_address = 5
        call code%check(stat)
        call check(stat == "Illegal instruction in variable definition section.", &
            "Wrong code offset 2", &
            "Wrong code offset was not caught. Error message: " // stat%string())
        call stat%discard
        call code%run(result)
    end subroutine
    
    subroutine test_optimisation
        type(FBytecode) :: code
        real(e) :: result, reference, time_unoptimised, time_ine, time_gcn, time_fma, &
            time_bubble, time_optimised
        integer :: i, nIter
        type(FTimer) :: timer
        
        call startTestGroup("Optimisations")
        
        call getBytecode(code)
        reference = 80.0316_e
        
        call code%run(result)
        call check(result==reference, "numerical result", "wrong result: " // result // &
            " instead of " // reference // ".")
        nIter = 1000000
        call timer%start
        do i=1, nIter
            call code%run(result)
        end do
        call timer%stop
        time_unoptimised = timer%wallTime()
        write(*,*) "Baseline time:", time_unoptimised
        
        call getBytecode(code)
        call code%optimise_ine
        call code%run(result)
        call check(result==reference, "unused instructions elimination", &
            "wrong result: " // result // " instead of " // reference // ".")
        call timer%reset
        call timer%start
        do i=1, nIter
            call code%run(result)
        end do
        call timer%stop
        time_ine = timer%wallTime()
!        call check(time_ine < time_unoptimised, "speedup after unused instructions " // &
!            "elimination: " // time_unoptimised/time_ine, "performance regression: " //&
!            time_ine // " compared to " // time_unoptimised // ".")
        
        call getBytecode(code)
        call code%optimise_gcn
        call code%run(result)
        call check(result==reference, "constants grouping", &
            "wrong result: " // result // " instead of " // reference // ".")
        call timer%reset
        call timer%start
        do i=1, nIter
            call code%run(result)
        end do
        call timer%stop
        time_gcn = timer%wallTime()
        call code%optimise_gcn
!        call check(time_gcn < time_unoptimised, "speedup after constants grouping " // &
!            time_unoptimised/time_gcn, "performance regression: " //&
!            time_gcn // " compared to " // time_unoptimised // ".")
        
        call getBytecode(code)
        call code%optimise_fma
        call code%run(result)
        call check(result==reference, "FMA substitution", &
            "wrong result: " // result // " instead of " // reference // ".")
        call timer%reset
        call timer%start
        do i=1, nIter
            call code%run(result)
        end do
        call timer%stop
        time_fma = timer%wallTime()
!        call check(time_fma < time_unoptimised, "speedup after FMA substitution :" // &
!            time_unoptimised/time_fma, "performance regression: " //&
!            time_fma // " compared to " // time_unoptimised // ".")
        
        call getBytecode(code)
        call code%optimise_compact
        call code%run(result)
        call check(result==reference, "bubble elimination", &
            "wrong result: " // result // " instead of " // reference // ".")
        call timer%reset
        call timer%start
        do i=1, nIter
            call code%run(result)
        end do
        call timer%stop
        time_bubble = timer%wallTime()
!        call check(time_bubble < time_unoptimised, "speedup after bubble elimination :" // &
!            time_unoptimised/time_bubble, "performance regression: " //&
!            time_bubble // " compared to " // time_unoptimised // ".")
        
        call getBytecode(code)
        call code%optimise
        call code%run(result)
        call check(result==reference, "standard optimisation", &
            "wrong result: " // result // " instead of " // reference // ".")
        call timer%reset
        call timer%start
        do i=1, nIter
            call code%run(result)
        end do
        call timer%stop
        time_optimised = timer%wallTime()
        call check(time_optimised < time_unoptimised, "speedup after standard optimisation :" // &
            time_unoptimised/time_optimised, "performance regression: " //&
            time_optimised // " compared to " // time_unoptimised // ".")
    end subroutine
    
    subroutine test_vectors
        type(FBytecode) :: code
        integer :: nIter, i
        real(e), dimension(:), allocatable :: x, y, reference
        real(e) :: dx, scalar_time, block_time , linear_time, standard_time
        type(FTimer) :: timer
        
        call startTestGroup("Vector execution")
        
        call getBytecode(code)
        
        niter = 1000010
        allocate(x(nIter))
        allocate(y(nIter))
        allocate(reference(nIter))
        dx = 0.0002_e
        do i=1, nIter
            x(i) = dx + (i-1)*dx
        end do
        y = 0
        
        ! Calculate the reference using direct calculations
        call timer%start
        do i=1, nIter
            reference(i) = (0*log(x(i))+(2*1)/x(i))*x(i)**2*3+x(i)**2*0-0
        end do
        call timer%stop
        standard_time = timer%wallTime()
        
        ! Standard bytecode execution
        call timer%reset
        call timer%start
        do i=1, nIter
            code%data(1) = x(i)
            call code%run(y(i))
        end do
        call timer%stop
        
        scalar_time = timer%wallTime()
        call check(all(y.approx.reference), "Accuracy of standard calculations", "Numerical errors: " // &
            count(y/=reference) // ".")
        write(*,*) "Standard times:", standard_time, scalar_time, standard_time/scalar_time
        
        ! Unoptimised batch version
        call getBytecode(code)
        call timer%reset
        call timer%start
        call code%run(1, x, y, 100)
        call timer%stop
        block_time = timer%wallTime()
        call check(all(y.approx.reference), "Accuracy of batch calculations", "Numerical errors: " // &
            count(y/=reference) // ".")
        call check(block_time < scalar_time, "speedup of batch execution " // &
            scalar_time/block_time, "performance regression: " //&
            block_time // " compared to " // scalar_time // ".")
        
        ! Optimised batch version
        call getBytecode(code)
        call code%optimise
        call timer%reset
        call timer%start
        call code%run(1, x, y, 100)
        call timer%stop
        block_time = timer%wallTime()
        call check(all(y.approx.reference), "Accuracy of optimised batch calculations", "Numerical errors: " // &
            count(y/=reference) // ".")
        call check(block_time < scalar_time, "speedup of optimised batch execution " // &
            scalar_time/block_time, "performance regression: " //&
            block_time // " compared to " // scalar_time // ".")
        
        ! Unoptimised linear batch version
        call getBytecode(code)
        call timer%reset
        call timer%start
        call code%run(1, dx, nIter, dx, y, 100)
        call timer%stop
        linear_time = timer%wallTime()
        call check(linear_time < scalar_time, "speedup of linear variable blocked execution " // &
            scalar_time/linear_time, "performance regression: " //&
            linear_time // " compared to " // scalar_time // ".")
        
        call check(all(y.approx.reference), "Accuracy of linear calculations", "Numerical errors: " // &
            count(y/=reference) // ".")
        
    end subroutine
        
    subroutine getBytecode(code)
!        #
!        # Instruction count: 21
!        # First instruction address: 1
!        #
!        # Data
!        # Code
!        $1 := 1.0000000000000000  # 1
!        $2 := 0.0000000000000000  # 0
!        $3 := log $1              # 0
!        $4 := $3 * $2             # 0
!        $5 := 2.0000000000000000  # 2
!        $6 := 1.0000000000000000  # 1
!        $7 := $6 * $5             # 2
!        $8 := $1 / $7             # 0.5
!        $9 := $8 + $4             # 0.5
!        $10 := 2.0000000000000000 # 2
!        $11 := $10 ^ $1           # 2
!        $12 := $11 * $9           # 1
!        $13 := 3.0000000000000000 # 3
!        $14 := $13 ^ $12          # 3
!        $15 := 2.0000000000000000 # 2
!        $16 := $15 ^ $1           # 2
!        $17 := 0.0000000000000000 # 0
!        $18 := $17 * $16          # 0
!        $19 := $18 + $14          # 3
!        $20 := 0.0000000000000000 # 0
        type(FBytecode), intent(out) :: code
        
        code = FBytecode(21, 0, 0)
        code%start_address = 1
        
        code%instruction(1) = CODE_NONE
        code%data(1) = 13.338600000000001_e
        
        code%instruction(2) = CODE_NONE
        code%data(2) = 0
        
        code%instruction(3) = CODE_LN
        code%argument(:,3) = [ 1, -1, -1]
        
        code%instruction(4) = CODE_MULTIPLY
        code%argument(:,4) = [ 3,  2, -1]
        
        code%instruction(5) = CODE_NONE
        code%argument(:,5) = [-1, -1, -1]
        code%data(5) = 2
        
        code%instruction(6) = CODE_NONE
        code%argument(:,6) = [-1, -1, -1]
        code%data(6) = 1
        
        code%instruction(7) = CODE_MULTIPLY
        code%argument(:,7) = [ 6, 5, -1]
        
        code%instruction(8) = CODE_DIVIDE
        code%argument(:,8) = [ 1, 7, -1]
        
        code%instruction(9) = CODE_ADD
        code%argument(:,9) = [ 8, 4, -1]
        
        code%instruction(10) = CODE_NONE
        code%argument(:,10) = [-1, -1, -1]
        code%data(10) = 2
        
        code%instruction(11) = CODE_POW
        code%argument(:,11) = [10,  1, -1]
        
        code%instruction(12) = CODE_MULTIPLY
        code%argument(:,12) = [11,  9, -1]
        
        code%instruction(13) = CODE_NONE
        code%argument(:,13) = [-1, -1, -1]
        code%data(13) = 3
        
        code%instruction(14) = CODE_MULTIPLY
        code%argument(:,14) = [13, 12, -1]
        
        code%instruction(15) = CODE_NONE
        code%argument(:,15) = [-1, -1, -1]
        code%data(15) = 2
        
        code%instruction(16) = CODE_POW
        code%argument(:,16) = [15, 1, -1]
        
        code%instruction(17) = CODE_NONE
        code%argument(:,17) = [-1, -1, -1]
        
        code%instruction(18) = CODE_MULTIPLY
        code%argument(:,18) = [17, 16, -1]
        
        code%instruction(19) = CODE_ADD
        code%argument(:,19) = [18, 14, -1]
        
        code%instruction(20) = CODE_NONE
        code%argument(:,20) = [-1, -1, -1]
        
        code%instruction(21) = CODE_SUBSTRACT
        code%argument(:,21) = [20, 19, -1]
    end subroutine
end module

program test
    use test_bytecode
    use mod_test

    call test_all
    call testsFinished
end program
