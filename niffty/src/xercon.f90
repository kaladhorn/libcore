    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    logical function xercon(inc, jump, n, lot)
        include 'kinds.inc'
        integer inc, jump, n, lot
        integer i, j, jnew, lcm

        ! Definition: positive integers INC, JUMP, N and LOT are consistent
        ! ----------
        ! if I1*INC + J1*JUMP = I2*INC + J2*JUMP for I1,I2 < N and J1,J2
        ! < LOT implies I1=I2 and J1=J2.

        ! For multiple FFTs to execute correctly, input parameters INC,
        ! JUMP, N and LOT must be consistent ... otherwise at least one
        ! array element mistakenly is transformed more than once.

        ! XERCON = .TRUE. if and only if INC, JUMP, N and LOT are
        ! consistent.

        ! ------------------------------------------------------------------

        ! Compute I = greatest common divisor (INC, JUMP)

        i = inc
        j = jump
100     continue
        if (j/=0) then
            jnew = mod(i, j)
            i = j
            j = jnew
            go to 100
        end if

        ! Compute LCM = least common multiple (INC, JUMP)

        lcm = (inc*jump)/i

        ! Check consistency of INC, JUMP, N, LOT

        if (lcm<=(n-1)*inc .and. lcm<=(lot-1)*jump) then
            xercon = .false.
        else
            xercon = .true.
        end if

        return
    end function
