    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    ! *                                                               *
    ! *                  copyright (c) 2011 by UCAR                   *
    ! *                                                               *
    ! *       University Corporation for Atmospheric Research         *
    ! *                                                               *
    ! *                      all rights reserved                      *
    ! *                                                               *
    ! *                     FFTPACK  version 5.1                      *
    ! *                                                               *
    ! *                 A Fortran Package of Fast Fourier             *
    ! *                                                               *
    ! *                Subroutines and Example Programs               *
    ! *                                                               *
    ! *                             by                                *
    ! *                                                               *
    ! *               Paul Swarztrauber and Dick Valent               *
    ! *                                                               *
    ! *                             of                                *
    ! *                                                               *
    ! *         the National Center for Atmospheric Research          *
    ! *                                                               *
    ! *                Boulder, Colorado  (80307)  U.S.A.             *
    ! *                                                               *
    ! *                   which is sponsored by                       *
    ! *                                                               *
    ! *              the National Science Foundation                  *
    ! *                                                               *
    ! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

    subroutine msntb1(lot, jump, n, inc, x, wsave, dsum, xh, work, ier)
        !implicit integer(i-n), double precision(a-h,o-z)
        include 'kinds.inc'
        real (rwp) x(inc, *), wsave(*), xh(lot, *)
        double precision dsum(*)

        ier = 0
        lj = (lot-1)*jump + 1
        if (n-2) 140, 100, 110
100     srt3s2 = sqrt(3.0_rwp)/2.
        do m = 1, lj, jump
            xhold = srt3s2*(x(m,1)+x(m,2))
            x(m, 2) = srt3s2*(x(m,1)-x(m,2))
            x(m, 1) = xhold
        end do
        go to 140
110     np1 = n + 1
        ns2 = n/2
        do k = 1, ns2
            kc = np1 - k
            m1 = 0
            do m = 1, lj, jump
                m1 = m1 + 1
                t1 = x(m, k) - x(m, kc)
                t2 = wsave(k)*(x(m,k)+x(m,kc))
                xh(m1, k+1) = t1 + t2
                xh(m1, kc+1) = t2 - t1
            end do
        end do
        modn = mod(n, 2)
        if (modn==0) go to 120
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            xh(m1, ns2+2) = 4.*x(m, ns2+1)
        end do
120     do m = 1, lot
            xh(m, 1) = 0.
        end do
        lnxh = lot - 1 + lot*(np1-1) + 1
        lnsv = np1 + int(log(real(np1, rwp))/log(2.0_rwp)) + 4
        lnwk = lot*np1

        call rfftmf(lot, 1, np1, lot, xh, lnxh, wsave(ns2+1), lnsv, work, &
          lnwk, ier1)
        if (ier1/=0) then
            ier = 20
            call xerfft('MSNTB1', -5)
            go to 140
        end if

        if (mod(np1,2)/=0) go to 130
        do m = 1, lot
            xh(m, np1) = xh(m, np1) + xh(m, np1)
        end do
130     fnp1s4 = float(np1)/4.
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            x(m, 1) = fnp1s4*xh(m1, 1)
            dsum(m1) = x(m, 1)
        end do
        do i = 3, n, 2
            m1 = 0
            do m = 1, lj, jump
                m1 = m1 + 1
                x(m, i-1) = fnp1s4*xh(m1, i)
                dsum(m1) = dsum(m1) + fnp1s4*xh(m1, i-1)
                x(m, i) = dsum(m1)
            end do
        end do
        if (modn/=0) go to 140
        m1 = 0
        do m = 1, lj, jump
            m1 = m1 + 1
            x(m, n) = fnp1s4*xh(m1, n+1)
        end do

140     continue
    end subroutine
