!******************************************************************************!
!                          expression_parse
!------------------------------------------------------------------------------!
! Procedures used when parsing a character variable to generage an abstract
! syntax tree.
!
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!


!******************************************************************************!
!> Build the abstract syntax tree of an expression
!******************************************************************************!
    recursive subroutine buildAst(list, token, visited, i)
        type(ast_node), dimension(:), intent(in) :: list
        type(ast_node), intent(out), target :: token
        logical, dimension(:), intent(inout) :: visited
        integer, intent(in) :: i
!------
        integer :: k
!------
        ! Get the next unvisited token
        k = i
        do while (visited(k))
            k = k - 1
            if (k == 0) return
        end do

        ! Copy the current token, and mark it as visited
        token = list(k)
        visited(k) = .true.

        if (token%nArg > 0) then

            ! Set the first argument
            allocate(token%argument1)
            token%argument1%parent => token
            call buildAst(list, token%argument1, visited, k-1)
            token%argument1%parent => token

            ! Set the second argument
            if (token%nArg > 1) then
                allocate(token%argument2)
                token%argument2%parent => token
                call buildAst(list, token%argument2, visited, k-1)
                token%argument2%parent => token
            end if
        end if
    end subroutine
!******************************************************************************!
!> Turn an expression into a token list
!******************************************************************************!
    subroutine tokenise(expression, list, status)
        character(*), intent(in) :: expression
        type(ast_node), dimension(:), allocatable, intent(out) :: list
        integer, intent(out), optional :: status
!------
        type(ast_node), dimension(:), allocatable :: input
        integer :: i, index, inputLevel
        type(FRegex), dimension(:), allocatable :: re
        character(:), allocatable :: expression_, lastExpression
        real(e) :: value
!------
        if (present(status)) status = 0

        ! Get an copy of the expression for internal use
        expression_ = trim(adjustl(expression))

        ! Allocate the input stack
        ! len_trim(expression) is an upper bound for the number of tokens in expression
        allocate(input(len_trim(expression_)))
        inputLevel = 0

        ! Set up the regular expressions for the tokens
        allocate(re(5))
        re(1) = FRegex("^\s*\p{L}[\p{L}0-9_\.]*") ! Variable or function name
        re(2) = FRegex("^\s*([0-9]+(\.[0-9]*([eE][\+\-]?[0-9]+)?)?|" // &
            "[0-9]*(\.[0-9]+([eE][\+\-]?[0-9]+)?))") ! Number
        re(3) = FRegex("^\s*\(") ! Left parenthesis
        re(4) = FRegex("^\s*\)") ! Right parenthesis
        re(5) = FRegex("^\s*(==|\.eq\.|\!=|\/=|\.ne\.|\.gt\.|\>=|\.ge\.|" // &
            "\<=|\.le\.|<|\.lt\.|>|\.and\.|&&|&|\.or\.|\|\||\||\.not\.|!|\+|\-|\*\*|\*|\^|\/)") ! Operator

        ! Build the input stack
        body: block
            type(ast_node) :: token
            type(FException) :: stat_

            lastExpression = ""
            do while (expression_ /= "")
                if (expression_ == lastExpression) then
                    call stat_%raise(FExceptionDescription("Internal error while creating token list", &
                        "when parsing expression '" // trim(expression) // "':", MESSAGE_LEVEL_IPE))
                    write(*,*) "Parser stack:"
                    do i=1, inputLevel
                        write(*,*) i, input(i)%type, input(i)%name
                    end do
                    return
                end if
                lastExpression = expression_

                ! Get the next token
                token%type = 0
                do i=1, size(re)
                    call re(i)%match(expression_, index, token%name, stat_)
                    if (index /= 0) then
                        token%type = i
                        expression_(index:index+len(token%name)-1) = " "
                        expression_ = adjustl(expression_)
                        exit
                    end if
                    if (stat_ == "No match found.") call stat_%discard
                    if (stat_ /= 0) call stat_%report
                end do
                ! Put it in the input stack
                call push(token, input, inputLevel)
            end do
        end block body

        ! Separate the variables from the functions
        do i=1, inputLevel
            if (input(i)%type == TOKEN_TYPE_NAME) then
                if (i+1 <= size(input)) then
                    if (input(i+1)%type == TOKEN_TYPE_LEFT_PAREN) then
                        input(i)%type = TOKEN_TYPE_FUNCTION
                    else
                        input(i)%type = TOKEN_TYPE_VARIABLE
                    end if
                else
                    input(i)%type = TOKEN_TYPE_VARIABLE
                end if
            end if
        end do

        ! Set the values of the constants
        do i=1, inputLevel
            if (input(i)%type == TOKEN_TYPE_NUMBER) then
                read(input(i)%name,*) value
                input(i) = getNumbernode(value)
            end if
        end do

        ! Set variable tokens
        do i=1, inputLevel
            if (input(i)%type == TOKEN_TYPE_VARIABLE) then
                select case(input(i)%name)
                    case ("pi", "Pi", "PI", "π")
                        input(i) = getPiNode()
                    case default
                        input(i) = getVariableNode(input(i)%name)
                end select
            end if
        end do

        ! Set the function tokens
        do i=1, inputLevel
            if (input(i)%type == TOKEN_TYPE_FUNCTION) then
                select case(input(i)%name)
                    case("cos")
                        input(i) = getCosNode()
                    case("sin")
                        input(i) = getSinNode()
                    case("tan")
                        input(i) = getTanNode()
                    case("cosh", "ch")
                        input(i) = getCoshNode()
                    case("sinh", "sh")
                        input(i) = getSinhNode()
                    case("tanh", "th")
                        input(i) = getTanhNode()
                    case("acos", "arccos")
                        input(i) = getAcosNode()
                    case("asin", "arcsin")
                        input(i) = getAsinNode()
                    case("atan", "arctan")
                        input(i) = getAtanNode()
                    case("acosh", "arccosh")
                        input(i) = getAcoshNode()
                    case("asinh", "arcsinh")
                        input(i) = getAsinhNode()
                    case("atanh", "arctanh")
                        input(i) = getAtanhNode()
                    case("erf")
                        input(i) = getErfNode()
                    case("erfc")
                        input(i) = getErfcNode()
                    case("exp")
                        input(i) = getExpNode()
                    case("log", "ln")
                        input(i) = getLnNode()
                    case("log10")
                        input(i) = getLog10Node()
                     case("abs")
                        input(i) = getAbsNode()
                    case("sqrt")
                        input(i) = getSqrtNode()
                   case default
                       write(*,*) "Unknown function "//input(i)%name
                end select
            end if
        end do

        ! Set the operators tokens
        do i=1, inputLevel
            if (input(i)%type == TOKEN_TYPE_OPERATOR) then
                select case(input(i)%name)
                    case ("+")
                        if (i == 1) then
                            input(i) = getIdNode()
                        else
                            if (input(i-1)%type == TOKEN_TYPE_NUMBER .or. input(i-1)%type == TOKEN_TYPE_VARIABLE .or. &
                                input(i-1)%type == TOKEN_TYPE_RIGHT_PAREN) then

                                call getAddNode(input(i))
                            else
                                input(i) = getIdNode()
                            end if
                        end if
                    case ("-")
                        if (i == 1) then
                            input(i) = getMinusNode()
                        else
                            if (input(i-1)%type == TOKEN_TYPE_NUMBER .or. input(i-1)%type == TOKEN_TYPE_VARIABLE .or. &
                                input(i-1)%type == TOKEN_TYPE_RIGHT_PAREN) then

                                input(i) = getSubstractNode()
                            else
                                input(i) = getMinusNode()
                            end if
                        end if
                    case ("*")
                        input(i) = getMultiplyNode()
                    case ("/")
                        call getDivideNode(input(i))
                    case ("**", "^")
                        call getPowNode(input(i))
                    case ("==", ".eq.")
                        call getEqualNode(input(i))
                    case ("/=", "!=", ".ne.")
                        call getDifferentNode(input(i))
                    case ("<", ".lt.")
                        call getLessNode(input(i))
                    case (">", ".gt.")
                        call getGreaterNode(input(i))
                    case ("<=", ".le.")
                        call getLessEqNode(input(i))
                    case (">=", ".ge.")
                        call getGreaterEqNode(input(i))
                    case (".and.", "&", "&&")
                        call getAndNode(input(i))
                    case (".or.", "|", "||")
                        call getOrNode(input(i))
                    case (".not.", "!")
                        call getNotNode(input(i))
                end select
            end if
        end do

        ! Set the final list
        list = input(:inputLevel)
    end subroutine
!******************************************************************************!
!>
!> Turn token list into a reversed form using a Shunting-yard algorithm.
!******************************************************************************!
    subroutine reverse(list, err)
        type(ast_node), dimension(:), allocatable, intent(inout) :: list
        type(FException), intent(out) :: err
!------
        type(ast_node), dimension(:), allocatable :: input, output, stack
        integer :: outputLevel, stackLevel, inputLevel
        type(ast_node) :: token, op
        integer :: j
!------
        body: block
            call move_alloc(from=list, to=input)

            ! Number of tokens in the input list
            inputLevel = size(input)

            ! Allocate the temporary output list
            ! The number of tokens in the input list is the maximum number of tokens in the output stack
            allocate(output(inputLevel))
            outputLevel = 0

            ! Allocate the operator stack
            ! ditto
            allocate(stack(inputLevel))
            stackLevel = 0

            ! Process the expression while there is still something to process
            do j=1, inputLevel

                ! Get the next token
                token = input(j)

                select case(token%type)

                    case(TOKEN_TYPE_NUMBER, TOKEN_TYPE_VARIABLE, TOKEN_TYPE_CONSTANT)
                        call push(token, output, outputLevel)

                    case(TOKEN_TYPE_FUNCTION, TOKEN_TYPE_LEFT_PAREN)
                        call push(token, stack, stackLevel)

                    case(TOKEN_TYPE_OPERATOR)
                        ! Push to the output list the operators on top of the operator stack that have a lower precedence
                        if (stackLevel > 0) then
                            do while (token%precedence <= stack(stackLevel)%precedence .and. &
                                    stack(stackLevel)%type == TOKEN_TYPE_OPERATOR)
                                call pop(stack, op, stackLevel)
                                call push(op, output, outputLevel)
                                if (stackLevel == 0) exit
                            end do
                        end if
                        call push(token, stack, stackLevel)

                    case(TOKEN_TYPE_RIGHT_PAREN)
                        ! Pop the operators until we find the corresponding left parenthesis
                        do while (stack(stackLevel)%type /= TOKEN_TYPE_LEFT_PAREN)
                            call pop(stack, op, stackLevel)
                            call push(op, output, outputLevel)
                        end do

                        ! Pop the left parenthesis
                        call pop(stack, op, stackLevel)

                        ! If the token at the top of the operator stack is a function, then move it to the output list
                        ! (ignore if the stack is empty)
                        if (stackLevel > 0) then
                            if (stack(stackLevel)%type == TOKEN_TYPE_FUNCTION) then
                                call pop(stack, op, stackLevel)
                                call push(op, output, outputLevel)
                            end if
                        end if

                    case default
                        call err%raise("Unknown error while building AST "//token%type)
                end select

            end do

            ! Move the remaining operators to the output list
            do while (stackLevel /= 0)
                if (stack(stackLevel)%type == TOKEN_TYPE_LEFT_PAREN .or. &
                        stack(stackLevel)%type == TOKEN_TYPE_RIGHT_PAREN) then
                    ! Syntax error: mismatched parentheses
                    call err%raise("mismatched parentheses")
                    exit body
                end if
                call pop(stack, op, stackLevel)
                call push(op, output, outputLevel)
            end do

            ! Set the final list
            list = output(:outputLevel)
        end block body
    end subroutine
!******************************************************************************!
!> Pop a token from a list of token objects
!******************************************************************************!
    subroutine pop(list, token, level)
        type(ast_node), dimension(:), allocatable, intent(inout) :: list
        type(ast_node), intent(out) :: token
        integer, intent(inout) :: level
!------
        if (level > 0) then
            token = list(level)
            level = level - 1
        end if
    end subroutine
!******************************************************************************!
!> Push a token on a list of token objects
!******************************************************************************!
    subroutine push(token, list, level)
        type(ast_node), intent(in) :: token
        type(ast_node), dimension(:), allocatable, intent(inout) :: list
        integer, intent(inout) :: level
!------
        type(ast_node), dimension(:), allocatable :: list_
        integer :: i
!------
        ! Make sure the stack has enough room for another element
        if (level + 1 > size(list)) then
            allocate(list_(size(list) + LIST_SIZE_INCREMENT))
            do i=1, size(list)
                list_(i) = list(i)
            end do
            deallocate(list)
            call move_alloc(from=list_, to=list)
        end if

        ! Add the element
        level = level + 1
        list(level) = token
    end subroutine