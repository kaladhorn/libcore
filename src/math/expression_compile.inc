    subroutine generateBytecode(this)
        class(FExpression), intent(inout) :: this
!------
        integer :: nVariables, nConstants, nInstructions
        integer :: i, n
!------

        ! Get the number of variables
        nVariables = size(this%variable)

        ! Get the number of instructions and constants
        nInstructions = 0
        nConstants = 0
        call this%root%countNodes(nInstructions, nConstants)

        ! Setup the bytecode data structure
        this%bytecode = FBytecode(nInstructions, nConstants, nVariables)

        ! Set the variables
        do i=1, nVariables
            this%bytecode%data(i) = this%variable(i)%value
            if (this%variable(i)%constant) then
                this%bytecode%instruction(i) = CODE_CONSTANT
                this%bytecode%data(i) = this%variable(i)%value
            else
                this%bytecode%instruction(i) = CODE_VAR
                this%bytecode%data(i) = this%variable(i)%value
            end if
        end do

        this%bytecode%start_address = nVariables + 1
        n = size(this%bytecode%instruction)

        ! Get the instruction list
        call bytecode_getInstructions(this%root, this%bytecode, n, this%variable)
    end subroutine

    recursive subroutine bytecode_getInstructions(this, code, n, variables)
        type(ast_node), intent(in) :: this
        type(FBytecode), intent(inout) :: code
        integer, intent(inout) :: n
        type(variable), dimension(:), intent(in) :: variables
!------
        integer :: i, j
!------
        i = n
        ! Detect integer powers
        if (this%id == CODE_POW .and. &
            (this%argument1%type == TOKEN_TYPE_NUMBER .or. this%argument1%type == TOKEN_TYPE_CONSTANT) .and. &
            (nint(this%argument1%value)*10 == nint(this%argument1%value*10))) then

            code%instruction(i) = CODE_POW_INT
        else
            code%instruction(i) = this%id
        end if
        code%data(i) = this%value

        if (associated(this%argument1)) then
            if (this%argument1%type == TOKEN_TYPE_VARIABLE) then
                do j=1, size(variables)
                    if (variables(j)%name == this%argument1%name) then
                        code%argument(1,i) = j
                        exit
                    end if
                end do
            else
                n = n - 1
                code%argument(1,i) = n
                call bytecode_getInstructions(this%argument1, code, n, variables)
            end if
        end if

        if (associated(this%argument2)) then
            if (this%argument2%type == TOKEN_TYPE_VARIABLE) then
                do j=1, size(variables)
                    if (variables(j)%name == this%argument2%name) then
                        code%argument(2,i) = j
                        exit
                    end if
                end do
            else
                n = n - 1
                code%argument(2,i) = n
                call bytecode_getInstructions(this%argument2, code, n, variables)
            end if
        end if
    end subroutine
