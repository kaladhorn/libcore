module test_core
    use ieee_exceptions
    use core
    use ext_character
    implicit none
    private
    
    public :: testAll
contains
    subroutine testAll()
        
        ! Set the halting modes
        call ieee_set_flag(IEEE_DIVIDE_BY_ZERO, .false.)
        call ieee_set_halting_mode(IEEE_DIVIDE_BY_ZERO, .true.)

        call ieee_set_flag(IEEE_UNDERFLOW, .false.)
        call ieee_set_halting_mode(IEEE_UNDERFLOW, .true.)

        call ieee_set_flag(IEEE_OVERFLOW, .false.)
        call ieee_set_halting_mode(IEEE_OVERFLOW, .true.)

        call ieee_set_flag(IEEE_INVALID, .false.)
        call ieee_set_halting_mode(IEEE_INVALID, .true.)

        call testAssertions
        call testFloatingPointcomparisons
        
    end subroutine
    
    subroutine testAssertions
        testing = .true.
        
        ! Test false assertion with a message
        call assert(.false., "This is expected to fail.")
        ! Test assertion without a message
        call assert(.false.)
        ! Test true assertion with a message
        call assert(.true., "This should not fail, ever.")
    end subroutine
    
    subroutine testFloatingPointcomparisons
        real(sp) :: pis, cs
        real(dp) :: pid, cd
        real(qp) :: piq, cq
        
        ! Pi comparisons
        pis = 3.1415926535897932384626433832795029_sp
        pid = 3.1415926535897932384626433832795029_dp
        piq = 3.1415926535897932384626433832795029_qp
        
        ! Single-precision comparisons
        call assert(pis == acos(-1.0_sp))
        call assert(pis .approx. acos(-1.0_sp), "Wrong result for single-precision acos(-1): " // acos(-1.0_sp))
        call assert(pis /= acos(-1.0_dp))
        call assert(pis .approx. acos(-1.0_dp), "Wrong result for single-precision acos(-1): " // acos(-1.0_dp))
        call assert(pis /= acos(-1.0_qp))
        call assert(pis .approx. acos(-1.0_qp), "Wrong result for single-precision acos(-1): " // acos(-1.0_qp))
        
        ! Double-precision comparisons
        call assert(pid /= acos(-1.0_sp))
        call assert(pid .approx. acos(-1.0_sp), "Wrong result for double-precision acos(-1): " // acos(-1.0_dp))
        call assert(pid == acos(-1.0_dp))
        call assert(pid .approx. acos(-1.0_dp), "Wrong result for double-precision acos(-1): " // acos(-1.0_dp))
        call assert(pid /= acos(-1.0_qp))
        call assert(pid .approx. acos(-1.0_qp), "Wrong result for double-precision acos(-1): " // acos(-1.0_dp))
        
        ! Quadruple-precision comparisons
        call assert(piq /= acos(-1.0_sp))
        call assert(piq .approx. acos(-1.0_sp), "Wrong result for quadruple-precision acos(-1): " // acos(-1.0_qp))
        call assert(piq /= acos(-1.0_dp))
        call assert(piq .approx. acos(-1.0_dp), "Wrong result for quadruple-precision acos(-1): " // acos(-1.0_qp))
        call assert(piq == acos(-1.0_qp))
        call assert(piq .approx. acos(-1.0_qp), "Wrong result for quadruple-precision acos(-1): " // acos(-1.0_qp))
        
        cs = 0.1_sp
        cd = 0.1_dp
        cq = 0.1_qp
        
        ! Single-precision comparisons
        call assert(cs == cs)
        call assert(cs .approx. cs, "Wrong comparison 1.")
        call assert(cs /= cd)
        call assert(cs .approx. cd, "Wrong comparison 2.")
        call assert(cs /= cq)
        call assert(cs .approx. cq, "Wrong comparison 3.")
        
        ! Double-precision comparisons
        call assert(cd /= cs)
        call assert(cd .approx. cs, "Wrong comparison 4.")
        call assert(cd == cd)
        call assert(cd .approx. cd, "Wrong comparison 5.")
        call assert(cd /= cq)
        call assert(cd .approx. cq, "Wrong comparison 6.")
        
        ! Quadruple-precision comparisons
        call assert(cq /= cs)
        call assert(cq .approx. cs, "Wrong comparison 7.")
        call assert(cq /= cd)
        call assert(cq .approx. cd, "Wrong comparison 8.")
        call assert(cq == cq)
        call assert(cq .approx. cq, "Wrong comparison 9.")
        
        ! Sensitivity test
        call assert(0.88137358701954305_e .approx. 0.88137358701954329_e, "sensitivity test 0")
        call assert(3.141592653589793_e .approx. 3.141592653589790_e, "sensitivity test 1")
        call assert(.not.(3.141592653589793_e .approx. 3.141592653589700_e), "sensitivity test 2")
        call assert(.not.(3.141592653589793_e .approx. 3.141592653589000_e), "sensitivity test 3")
    end subroutine
end module

program test
    use test_core
    
    call testAll
end program