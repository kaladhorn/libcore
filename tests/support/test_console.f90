module test_console
    use core
    use mod_test
    use mod_console, only: FConsole
    
    implicit none
    public
    save
contains
    subroutine testAll
        call testsFinished
    end subroutine
    
end module

program test
    use test_console
    
    call testAll
end program