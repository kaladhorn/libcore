!******************************************************************************!
!                         mod_textBzip2FileReader module
!------------------------------------------------------------------------------!
!> `[[FBzip2FileReader(type)]]` derived type and TBPs, for bzip2-compressed
!> file I/O.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_bzip2FileReader
    use iso_c_binding

    use core
    use ext_character
    use com_io

    use mod_string,     only: FString
    use mod_exception,  only: FException, FExceptionDescription
    use mod_fileReader, only: FFileReader

    implicit none
    private
    save

!******************************************************************************!
!> File reader object for bzip2-compressed file I/O
!******************************************************************************!
    type, public, extends(FFILEREADER) :: FBzip2FileReader
        private
        type(c_ptr) :: stream = C_NULL_PTR !< Pointer to the C stream
        logical :: readMode   = .false.    !< Indicates whether the file has been opened for reading
        logical :: writeMode  = .false.    !< Indicates whether the file has been opened for writing

        character(:), allocatable :: tmpname   !< Name of the temporary file
        character(:), allocatable :: finalname !< Name of the actual file
    contains
        procedure, public :: canRead   !< Check whether the file can be read
        procedure, public :: canWrite  !< Check whether the file can be written

        procedure, public :: openFile  !< Open the file
        procedure, public :: closeFile !< Close the file

        procedure, public :: getNextLine_character !< Read the next file from the file into a character variable
        procedure, public :: getNextLine_string    !< Read the next file from the file into a string object
        procedure, public :: getLines              !< Read all the file into a character array

        procedure, public :: writeLine  !< Write one line to the file
        procedure, public :: writeLines !< Write a character array to the file

        final :: textBzip2FileReader_finalise !< Clean up
    end type

    integer, parameter :: BUFFER_LENGTH = 65536
contains
!******************************************************************************!
!> Final subroutine, to make sure that the file is closed properly when the
!> reader disappears.
!******************************************************************************!
    elemental impure subroutine textBzip2FileReader_finalise(this)
        type(FBzip2FileReader), intent(inout) :: this
!------
        call closeFile(this)
    end subroutine
!******************************************************************************!
!> Check whether the reader can be used to read data.
!******************************************************************************!
    function canRead(this)
        class(FBzip2FileReader), intent(in) :: this
        logical :: canRead
!------
        canRead = (c_associated(this%stream) .and. this%readMode)
    end function
!******************************************************************************!
!> Check whether the reader can be used to write data.
!******************************************************************************!
    function canWrite(this)
        class(FBzip2FileReader), intent(in) :: this
        logical :: canWrite
!------
        canWrite = (c_associated(this%stream) .and. this%writeMode)
    end function
!******************************************************************************!
!> Open a bzip2-compressed file.
!******************************************************************************!
    subroutine openFile(this, filename, mode, stat)
        class(FBzip2FileReader), intent(inout), target :: this
        character(*), intent(in) :: filename
        character(*), intent(in) :: mode
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        type(c_ptr) :: out
        logical :: exists
!------
        body: block
            select case(mode)

                ! Open the file for reading
                case ("r", "read")

                    ! Check whether the file exists
                    inquire(file=filename, exist=exists)
                    if (.not.exists) then

                        ! If yes, raise an error
                        call stat_%raise(FExceptionDescription("File does not exist.", &
                            "when trying to open " // filename // " for reading:"))
                        exit body
                    end if

                    ! Check whether the file is already open
                    if (c_associated(this%stream)) then

                        ! If yes, raise an error
                        call stat_%raise(FExceptionDescription("File already open.", &
                            "when trying to open " // filename // " for reading:"))
                        exit body
                    end if

                    ! If no error happened, the file has to be decompressed

                    ! Get a unique file name
                    this%tmpname = "libcore_temp_XXXXXX"//C_NULL_CHAR
                    out = mktemp(this%tmpname)

                    ! Make sure we have a valid temporary name
                    if (this%tmpname == "") then
                        call stat_%raise(FExceptionDescription("Could not get temporary file", &
                            "when trying to open " // filename // " for reading:"))
                        exit body
                    end if

                    ! Uncompress the file
                    call execute_command_line("bzcat -d " // filename // &
                        " > " // this%tmpname)

                    ! Then, open it for reading
                    this%stream = fopen(this%tmpname//C_NULL_CHAR, &
                        "r"//C_NULL_CHAR)

                    ! Error check
                    if (.not.c_associated(this%stream)) then
                        call stat_%raise(FExceptionDescription(ioErrorMessage(), &
                            "when trying to open " // filename // " for reading:"))
                        exit body
                    end if

                    ! Set the mode
                    this%readMode = .true.
                    this%writeMode = .false.

                ! Open the file for writing
                case ("w", "write")
                    ! Check whether the file is already open
                    if (c_associated(this%stream)) then

                        ! If yes, raise an error
                        call stat_%raise("Error while trying to open file " // filename // ": file already open.")
                        exit body
                    else
                        ! If not, create an uncompressed file

                        ! Save the final file name
                        this%finalname = filename

                        ! Get a unique file name
                        this%tmpname = "libcore_temp_XXXXXX"//C_NULL_CHAR
                        out = mktemp(this%tmpname)

                        ! Call library C functions
                        this%stream = fopen(this%tmpname, &
                            "w"//C_NULL_CHAR)

                        ! Remove the trailing null character
                        this%tmpname = this%tmpname(:len(this%tmpname)-1)

                        ! Error check
                        if (.not.c_associated(this%stream)) then

                            call stat_%raise("Error while trying to open file " // filename // " for writing.")
                            exit body
                        end if

                        ! Set the mode
                        this%readMode = .false.
                        this%writeMode = .true.
                    end if

                ! Handle unknown modes
                case default
                    call stat_%raise("Error while trying to open file " // filename // &
                        ": '"//mode//" is not a valid mode.")
                    exit body
            end select
        end block body

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Close a bzip2-compressed file.
!******************************************************************************!
    subroutine closeFile(this, stat)
        class(FBzip2FileReader), intent(inout):: this
        type(FException), intent(out), optional :: stat
!------
        integer :: err
!------
        if (c_associated(this%stream)) then
            err = fclose(this%stream)
            this%stream = C_NULL_PTR
        end if
        if (allocated(this%tmpname)) then
            if (this%readMode) then
                ! Remove the temporary file
                call execute_command_line("rm "//this%tmpname)
                deallocate(this%tmpname)
            else if (this%writeMode) then
                ! Compress the temporary file
                call execute_command_line("bzip2 -z " // this%tmpname)
                call execute_command_line("mv " // this%tmpname // ".bz2 " // this%finalname)
                deallocate(this%tmpname)
                deallocate(this%finalname)
            end if
        end if
    end subroutine
!******************************************************************************!
!> Read the next line from a bzip2-compressed file into a character scalar.
!******************************************************************************!
    subroutine getNextLine_character(this, line, stat)
        class(FBzip2FileReader), intent(inout):: this
        character(*), intent(out) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: i
        character(1,c_char), dimension(BUFFER_LENGTH), target :: cbuff
        type(FException) :: stat_
        type(c_ptr) :: out
        integer :: size_
        character(:), allocatable :: tmp
!------

        body: block

            ! Stop here if the file is not open
            if (.not.c_associated(this%stream)) then
                call stat_%raise("File not open.")
                exit body
            end if

            ! Make sure we do not read past end of file
            if (feof(this%stream) /= 0) then
                call stat_%raise("Trying to read past end of file.")
                exit body
            end if

            ! Get the line from the file
            cbuff = " "
            out = fgets(cbuff, size(cbuff)-1, this%stream)

            call c_f_string(out, tmp)

            ! Handle errors
            if (.not.c_associated(out)) then
                line = ""
                call stat_%raise(ioErrorMessage())
                exit body
            end if

            ! Get the index of the last useful character (ignoring t
            ! railing C_NULL_CHARs and new line)
            size_ = 0
            do i=1, size(cbuff)
                if (cbuff(i) == new_line(cbuff) .or. cbuff(i) == C_NULL_CHAR) then
                    size_ = i-1
                    exit
                end if
            end do

            ! Copy the useful string
            do i=1, min(size_, len(line))
                line(i:i) = cbuff(i)
            end do
            if (size_ < len(line)) then
                line(size_+1:) = ""
            else
                call stat_%raise("Line too long for the buffer.")
            end if
        end block body

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Read the next line from a bzip2-compressed file into a string object.
!******************************************************************************!
    subroutine getNextLine_string(this, line, stat)
        class(FBzip2FileReader), intent(inout):: this
        type(FString), intent(out) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: i
        character(BUFFER_LENGTH) :: lbuf
        character(1,c_char), dimension(BUFFER_LENGTH) :: cbuff
        type(FException) :: stat_
        type(c_ptr) :: out
        integer :: size_
!------

        body: block

            ! Stop here if the file is not open
            if (.not.c_associated(this%stream)) then
                call stat_%raise("File not open.")
                exit body
            end if

            ! Get the line from the file
            out = fgets(cbuff, size(cbuff)-1, this%stream)

            ! Handle errors
            if (.not.c_associated(out)) then
                line = ""
                if (errno() == 0) then
                    call stat_%raise("End of file.")
                else
                    call stat_%raise(ioErrorMessage())
                end if
                exit body
            end if

            ! Get the index of the last useful character (ignoring t
            ! railing C_NULL_CHARs and new line)
            size_ = 0
            do i=1, size(cbuff)
                if (cbuff(i) == new_line(cbuff) .or. cbuff(i) == C_NULL_CHAR) then
                    size_ = i-1
                    exit
                end if
            end do

            ! Copy the useful string
            do i=1, size_
                lbuf(i:i) = cbuff(i)
            end do
            line = lbuf(:size_)
        end block body

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Read the next line from a bzip2-compressed file into a string object array.
!******************************************************************************!
    subroutine getLines(this, lines, stat)
        class(FBzip2FileReader), intent(inout):: this
        type(FString), dimension(:), allocatable, intent(out) :: lines
        type(FException), intent(out), optional :: stat
!------
        integer(c_long) :: pos
        integer(c_int) :: n, err
        integer :: i
        type(FException) :: stat_
!------
        ! Save the position within the file
        pos = ftell(this%stream)

        ! Count the number of lines
        n = countLines(this%stream)

        ! Rewind to the initial position in file
        err = fseek(this%stream, pos, 0_c_int)

        if (err == 0) then
            ! Allocate the output array
            allocate(lines(n))

            ! Fill the output array
            do i=1, n
                call getNextLine_string(this, lines(i), stat_)
                if (stat_ /= 0) exit
            end do
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Write a character string to a bzip2-compressed file.
!******************************************************************************!
    subroutine writeLine(this, line, stat)
        class(FBzip2FileReader), intent(inout):: this
        character(*), intent(in) :: line
        type(FException), intent(out), optional :: stat
!------
        integer :: iostat, i
        type(FException) :: stat_
        character(1,c_char), dimension(1000) :: buffer
!------

        if (c_associated(this%stream) .and. this%writeMode) then
            do i=1, len(line)
                buffer(i) = line(i:i)
            end do
            buffer(len(line)+1) = new_line(buffer)
            buffer(len(line)+2) = C_NULL_CHAR

            ! Call library C functions
            iostat = fputs(buffer, this%stream)

            ! Report errors
            if (iostat < 0) then
                call stat_%raise(ioErrorMessage())
            end if
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
!******************************************************************************!
!> Write a string object array to a bzip2-compressed file.
!******************************************************************************!
    subroutine writeLines(this, lines, stat)
        class(FBzip2FileReader), intent(inout) :: this
        type(FString), dimension(:), intent(in) :: lines
        type(FException), intent(out), optional :: stat
!------
        type(FException) :: stat_
        integer :: i, iostat, a
        character(1,c_char), dimension(1000) :: buffer
        character(:), allocatable :: line
!------
        if (c_associated(this%stream) .and. this%writeMode) then
            do i=1, size(lines)
                line = lines(i)%string()

                ! Fill the buffer
                do a=1, len(line)
                    buffer(a) = line(a:a)
                end do
                buffer(len(line)+1) = new_line(buffer)
                buffer(len(line)+2) = C_NULL_CHAR

                ! Call library C functions
                iostat = 0
                iostat = fputs(buffer, this%stream)

                ! Report errors
                if (iostat < 0) then
                    write(*,*) "Error condition when writing to file"
                    write(*,*) ioErrorMessage()
                    write(*,*) "tmpname:", this%tmpname
                    call stat_%raise(ioErrorMessage())
                    exit
                end if
            end do
        end if

        ! Report any issue
        if (present(stat)) call stat%transfer(stat_)
    end subroutine
end module
