title: How to use properties


Properties are key-value pairs that can be organised in a tree structure. 
They can be set from the code, read from the command line or from files. 
They are a convenient way to get input from the user or make different parts 
of a program communicate whilst reducing coupling.
They were designed to be easy to use both for users and developers.

# Property types

A property is a key-value pair. The key is a character string of alphanumeric
characters, and the value can contain any character. Values are internally 
stored as character strings, but do not have a fixed type. Instead, they are
interpreted depending on how the programer wants to use them. For example, 
the value "42" can be used as a real number, an integer or a character string 
depending on the context. A fundamental characteristic of properties is that 
their characteristics are determined when their values are acceded. For example,
a property can be a valid integer at one point in time, and a valid logical later.


Apart from having a value, a property can have children, thus creating a tree-like
structure of nested properties. Such trees are useful to group together properties
that have a common context.

@note In the following documentation, a property is described by its
   key and value, separated by a `=`sign, as in `key = value`. If a property has 
   children, they are put between braces, like in 
   `parent = {child1 = value1, child2 = value2}`.

Line breaks might be used to separate children, but the definition of an atomic 
property cannot span more than one line. Thus, the following definition is valid:

``````
parent = {
  child1 = value1,
  child2 = value2
}
``````

but 

``````
parent = {
  child1 = 
    value1,
  child2 = value2
}
``````

is not.


The key of a nested property is the concatenation of its parents' keys, starting 
from the root property, separated by `-`signs. For example, consider the definition 
``````
energy = {
  type = lennard-jones, 
  arg = {
    epsilon = 0.9,
    sigma = 1.1
  }
}
``````
It is equivalent to
``````
energy-type = lennard-jones
energy-arg-epsilon = 0.9
energy-arg-sigma = 1.1
``````
This notation allows to simplify property definitions for the user when several 
properties are related. In this example, all these property define how an energy 
should be calculated, therefore they have a logical, semantic reason to be grouped.
To the user, it makes clear that they are all relevant
to the same context (energy calculation). At the same time, each property has a key 
that is clearly-defined an unambiguous, even if a bit verbose. This is especially 
useful when writing programs, as in this case it is important that each property has
a name that makes its purpose clear and independent on the context.


# Interpolation

Properties values can contain references to other properties, that is, the 
name of the referenced property preceded by a `$` sign or in `${...}`. In 
this case, the reference is replaced by the value of the referenced property
(value substitution). For example, if the following properties are defined:

``````
foo = 2
bar = 4$foo
``````

when any part of a program needs the value of `bar` it is interpolated and the 
reference `$foo` is replaced by `foo`'s value, and so its value is `42`.
The `${...}` variant can be used to clearly delimit the name of the referenced
variable, like in `baz = ${foo}nd`, which would be interpolated into `2nd`.


Another form of interpolation is arithmetic evaluation. Such expressions are in `$(...)` 
or `((...))`. Arithmetic substitution happens as follows. First, any variable reference
in the expression is substituted with its value. Then, if the expression is unambiguous
(/i.e./ all the variables have a value, and evaluation the expression gives a unique 
number as a result), it is replace by its result. Otherwise, if the expression contains
parameters or variables, it is not replaced. For example, with the following declaration

``````
epsilon = 0.9
sigma = 1.1
energy = (( ($epsilon/r^8 - $sigma/r^6) ))
ratio = (( $epsilon/$sigma ))
``````

is equivalent to `energy = (0.9/r^8 - 1.1/r^6)` and `ratio = 0.8181818182`.


# Property files

Properties can be read from or saved to files that follow the syntax used in this 
documentation. Alternatively, the JSON format can be used. In this case, the conversion to JSON follows the rules:

  - property keys are put between quotes;
  - the `=` sign between a property key and its value is replaced by `:`;
  - the string values are put between quotes;
  - the real, integer and logical values are written directly.

The JSON I/O procedures are in the `[[json_module(module)]]` module, which comes from the 
JSON-Fortran project.


# Using properties in code

In code, a property is represented by a `[[FProperty(type)]]` object. A property tree is manipulated
by its root node, and there is usually no need to walk the structure explicitly. Instead,
property values can be set or queried by calling one of the TBPs of the root node. These 
procedures can be separated into three groups: procedures to set properties; procedures to get 
property values; and procedures to get metadata or information about the properties themselves.
All these procedures work with key paths from the root property.

## Reading properties

A property structure is loaded from a file using the subroutines `[[FProperty(type):readJson(bound)]]` 
or `[[FProperty(type):readFile(bound)]]`. It can also be read from the command line using the 
`[[FProperty(type):readCommandLine(bound)]]` subroutine.

## Setting individual properties

A property can be set by calling the `[[FProperty(type):setValue(bound)]]` subroutine. This is a generic, with specifics
for various variable types (integer, real, logical, `[[FString(type)]]`, etc). Scalars and 
arrays are supported.

## Getting property values

A property value can be obtained by calling the `[[FProperty(type):getValue(bound)]]` subroutine, which is a generic like
`[[FProperty(type):setValue(bound)]]`. It has a function equivalent `[[FProperty(type):value(bound)]]`. The subroutine 
version allows error detection, while the function version is mostly pure (not strictly, but using it in I/O context 
should be fine). This means that error handling in the function version is rudimentary.

Interpolation is done automatically.

## Saving properties to files

Property can be saved files using the subroutines `[[FProperty(type):saveToJson(bound)]]` (JSON files) 
and `[[FProperty(type):readFile(bound)]]` (property files).

## Metadata

These procedures are used to test the validity of a value in different contexts. There are
procedures tot test whether a property is defined, whether its value is of one particular type
(integer, real, etc.), and whether it is a scalar, a vector, a list, a matrix, etc. For example,
`ìsInteger` checks whether the property can be safely used to get an integer value with the
corresponding specific `getValue` or `value`. This especially important when using the `value`
function, as there is no other form of error checking.
