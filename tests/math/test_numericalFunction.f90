module test_numericalFunction
    use core
    use mod_test
    use ext_character
    use config,     only: TEST_PATH

    use mod_timer,             only: FTimer
    use mod_function,          only: FFunction
    use mod_exception,         only: FException
    use mod_expression,        only: FExpression
    use mod_numericalFunction, only: FNumericalFunction, &
        INTERPOLATION_LINEAR, INTERPOLATION_QUADRATIC, INTERPOLATION_SPLINE, &
            numericalFunction_initWithExpression

    implicit none
    private

    public :: testAll
contains
    subroutine testAll()
        call testConstructor
        call testAccuracy
        call testDerivative
        call testTransformations
        call testFileIO
        call testComparison
        call testOperations
!        call testFileOutput
!        call testDerivative
        call testFFTEven
!        call testFFTOdd
!!        call testPrecision
!        call testPerformance
!!        call testFFTPerformance
        call testsFinished
    end subroutine

    subroutine testConstructor
        type(FNumericalFunction) :: f, g

        call startTestGroup("constructor")
        f = FNumericalFunction("sin(x)/x", "x", [-PI,PI], 400)
        g = FNumericalFunction("sin(x)/x", "x", [-PI,PI], 400)
        call check(f%isSimilar(g), "metadata", "")
        call check(f%lowerBound()==-PI, "lower bound", "")
        call check(f%upperBound()==PI, "upper bound", "")
        call check(f%precision()==1.574733159694132e-2_e, "precision", &
            "wrong precision: " // f%precision() // " instead of " // 1.574733159694132e-2_e // "." )
    end subroutine
    
    subroutine testOperations
        type(FNumericalFunction) :: f, g, a, b
        real(e) :: lambda
        
        call startTestGroup("functions operations")
        
        f = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[0.0_e,10.0_e], &
            points=1000, &
            mode="linear")

        g = numericalFunction_initWithExpression("3*r/12", &
            variable="r", &
            range=[0.0_e,10.0_e], &
            points=1000, &
            mode="linear")

        a = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2 + 3*r/12", &
            variable="r", &
            range=[0.0_e,10.0_e], &
            points=1000, &
            mode="linear")
        
        b = f + g
        call check(a==b, "function addition", "")
        
        a = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2 - 3*r/12", &
            variable="r", &
            range=[0.0_e,10.0_e], &
            points=1000, &
            mode="linear")
        
        b = f - g
        call check(a==b, "function substraction", "")
        
        a = numericalFunction_initWithExpression("((2/r^8) * (1+erf(20*(r-1.5)))/2) * 4.265", &
            variable="r", &
            range=[0.0_e,10.0_e], &
            points=1000, &
            mode="linear")
        
        b = f * 4.265_e
        call check(a==b, "function multiplication", "")
        
        b =  4.265_e * f
        call check(a==b, "function multiplication (reversed)", "")
        
        f = numericalFunction_initWithExpression("(3*x**2 - 1) / 2", &
            variable="x", &
            range=[-1.0_e,1.0_e], &
            points=100000, &
            mode="linear")
        g = numericalFunction_initWithExpression("(35*x**4 - 30*x**2 + 3) / 8", &
            variable="x", &
            range=[-1.0_e,1.0_e], &
            points=100000, &
            mode="linear")
        call f%getScalarProduct(g, lambda)
        write(*,*) "Inner product: ", lambda
        call f%getScalarProduct(f, lambda)
        write(*,*) "Inner product 2: ", lambda
        write(*,*) "Norm:", f%norm()
        
        
        f = numericalFunction_initWithExpression("exp(-x^2)", &
            variable="x", &
            range=[-10.0_e,10.0_e], &
            points=100, &
            mode="linear")
        write(*,*) "Gaussian:", f%norm()**2, sqrt(PI)
        write(*,*) f%integral()
        write(*,*) sqrt(PI)*erf(10.0_e)
        write(*,*) f%integral() - sqrt(PI)*erf(10.0_e)
        write(*,*) f%integral() == sqrt(PI)*erf(10.0_e), &
            f%integral() .approx. sqrt(PI)*erf(10.0_e)
        call f%saveToFile("test.dat")
    end subroutine

    subroutine testAccuracy
        type(FNumericalFunction) :: fl, fq, fs
        integer :: i
        real(e) :: dl, dq, ds
        real(e) :: x, y, yl, yq, ys

        call startTestGroup("interpolation accuracy")

        fl = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[0.0_e,10.0_e], &
            points=1000, &
            mode="linear")

        fq = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[0.0_e,10.0_e], &
            points=1000, &
            mode="quadratic")

        fs = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[0.0_e,10.0_e], &
            points=1000, &
            mode="spline")

        dl = 0
        dq = 0
        ds = 0

        do i=1, 1000
            call random_number(x)
            x = x * 10
            y = 2.0_e/x**8 * (1.0_e+erf(20.0_e*(x-1.5_e)))/2.0_e

            yl = fl%approxf(x)
            dl = dl + abs(y-yl)

            yq = fq%approxf(x)
            dq = dq + abs(y-yq)

            ys = fs%approxf(x)
            ds = ds + abs(y-ys)

        end do
!        write(*,*) "Error:"
!        write(*,*) " linear: ", dl, dl/1000
!        write(*,*) " quadratic: ", dq, dq/1000
!        write(*,*) " spline: ", ds, ds/1000
        call check(5.0e-3_e>dl, "linear interpolation accuracy", "accuracy issue")
        call check(dl>dq, "linear/quadratic relative accuracy", "accuracy issue")
        call check(dq>ds, "quadratic/spline relative accuracy", "accuracy issue")
    end subroutine

    subroutine testDerivative
        type(FNumericalFunction) :: f, df, ref
        real(e) :: mdiff, sdiff

        call startTestGroup("derivatives")

        f = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[1.0e-5_e,10.0_e], &
            points=1000, &
            mode="linear")
        call f%getDerivative(df)

        ref = numericalFunction_initWithExpression( &
            "2*(1/4*2*20*2/sqrt(π)*exp(-(20*(r-1.5))**2))/r**8+-((2*8*r**7)/r**16*(1+erf(20*(r-1.5)))/2)", &
            variable="r", &
            range=[1.0e-5_e,10.0_e], &
            points=1000, &
            mode="linear")

        call check(df==ref, "formal derivative expression", &
            "wrong function: " // df%string() // ".")
        call df%getMaximumDifference(ref, mdiff)
        call check(abs(mdiff)<1.0e-15_e, "formal derivative maximum error", &
            "non-zero error: " // mdiff // ".")
        call df%getSquaredDifference(ref, sdiff)
        call check(sdiff.approx.0.0_e, "formal derivative squared error", &
            "non-zero error: " // sdiff // ".")
!
!        write(*,*) "Differences:", mdiff, sdiff
    end subroutine

    subroutine testTransformations
        type(FNumericalFunction) :: f
        real(e) :: n

        call startTestGroup("calculations")

        f = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[1.0e-5_e,10.0_e], &
            points=1000, &
            mode="linear")

        n = f%norm()
        call check(n.approx.2.284550738357998e-2_e, "function norm", &
            "wrong norm: " // n // " instead of " // 2.284550738357998e-2_e // ".")

        call f%normalise
        n = f%norm()
        call check(approx(n, 1.0_e, 1.0e-14_e, 1.0_e), "normalised function norm", &
            "wrong norm: " // n // " instead of 1.")

        call f%scale(PI)
        n = f%norm()
        call check(approx(n, PI, 1.0e-14_e, 1.0_e), "scaled function norm", &
            "wrong norm: " // n // " instead of " // PI // ".")
    end subroutine

    subroutine testFileIO
        type(FNumericalFunction) :: f, g

        call startTestGroup("file output")
        f = FNumericalFunction("sin(x)", "x", [-PI,PI], 400)
        call f%saveToFile("test_numericalFunction_sin.dat")

        g = FNumericalFunction("test_numericalFunction_sin.dat")
        call check(g==f, "compare sin(x) with its file output", "")
        call check(g%isSimilar(f), "compare sin(x) with its file output", "")
        call check(g%lowerBound()==f%lowerBound(), "compare functions lower bounds", "")
        call check(g%upperBound()==f%upperBound(), "compare functions upper bounds", "")
        
!        f = FNumericalFunction("(2/r^8) * (1+erf(20*(r-1.5)))/2", "r", [-0.0_e,10.0_e], 1000)
!        call f%saveToFile("test_numericalFunction_erf.dat")
!        g = FNumericalFunction(TEST_PATH//"/test_numericalFunction_erf.dat")
!        call xfail(g == f, "compare (2/r^8) * (1+erf(20*(r-1.5)))/2 with its file output", stat%string())
!        call stat%discard
    end subroutine


    subroutine testComparison
        type(FNumericalFunction) :: f, g
        
        call startTestGroup("function comparison")
        f = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[1.0e-5_e,10.0_e], &
            points=1000, &
            mode="linear")
        call check(f/=g, "comparison with uninitialised function", "")
        
        g = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[1.0e-5_e,15.0_e], &
            points=1000, &
            mode="linear")
        call check(f/=g, "comparison with different bounds", "")
        
        g = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))/2", &
            variable="r", &
            range=[1.0e-5_e,10.0_e], &
            points=800, &
            mode="linear")
        call check(f/=g, "comparison with different precision", "")
        
        f = numericalFunction_initWithExpression("(2/r^8) * (1+erf(20*(r-1.5)))", &
            variable="r", &
            range=[1.0e-5_e,10.0_e], &
            points=1000, &
            mode="linear")
        call check(f/=g, "comparison with different function", "")
        
    end subroutine

    subroutine testFFTEven
        integer :: nPoints
        real(e) :: xMin, xMax, dx, dk, fN1, fN2, x
        logical :: dumpResults
        type(FNumericalFunction) :: f, g, tf, if, dif

        dumpResults = .false. ! Set to .true. to output data files

        call startTestGroup("Discrete Fourier Transforms (even size)")

        ! Parameters
        nPoints = 40000
        xMax = 10000.0_e
        xMin = 0.0_e
        dx = (xMax-xMin)/(nPoints-1)
        dk = TWOPI / (dx * (nPoints))
        fN1 = -(nPoints-2) * dk / 2
        fN2 = fN1 + (nPoints-1) * dk

        ! Perfect transforms for comparison purposes
        f = FNumericalFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x", [xmin,xmax], nPoints)
        g = FNumericalFunction("exp(-x^2/4)/(2*sqrt(pi))", "x", [fN1,fN2], nPoints)

        ! Get the Fourier transform (Complex version)
        call f%getFFT(tf)
        call check(g%isSimilar(tf), "bounds after transformation")
        call g%getMaximumDifference(tf, x)
        call check(x<4*epsilon(1.0_e), "transformation accuracy (largest difference)", "error: "//x)
        call g%getSquaredDifference(tf, x)
        call check(x<4*epsilon(1.0_e), "transformation accuracy (squared distance)", "error: "//x)

        ! Get the inverse Fourier transform
        call tf%getIFFT(if)
        call check(if%isSimilar(f), "bounds after inverse transformation")
        call if%getMaximumDifference(f, x)
        call check(x<4*epsilon(1.0_e), "transformation accuracy (largest difference)", "error: "//x)
        call if%getSquaredDifference(f, x)
        call check(x<4*epsilon(1.0_e), "transformation accuracy (squared distance)", "error: "//x)

        ! Get the Fourier transform (Real version)
        call f%getRFFT(tf)
        call check(g%isSimilar(tf), "bounds after real transformation")
        call g%getMaximumDifference(tf, x)
        call check(x<4*epsilon(1.0_e), "real transformation accuracy (largest difference)", "error: "//x)
        call g%getSquaredDifference(tf, x)
        call check(x<4*epsilon(1.0_e), "real transformation accuracy (squared distance)", "error: "//x)

        ! Save representations of the functions if needed
        if (dumpResults) then
            call f%saveToFile("init_even.dat")
            call g%saveToFile("control_even.dat")
            call tf%saveToFile("fft_even.dat")
            dif = tf - g
            call dif%saveToFile("fft_prec_even.dat")
            call if%saveToFile("ifft_even.dat")
            dif = if - f
            call dif%saveToFile("ifft_prec_even.dat")
        end if
    end subroutine


!    subroutine testFFTOdd
!        integer :: nPoints
!        real(e) :: xMin, xMax, dx, dk, fN1, fN2, x
!        logical :: dumpResults
!        type(FNumericalFunction) :: f, g, tf, if, dif
!
!        dumpResults = .false. ! Set to .true. to output data files
!
!        call startTestGroup("Discrete Fourier Transforms (odd size)")
!
!        ! Parameters
!        nPoints = 40001
!        xMax = 10000.0_e
!        xMin = 0.0_e
!        dx = (xMax-xMin)/(nPoints-1)
!        dk = TWOPI / (dx * (nPoints))
!        fN1 = -(nPoints-2) * dk / 2
!        fN2 = fN1 + (nPoints-1) * dk
!
!        ! Perfect transforms for comparison purposes
!        f = FNumericalFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x", [xmin,xmax], nPoints)
!        g = FNumericalFunction("exp(-x^2/4)/(2*sqrt(pi))", "x", [fN1,fN2], nPoints)
!
!        ! Get the Fourier transform
!        call f%getFFT(tf)
!        call check(g%isSimilar(tf), "bounds after transformation")
!        call g%getMaximumDifference(tf, x)
!        call check(x<4*epsilon(1.0_e), "transformation accuracy (largest difference)", "error: "//x)
!        call g%getSquaredDifference(tf, x)
!        call check(x<4*epsilon(1.0_e), "transformation accuracy (squared distance)", "error: "//x)
!
!        ! Get the inverse Fourier transform
!        call tf%getIFFT(if)
!        call check(if%isSimilar(f), "bounds after inverse transformation")
!        call if%getMaximumDifference(f, x)
!        call check(x<4*epsilon(1.0_e), "transformation accuracy (largest difference)", "error: "//x)
!        call if%getSquaredDifference(f, x)
!        call check(x<4*epsilon(1.0_e), "transformation accuracy (squared distance)", "error: "//x)
!
!        ! Save representations of the functions if needed
!        if (dumpResults) then
!            call f%saveToFile("init_odd.dat")
!            call g%saveToFile("control_odd.dat")
!            call tf%saveToFile("fft_odd.dat")
!            dif = tf - g
!            call dif%saveToFile("fft_prec_odd.dat")
!            call if%saveToFile("ifft_odd.dat")
!            dif = if - f
!            call dif%saveToFile("ifft_prec_odd.dat")
!        end if
!
!        ! Get the Fourier transform (Real version)
!        call f%getRFFT(tf)
!        call check(g%isSimilar(tf), "bounds after real transformation")
!        call g%getMaximumDifference(tf, x)
!        call check(x<4*epsilon(1.0_e), "real transformation accuracy (largest difference)", "error: "//x)
!        call g%getSquaredDifference(tf, x)
!        call check(x<4*epsilon(1.0_e), "real transformation accuracy (squared distance)", "error: "//x)
!
!        if (dumpResults) then
!            call tf%saveToFile("rfft_odd.dat")
!            dif = tf - g
!            call dif%saveToFile("rfft_prec_odd.dat")
!        end if
!
!    end subroutine
!
!    subroutine testFFTPerformance
!        type(FNumericalFunction) :: f, g, tf, dif
!        integer :: nPoints
!        real(e) :: xMin, xMax, dk, dx, fN1, fN2, x
!        type(FTimer) :: timer
!        integer :: i, j, attempts
!
!        ! Parameters
!        xMax = 10000.0_e
!        xMin = 0.0_e
!        attempts = 5
!
!        do i=4, 20
!            nPoints = 2**i
!            dx = (xMax-xMin)/(nPoints-1)
!            dk = TWOPI / (dx * (nPoints))
!            fN1 = -(nPoints-2) * dk / 2
!            fN2 = fN1 + (nPoints-1) * dk
!            f = FNumericalFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x", [xmin,xmax], nPoints)
!            g = FNumericalFunction("exp(-x^2/4)/(2*sqrt(pi))", "x", [fN1,fN2], nPoints)
!
!            call timer%start()
!            do j=1, attempts
!                call f%getFFT(tf)
!            end do
!            call timer%stop()
!            call tf%getMaximumDifference(g, x)
!
!            write(*,*) nPoints, timer%wallTime()/attempts, x
!        end do
!    end subroutine
!
!    subroutine testDerivative
!        type(FNumericalFunction) :: f, df, ref
!        logical :: identical
!        type(FException) :: stat
!        integer :: i
!
!        call startTestGroup("derivatives")
!        f = FNumericalFunction("(2/r^8) * (1+erf(20*(r-1.5)))/2", "r", [-0.0_e,10.0_e], 1000)
!        call f%getDerivative(df)
!        ref = FNumericalFunction( &
!            "2*(1/4*2*20*2/sqrt(π)*exp(-(20*(r-1.5))**2))/r**8+-((2*8*r**7)/r**16*(1+erf(20*(r-1.5)))/2)", &
!            "r", [-0.0_e,10.0_e], 1000)
!        call check(df == ref, "calculating derivative of (2/r^8) * (1+erf(20*(r-1.5)))/2", "wrong function: "//df%string())
!    end subroutine
!
!    subroutine testPrecision
!        type(FNumericalFunction) :: fl, fs, fq
!        type(FFunction) :: f
!        real(e) :: x, yl, ys, yq, y
!        integer :: nPoints, i
!
!        call startTestGroup("precision")
!!        f = FNumericalFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x", [0.0_e,20.0_e], 400)
!!        fs = FNumericalFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x", [0.0_e,20.0_e], 400, "spline")
!!        fp = FFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x")
!
!        x = 1.55_e
!        nPoints = 1000
!        do i=1, nPoints
!            fl = FNumericalFunction("(2/r^8) * (1+erf(20*(r-1.5)))/2", "r", [0.0_e,20.0_e], 20*i)
!            fs = FNumericalFunction("(2/r^8) * (1+erf(20*(r-1.5)))/2", "r", [0.0_e,20.0_e], 20*i, "spline")
!            fq = FNumericalFunction("(2/r^8) * (1+erf(20*(r-1.5)))/2", "r", [0.0_e,20.0_e], 20*i, "quadratic")
!            f = FFunction("(2/r^8) * (1+erf(20*(r-1.5)))/2", "r")
!
!            y  = f%evalf(x)
!            yl = fl%approxf(x)
!            yq = fq%approxf(x)
!            ys = fs%approxf(x)
!
!!            write(*,*) x, yl
!!            write(*,*) x, yq
!!            write(*,*) x, ys
!            write(*,*) i, abs(yl-y), abs(yq-y), abs(ys-y)
!        end do
!!        call check(abs(nint(log10(abs(y-yp))))  > precision(1.0_wp)/5, "linear interpolation precision")
!!        call check(abs(nint(log10(abs(ys-yp)))) > precision(1.0_wp)/3, "spline interpolation precision")
!    end subroutine
!
!    subroutine testPerformance
!        type(FNumericalFunction) :: f, fs, fq
!        type(FFunction) :: fp
!        integer :: i, n
!        real(e) :: error, x, y, yp, ys, yq
!        type(FTimer) :: timer
!        real(e), dimension(:), allocatable :: input
!!------
!        call startTestGroup("performance")
!        f = FNumericalFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x", [0.0_e,20.0_e], 400)
!        fs = FNumericalFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x", [0.0_e,20.0_e], 400, "spline")
!        fq = FNumericalFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x", [0.0_e,20.0_e], 400, "quadratic")
!        fp = FFunction("exp(-((x-10)^2))/(sqrt(2*pi))", "x")
!        x = 11.058060765115698_e
!
!        allocate(input(1000000))
!        do i=1, size(input)
!            call random_number(input(i))
!            input(i) = input(i) * 20
!        end do
!
!        call timer%reset
!        call timer%start
!        do i=1, 1000000
!            yp = fp%evalf(input(i))
!        end do
!        call timer%stop
!        write(*,*) "Direct interpretation elapsed time:", timer%walltime()
!
!        call timer%reset
!        call timer%start
!        do i=1, 1000000
!            y = f%approxf(input(i))
!        end do
!        call timer%stop
!        write(*,*) "Linear interpolation elapsed time:", timer%walltime()
!
!        call timer%reset
!        call timer%start
!        do i=1, 1000000
!            yq = fq%approxf(input(i))
!        end do
!        call timer%stop
!        write(*,*) "Quadratic interpolation elapsed time:", timer%walltime()
!
!        call timer%reset
!        call timer%start
!        do i=1, 1000000
!            ys = fs%approxf(input(i))
!        end do
!        call timer%stop
!        write(*,*) "Cubic interpolation elapsed time:", timer%walltime()
!    end subroutine
end module

program test
    use mod_test
    use test_numericalFunction

    call testAll

    call testsFinished
end program