program testPerformancesVector
    use core
    use ext_character
    
    use mod_expression, only: FExpression
    use mod_timer, only: FTimer
    
    implicit none
    
    real(e) :: x, y, z, answer
    integer :: i, Niter
    type(FExpression) :: f
    type(FTimer) :: timer
    real(e), dimension(:), allocatable :: res
    
    Niter = 5000000
    allocate(res(Niter))
    res = 0
    
    x = 0.175_e
    y = 0.110_e
    z = 0.900_e
    
    answer = (x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+ &
        x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z))
    
    f = FExpression("x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+x*cos(x)+y*sin(y)+z*tan(z)*2/(x+y+z+x*y+x*z+y*z+x/y+x/z+y/z+" // &
        "x*cos(x)+y*sin(y)+z*tan(z))*3+sqrt(x*y*z+x+y+z)*log10(sqrt(x*2+y*2+z*2)+x+y+z)")
    
    
    call f%setVariable("x", x)
    call f%setVariable("y", y)
    call f%setVariable("z", z)
!        call f%printBytecode
    call timer%start
    call f%eval("z", 0.900_e, Niter, 0.0_e, res)
    call timer%stop
    write(*,*) "Array time:", timer%walltime()
end program