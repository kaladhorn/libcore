!******************************************************************************!
!                            mod_console module
!______________________________________________________________________________!
!> `[[FConsole(type)]]` derived type and TBPs, to handle output to a terminal.
!
!  Part of the Core library version 2.0.
!  Written by Paul Fossati, <paul.fossati@gmail.com>
!  Copyright (c) 2009-2017 Paul Fossati
!
!------------------------------------------------------------------------------!
! Redistribution and use in source and binary forms, with or without           !
! modification, are permitted provided that the following conditions are met:  !
!                                                                              !
!     * Redistributions of source code must retain the above copyright notice, !
!       this list of conditions and the following disclaimer.                  !
!                                                                              !
!     * Redistributions in binary form must reproduce the above copyright      !
!       notice, this list of conditions and the following disclaimer in the    !
!       documentation and/or other materials provided with the distribution.   !
!                                                                              !
!     * The name of the author may not be used to endorse or promote products  !
!      derived from this software without specific prior written permission    !
!      from the author.                                                        !
!                                                                              !
! THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"  !
! AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE    !
! IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE   !
! ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE     !
! LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR          !
! CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         !
! SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     !
! INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      !
! CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      !
! ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   !
! POSSIBILITY OF SUCH DAMAGE.                                                  !
!******************************************************************************!
module mod_console
    use iso_fortran_env

    use core
    use ext_character
    use mod_exception

    use mod_regex,           only: FRegex
    use mod_string,          only: FString
    use mod_formattedString, only: FFormattedString

    implicit none
    private
    save


    type, public :: FConsole
        private
        integer :: minLevel = MESSAGE_LEVEL_VERBOSE         !< Level above which the messages are printed
        integer :: errorUnit = OUTPUT_UNIT                  !< Unit number of the error stream
        integer :: outputUnit = OUTPUT_UNIT                 !< Unit number of the output stream
        integer :: messageWidth = 30                        !< Width of the message column of the console output
        logical :: colorOutput = .true.                     !< Use color when printing messages

        integer :: lineLength = 100
    contains
        procedure :: printOption                            !<
        procedure :: traceback                              !<
        procedure :: printError                             !< Print an error to the error stream
        procedure :: printWarning                           !< Print a warning to the error stream
        procedure :: debug                                  !< Print a debug message to the error stream
        procedure :: log                                    !< Print an logging message to the output stream

        procedure, private :: print_empty
        procedure, private :: print_char
        procedure, private :: print_string
        procedure, private :: print_real
        procedure, private :: print_real_vector
        procedure, private :: print_real_matrix
        procedure, private :: print_integer
        procedure, private :: print_integer_vector
        procedure, private :: print_logical
        generic, public :: print => print_char, print_string, print_empty, &
            print_real, print_real_vector, print_real_matrix, &
            print_integer, print_integer_vector, print_logical

        procedure, public :: printheader
        procedure, public :: setVerbosity
    end type

    interface
        function getTerminalWidth() bind(C, name="getTerminalWidth")
            use iso_c_binding
            integer(c_int) :: getTerminalWidth
        end function
    end interface
contains
!******************************************************************************!
!> Print an traceback message to the error stream of a console object
!******************************************************************************!
    subroutine traceback(this, string)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: string
!------
        character(:), allocatable :: message
!------

        message = message//" "//string ! Append the information string if present

        call printInternal(this, &
            message=message, &
            priority=MESSAGE_LEVEL_TRACEBACK &
        )
    end subroutine
!******************************************************************************!
!> Print the description of a command-line option
!******************************************************************************!
    subroutine printOption(this, option, help)
        class(FConsole), intent(in) :: this
        character(*),    intent(in) :: option
        character(*),    intent(in) :: help
!------
        integer :: first_column_width, second_column, i, second_column_end, second_column_start
        character(:), allocatable :: bolded, space, helpBuffer, padding, first_column
!------
        first_column_width = 27
        bolded = bold(option)
        if (len(bolded)+3 > first_column_width) then
            allocate(character(len(bolded)+3) :: first_column)
        else
            allocate(character(first_column_width) :: first_column)
        end if
        first_column(:) = "  " // bolded

        allocate(character(first_column_width) :: padding)
        padding(:) = " "

        second_column = getTerminalWidth() - first_column_width
        helpBuffer = help
        second_column_end = getTerminalWidth() - apparentLength(first_column(:))
        if (len(help) > second_column_end) then
            do i=second_column_end, 1, -1
                if (help(i:i) == " ") then
                    helpBuffer = help(:i-1) // &
                        NEW_LINE(help) // padding // help(i+1:)
                    exit
                end if
            end do
        end if

        write(this%outputUnit,'(5a)') first_column, " ", helpBuffer
    end subroutine

    subroutine printWarning(this, message)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message

        call printInternal(this, priority=MESSAGE_LEVEL_WARNING, message=message)
    end subroutine

    subroutine printError(this, message)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message

        call printInternal(this, priority=MESSAGE_LEVEL_ERROR, message=message)
        error stop
    end subroutine

    subroutine log(this, message)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message

        call printInternal(this, priority=MESSAGE_LEVEL_LOG, message=message)
    end subroutine

    subroutine debug(this, message)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message

        call printInternal(this, priority=MESSAGE_LEVEL_DEBUG, message=message)
    end subroutine
!******************************************************************************!
!> Print a message accompanied by an standard precision real number to
!>  the output stream of a console object.
!******************************************************************************!
    subroutine print_real(this, message, x)
        class(FConsole), intent(in) :: this
        character(*),    intent(in) :: message
        real(wp),         intent(in) :: x
!------
        call printInternal(this, message, "" // x)
    end subroutine
!******************************************************************************!
!> Print a message accompanied by an standard precision real vector to the
!>  output stream of a console object.
!******************************************************************************!
    subroutine print_real_vector(this, message, x)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message
        real(wp), dimension(:), intent(in) :: x
!------
        character(200) :: string
        character(27) :: format
        integer :: stat
!------
        format = ""
        write(format,'(a,i1,a)') "('[',f0.6,", size(x)-1,"(', ',f0.6),']')"
        write(string,format,iostat=stat) x
        if (stat == 0) then
            call this%print(message, trim(string))
        else
            call this%print(message, "<format error>")
        end if
    end subroutine
!******************************************************************************!
!> Print a message accompanied by an standard precision real vector to the
!>  output stream of a console object.
!******************************************************************************!
    subroutine print_real_matrix(this, message, x)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message
        real(wp), dimension(:,:), intent(in) :: x
!------
        character(200) :: string
        character(27) :: format
        integer :: linePosition, i, pos, iostat
        character(:), allocatable :: message_
!------
        format = ""
        pos = max(ceiling(log10(maxval(abs(x)))), 1) + 6
        if (minval(x) < 0) pos = pos + 1 ! Add a position if there are negative numbers
        if (pos < 0) pos = 6
        linePosition = (size(x,1)+1) / 2
        do i=1, size(x,1)
            if (i==linePosition) then
                message_ = message
            else
                message_ = ""
            end if
            if (i==1) then
                write(format,'(a,i1,a,i0,a)') "('⎛',", size(x,2),"(f",pos,".4,1x),'⎞')"
            else if (i==size(x,1)) then
                write(format,'(a,i1,a,i0,a)') "('⎝',", size(x,2),"(f",pos,".4,1x),'⎠')"
            else
                write(format,'(a,i1,a,i0,a)') "('⎜',", size(x,2),"(f",pos,".4,1x),'⎟')"
            end if
            write(string,format,iostat=iostat) x(i,:)
            if (iostat==0) then
                call this%print(message_, trim(string))
            else
                call this%print(message_, "****")
            end if
        end do
    end subroutine
!******************************************************************************!
!> Print a message accompanied by a logical value to the output stream of
!>  a console object.
!******************************************************************************!
    subroutine print_logical(this, message, x)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message
        logical, intent(in) :: x
!------
        call printInternal(this, message, "" // x)
    end subroutine
!******************************************************************************!
!> Print a message accompanied by an integer number to the output stream
!>  of a console object.
!******************************************************************************!
    subroutine print_integer(this, message, x)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message
        integer, intent(in) :: x
!------
        call printInternal(this, message, "" // x)
    end subroutine
!******************************************************************************!
!> Print a message accompanied by an integer vector to the output stream
!>  of a console object.
!******************************************************************************!
    subroutine print_integer_vector(this, message, x)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message
        integer, dimension(:), intent(in) :: x
!------
        character(200) :: string
        character(23) :: format
!------
        format = ""
        write(format,'(a,i1,a)') "('[',i0,", size(x)-1,"(', ',i0),']')"
        write(string,format) x
        call printInternal(this, message, trim(string))
    end subroutine
!******************************************************************************!
!> Print a message accompanied by an character string to the output
!>  stream of a console object.
!******************************************************************************!
    subroutine print_char(this, message, string)
        class(FConsole), intent(in) :: this
        character(*), intent(in) :: message
        character(*), intent(in), optional :: string
!------
        if (present(string)) then
            call printInternal(this, message, string)
        else
            call printInternal(this, message)
        end if
    end subroutine
!******************************************************************************!
!> Print a message accompanied by an character string to the output
!>  stream of a console object.
!******************************************************************************!
    subroutine print_string(this, message, string)
        class(FConsole), intent(in) :: this
        type(FString), intent(in) :: message
        type(FString), intent(in), optional :: string
!------
        type(FFormattedString), allocatable :: formattedMessage, formattedString
!------
        ! Get formatted internal representations
        formattedMessage = FFormattedString(message)
        if (present(string)) then
            formattedString = FFormattedString(string)
        end if

        ! Call the lower-level subroutine
        if (allocated(formattedString)) then
            call printInternal(this, formattedMessage%string(), formattedString%string())
        else
            call printInternal(this, formattedMessage%string())
        end if
    end subroutine
!******************************************************************************!
!> Print an empty line to the output stream of a console object.
!******************************************************************************!
    subroutine print_empty(this)
        class(FConsole), intent(in) :: this

        call printInternal(this)
    end subroutine
!******************************************************************************!
!> Print an section header to the output stream of a console object.
!******************************************************************************!
    subroutine printHeader(this, name)
        class(FConsole), intent(in) :: this
        character(80) :: title
        character(*), intent(in) :: name
!------
        call this%print
        call this%print(" ┌──────────────────────────────" // &
            "───────────────────────────" // &
            "────────────────┐")
        write(title,'(a)') " │      "//adjustl(name)
        write(title(len(title)-3:),'(a)') " │"
        call this%print(title)
        call this%print(" └──────────────────────────────" // &
            "───────────────────────────" // &
            "────────────────┘")
        call this%print
    end subroutine
!******************************************************************************!
!> Print a message (for internal use only).
!******************************************************************************!
    subroutine printInternal(this, message, column2, priority)
        class(FConsole), intent(in)           :: this
        character(*),    intent(in), optional :: message
        character(*),    intent(in), optional :: column2
        integer,         intent(in), optional :: priority
!------
        integer :: unit, level_, i, l, columnWidth
        character(:), allocatable :: prefix_, column1_, column2_, space, newColumn
!------
        ! Set the logging level
        if (present(priority)) then
            level_ = priority
        else
            level_ = MESSAGE_LEVEL_VERBOSE
        end if

        ! Return if the level of the message is lower than the minimum level
        if (level_ < this%minLevel) return

        ! Set the unit to use to print the message
        if (level_ >= MESSAGE_LEVEL_WARNING) then
            unit = this%errorUnit
        else
            unit = this%outputUnit
        end if

        ! Set the prefix
        if (this%colorOutput) then
            select case(level_)
                case(MESSAGE_LEVEL_DEBUG)
                    prefix_ = "["//char(27)//"[1;33mDebug"//char(27)//"[0m]"
                case(MESSAGE_LEVEL_LOG)
                    prefix_ = "["//char(27)//"[1;32mLog"//char(27)//"[0m]"
                case(MESSAGE_LEVEL_VERBOSE)
                    prefix_ = ""
                case(MESSAGE_LEVEL_QUIET)
                    prefix_ = ""
                case(MESSAGE_LEVEL_WARNING)
                    prefix_ = "["//char(27)//"[1;31mWarning"//char(27)//"[0m]"
                case(MESSAGE_LEVEL_TRACEBACK)
                    prefix_ = "["//char(27)//"[1;31mError"//char(27)//"[0m]"
                case(MESSAGE_LEVEL_ERROR)
                    prefix_ = "["//char(27)//"[1;31mError"//char(27)//"[0m]"
                case default
                    prefix_ = ""
            end select
        else
            select case(level_)
                case(MESSAGE_LEVEL_DEBUG)
                    prefix_ = "[Debug]"
                case(MESSAGE_LEVEL_LOG)
                    prefix_ = "[Log]"
                case(MESSAGE_LEVEL_VERBOSE)
                    prefix_ = ""
                case(MESSAGE_LEVEL_QUIET)
                    prefix_ = ""
                case(MESSAGE_LEVEL_WARNING)
                    prefix_ = "[Warning]"
                case(MESSAGE_LEVEL_TRACEBACK)
                    prefix_ = "[Error]"
                case(MESSAGE_LEVEL_ERROR)
                    prefix_ = "[Error]"
                case default
                    prefix_ = ""
            end select
        end if

        ! Add the first column to the prefix
        if (present(message)) then
            column1_ = prefix_ // message
        else
            column1_ = prefix_
        end if

        ! Print the first column
        write(unit,'(a)',advance="no") column1_

        ! Return if there is nothing more to print
        if (.not. present(column2)) then
            write(unit,'(a)')
            return
        end if
        column2_ = column2

        ! Complete the first column with padding
        if (apparentLength(column1_) < this%messageWidth) then
            i = this%messageWidth - apparentLength(column1_)
            allocate(character(i) :: space)
            write(space,'(a)')
            write(unit,'(a)',advance="no") space
            deallocate(space)
            columnWidth = this%lineLength - this%messageWidth
        else
            write(unit,'(a)',advance="no") " "
            columnWidth = this%lineLength - mod(apparentLength(column1_), this%lineLength) - 1
        end if

        ! Complete the first line
        l = min(columnWidth, len(column2_))

        ! Get the position of the last space in the string
        if (l < len(column2_)) then
            ! The limiting length is that of the column

            i = index(column2_(:l), " ", back=.true.)
            if (i /= 0) l = i
        end if

        write(unit,'(a)') column2_(:l)
        newColumn = column2_(l+1:)
        column2_ = newColumn

        ! Get the second column size for the other lines
        columnWidth = this%lineLength - this%messageWidth

        ! Get the spaces for the first column
        allocate(character(this%messageWidth) :: space)
        write(space,'(a)')

        l = min(columnWidth, len(column2_))
        do while(l > 0)
            ! Get the position of the last space in the string
            if (l < len(column2_)) then
                i = index(column2_(:l), " ", back=.true.)
                if (i /= 0) l = i
            end if

            write(unit,'(a)') space // column2_(:l)
            column2_ = column2_(l+1:)
            l = min(columnWidth, len(column2_))
        end do
    end subroutine
!******************************************************************************!
!> Set the verbosity level of a console object.
!******************************************************************************!
    subroutine setVerbosity(this, level)
        class(FConsole), intent(inout) :: this
        integer, intent(in) :: level

        select case(level)
            case (MESSAGE_LEVEL_DEBUG, &
                MESSAGE_LEVEL_LOG, &
                MESSAGE_LEVEL_VERBOSE, &
                MESSAGE_LEVEL_QUIET, &
                MESSAGE_LEVEL_WARNING, &
                MESSAGE_LEVEL_TRACEBACK, &
                MESSAGE_LEVEL_ERROR)

                ! Set the level in the console object
                this%minLevel = level
            case default

                ! Invalid level code
        end select
    end subroutine
end module
